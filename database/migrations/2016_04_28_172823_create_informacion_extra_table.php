<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInformacionExtraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('informacion_extra', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion'); 
            $table->integer('diario_id')->unsigned();        
            $table->integer('pi_id')->unsigned(); 
            $table->date('fecha_llegada');
            $table->date('fecha_salida');
            $table->boolean('situacion');
            $table->timestamps();
			$table->foreign('diario_id')->references('id')->on('diarios')->onDelete('cascade');
			$table->foreign('pi_id')->references('id')->on('punto_interes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('informacion_extra');
    }

}
