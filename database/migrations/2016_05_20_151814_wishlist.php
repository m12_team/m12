<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Wishlist extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wishlist', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('punto_interes_id')->unsigned(); 
            $table->integer('visitante_id')->unsigned(); 
            $table->timestamps();
            $table->foreign('punto_interes_id')->references('id')->on('punto_interes')->onDelete('cascade');
            $table->foreign('visitante_id')->references('id')->on('users')->onDelete('cascade');
        });   
     }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('wishlist');
    }
}
