<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFotosdiarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fotos_diario', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ruta'); 
            $table->integer('diario_id')->unsigned();        
            $table->integer('creador_id')->unsigned();        
            $table->timestamps();
			$table->foreign('diario_id')->references('id')->on('diarios')->onDelete('cascade');
			$table->foreign('creador_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fotos_diario');
    }

}
