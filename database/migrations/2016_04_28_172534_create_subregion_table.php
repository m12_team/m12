<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubregionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subregion', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre'); 
            $table->integer('region_id')->unsigned()->nullable();        
            $table->timestamps();
			$table->foreign('region_id')->references('id')->on('region')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('subregion');
    }

}
