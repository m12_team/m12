<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('primer_apellido');
            $table->string('segundo_apellido');
            $table->string('descripcion');
            $table->string('email');
            $table->integer('ciudad_natal_id')->unsigned()->nullable();
            $table->integer('ciudad_actual_id')->unsigned()->nullable();
            $table->integer('empresa_id')->unsigned()->nullable();            
            $table->string('password', 60);
            $table->date('fecha_nacimiento');
            $table->string('username');
            $table->string('foto');
			$table->integer('role_id')->unsigned()->nullable();
            $table->string('codigo')->nullable();
			$table->boolean('estaBloqueado');     			
            $table->rememberToken();
            $table->timestamps();
			$table->foreign('empresa_id')->references('id')->on('empresa')->onDelete('set null');
            $table->foreign('ciudad_natal_id')->references('id')->on('ciudad')->onDelete('set null');
            $table->foreign('ciudad_actual_id')->references('id')->on('ciudad')->onDelete('set null');
			$table->foreign('role_id')->references('id')->on('roles')->onDelete('set null');
        });
	}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
