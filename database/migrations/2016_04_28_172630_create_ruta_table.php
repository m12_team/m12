<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRutaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ruta', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
			$table->string('descripcion');
			$table->integer('creador_id')->unsigned()->nullable();       
            $table->boolean('estaBloqueado');     
            $table->timestamps();
			$table->foreign('creador_id')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ruta');

    }

}
