<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PokemonCpUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pokemon_cp_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pokemon_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned()->nullable();
			$table->integer('cp');
            $table->string('contenido');
            $table->timestamps();
			$table->foreign('pokemon_id')->references('id')->on('pokemon')->onDelete('set null');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('pokemon_cp_user');
    }
}
