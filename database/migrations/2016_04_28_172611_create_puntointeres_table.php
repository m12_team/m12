<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePuntointeresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('punto_interes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');   
			$table->string('descripcion');
			$table->float('precio');
			$table->decimal('longitud', 10, 7);
			$table->decimal('latitud', 10, 7);
			$table->integer('categoria_pi_id')->unsigned()->nullable();
			$table->integer('entorno_id')->unsigned()->nullable();
            $table->integer('transporte_id')->unsigned()->nullable();
			$table->integer('creador_id')->unsigned()->nullable();	
            $table->boolean('estaBloqueado');  	
            $table->timestamps();
			$table->foreign('categoria_pi_id')->references('id')->on('categoria_puntointeres')->onDelete('cascade');
            $table->foreign('entorno_id')->references('id')->on('entorno')->onDelete('cascade');
            $table->foreign('creador_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('transporte_id')->references('id')->on('transportes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::drop('punto_interes');
    }

}
