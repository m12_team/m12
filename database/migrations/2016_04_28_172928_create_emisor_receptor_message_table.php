8<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmisorReceptorMessageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       
        Schema::create('emisor_receptor_message', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('emisor_id')->unsigned();
            $table->integer('receptor_id')->unsigned();
            $table->timestamps();
			$table->foreign('emisor_id')->references('id')->on('users')->onDelete('cascade');
			$table->foreign('receptor_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('emisor_receptor_message');
    }

}
