<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentMessageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_message', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('emisor_receptor')->unsigned();
            $table->string('contenido');
            $table->timestamps();
			$table->foreign('emisor_receptor')->references('id')->on('emisor_receptor_message')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('content_message');
    }

}
