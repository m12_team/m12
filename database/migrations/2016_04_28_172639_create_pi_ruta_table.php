<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePiRutaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pi_ruta', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('punto_interes_id')->unsigned()->nullable();         
			$table->integer('ruta_id')->unsigned()->nullable();         
            $table->timestamps();
			$table->foreign('punto_interes_id')->references('id')->on('punto_interes')->onDelete('cascade');
			$table->foreign('ruta_id')->references('id')->on('ruta')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('pi_ruta');
    }

}
