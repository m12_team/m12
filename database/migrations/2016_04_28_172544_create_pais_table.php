<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pais', function (Blueprint $table) {
            $table->increments('id');
			$table->string('sortname'); 
            $table->string('name'); 
            $table->integer('subregion_id')->unsigned()->nullable();        
            $table->timestamps();
			$table->foreign('subregion_id')->references('id')->on('subregion')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pais');
    }

}
