<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosdiarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos_diario', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url'); 
            $table->integer('creador_id')->unsigned();    
            $table->integer('diario_id')->unsigned();    
            $table->timestamps();
			$table->foreign('creador_id')->references('id')->on('users')->onDelete('cascade');
			$table->foreign('diario_id')->references('id')->on('diarios')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('videos_diario');
    }

}
