<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TiposPokemon extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipos_pokemon', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tipos_id')->unsigned(); 
			$table->integer('pokemon_id')->unsigned();     
            $table->timestamps();
			$table->foreign('tipos_id')->references('id')->on('tipos')->onDelete('cascade');
			$table->foreign('pokemon_id')->references('id')->on('pokemon')->onDelete('cascade');
			
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tipos_pokemon');
    }
}
