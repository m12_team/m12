<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComentariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comentarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('contenido');        
            $table->string('titulo'); 
			$table->integer('valoracion');
			$table->integer('punto_interes_id')->unsigned()->nullable();  
            $table->integer('creador_id')->unsigned()->nullable();       
            $table->timestamps();
			$table->foreign('punto_interes_id')->references('id')->on('punto_interes')->onDelete('cascade');
			$table->foreign('creador_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('comentarios');
    }

}
