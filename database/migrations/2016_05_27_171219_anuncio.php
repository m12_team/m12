<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Anuncio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('anuncio', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion'); 
			$table->integer('punto_interes_id')->unsigned(); 
            $table->integer('creador_id')->unsigned(); 
            $table->timestamps();
            $table->foreign('punto_interes_id')->references('id')->on('punto_interes')->onDelete('cascade');
            $table->foreign('creador_id')->references('id')->on('users')->onDelete('cascade');
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('anuncio');
    }
}
