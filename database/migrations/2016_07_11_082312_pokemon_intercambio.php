<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PokemonIntercambio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pokemon_intercambio', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pokemon_que_cambia')->unsigned()->nullable();
            $table->integer('pokemon_id')->unsigned()->nullable();
            $table->integer('emisor_id')->unsigned()->nullable();
            $table->integer('remitente_id')->unsigned()->nullable();  
            $table->integer('state');
            $table->foreign('pokemon_que_cambia')->references('id')->on('pokemon')->onDelete('set null');
            $table->foreign('pokemon_id')->references('id')->on('pokemon')->onDelete('set null');
            $table->foreign('emisor_id')->references('id')->on('users')->onDelete('set null');
            $table->foreign('remitente_id')->references('id')->on('users')->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pokemon_intercambio');
    }
}
