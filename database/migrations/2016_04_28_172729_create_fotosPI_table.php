<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFotosPITable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fotos_punto_interes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ruta');         
			$table->integer('creador_id')->unsigned()->nullable();
			$table->integer('punto_interes_id')->unsigned()->nullable();         
            $table->timestamps();
			$table->foreign('creador_id')->references('id')->on('users')->onDelete('set null');
            $table->foreign('punto_interes_id')->references('id')->on('punto_interes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fotos_punto_interes');
    }

}
