<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRespuestaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('respuesta', function (Blueprint $table) {
            $table->increments('id');
            $table->string('contenido'); 
            $table->integer('valoracion');          
            $table->integer('punto_interes_id')->unsigned(); 
            $table->integer('creador_id')->unsigned(); 
            $table->integer('pregunta_id')->unsigned(); 
            $table->timestamps();
            $table->foreign('punto_interes_id')->references('id')->on('punto_interes')->onDelete('cascade');
            $table->foreign('creador_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('pregunta_id')->references('id')->on('pregunta')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('respuesta');
    }
}
