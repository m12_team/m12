<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ComentariosPokemon extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comentarios_pokemon', function (Blueprint $table) {
            $table->increments('id');
            $table->string('contenido');        
            $table->string('titulo'); 
            $table->integer('valoracion');
            $table->integer('pokemon_id')->unsigned()->nullable();  
            $table->integer('creador_id')->unsigned()->nullable();       
            $table->timestamps();
            $table->foreign('pokemon_id')->references('id')->on('pokemon')->onDelete('cascade');
            $table->foreign('creador_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
