<?php

use Illuminate\Database\Seeder;
use App\User;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$faker = Faker\Factory::create();
        

        User::create([
                'nombre'      => "Kiko",
                'primer_apellido' => "Fernandez",
                'segundo_apellido' =>"Herreros",
                'descripcion' => "Me llamo Kiko y soy de Granollers",
                'email' => "kfh1992@gmail.com",
                'ciudad_natal_id'=>38273,
                'ciudad_actual_id'=>38273,
                'empresa_id'=>1,
                'password'=>Hash::make('123456789'),
                'fecha_nacimiento'=>$string = date("1992-08-05"),
                'username' => "kiferhe",
                'foto' => 'images/profile/default/default.jpeg',
                'role_id' => 1,
                'estaBloqueado'=>0
        ]);

        User::create([
                'nombre'      => "Paco",
                'primer_apellido' => "Gonzalez",
                'segundo_apellido' =>"Tonie",
                'descripcion' => "Me llamo Pacp y no se de donde soy",
                'email' => "paco@gmail.com",
                'ciudad_natal_id'=>38273,
                'ciudad_actual_id'=>38274,
                'empresa_id'=>1,
                'password'=>Hash::make('123456789'),
                'fecha_nacimiento'=>$string = date("1992-07-05"),
                'username' => "pacote",
                'foto' => 'images/profile/default/default.jpeg',
                'role_id' => 1,
                'estaBloqueado'=>0
        ]);
        User::create([
                'nombre'      => "Alberto",
                'primer_apellido' => "Gonzalez",
                'segundo_apellido' =>"Martin",
                'descripcion' => "Me llamo Alberto y soy de Málaga",
                'email' => "profesional1@gmail.com",
                'ciudad_natal_id'=>38599,
                'ciudad_actual_id'=>38273,
                'empresa_id'=>1,
                'password'=>Hash::make('profesional1'),
                'fecha_nacimiento'=>$string = date("1990-08-05"),
                'username' => "albertiho",
                'foto' => 'images/profile/default/default.jpeg',
                'role_id' => 2,
                'estaBloqueado'=>0
        ]);
        User::create([
                'nombre'      => "Denis",
                'primer_apellido' => "Marin",
                'segundo_apellido' =>"Moran",
                'descripcion' => "Me llamo Denis y soy de Sant Celoni",
                'email' => "profesional2@gmail.com",
                'ciudad_natal_id'=>38305,
                'ciudad_actual_id'=>38305,
                'empresa_id'=>1,
                'password'=>Hash::make('profesional2'),
                'fecha_nacimiento'=>$string = date("1990-08-05"),
                'username' => "denisado",
                'foto' => 'images/profile/default/default.jpeg',
                'role_id' => 2,
                'estaBloqueado'=>0
        ]);
         User::create([
                'nombre'      => "Marcos",
                'primer_apellido' => "Aurelio",
                'segundo_apellido' =>"Benites",
                'descripcion' => "Me llamo Alberto y soy de Benicarlo",
                'email' => "admin@gmail.com",
                'ciudad_natal_id'=>38382,
                'ciudad_actual_id'=>38273,
                'empresa_id'=>1,
                'password'=>Hash::make('admin'),
                'fecha_nacimiento'=>$string = date("1991-08-04"),
                'username' => "marcquesio",
                'foto' => 'images/profile/default/default.jpeg',
                'role_id' => 3,
                'estaBloqueado'=>0
        ]);
        User::create([
                'nombre'      => "Danni",
                'primer_apellido' => "Vargas",
                'segundo_apellido' =>"Felix",
                'descripcion' => "Me llamo Danni y soy de Llinars",
                'email' => "vargas@gmail.com",
                'ciudad_natal_id'=>38382,
                'ciudad_actual_id'=>38273,
                'empresa_id'=>1,
                'password'=>Hash::make('admin'),
                'fecha_nacimiento'=>$string = date("1991-08-04"),
                'username' => "vargas",
                'foto' => 'images/profile/default/default.jpeg',
                'role_id' => 3,
                'estaBloqueado'=>0
        ]);
       
        foreach (range(1, 20) as $index)
        {
            $test1 = rand(1,1262055681);
            $int= mt_rand($test1,1262055681);
            User::create([
                'nombre'      => $faker->firstName,
				'primer_apellido' => $faker->lastName,
				'segundo_apellido' => $faker->lastName,
                'descripcion' => $faker->lastName,
                'email' => $faker->email,
                'ciudad_natal_id'=>rand(1,47576),
                'ciudad_actual_id'=>rand(1,47576),
                'empresa_id'=>1,
                'password'=>Hash::make('123456789'),
                'fecha_nacimiento'=>$string = date("Y-m-d H:i:s",$int),
                'username' => $faker->userName,
                'foto' => 'images/profile/default/default.jpeg',
                'role_id' => rand(1,3),
                'estaBloqueado'=>rand(0,1)
            ]);
        }
    }
}

