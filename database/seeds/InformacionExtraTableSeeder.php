<?php

use Illuminate\Database\Seeder;
use App\InformacionExtra;
class InformacionExtraTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
    {

            InformacionExtra::create([
				'descripcion' => "Este museo fue precioso",
				'diario_id' => 4,
                'fecha_llegada' => '2015-06-08',
                'fecha_salida' =>'2015-06-09',
                'situacion' => 0,
                'pi_id' => 1
            ]);
             InformacionExtra::create([
                'descripcion' => "Este museo fue precioso",
                'diario_id' => 4,
                'fecha_llegada' => '2015-06-10',
                'fecha_salida' =>'2015-06-11',
                'situacion'=> 1,
                'pi_id' => 2
            ]);
            InformacionExtra::create([
                'descripcion' => "Edificio interesante",
                'diario_id' => 2,
                'fecha_llegada' => '2015-06-10',
                'fecha_salida' =>'2015-06-11',
                'situacion'=> 1,
                'pi_id' => 7
            ]);
            InformacionExtra::create([
                'descripcion' => "Perfecto edificio",
                'diario_id' => 2,
                'fecha_llegada' => '2015-06-10',
                'fecha_salida' =>'2015-06-11',
                'situacion'=> 1,
                'pi_id' => 8
            ]);
            InformacionExtra::create([
                'descripcion' => "No sabía nada de esto",
                'diario_id' => 2,
                'fecha_llegada' => '2015-06-10',
                'fecha_salida' =>'2015-06-11',
                'situacion'=> 1,
                'pi_id' => 9
            ]);
              InformacionExtra::create([
                'descripcion' => "Perfecto vistas",
                'diario_id' => 1,
                'fecha_llegada' => '2015-06-10',
                'fecha_salida' =>'2015-06-11',
                'situacion'=> 1,
                'pi_id' => 10
            ]);
            InformacionExtra::create([
                'descripcion' => "Muy bien todo",
                'diario_id' => 1,
                'fecha_llegada' => '2015-06-10',
                'fecha_salida' =>'2015-06-11',
                'situacion'=> 1,
                'pi_id' => 11
            ]);
              InformacionExtra::create([
                'descripcion' => "Perfecto vistas",
                'diario_id' => 3,
                'fecha_llegada' => '2015-06-10',
                'fecha_salida' =>'2015-06-11',
                'situacion'=> 1,
                'pi_id' => 3
            ]);
            InformacionExtra::create([
                'descripcion' => "Muy bien todo",
                'diario_id' => 3,
                'fecha_llegada' => '2015-06-10',
                'fecha_salida' =>'2015-06-11',
                'situacion'=> 0,
                'pi_id' => 4
            ]);
              InformacionExtra::create([
                'descripcion' => "Perfecto vistas",
                'diario_id' => 3,
                'fecha_llegada' => '2015-06-10',
                'fecha_salida' =>'2015-06-11',
                'situacion'=> 1,
                'pi_id' => 5
            ]);
            InformacionExtra::create([
                'descripcion' => "Muy bien todo",
                'diario_id' => 3,
                'fecha_llegada' => '2015-06-10',
                'fecha_salida' =>'2015-06-11',
                'situacion'=> 1,
                'pi_id' => 6
            ]);
        
	}
}
