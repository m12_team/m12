<?php

use Illuminate\Database\Seeder;
use App\Regiones;

class RegionesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $regiones =[
			
			[
				'nombre'  => 'Africa'
			],
			[
				'nombre'  => 'América'
			],
			[
				'nombre'  => 'Asia'
			],
						[
				'nombre'  => 'Europa'
			],
			[
				'nombre'  => 'Oceania'
			],

		];
		
		DB::table('region')->delete();
		foreach ($regiones as $region){
			Regiones::create($region);
		}
    }
}
