<?php

use Illuminate\Database\Seeder;
use App\FotosPokemon;

class FotosPokemonTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$faker = Faker\Factory::create();
        
          $fotos =[
            
            [
                'ruta'  => 'images/pokemon/1-bulbasour.png',
                'pokemon_id'=>1
            ],
            [
                'ruta'  => 'images/pokemon/2-ivysaur.png',
                'pokemon_id'=>2
            ],
            [
                'ruta'  => 'images/pokemon/3-venusaur.jpg',
                'pokemon_id'=>3
            ],
            [
                'ruta'  => 'images/pokemon/4-charmander.png',
                'pokemon_id'=>4
            ],
            [
                'ruta'  => 'images/pokemon/5-charmeleon.png',
                'pokemon_id'=>5
            ],
            [
               'ruta'  => 'images/pokemon/6-charizard.png',
                'pokemon_id'=>6
            ],
            [
               'ruta'  => 'images/pokemon/7-squirtle.jpg',
                'pokemon_id'=>7
            ],
            [
               'ruta'  => 'images/pokemon/8-wartortle.png',
                'pokemon_id'=>8
            ],
            [
               'ruta'  => 'images/pokemon/9-blastoise.png',
                'pokemon_id'=>9
            ],
            [
               'ruta'  => 'images/pokemon/10-caterpie.png',
                'pokemon_id'=>10
            ],
            [
               'ruta'  => 'images/pokemon/11-metapod.png',
                'pokemon_id'=>11
            ],
            [
               'ruta'  => 'images/pokemon/12-butterfree.png',
                'pokemon_id'=>12
            ],
            [
               'ruta'  => 'images/pokemon/13-Weedle.png',
                'pokemon_id'=>13
            ],
            [
               'ruta'  => 'images/pokemon/14-Kakuna.png',
                'pokemon_id'=>14
            ],
            [
               'ruta'  => 'images/pokemon/15-Beedrill.png',
                'pokemon_id'=>15
            ],
            [
               'ruta'  => 'images/pokemon/16-Pidgey.png',
                'pokemon_id'=>16
            ],
            [
               'ruta'  => 'images/pokemon/17-Pidgeotto.png',
                'pokemon_id'=>17
            ],
            [
               'ruta'  => 'images/pokemon/18-Pidgeot.png',
                'pokemon_id'=>18
            ],
            [
               'ruta'  => 'images/pokemon/19-Rattata.png',
                'pokemon_id'=>19
            ],
            [
               'ruta'  => 'images/pokemon/20-Raticate.png',
                'pokemon_id'=>20
            ],
            [
               'ruta'  => 'images/pokemon/21-Spearow.png',
                'pokemon_id'=>21
            ],
            [
               'ruta'  => 'images/pokemon/22-Fearow.png',
                'pokemon_id'=>22
            ],
            [
               'ruta'  => 'images/pokemon/23-Ekans.png',
                'pokemon_id'=>23
            ],
            [
               'ruta'  => 'images/pokemon/24-Arbok.png',
                'pokemon_id'=>24
            ],
            [
               'ruta'  => 'images/pokemon/25-Pikachu.png',
                'pokemon_id'=>25
            ],
            [
               'ruta'  => 'images/pokemon/26-Raichu.png',
                'pokemon_id'=>26
            ],
            [
               'ruta'  => 'images/pokemon/27-Sandshrew.png',
                'pokemon_id'=>27
            ],
            [
               'ruta'  => 'images/pokemon/28-Sandlash.png',
                'pokemon_id'=>28
            ],
            [
               'ruta'  => 'images/pokemon/29-Nidoran1.png',
                'pokemon_id'=>29
            ],
            [
               'ruta'  => 'images/pokemon/30-Nidorina.png',
                'pokemon_id'=>30
            ],
            [
               'ruta'  => 'images/pokemon/31-Nidoqueen.png',
                'pokemon_id'=>31
            ],
            [
               'ruta'  => 'images/pokemon/32-NidoranM.png',
                'pokemon_id'=>32
            ],
            [
               'ruta'  => 'images/pokemon/33-Nidorino.png',
                'pokemon_id'=>33
            ],
            [
               'ruta'  => 'images/pokemon/34-Nidoking.png',
                'pokemon_id'=>34
            ],
            [
               'ruta'  => 'images/pokemon/35-Clefairy.png',
                'pokemon_id'=>35
            ],
            [
               'ruta'  => 'images/pokemon/36-Clefable.png',
                'pokemon_id'=>36
            ],
            [
               'ruta'  => 'images/pokemon/37-Vulpix.png',
                'pokemon_id'=>37
            ],
            [
               'ruta'  => 'images/pokemon/38-Ninetales.png',
                'pokemon_id'=>38
            ],
            [
               'ruta'  => 'images/pokemon/39-Jigglypuff.png',
                'pokemon_id'=>39
            ],
            [
               'ruta'  => 'images/pokemon/40-Wigglytuff.png',
                'pokemon_id'=>40
            ],
            [
               'ruta'  => 'images/pokemon/41-Zubat.png',
                'pokemon_id'=>41
            ],
            [
               'ruta'  => 'images/pokemon/42-Golbat.png',
                'pokemon_id'=>42
            ],
            [
               'ruta'  => 'images/pokemon/43-Oddish.png',
                'pokemon_id'=>43
            ],
            [
               'ruta'  => 'images/pokemon/44-Gloom.png',
                'pokemon_id'=>44
            ],
            [
               'ruta'  => 'images/pokemon/45-Vileplume.png',
                'pokemon_id'=>45
            ],
            [
               'ruta'  => 'images/pokemon/46-Paras.png',
                'pokemon_id'=>46
            ],
            [
               'ruta'  => 'images/pokemon/47-Parasect.png',
                'pokemon_id'=>47
            ],
            [
               'ruta'  => 'images/pokemon/48-Venonat.png',
                'pokemon_id'=>48
            ],
             [
               'ruta'  => 'images/pokemon/49-Venomoth.png',
                'pokemon_id'=>49
            ],
             [
               'ruta'  => 'images/pokemon/50-Diglett.png',
                'pokemon_id'=>50
            ],
            [
               'ruta'  => 'images/pokemon/51-Dugtrio.png',
                'pokemon_id'=>51
            ],
            [
               'ruta'  => 'images/pokemon/52-Meowt.png',
                'pokemon_id'=>52
            ],
            [
               'ruta'  => 'images/pokemon/53-Persian.png',
                'pokemon_id'=>53
            ],
             [
               'ruta'  => 'images/pokemon/54-Psyduck.png',
                'pokemon_id'=>54
            ],
             [
               'ruta'  => 'images/pokemon/55-Golduck.png',
                'pokemon_id'=>55
            ],
            [
               'ruta'  => 'images/pokemon/56-Mankey.png',
                'pokemon_id'=>56
            ],
            [
               'ruta'  => 'images/pokemon/57-Primeape.png',
                'pokemon_id'=>57
            ],
            [
               'ruta'  => 'images/pokemon/58-Growlithe.png',
                'pokemon_id'=>58
            ],
            [
               'ruta'  => 'images/pokemon/59-Arcanine.png',
                'pokemon_id'=>59
            ],
            [
               'ruta'  => 'images/pokemon/60-Poliwag.png',
                'pokemon_id'=>60
            ],
            [
               'ruta'  => 'images/pokemon/61-Poliwhirl.png',
                'pokemon_id'=>61
            ],
            [
               'ruta'  => 'images/pokemon/62-Poliwrath.png',
                'pokemon_id'=>62
            ],
            [
               'ruta'  => 'images/pokemon/63-Abra.png',
                'pokemon_id'=>63
            ],
            [
               'ruta'  => 'images/pokemon/64-Kadabra.png',
                'pokemon_id'=>64
            ],
            [
               'ruta'  => 'images/pokemon/65-Alakazam.png',
                'pokemon_id'=>65
            ],
             [
               'ruta'  => 'images/pokemon/66-Machop.png',
                'pokemon_id'=>66
            ],
             [
               'ruta'  => 'images/pokemon/67-Machoke.png',
                'pokemon_id'=>67
            ],
            [
               'ruta'  => 'images/pokemon/68-Machamp.png',
                'pokemon_id'=>68
            ],
             [
               'ruta'  => 'images/pokemon/69-Bellsprout.png',
                'pokemon_id'=>69
            ],
            [
               'ruta'  => 'images/pokemon/70-Weepinbell.png',
                'pokemon_id'=>70
            ],
             [
               'ruta'  => 'images/pokemon/71-Victreebel.png',
                'pokemon_id'=>71
            ],
            [
               'ruta'  => 'images/pokemon/72-Tentacool.png',
                'pokemon_id'=>72
            ],
            [
               'ruta'  => 'images/pokemon/73-Tentacruel.png',
                'pokemon_id'=>73
            ],
            [
               'ruta'  => 'images/pokemon/74-Geodude.png',
                'pokemon_id'=>74
            ],
            [
               'ruta'  => 'images/pokemon/75-Graveler.png',
                'pokemon_id'=>75
            ],
            [
               'ruta'  => 'images/pokemon/76-Golem.png',
                'pokemon_id'=>76
            ],
            [
               'ruta'  => 'images/pokemon/77-Ponyta.png',
                'pokemon_id'=>77
            ],
             [
               'ruta'  => 'images/pokemon/78-Rapidash.png',
                'pokemon_id'=>78
            ],
            [
               'ruta'  => 'images/pokemon/79-Slowpoke.png',
                'pokemon_id'=>79
            ],
            [
               'ruta'  => 'images/pokemon/80-Slowro.png',
                'pokemon_id'=>80
            ],
            [
               'ruta'  => 'images/pokemon/81-Magnemite.png',
                'pokemon_id'=>81
            ],
             [
               'ruta'  => 'images/pokemon/82-Magneton.png',
                'pokemon_id'=>82
            ],
            [
               'ruta'  => 'images/pokemon/83-Farfetch.png',
                'pokemon_id'=>83
            ],
            [
               'ruta'  => 'images/pokemon/84-Doduo.png',
                'pokemon_id'=>84
            ],
            [
               'ruta'  => 'images/pokemon/85-Dodrio.png',
                'pokemon_id'=>85
            ],
            [
               'ruta'  => 'images/pokemon/86-Seel.png',
                'pokemon_id'=>86
            ],
             [
               'ruta'  => 'images/pokemon/87-Dewong.png',
                'pokemon_id'=>87
            ],
            [
               'ruta'  => 'images/pokemon/88-Grimer.png',
                'pokemon_id'=>88
            ],
            [
               'ruta'  => 'images/pokemon/89-Muk.png',
                'pokemon_id'=>89
            ],
            [
               'ruta'  => 'images/pokemon/90-Shellder.png',
                'pokemon_id'=>90
            ],
            [
               'ruta'  => 'images/pokemon/91-Cloyster.png',
                'pokemon_id'=>91
            ],
             [
               'ruta'  => 'images/pokemon/92-Gastly.png',
                'pokemon_id'=>92
            ],
             [
               'ruta'  => 'images/pokemon/93-Haunter.png',
                'pokemon_id'=>93
            ],
             [
               'ruta'  => 'images/pokemon/94-Gengar.png',
                'pokemon_id'=>94
            ],
              [
               'ruta'  => 'images/pokemon/95-Onix.png',
                'pokemon_id'=>95
            ],
            [
               'ruta'  => 'images/pokemon/96-Drowzee.png',
                'pokemon_id'=>96
            ],
            [
               'ruta'  => 'images/pokemon/97-Hypno.png',
                'pokemon_id'=>97
            ],
            [
               'ruta'  => 'images/pokemon/98-Krabby.png',
                'pokemon_id'=>98
            ],
             [
               'ruta'  => 'images/pokemon/99-Kingler.png',
                'pokemon_id'=>99
            ],
             [
               'ruta'  => 'images/pokemon/100-Voltorb.png',
                'pokemon_id'=>100
            ],
              [
               'ruta'  => 'images/pokemon/101-Electrode.png',
                'pokemon_id'=>101
            ],
            [
               'ruta'  => 'images/pokemon/102-Exeggcute.png',
                'pokemon_id'=>102
            ],
            [
               'ruta'  => 'images/pokemon/103-Exeggutor.png',
                'pokemon_id'=>103
            ],
            [
               'ruta'  => 'images/pokemon/104-Cubone.png',
                'pokemon_id'=>104
            ],
            [
               'ruta'  => 'images/pokemon/105-Marowak.png',
                'pokemon_id'=>105
            ],
            [
               'ruta'  => 'images/pokemon/106-Hitmonlee.png',
                'pokemon_id'=>106
            ],
            [
               'ruta'  => 'images/pokemon/107-Hitmonchan.png',
                'pokemon_id'=>107
            ],
             [
               'ruta'  => 'images/pokemon/108-Lickitung.png',
                'pokemon_id'=>108
            ],
             [
               'ruta'  => 'images/pokemon/109-Koffing.png',
                'pokemon_id'=>109
            ],
            [
               'ruta'  => 'images/pokemon/110-Weezing.png',
                'pokemon_id'=>110
            ],
             [
               'ruta'  => 'images/pokemon/111-Rhyhorn.png',
                'pokemon_id'=>111
            ],
            [
               'ruta'  => 'images/pokemon/112-Rhydon.png',
                'pokemon_id'=>112
            ],
            [
               'ruta'  => 'images/pokemon/113-Chansey.png',
                'pokemon_id'=>113
            ],
            [
               'ruta'  => 'images/pokemon/114-Tangela.png',
                'pokemon_id'=>114
            ],
            [
               'ruta'  => 'images/pokemon/115-Kangaskhan.png',
                'pokemon_id'=>115
            ],
            [
               'ruta'  => 'images/pokemon/116-Horsea.png',
                'pokemon_id'=>116
            ],
            [
               'ruta'  => 'images/pokemon/117-Seadra.png',
                'pokemon_id'=>117
            ],
            [
               'ruta'  => 'images/pokemon/118-Goldeen.png',
                'pokemon_id'=>118
            ],
            [
               'ruta'  => 'images/pokemon/119-Seaking.png',
                'pokemon_id'=>119
            ],
            [
               'ruta'  => 'images/pokemon/120-Staryu.png',
                'pokemon_id'=>120
            ],
            [
               'ruta'  => 'images/pokemon/121-Starmie.png',
                'pokemon_id'=>121
            ],
            [
               'ruta'  => 'images/pokemon/122-MrMime.png',
                'pokemon_id'=>122
            ],
            [
               'ruta'  => 'images/pokemon/123-Scyther.png',
                'pokemon_id'=>123
            ],
            [
               'ruta'  => 'images/pokemon/124-Jynx.png',
                'pokemon_id'=>124
            ],
            [
               'ruta'  => 'images/pokemon/125-Electabuzz.png',
                'pokemon_id'=>125
            ],
            [
               'ruta'  => 'images/pokemon/126-Magmar.png',
                'pokemon_id'=>126
            ],
            [
               'ruta'  => 'images/pokemon/127-Pinsir.png',
                'pokemon_id'=>127
            ],
             [
               'ruta'  => 'images/pokemon/128-Tauros.png',
                'pokemon_id'=>128
            ],
            [
               'ruta'  => 'images/pokemon/129-Magikarp.png',
                'pokemon_id'=>129
            ],
            [
               'ruta'  => 'images/pokemon/130-Gyarados.png',
                'pokemon_id'=>130
            ],
            [
               'ruta'  => 'images/pokemon/131-Lapras.png',
                'pokemon_id'=>131
            ],
             [
               'ruta'  => 'images/pokemon/132-Ditto.png',
                'pokemon_id'=>132
            ],
             [
               'ruta'  => 'images/pokemon/133-Eevee.png',
                'pokemon_id'=>133
            ],
            [
               'ruta'  => 'images/pokemon/134-Vaporeon.png',
                'pokemon_id'=>134
            ],
            [
               'ruta'  => 'images/pokemon/135-Jolteon.png',
                'pokemon_id'=>135
            ],
            [
               'ruta'  => 'images/pokemon/136-Flareon.png',
                'pokemon_id'=>136
            ],
             [
               'ruta'  => 'images/pokemon/137-Porygon.png',
                'pokemon_id'=>137
            ],
            [
               'ruta'  => 'images/pokemon/138-Omanyte.png',
                'pokemon_id'=>138
            ],
             [
               'ruta'  => 'images/pokemon/139-Omastar.png',
                'pokemon_id'=>139
            ],
            [
               'ruta'  => 'images/pokemon/140-Kabuto.png',
                'pokemon_id'=>140
            ],
            [
               'ruta'  => 'images/pokemon/141-Kabutops.png',
                'pokemon_id'=>141
            ],
            [
               'ruta'  => 'images/pokemon/142-Aerodactyl.png',
                'pokemon_id'=>142
            ],
            [
               'ruta'  => 'images/pokemon/143-Snorlax.png',
                'pokemon_id'=>143
            ],
            [
               'ruta'  => 'images/pokemon/144-Articuno.png',
                'pokemon_id'=>144
            ],
            [
               'ruta'  => 'images/pokemon/145-Zapdos.png',
                'pokemon_id'=>145
            ],
            [
               'ruta'  => 'images/pokemon/146-Moltres.png',
                'pokemon_id'=>146
            ],
             [
               'ruta'  => 'images/pokemon/147-Dratini.png',
                'pokemon_id'=>147
            ],
            [
               'ruta'  => 'images/pokemon/148-Dragonair.png',
                'pokemon_id'=>148
            ],
             [
               'ruta'  => 'images/pokemon/149-Dragonite.png',
                'pokemon_id'=>149
            ],
            [
               'ruta'  => 'images/pokemon/150-Mewto.png',
                'pokemon_id'=>150
            ],
            [
               'ruta'  => 'images/pokemon/151-Mew.png',
                'pokemon_id'=>151
            ]
            
            
        ];
        
        DB::table('fotos_pokemon')->delete();
        foreach ($fotos as $foto){
            FotosPokemon::create($foto);
        }

		
    }
}
