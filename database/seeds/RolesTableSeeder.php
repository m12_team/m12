<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*Roles ARRAY*/
		$roles =[
			
			[
				'nombre'  => 'Usuario registrado'
			],
			[
				'nombre'  => 'Usuario registrado profesional'
			],
			[
				'nombre'  => 'Usuario administrador'
			],
		];
		
		DB::table('roles')->delete();
		foreach ($roles as $role){
			Role::create($role);
		}
    }
}
