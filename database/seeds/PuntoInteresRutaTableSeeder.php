<?php

use Illuminate\Database\Seeder;
use App\PuntoInteresRuta;

class PuntoInteresRutaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*Roles ARRAY*/
		$piruta =[
			
			[
				'punto_interes_id'  => 3,
				'ruta_id' => 1
			],
			[
				'punto_interes_id'  => 4,
				'ruta_id' => 1
			],
			[
				'punto_interes_id'  => 5,
				'ruta_id' => 1
			],
			[
				'punto_interes_id'  => 6,
				'ruta_id' => 1
			],
			[
				'punto_interes_id'  => 7,
				'ruta_id' => 2
			],
			[
				'punto_interes_id'  => 8,
				'ruta_id' => 2
			],
			[
				'punto_interes_id'  => 9,
				'ruta_id' => 2
			],
			[
				'punto_interes_id'  => 1,
				'ruta_id' => 3
			],
			[
				'punto_interes_id'  => 2,
				'ruta_id' => 3
			],
			[
				'punto_interes_id'  => 10,
				'ruta_id' => 4
			],
			[
				'punto_interes_id'  => 11,
				'ruta_id' => 4
			],

		];
		
		DB::table('pi_ruta')->delete();
		foreach ($piruta as $pr){
			PuntoInteresRuta::create($pr);
		}
    }
}
