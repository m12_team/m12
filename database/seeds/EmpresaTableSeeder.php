<?php

use Illuminate\Database\Seeder;
use App\Empresa;

class EmpresaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $empresas =[
			
			[
				'nombre'  => 'AlwaysTravelling'
			]
			
		];
		
		DB::table('empresa')->delete();
		foreach ($empresas as $empresa){
			Empresa::create($empresa);
		}
    }
}
