<?php

use Illuminate\Database\Seeder;
use App\Ruta;

class RutaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$rutas =[
			
			[
				'nombre'  => 'Ruta parques divertidos',
				'descripcion' => 'Rutas por los parques divertidos de Barcelona',
				'creador_id'=>1
			],
			[
				'nombre'  => 'Ruta por la antigua Roma',
				'descripcion' => 'Rutas por los parques romanos',
				'creador_id'=>2
			],
			[
				'nombre'  => 'Los museos franceses',
				'descripcion' => 'Ruta por los museos más importantes de Francia',
				'creador_id'=>3
			],
			[
				'nombre'  => 'Ruta Barcelona',
				'descripcion' => 'Ruta por Barcelona',
				'creador_id'=>1
			],
		];
		
		DB::table('ruta')->delete();
		foreach ($rutas as $ruta){
			Ruta::create($ruta);
		}
    }
}
