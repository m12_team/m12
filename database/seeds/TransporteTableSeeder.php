<?php

use Illuminate\Database\Seeder;
use App\Transporte;

class TransporteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $transportes =[
			
			[
				'nombre'  => 'Barco'
			],
			[
				'nombre'  => 'Caminando'
			],
			[
				'nombre'  => 'Bicicleta'
			],
			[
				'nombre'  => 'Moto'
			],
			[
				'nombre'  => 'Coche'
			],
		];
		
		DB::table('transportes')->delete();
		foreach ($transportes as $transporte){
			Transporte::create($transporte);
		}
    }
}
