<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void 
     */
    public function run()
    {
		$this->call(EmpresaTableSeeder::class);
		$this->call(RolesTableSeeder::class);
		$this->call(EntornoTableSeeder::class);
		$this->call(Z_CategoriaPuntoInteresTableSeeder::class);
		$this->call(TransporteTableSeeder::class);
		$this->call(RegionesTableSeeder::class);
		$this->call(SubregionesTableSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(StatesTableSeeder::class);
        $this->call(CitiesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(PuntoInteresSeeder::class);
        $this->call(FotosPuntoInteresSeeder::class);
        $this->call(RutaTableSeeder::class);
        $this->call(PuntoInteresRutaTableSeeder::class);
        $this->call(DiarioTableSeeder::class);
        $this->call(InformacionExtraTableSeeder::class);
		$this->call(PokemonTableSeeder::class);
        $this->call(FotosPokemonTableSeeder::class);
		$this->call(TiposTableSeeder::class);
        $this->call(TiposPokemonSeeder::class);
    }
}
