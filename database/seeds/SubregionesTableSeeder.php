<?php

use Illuminate\Database\Seeder;
use App\Subregiones;

class SubregionesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		 $subregiones =[
			
			[
				'nombre'  => 'África Oriental',
				'region_id'=>1

			],
			[
				'nombre'  => 'África Central',
				'region_id'=>1
			],
			[
				'nombre'  => 'África Septentrional',
				'region_id'=>1
			],
			[
				'nombre'  => 'África Meridional',
				'region_id'=>1
			],
			[
				'nombre'  => 'África Occidental',
				'region_id'=>1
			],
				[
				'nombre'  => 'América Septentrional',
				'region_id'=>2

			],
			[
				'nombre'  => 'América Latina',
				'region_id'=>2
			],
			[
				'nombre'  => 'El Caribe',
				'region_id'=>2
			],
			[
				'nombre'  => 'América Central',
				'region_id'=>2
			],
			[
				'nombre'  => 'América Meridional',
				'region_id'=>2
			],
			[
				'nombre'  => 'Asia Central',
				'region_id'=>3
			],
			[
				'nombre'  => 'Asia Oriental',
				'region_id'=>3
			],
			[
				'nombre'  => 'Asia Meridional',
				'region_id'=>3
			],
			[
				'nombre'  => 'Asia Suroriental',
				'region_id'=>3
			],
			[
				'nombre'  => 'Asia Occidental',
				'region_id'=>3
			],
			[
				'nombre'  => 'Europa Oriental',
				'region_id'=>4
			],
			[
				'nombre'  => 'Europa Septentrional',
				'region_id'=>4
			],
				[
				'nombre'  => 'Europa Meridional',
				'region_id'=>4
			],
				[
				'nombre'  => 'Europa Occidental',
				'region_id'=>4
			],
			[
				'nombre'  => 'Australia y Nueva Zelanda',
				'region_id'=>5
			],
			[
				'nombre'  => 'Melanesia',
				'region_id'=>5
			],
			[
				'nombre'  => 'Micronesia',
				'region_id'=>5
			],
			[
				'nombre'  => 'Polinesia',
				'region_id'=>5
			],
		];
		
		DB::table('subregion')->delete();
		foreach ($subregiones as $subregion){
			Subregiones::create($subregion);
		}
		
    }
}
