<?php

use Illuminate\Database\Seeder;
use App\FotosPI;
class FotosPuntoInteresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FotosPI::create([
                'ruta'      => "images/punto_interes/1/img_1.jpg",
				'creador_id' => 1,
				'punto_interes_id' => 1
         ]);
        FotosPI::create([
                'ruta'      => "images/punto_interes/1/img_2.jpg",
                'creador_id' => 1,
                'punto_interes_id' => 1
         ]);
        FotosPI::create([
                'ruta'      => "images/punto_interes/1/img_3.jpg",
                'creador_id' =>1,
                'punto_interes_id' => 1
         ]);
         FotosPI::create([
                'ruta'      => "images/punto_interes/1/img_4.jpg",
                'creador_id' => 1,
                'punto_interes_id' => 1
         ]);
         /**Punto de interes 2***/
         FotosPI::create([
                'ruta'      => "images/punto_interes/2/img_1.jpg",
                'creador_id' => 1,
                'punto_interes_id' => 2
         ]);
        FotosPI::create([
                'ruta'      => "images/punto_interes/2/img_2.jpg",
                'creador_id' => 1,
                'punto_interes_id' => 2
         ]);
        FotosPI::create([
                'ruta'      => "images/punto_interes/2/img_3.jpg",
                'creador_id' =>1,
                'punto_interes_id' => 2
         ]);
         FotosPI::create([
                'ruta'      => "images/punto_interes/2/img_4.jpg",
                'creador_id' => 1,
                'punto_interes_id' => 2
         ]);
         /***Punto de interes 3**/
        FotosPI::create([
                'ruta'      => "images/punto_interes/3/img_1.jpg",
                'creador_id' => 1,
                'punto_interes_id' => 3
         ]);
        FotosPI::create([
                'ruta'      => "images/punto_interes/3/img_2.jpg",
                'creador_id' =>1,
                'punto_interes_id' => 3
         ]);
         FotosPI::create([
                'ruta'      => "images/punto_interes/3/img_3.jpg",
                'creador_id' => 1,
                'punto_interes_id' => 3
         ]);
         FotosPI::create([
                'ruta'      => "images/punto_interes/3/img_4.jpg",
                'creador_id' =>1,
                'punto_interes_id' => 3
         ]);
         FotosPI::create([
                'ruta'      => "images/punto_interes/3/img_5.jpg",
                'creador_id' => 1,
                'punto_interes_id' => 3
         ]);
         /***Punto de interés 4***/
         FotosPI::create([
                'ruta'      => "images/punto_interes/4/img_1.jpg",
                'creador_id' => 1,
                'punto_interes_id' => 4
         ]);
        FotosPI::create([
                'ruta'      => "images/punto_interes/4/img_2.jpg",
                'creador_id' =>1,
                'punto_interes_id' => 4
         ]);
         FotosPI::create([
                'ruta'      => "images/punto_interes/4/img_3.jpg",
                'creador_id' => 1,
                'punto_interes_id' => 4
         ]);
         FotosPI::create([
                'ruta'      => "images/punto_interes/4/img_4.jpg",
                'creador_id' =>1,
                'punto_interes_id' => 4
         ]);
         /***Punto de interés 5***/
         FotosPI::create([
                'ruta'      => "images/punto_interes/5/img_1.jpg",
                'creador_id' =>1,
                'punto_interes_id' => 5
         ]);
         FotosPI::create([
                'ruta'      => "images/punto_interes/5/img_2.jpg",
                'creador_id' =>1,
                'punto_interes_id' => 5
         ]);
         FotosPI::create([
                'ruta'      => "images/punto_interes/5/img_3.jpg",
                'creador_id' =>1,
                'punto_interes_id' => 5
         ]);
         FotosPI::create([
                'ruta'      => "images/punto_interes/5/img_4.jpg",
                'creador_id' =>1,
                'punto_interes_id' => 5
         ]);
         FotosPI::create([
                'ruta'      => "images/punto_interes/5/img_5.jpg",
                'creador_id' =>1,
                'punto_interes_id' => 5
         ]);
         /*****6****/
         FotosPI::create([
                'ruta'      => "images/punto_interes/6/img_1.jpg",
                'creador_id' =>2,
                'punto_interes_id' => 6
         ]);
         FotosPI::create([
                'ruta'      => "images/punto_interes/6/img_2.jpg",
                'creador_id' =>2,
                'punto_interes_id' => 6
         ]);
         FotosPI::create([
                'ruta'      => "images/punto_interes/6/img_3.jpg",
                'creador_id' =>2,
                'punto_interes_id' => 6
         ]);
         FotosPI::create([
                'ruta'      => "images/punto_interes/6/img_4.jpg",
                'creador_id' =>2,
                'punto_interes_id' => 6
         ]);
         FotosPI::create([
                'ruta'      => "images/punto_interes/6/img_5.jpg",
                'creador_id' =>2,
                'punto_interes_id' => 6
         ]);
         /***7***/
        FotosPI::create([
                'ruta'      => "images/punto_interes/7/img_1.jpg",
                'creador_id' =>2,
                'punto_interes_id' => 7
         ]);
         FotosPI::create([
                'ruta'      => "images/punto_interes/7/img_2.jpg",
                'creador_id' =>2,
                'punto_interes_id' => 7
         ]);
         FotosPI::create([
                'ruta'      => "images/punto_interes/7/img_3.jpg",
                'creador_id' =>2,
                'punto_interes_id' => 7
         ]);
         FotosPI::create([
                'ruta'      => "images/punto_interes/7/img_4.jpg",
                'creador_id' =>2,
                'punto_interes_id' => 7
         ]);
         FotosPI::create([
                'ruta'      => "images/punto_interes/7/img_5.jpg",
                'creador_id' =>2,
                'punto_interes_id' => 7
         ]);
         /****8*****/
         FotosPI::create([
                'ruta'      => "images/punto_interes/8/img_1.jpg",
                'creador_id' =>2,
                'punto_interes_id' => 8
         ]);
         FotosPI::create([
                'ruta'      => "images/punto_interes/8/img_2.jpg",
                'creador_id' =>2,
                'punto_interes_id' => 8
         ]);
         FotosPI::create([
                'ruta'      => "images/punto_interes/8/img_3.jpg",
                'creador_id' =>2,
                'punto_interes_id' => 8
         ]);
         FotosPI::create([
                'ruta'      => "images/punto_interes/8/img_4.jpg",
                'creador_id' =>2,
                'punto_interes_id' => 8
         ]);
         FotosPI::create([
                'ruta'      => "images/punto_interes/8/img_5.jpg",
                'creador_id' =>2,
                'punto_interes_id' => 8
         ]);
         /****9*****/
         FotosPI::create([
                'ruta'      => "images/punto_interes/9/img_1.jpg",
                'creador_id' =>2,
                'punto_interes_id' => 9
         ]);
         FotosPI::create([
                'ruta'      => "images/punto_interes/9/img_2.jpg",
                'creador_id' =>2,
                'punto_interes_id' => 9
         ]);
         FotosPI::create([
                'ruta'      => "images/punto_interes/9/img_3.jpg",
                'creador_id' =>2,
                'punto_interes_id' => 9
         ]);
         FotosPI::create([
                'ruta'      => "images/punto_interes/9/img_4.jpg",
                'creador_id' =>2,
                'punto_interes_id' => 9
         ]);
         FotosPI::create([
                'ruta'      => "images/punto_interes/9/img_5.jpg",
                'creador_id' =>2,
                'punto_interes_id' => 9
         ]);
         /***10****/
        FotosPI::create([
                'ruta'      => "images/punto_interes/10/img_1.jpg",
                'creador_id' =>2,
                'punto_interes_id' => 10
         ]);
         FotosPI::create([
                'ruta'      => "images/punto_interes/10/img_2.jpg",
                'creador_id' =>2,
                'punto_interes_id' => 10
         ]);
         FotosPI::create([
                'ruta'      => "images/punto_interes/10/img_3.jpg",
                'creador_id' =>2,
                'punto_interes_id' => 10
         ]);
         FotosPI::create([
                'ruta'      => "images/punto_interes/10/img_4.jpg",
                'creador_id' =>2,
                'punto_interes_id' => 10
         ]);
         FotosPI::create([
                'ruta'      => "images/punto_interes/10/img_5.jpg",
                'creador_id' =>2,
                'punto_interes_id' => 10
         ]);

         ///****/
         FotosPI::create([
                'ruta'      => "images/punto_interes/11/img_1.jpg",
                'creador_id' =>15,
                'punto_interes_id' => 11
         ]);
         FotosPI::create([
                'ruta'      => "images/punto_interes/11/img_2.jpg",
                'creador_id' =>16,
                'punto_interes_id' => 11
         ]);
         FotosPI::create([
                'ruta'      => "images/punto_interes/11/img_3.jpg",
                'creador_id' =>15,
                'punto_interes_id' => 11
         ]);
         FotosPI::create([
                'ruta'      => "images/punto_interes/11/img_4.jpg",
                'creador_id' =>12,
                'punto_interes_id' => 11
         ]);
         FotosPI::create([
                'ruta'      => "images/punto_interes/11/img_5.jpg",
                'creador_id' =>14,
                'punto_interes_id' => 11
         ]);
	}     
}
