<?php

use Illuminate\Database\Seeder;
use App\TiposPokemon;

class TiposPokemonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        
        $tipospokemon =[
            
            [
                'tipos_id'  => 12,
                'pokemon_id'=>1
            ],
            [
               'tipos_id'  => 17,
                'pokemon_id'=>1
            ],
            [
               'tipos_id'  => 12,
                'pokemon_id'=>2
            ]   ,
            [
               'tipos_id'  => 17,
                'pokemon_id'=>2
            ],
            [
               'tipos_id'  => 12,
                'pokemon_id'=>3
            ] ,
            [
               'tipos_id'  => 17,
                'pokemon_id'=>3
            ] ,        
            [
               'tipos_id'  => 7,
                'pokemon_id'=>4
            ] ,
            [
               'tipos_id'  => 7,
                'pokemon_id'=>5
            ] ,
            [
               'tipos_id'  => 7,
                'pokemon_id'=>6
            ] ,
            [
               'tipos_id'  => 18,
                'pokemon_id'=>6
            ] ,
            [
               'tipos_id'  => 2,
                'pokemon_id'=>7
            ] ,
            [
               'tipos_id'  => 2,
                'pokemon_id'=>8
            ] ,
            [
               'tipos_id'  => 2,
                'pokemon_id'=>9
            ] ,
         [
               'tipos_id'  => 3,
                'pokemon_id'=>10
            ] ,

            [
               'tipos_id'  => 3,
                'pokemon_id'=>11
            ] ,
            [
               'tipos_id'  => 3,
                'pokemon_id'=>12
            ] ,
            [
               'tipos_id'  => 18,
                'pokemon_id'=>12
            ] ,
            [
               'tipos_id'  => 3,
                'pokemon_id'=>13
            ] ,
             [
               'tipos_id'  => 17,
                'pokemon_id'=>13
            ] ,
             [
               'tipos_id'  => 3,
                'pokemon_id'=>14
            ] ,
             [
               'tipos_id'  => 17,
                'pokemon_id'=>14
            ] ,
             [
               'tipos_id'  => 3,
                'pokemon_id'=>15
            ] ,
            [
               'tipos_id'  => 17,
                'pokemon_id'=>15
            ] ,
            [
               'tipos_id'  => 11,
                'pokemon_id'=>16
            ] ,
            [
               'tipos_id'  => 18,
                'pokemon_id'=>16
            ] ,
             [
               'tipos_id'  => 11,
                'pokemon_id'=>17
            ] ,
            [
               'tipos_id'  => 18,
                'pokemon_id'=>17
            ] ,

            [         
               'tipos_id'  => 11,
                'pokemon_id'=>18
            ] ,
            [
               'tipos_id'  => 18,
                'pokemon_id'=>18
            ] ,
            [
               'tipos_id'  => 11,
                'pokemon_id'=>19
            ] ,    
            [
               'tipos_id'  => 11,
                'pokemon_id'=>20
            ] ,
            [
               'tipos_id'  => 11,
                'pokemon_id'=>21
            ] ,    
            [
               'tipos_id'  => 11,
                'pokemon_id'=>22
            ] ,
             [
               'tipos_id'  => 18,
                'pokemon_id'=>21
            ] ,    
            [
               'tipos_id'  => 18,
                'pokemon_id'=>22
            ] ,
             [
               'tipos_id'  => 17,
                'pokemon_id'=>23
            ] ,    
            [
               'tipos_id'  => 17,
                'pokemon_id'=>24
            ] ,
               [
               'tipos_id'  => 5,
                'pokemon_id'=>25
            ] ,    
            [
               'tipos_id'  => 5,
                'pokemon_id'=>26
            ] ,
            [
               'tipos_id'  => 16,
                'pokemon_id'=>27
            ] ,    
            [
               'tipos_id'  => 16,
                'pokemon_id'=>28
            ] ,
            [
               'tipos_id'  => 17,
                'pokemon_id'=>29
            ] ,
            [
                'tipos_id'  => 17,
                'pokemon_id'=>30
            ] ,
            [
                'tipos_id'  => 17,
                'pokemon_id'=>31
            ] ,
            [
                'tipos_id'  => 16,
                'pokemon_id'=>31
            ] ,
            [
                'tipos_id'  => 17,
                'pokemon_id'=>32
            ] ,
            [
                'tipos_id'  => 17,
                'pokemon_id'=>33
            ] ,
            [
                'tipos_id'  => 17,
                'pokemon_id'=>34
            ] ,
             [
                'tipos_id'  => 16,
                'pokemon_id'=>34
            ] ,
              [
                'tipos_id'  => 8,
                'pokemon_id'=>35
            ] ,
              [
                'tipos_id'  => 8,
                'pokemon_id'=>36
            ] ,
            [
                'tipos_id'  => 7,
                'pokemon_id'=>37
            ] ,
            [
                'tipos_id'  => 7,
                'pokemon_id'=>38
            ] ,
            [
                'tipos_id'  => 11,
                'pokemon_id'=>39
            ] ,
            [
                'tipos_id'  => 11,
                'pokemon_id'=>40
            ] ,
            [
                'tipos_id'  => 8,
                'pokemon_id'=>39
            ] ,
            [
                'tipos_id'  => 8,
                'pokemon_id'=>40
            ] ,
            [
                'tipos_id'  => 17,
                'pokemon_id'=>41
            ] ,
            [
                'tipos_id'  => 17,
                'pokemon_id'=>42
            ] ,
              [
                'tipos_id'  => 18,
                'pokemon_id'=>41
            ] ,
            [
                'tipos_id'  => 18,
                'pokemon_id'=>42
            ] ,
            [
                'tipos_id'  => 12,
                'pokemon_id'=>43
            ] ,
            [
                'tipos_id'  => 12,
                'pokemon_id'=>44
            ] ,
            [
                'tipos_id'  => 12,
                'pokemon_id'=>45
            ] ,
             [
                'tipos_id'  => 17,
                'pokemon_id'=>43
            ] ,
            [
                'tipos_id'  => 17,
                'pokemon_id'=>44
            ] ,
            [
                'tipos_id'  => 17,
                'pokemon_id'=>45
            ] ,
             [
                'tipos_id'  => 3,
                'pokemon_id'=>46
            ] ,
            [
                'tipos_id'  => 3,
                'pokemon_id'=>47
            ] ,
            [
                'tipos_id'  => 12,
                'pokemon_id'=>46
            ] ,
            [
                'tipos_id'  => 12,
                'pokemon_id'=>47
            ] ,
            [
                'tipos_id'  => 3,
                'pokemon_id'=>48
            ] ,
            [
                'tipos_id'  => 3,
                'pokemon_id'=>49
            ] ,
            [
                'tipos_id'  => 17,
                'pokemon_id'=>48
            ] ,
            [
                'tipos_id'  => 17,
                'pokemon_id'=>49
            ] ,
             [
                'tipos_id'  => 16,
                'pokemon_id'=>50
            ] ,
            [
                'tipos_id'  => 16,
                'pokemon_id'=>51
            ] ,
            [
                'tipos_id'  => 11,
                'pokemon_id'=>52
            ] ,
            [
                'tipos_id'  => 11,
                'pokemon_id'=>53
            ] ,
             [
                'tipos_id'  => 2,
                'pokemon_id'=>54
            ] ,
            [
                'tipos_id'  => 2,
                'pokemon_id'=>55
            ] ,
             [
                'tipos_id'  => 10,
                'pokemon_id'=>56
            ] ,
            [
                'tipos_id'  => 10,
                'pokemon_id'=>57
            ] ,
            [
                'tipos_id'  => 7,
                'pokemon_id'=>58
            ] ,
            [
                'tipos_id'  => 7,
                'pokemon_id'=>59
            ] ,
            [
                'tipos_id'  => 2,
                'pokemon_id'=>60
            ] ,
            [
                'tipos_id'  => 2,
                'pokemon_id'=>61
            ] ,
            [
                'tipos_id'  => 2,
                'pokemon_id'=>62
            ] ,
            [
                'tipos_id'  => 10,
                'pokemon_id'=>62
            ] ,
            [
                'tipos_id'  => 13,
                'pokemon_id'=>63
            ] ,
            [
                'tipos_id'  => 13,
                'pokemon_id'=>64
            ] ,
            [
                'tipos_id'  => 13,
                'pokemon_id'=>65
            ] ,
            [
                'tipos_id'  => 10,
                'pokemon_id'=>66
            ] ,
            [
                'tipos_id'  => 10,
                'pokemon_id'=>67
            ] ,
            [
                'tipos_id'  => 10,
                'pokemon_id'=>68
            ] ,
            [
                'tipos_id'  => 12,
                'pokemon_id'=>69
            ] ,
            [
                'tipos_id'  => 12,
                'pokemon_id'=>70
            ] ,
            [
                'tipos_id'  => 12,
                'pokemon_id'=>71
            ] ,
             [
                'tipos_id'  => 17,
                'pokemon_id'=>69
            ] ,
            [
                'tipos_id'  => 17,
                'pokemon_id'=>70
            ] ,
            [
                'tipos_id'  => 17,
                'pokemon_id'=>71
            ],
             [
                'tipos_id'  => 17,
                'pokemon_id'=>72
            ],
            [
                'tipos_id'  => 17,
                'pokemon_id'=>73
            ],

             [
                'tipos_id'  => 2,
                'pokemon_id'=>72
            ],
            [
                'tipos_id'  => 2,
                'pokemon_id'=>73
            ],
            [
                'tipos_id'  => 16,
                'pokemon_id'=>74
            ],
            [
                'tipos_id'  => 16,
                'pokemon_id'=>75
            ],
            [
                'tipos_id'  => 16,
                'pokemon_id'=>76
            ],
             [
                'tipos_id'  => 14,
                'pokemon_id'=>74
            ],
            [
                'tipos_id'  => 14,
                'pokemon_id'=>75
            ],
            [
                'tipos_id'  => 14,
                'pokemon_id'=>76
            ],
            [
                'tipos_id'  => 7,
                'pokemon_id'=>77
            ],
            [
                'tipos_id'  => 7,
                'pokemon_id'=>78
            ],
             [
                'tipos_id'  => 2,
                'pokemon_id'=>79
            ],
            [
                'tipos_id'  => 2,
                'pokemon_id'=>80
            ],
            [
                'tipos_id'  => 13,
                'pokemon_id'=>79
            ],
            [
                'tipos_id'  => 13,
                'pokemon_id'=>80
            ],
            [
                'tipos_id'  => 5,
                'pokemon_id'=>81
            ],
            [
                'tipos_id'  => 5,
                'pokemon_id'=>82
            ],
              [
                'tipos_id'  => 1,
                'pokemon_id'=>81
            ],
            [
                'tipos_id'  => 1,
                'pokemon_id'=>82
            ],
            [
                'tipos_id'  => 11,
                'pokemon_id'=>83
            ],
            [
                'tipos_id'  => 11,
                'pokemon_id'=>84
            ],
            [
                'tipos_id'  => 11,
                'pokemon_id'=>85
            ],
             [
                'tipos_id'  => 18,
                'pokemon_id'=>83
            ],
            [
                'tipos_id'  => 18,
                'pokemon_id'=>84
            ],
            [
                'tipos_id'  => 18,
                'pokemon_id'=>85
            ],
            [
                'tipos_id'  => 2,
                'pokemon_id'=>86
            ],
            [
                'tipos_id'  => 2,
                'pokemon_id'=>87
            ],
            [
                'tipos_id'  => 9,
                'pokemon_id'=>87
            ],
            [
                'tipos_id'  => 17,
                'pokemon_id'=>88
            ],
            [
                'tipos_id'  => 17,
                'pokemon_id'=>89
            ],
            [
                'tipos_id'  => 2,
                'pokemon_id'=>90
            ],
            [
                'tipos_id'  => 2,
                'pokemon_id'=>91
            ],
            [
                'tipos_id'  => 9,
                'pokemon_id'=>91
            ],
            [
                'tipos_id'  => 6,
                'pokemon_id'=>92
            ],
            [
                'tipos_id'  => 6,
                'pokemon_id'=>93
            ],
            [
                'tipos_id'  => 6,
                'pokemon_id'=>94
            ],
             [
                'tipos_id'  =>17,
                'pokemon_id'=>92
            ],
            [
                'tipos_id'  =>17,
                'pokemon_id'=>93
            ],
            [
                'tipos_id'  =>17,
                'pokemon_id'=>94
            ],
             [
                'tipos_id'  =>14,
                'pokemon_id'=>95
            ],
             [
                'tipos_id'  =>16,
                'pokemon_id'=>95
            ],
             [
                'tipos_id'  =>13,
                'pokemon_id'=>96
            ],
            [
                'tipos_id'  =>13,
                'pokemon_id'=>97
            ],
            [
                'tipos_id'  =>2,
                'pokemon_id'=>98
            ],
            [
                'tipos_id'  =>2,
                'pokemon_id'=>99
            ],
            [
                'tipos_id'  =>5,
                'pokemon_id'=>100
            ],
            [
                'tipos_id'  =>5,
                'pokemon_id'=>101
            ],
            [
                'tipos_id'  =>12,
                'pokemon_id'=>102
            ],
            [
                'tipos_id'  =>12,
                'pokemon_id'=>103
            ],
            [
                'tipos_id'  =>13,
                'pokemon_id'=>102
            ],
            [
                'tipos_id'  =>13,
                'pokemon_id'=>103
            ],
            [
                'tipos_id'  =>16,
                'pokemon_id'=>104
            ],
            [
                'tipos_id'  =>16,
                'pokemon_id'=>105
            ],
             [
                'tipos_id'  =>10,
                'pokemon_id'=>106
            ],
            [
                'tipos_id'  =>10,
                'pokemon_id'=>107
            ],
            [
                'tipos_id'  =>11,
                'pokemon_id'=>108
            ],
            [
                'tipos_id'  =>17,
                'pokemon_id'=>109
            ],
            [
                'tipos_id'  =>17,
                'pokemon_id'=>110
            ],
            [
                'tipos_id'  =>14,
                'pokemon_id'=>111
            ],
            [
                'tipos_id'  =>14,
                'pokemon_id'=>112
            ],
            [
                'tipos_id'  =>16,
                'pokemon_id'=>111
            ],
            [
                'tipos_id'  =>16,
                'pokemon_id'=>112
            ],
            [
                'tipos_id'  =>11,
                'pokemon_id'=>113
            ],
             [
                'tipos_id'  =>12,
                'pokemon_id'=>114
            ],
            [
                'tipos_id'  =>11,
                'pokemon_id'=>115
            ],
            [
                'tipos_id'  =>2,
                'pokemon_id'=>116
            ],
              [
                'tipos_id'  =>2,
                'pokemon_id'=>117
            ],
            [
                'tipos_id'  =>2,
                'pokemon_id'=>118
            ],
            [
                'tipos_id'  =>2,
                'pokemon_id'=>119
            ],
            [
                'tipos_id'  =>2,
                'pokemon_id'=>120
            ],
            [
                'tipos_id'  =>2,
                'pokemon_id'=>121
            ],
             [
                'tipos_id'  =>13,
                'pokemon_id'=>121
            ],
             [
                'tipos_id'  =>13,
                'pokemon_id'=>122
            ],
             [
                'tipos_id'  =>8,
                'pokemon_id'=>122
            ],
            [
                'tipos_id'  =>3,
                'pokemon_id'=>123
            ],
            [
                'tipos_id'  =>18,
                'pokemon_id'=>123
            ],
            [
                'tipos_id'  =>9,
                'pokemon_id'=>124
            ],
            [
                'tipos_id'  =>13,
                'pokemon_id'=>124
            ],
            [
                'tipos_id'  =>5,
                'pokemon_id'=>125
            ],
            [
                'tipos_id'  =>7,
                'pokemon_id'=>126
            ],
            [
                'tipos_id'  =>3,
                'pokemon_id'=>127
            ],
            [
                'tipos_id'  =>11,
                'pokemon_id'=>128
            ],
            [
                'tipos_id'  =>2,
                'pokemon_id'=>129
            ],
            [
                'tipos_id'  =>2,
                'pokemon_id'=>130
            ],
            [
                'tipos_id'  =>18,
                'pokemon_id'=>130
            ],
             [
                'tipos_id'  =>2,
                'pokemon_id'=>131
            ],
            [
                'tipos_id'  =>9,
                'pokemon_id'=>131
            ],
             [
                'tipos_id'  =>11,
                'pokemon_id'=>132
            ],
            [
                'tipos_id'  =>11,
                'pokemon_id'=>133
            ],
             [
                'tipos_id'  =>2,
                'pokemon_id'=>134
            ],
             [
                'tipos_id'  =>5,
                'pokemon_id'=>135
            ],
            [
                'tipos_id'  =>7,
                'pokemon_id'=>136
            ],            
            [
                'tipos_id'  =>11,
                'pokemon_id'=>137
            ],
            [
                'tipos_id'  =>14,
                'pokemon_id'=>138
            ],
            [
                'tipos_id'  =>14,
                'pokemon_id'=>139
            ],
            [
                'tipos_id'  =>14,
                'pokemon_id'=>140
            ],
            [
                'tipos_id'  =>14,
                'pokemon_id'=>141
            ],
            [
                'tipos_id'  =>14,
                'pokemon_id'=>142
            ],
            [
                'tipos_id'  =>2,
                'pokemon_id'=>138
            ],
            [
                'tipos_id'  =>2,
                'pokemon_id'=>139
            ],
            [
                'tipos_id'  =>2,
                'pokemon_id'=>140
            ],
            [
                'tipos_id'  =>2,
                'pokemon_id'=>141
            ],
            [
                'tipos_id'  =>18,
                'pokemon_id'=>142
            ],
            [
                'tipos_id'  =>11,
                'pokemon_id'=>143
            ],
             [
                'tipos_id'  =>18,
                'pokemon_id'=>144
            ],
            [
                'tipos_id'  =>9,
                'pokemon_id'=>144
            ],
             [
                'tipos_id'  =>18,
                'pokemon_id'=>145
            ],
            [
                'tipos_id'  =>5,
                'pokemon_id'=>145
            ],
            [
                'tipos_id'  =>18,
                'pokemon_id'=>146
            ],
            [
                'tipos_id'  =>7,
                'pokemon_id'=>146
            ],
            [
                'tipos_id'  =>4,
                'pokemon_id'=>147
            ],
            [
                'tipos_id'  =>4,
                'pokemon_id'=>148
            ],
            [
                'tipos_id'  =>4,
                'pokemon_id'=>149
            ],
            [
                'tipos_id'  =>18,
                'pokemon_id'=>149
            ],
             [
                'tipos_id'  =>13,
                'pokemon_id'=>150
            ],
            [
                'tipos_id'  =>13,
                'pokemon_id'=>151
            ]

        ];
        
        DB::table('tipos_pokemon')->delete();
        foreach ($tipospokemon as $pokemon){
            TiposPokemon::create($pokemon);
        }    }
}
