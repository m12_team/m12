<?php

use Illuminate\Database\Seeder;
use App\Entorno;

class EntornoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*Entorno ARRAY*/
		$entornos =[
			
			[
				'nombre'  => 'Mar'
			],
			[
				'nombre'  => 'Montaña'
			],
			[
				'nombre'  => 'Ciudad'
			],
		];
		
		DB::table('entorno')->delete();
		foreach ($entornos as $entorno){
			Entorno::create($entorno);
		}
    }
}
