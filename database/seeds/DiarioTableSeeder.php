<?php

use Illuminate\Database\Seeder;
use App\Diario;

class DiarioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    
    public function run()
    {
		$diarios =[
			
			[
				'nombre'  => 'Un dia por Barcelona',
				'descripcion' => 'Un viaje corto que hicimos por Barcelona',
				'creador_id'=>1
			],
			[
				'nombre'  => 'Dos dias viendo Roma',
				'descripcion' => 'Una escapada por Roma',
				'creador_id'=>2
			],
			[
				'nombre'  => 'Viaje por los parques de atracciones',
				'descripcion' => 'Una semana muy divertida en Barcelona visitando los parques de atracciones',
				'creador_id'=>3
			],
			[
				'nombre'  => 'Un diario de Museos',
				'descripcion' => 'Hoy visitamos dos de los museos más importantes del mundo',
				'creador_id'=> 1
			],
		];
		
		DB::table('diarios')->delete();
		foreach ($diarios as $diario){
			Diario::create($diario);
		}
    }
}
