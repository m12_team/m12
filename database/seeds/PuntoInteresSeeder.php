<?php

use Illuminate\Database\Seeder;
use App\PuntoInteres;
class PuntoInteresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
    {

            PuntoInteres::create([
                'nombre'      => "Museo Louvre",
				'descripcion' => "Inaugurado a finales del siglo XVIII, el Museo del Louvre es el museo más importante de Francia y el más visitado del mundo. Actualmente recibe más de ocho millones de visitantes cada año.",
				'precio' => 15,
                'longitud' => 48.860667,
                'latitud'=> 2.338127,
                'categoria_pi_id'=>1,
                'entorno_id'=>rand(1,3),
				'transporte_id'=>rand(1,5),
				'creador_id'=>rand(1,20),
				'estaBloqueado'=>0
            ]);

            PuntoInteres::create([
                'nombre'      => "Museo de Osray",
                'descripcion' => "Ubicado en una antigua estación de tren y dedicado al arte del siglo XIX, el Museo de Orsay cubre los periodos artísticos transcurridos entre la colección del Louvre y la del Centro Pompidou.",
                'precio' => 15,
                'longitud' =>48.859918,
                'latitud'=> 2.326944,
                'categoria_pi_id'=>1,
                'entorno_id'=>rand(1,3),
                'transporte_id'=>rand(1,5),
                'creador_id'=>rand(1,20),
                'estaBloqueado'=>0
            ]);

            PuntoInteres::create([
                'nombre'      => "Port Aventura",
                'descripcion' => "PortAventura World es un complejo de ocio, situado en los municipios tarraconenses de Vila-seca y Salou en España",
                'precio' => 15,
                'longitud' => 41.086794,
                'latitud'=> 1.156840,
                'categoria_pi_id'=>1,
                'entorno_id'=>rand(1,3),
                'transporte_id'=>rand(1,5),
                'creador_id'=>rand(1,20),
                'estaBloqueado'=>0
            ]);

            PuntoInteres::create([
                'nombre'      => "Tibidabo",
                'descripcion' => "El Parque de Atracciones Tibidabo es un parque de atracciones situado en la montaña del Tibidabo, perteneciente a la sierra de Collserola, al oeste del municipio de Barcelona capital.",
                'precio' => 25,
                'longitud' => 41.422179,
                'latitud'=> 2.119836,
                'categoria_pi_id'=>1,
                'entorno_id'=>rand(1,3),
                'transporte_id'=>rand(1,5),
                'creador_id'=>rand(1,20),
                'estaBloqueado'=>0
            ]);

            PuntoInteres::create([
                'nombre'      => "Isla Fantasia",
                'descripcion' => "Illa Fantasia es un parque acuático situado en Vilassar de Dalt en la comarca del Maresme",
                'precio' => 15,
                'longitud' => 41.504668,
                'latitud'=> 2.362761,
                'categoria_pi_id'=>2,
                'entorno_id'=>rand(1,3),
                'transporte_id'=>rand(1,5),
                'creador_id'=>rand(1,20),
                'estaBloqueado'=>0
            ]);
            PuntoInteres::create([
                'nombre'      => "WaterWorld",
                'descripcion' => "Illa Fantasia es un parque acuático situado en Vilassar de Dalt en la comarca del Maresme",
                'precio' => 15,
                'longitud' =>41.467971,
                'latitud'=> 2.082609,
                'categoria_pi_id'=>2,
                'entorno_id'=>rand(1,3),
                'transporte_id'=>rand(1,5),
                'creador_id'=>rand(1,20),
                'estaBloqueado'=>0
            ]);
          
           PuntoInteres::create([
                'nombre'      => "Foro Romano",
                'descripcion' => "El Foro Romano era el foro de la ciudad de Roma, es decir, la zona central, semejante a las plazas centrales en las ciudades actuales, donde se encuentran las instituciones de gobierno, mercado y religión.",
                'precio' => 15,
                'longitud' =>41.892446,
                'latitud'=> 12.486097,
                'categoria_pi_id'=>2,
                'entorno_id'=>rand(1,3),
                'transporte_id'=>rand(1,5),
                'creador_id'=>rand(1,20),
                'estaBloqueado'=>0
            ]);

            PuntoInteres::create([
                'nombre'      => "Monte Palatino",
                'descripcion' => "El monte Palatino forma parte de la llamada Roma Quadrata. El Palatino es la más céntrica de las siete colinas de Roma y es una de las partes más antiguas de la ciudad.",
                'precio' => 15,
                'longitud' =>41.891542,
                'latitud'=> 12.487552,
                'categoria_pi_id'=>1,
                'entorno_id'=>rand(1,3),
                'transporte_id'=>rand(1,5),
                'creador_id'=>rand(1,20),
                'estaBloqueado'=>0
            ]);

            PuntoInteres::create([
                'nombre'      => "Capilla sixtina",
                'descripcion' => "La Capilla Sixtina es la capilla de la Basílica de San Pedro y la estancia más conocida del Palacio Apostólico de la Ciudad del Vaticano, la residencia oficial del papa",
                'precio' => 15,
                'longitud' =>41.903309,
                'latitud'=>12.467184,
                'categoria_pi_id'=>1,
                'entorno_id'=>rand(1,3),
                'transporte_id'=>rand(1,5),
                'creador_id'=>rand(1,20),
                'estaBloqueado'=>0
            ]);

            PuntoInteres::create([
                'nombre'      => "Baterías del Turó de la Rovira",
                'descripcion' => "Cada vez más frecuentados, los miradores de las antiguas baterías antiaéreas del Turó de la Rovira, en lo más alto del barrio del Carmel de Barcelona",
                'precio' => 15,
                'longitud' =>41.419434,
                'latitud'=>2.160875,
                'categoria_pi_id'=>2,
                'entorno_id'=>rand(1,3),
                'transporte_id'=>rand(1,5),
                'creador_id'=>rand(1,20),
                'estaBloqueado'=>0
            ]);

            PuntoInteres::create([
                'nombre'      => "Puerto de Barcelona",
                'descripcion' => "El Puerto de Barcelona es un puerto marítimo español, situado en el noreste de la península Ibérica junto al mar Mediterráneo, encajado entre la nueva desembocadura del río Llobregat y el barrio de La Barceloneta en la ciudad de Barcelona",
                'precio' => 15,
                'longitud' =>41.334371,
                'latitud'=>2.160608,
                'categoria_pi_id'=>1,
                'entorno_id'=>rand(1,3),
                'transporte_id'=>rand(1,5),
                'creador_id'=>rand(1,20),
                'estaBloqueado'=>0
            ]);
        
	}
}
