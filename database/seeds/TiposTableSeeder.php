<?php

use Illuminate\Database\Seeder;
use App\Tipos;

class TiposTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        
          $tipos =[
            
            [
                'nombre'  => 'Acero'
            ],
            [
                'nombre'  => 'Agua'
            ],
            [
                'nombre'  => 'Bicho'
            ],
            [
                'nombre'  => 'Dragón'
            ],
            [
                'nombre'  => 'Eléctrico'
            ] ,
            [
                'nombre'  => 'Fantasma'
            ],
            [
                'nombre'  => 'Fuego'
            ],
            [
                'nombre'  => 'Hada'
            ],
            [
                'nombre'  => 'Hielo'
            ],
            [
                'nombre'  => 'Lucha'
            ] ,
            [
                'nombre'  => 'Normal'
            ],
            [
                'nombre'  => 'Planta'
            ],
            [
                'nombre'  => 'Psiquico'
            ] ,
            [
                'nombre'  => 'Roca'
            ],
            [
                'nombre'  => 'Siniestro'
            ],
            [
                'nombre'  => 'Tierra'
            ]  ,
            [
                'nombre'  => 'Veneno'
            ],
            [
                'nombre'  => 'Volador'
            ]                    
        ];
        
        DB::table('tipos')->delete();
        foreach ($tipos as $tipo){
            Tipos::create($tipo);
        }
    }
}
