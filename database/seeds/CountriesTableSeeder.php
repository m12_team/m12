<?php

use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('pais')->delete();
        
        \DB::table('pais')->insert(array (
            0 => 
            array (
                'id' => 1,
                'sortname' => 'AF',
                'name' => 'Afghanistan',
                'subregion_id' => 1,
            ),
            1 => 
            array (
                'id' => 2,
                'sortname' => 'AL',
                'name' => 'Albania',
                'subregion_id' => 1,
            ),
            2 => 
            array (
                'id' => 3,
                'sortname' => 'DZ',
                'name' => 'Algeria',
                'subregion_id' => 1,
            ),
            3 => 
            array (
                'id' => 4,
                'sortname' => 'AS',
                'name' => 'American Samoa',
                'subregion_id' => 1,
            ),
            4 => 
            array (
                'id' => 5,
                'sortname' => 'AD',
                'name' => 'Andorra',
                'subregion_id' => 1,
            ),
            5 => 
            array (
                'id' => 6,
                'sortname' => 'AO',
                'name' => 'Angola',
                'subregion_id' => 6,
            ),
            6 => 
            array (
                'id' => 7,
                'sortname' => 'AI',
                'name' => 'Anguilla',
                'subregion_id' => 4,
            ),
            7 => 
            array (
                'id' => 8,
                'sortname' => 'AQ',
                'name' => 'Antarctica',
                'subregion_id' => 1,
            ),
            8 => 
            array (
                'id' => 9,
                'sortname' => 'AG',
                'name' => 'Antigua And Barbuda',
                'subregion_id' => 4,
            ),
            9 => 
            array (
                'id' => 10,
                'sortname' => 'AR',
                'name' => 'Argentina',
                'subregion_id' => 3,
            ),
            10 => 
            array (
                'id' => 11,
                'sortname' => 'AM',
                'name' => 'Armenia',
                'subregion_id' => 1,
            ),
            11 => 
            array (
                'id' => 12,
                'sortname' => 'AW',
                'name' => 'Aruba',
                'subregion_id' => 4,
            ),
            12 => 
            array (
                'id' => 13,
                'sortname' => 'AU',
                'name' => 'Australia',
                'subregion_id' => 1,
            ),
            13 => 
            array (
                'id' => 14,
                'sortname' => 'AT',
                'name' => 'Austria',
                'subregion_id' => 1,
            ),
            14 => 
            array (
                'id' => 15,
                'sortname' => 'AZ',
                'name' => 'Azerbaijan',
                'subregion_id' => 1,
            ),
            15 => 
            array (
                'id' => 16,
                'sortname' => 'BS',
                'name' => 'Bahamas The',
                'subregion_id' => 4,
            ),
            16 => 
            array (
                'id' => 17,
                'sortname' => 'BH',
                'name' => 'Bahrain',
                'subregion_id' => 1,
            ),
            17 => 
            array (
                'id' => 18,
                'sortname' => 'BD',
                'name' => 'Bangladesh',
                'subregion_id' => 1,
            ),
            18 => 
            array (
                'id' => 19,
                'sortname' => 'BB',
                'name' => 'Barbados',
                'subregion_id' => 4,
            ),
            19 => 
            array (
                'id' => 20,
                'sortname' => 'BY',
                'name' => 'Belarus',
                'subregion_id' => 1,
            ),
            20 => 
            array (
                'id' => 21,
                'sortname' => 'BE',
                'name' => 'Belgium',
                'subregion_id' => 1,
            ),
            21 => 
            array (
                'id' => 22,
                'sortname' => 'BZ',
                'name' => 'Belize',
                'subregion_id' => 2,
            ),
            22 => 
            array (
                'id' => 23,
                'sortname' => 'BJ',
                'name' => 'Benin',
                'subregion_id' => 1,
            ),
            23 => 
            array (
                'id' => 24,
                'sortname' => 'BM',
                'name' => 'Bermuda',
                'subregion_id' => 1,
            ),
            24 => 
            array (
                'id' => 25,
                'sortname' => 'BT',
                'name' => 'Bhutan',
                'subregion_id' => 1,
            ),
            25 => 
            array (
                'id' => 26,
                'sortname' => 'BO',
                'name' => 'Bolivia',
                'subregion_id' => 3,
            ),
            26 => 
            array (
                'id' => 27,
                'sortname' => 'BA',
                'name' => 'Bosnia and Herzegovina',
                'subregion_id' => 1,
            ),
            27 => 
            array (
                'id' => 28,
                'sortname' => 'BW',
                'name' => 'Botswana',
                'subregion_id' => 5,
            ),
            28 => 
            array (
                'id' => 29,
                'sortname' => 'BV',
                'name' => 'Bouvet Island',
                'subregion_id' => 1,
            ),
            29 => 
            array (
                'id' => 30,
                'sortname' => 'BR',
                'name' => 'Brazil',
                'subregion_id' => 3,
            ),
            30 => 
            array (
                'id' => 31,
                'sortname' => 'IO',
                'name' => 'British Indian Ocean Territory',
                'subregion_id' => 1,
            ),
            31 => 
            array (
                'id' => 32,
                'sortname' => 'BN',
                'name' => 'Brunei',
                'subregion_id' => 1,
            ),
            32 => 
            array (
                'id' => 33,
                'sortname' => 'BG',
                'name' => 'Bulgaria',
                'subregion_id' => 1,
            ),
            33 => 
            array (
                'id' => 34,
                'sortname' => 'BF',
                'name' => 'Burkina Faso',
                'subregion_id' => 1,
            ),
            34 => 
            array (
                'id' => 35,
                'sortname' => 'BI',
                'name' => 'Burundi',
                'subregion_id' => 1,
            ),
            35 => 
            array (
                'id' => 36,
                'sortname' => 'KH',
                'name' => 'Cambodia',
                'subregion_id' => 1,
            ),
            36 => 
            array (
                'id' => 37,
                'sortname' => 'CM',
                'name' => 'Cameroon',
                'subregion_id' => 6,
            ),
            37 => 
            array (
                'id' => 38,
                'sortname' => 'CA',
                'name' => 'Canada',
                'subregion_id' => 1,
            ),
            38 => 
            array (
                'id' => 39,
                'sortname' => 'CV',
                'name' => 'Cape Verde',
                'subregion_id' => 1,
            ),
            39 => 
            array (
                'id' => 40,
                'sortname' => 'KY',
                'name' => 'Cayman Islands',
                'subregion_id' => 1,
            ),
            40 => 
            array (
                'id' => 41,
                'sortname' => 'CF',
                'name' => 'Central African Republic',
                'subregion_id' => 1,
            ),
            41 => 
            array (
                'id' => 42,
                'sortname' => 'TD',
                'name' => 'Chad',
                'subregion_id' => 6,
            ),
            42 => 
            array (
                'id' => 43,
                'sortname' => 'CL',
                'name' => 'Chile',
                'subregion_id' => 3,
            ),
            43 => 
            array (
                'id' => 44,
                'sortname' => 'CN',
                'name' => 'China',
                'subregion_id' => 1,
            ),
            44 => 
            array (
                'id' => 45,
                'sortname' => 'CX',
                'name' => 'Christmas Island',
                'subregion_id' => 1,
            ),
            45 => 
            array (
                'id' => 46,
                'sortname' => 'CC',
            'name' => 'Cocos (Keeling) Islands',
                'subregion_id' => 1,
            ),
            46 => 
            array (
                'id' => 47,
                'sortname' => 'CO',
                'name' => 'Colombia',
                'subregion_id' => 3,
            ),
            47 => 
            array (
                'id' => 48,
                'sortname' => 'KM',
                'name' => 'Comoros',
                'subregion_id' => 1,
            ),
            48 => 
            array (
                'id' => 49,
                'sortname' => 'CG',
                'name' => 'Congo',
                'subregion_id' => 6,
            ),
            49 => 
            array (
                'id' => 50,
                'sortname' => 'CD',
                'name' => 'Congo The Democratic Republic Of The',
                'subregion_id' => 6,
            ),
            50 => 
            array (
                'id' => 51,
                'sortname' => 'CK',
                'name' => 'Cook Islands',
                'subregion_id' => 1,
            ),
            51 => 
            array (
                'id' => 52,
                'sortname' => 'CR',
                'name' => 'Costa Rica',
                'subregion_id' => 2,
            ),
            52 => 
            array (
                'id' => 53,
                'sortname' => 'CI',
            'name' => 'Cote D\'Ivoire (Ivory Coast)',
                'subregion_id' => 1,
            ),
            53 => 
            array (
                'id' => 54,
                'sortname' => 'HR',
            'name' => 'Croatia (Hrvatska)',
                'subregion_id' => 1,
            ),
            54 => 
            array (
                'id' => 55,
                'sortname' => 'CU',
                'name' => 'Cuba',
                'subregion_id' => 4,
            ),
            55 => 
            array (
                'id' => 56,
                'sortname' => 'CY',
                'name' => 'Cyprus',
                'subregion_id' => 1,
            ),
            56 => 
            array (
                'id' => 57,
                'sortname' => 'CZ',
                'name' => 'Czech Republic',
                'subregion_id' => 1,
            ),
            57 => 
            array (
                'id' => 58,
                'sortname' => 'DK',
                'name' => 'Denmark',
                'subregion_id' => 1,
            ),
            58 => 
            array (
                'id' => 59,
                'sortname' => 'DJ',
                'name' => 'Djibouti',
                'subregion_id' => 1,
            ),
            59 => 
            array (
                'id' => 60,
                'sortname' => 'DM',
                'name' => 'Dominica',
                'subregion_id' => 4,
            ),
            60 => 
            array (
                'id' => 61,
                'sortname' => 'DO',
                'name' => 'Dominican Republic',
                'subregion_id' => 4,
            ),
            61 => 
            array (
                'id' => 62,
                'sortname' => 'TP',
                'name' => 'East Timor',
                'subregion_id' => 1,
            ),
            62 => 
            array (
                'id' => 63,
                'sortname' => 'EC',
                'name' => 'Ecuador',
                'subregion_id' => 3,
            ),
            63 => 
            array (
                'id' => 64,
                'sortname' => 'EG',
                'name' => 'Egypt',
                'subregion_id' => 1,
            ),
            64 => 
            array (
                'id' => 65,
                'sortname' => 'SV',
                'name' => 'El Salvador',
                'subregion_id' => 2,
            ),
            65 => 
            array (
                'id' => 66,
                'sortname' => 'GQ',
                'name' => 'Equatorial Guinea',
                'subregion_id' => 6,
            ),
            66 => 
            array (
                'id' => 67,
                'sortname' => 'ER',
                'name' => 'Eritrea',
                'subregion_id' => 1,
            ),
            67 => 
            array (
                'id' => 68,
                'sortname' => 'EE',
                'name' => 'Estonia',
                'subregion_id' => 1,
            ),
            68 => 
            array (
                'id' => 69,
                'sortname' => 'ET',
                'name' => 'Ethiopia',
                'subregion_id' => 1,
            ),
            69 => 
            array (
                'id' => 70,
                'sortname' => 'XA',
                'name' => 'External Territories of Australia',
                'subregion_id' => 1,
            ),
            70 => 
            array (
                'id' => 71,
                'sortname' => 'FK',
                'name' => 'Falkland Islands',
                'subregion_id' => 3,
            ),
            71 => 
            array (
                'id' => 72,
                'sortname' => 'FO',
                'name' => 'Faroe Islands',
                'subregion_id' => 1,
            ),
            72 => 
            array (
                'id' => 73,
                'sortname' => 'FJ',
                'name' => 'Fiji Islands',
                'subregion_id' => 1,
            ),
            73 => 
            array (
                'id' => 74,
                'sortname' => 'FI',
                'name' => 'Finland',
                'subregion_id' => 1,
            ),
            74 => 
            array (
                'id' => 75,
                'sortname' => 'FR',
                'name' => 'France',
                'subregion_id' => 1,
            ),
            75 => 
            array (
                'id' => 76,
                'sortname' => 'GF',
                'name' => 'French Guiana',
                'subregion_id' => 1,
            ),
            76 => 
            array (
                'id' => 77,
                'sortname' => 'PF',
                'name' => 'French Polynesia',
                'subregion_id' => 1,
            ),
            77 => 
            array (
                'id' => 78,
                'sortname' => 'TF',
                'name' => 'French Southern Territories',
                'subregion_id' => 1,
            ),
            78 => 
            array (
                'id' => 79,
                'sortname' => 'GA',
                'name' => 'Gabon',
                'subregion_id' => 6,
            ),
            79 => 
            array (
                'id' => 80,
                'sortname' => 'GM',
                'name' => 'Gambia The',
                'subregion_id' => 1,
            ),
            80 => 
            array (
                'id' => 81,
                'sortname' => 'GE',
                'name' => 'Georgia',
                'subregion_id' => 1,
            ),
            81 => 
            array (
                'id' => 82,
                'sortname' => 'DE',
                'name' => 'Germany',
                'subregion_id' => 1,
            ),
            82 => 
            array (
                'id' => 83,
                'sortname' => 'GH',
                'name' => 'Ghana',
                'subregion_id' => 1,
            ),
            83 => 
            array (
                'id' => 84,
                'sortname' => 'GI',
                'name' => 'Gibraltar',
                'subregion_id' => 1,
            ),
            84 => 
            array (
                'id' => 85,
                'sortname' => 'GR',
                'name' => 'Greece',
                'subregion_id' => 1,
            ),
            85 => 
            array (
                'id' => 86,
                'sortname' => 'GL',
                'name' => 'Greenland',
                'subregion_id' => 1,
            ),
            86 => 
            array (
                'id' => 87,
                'sortname' => 'GD',
                'name' => 'Grenada',
                'subregion_id' => 4,
            ),
            87 => 
            array (
                'id' => 88,
                'sortname' => 'GP',
                'name' => 'Guadeloupe',
                'subregion_id' => 4,
            ),
            88 => 
            array (
                'id' => 89,
                'sortname' => 'GU',
                'name' => 'Guam',
                'subregion_id' => 1,
            ),
            89 => 
            array (
                'id' => 90,
                'sortname' => 'GT',
                'name' => 'Guatemala',
                'subregion_id' => 2,
            ),
            90 => 
            array (
                'id' => 91,
                'sortname' => 'XU',
                'name' => 'Guernsey and Alderney',
                'subregion_id' => 1,
            ),
            91 => 
            array (
                'id' => 92,
                'sortname' => 'GN',
                'name' => 'Guinea',
                'subregion_id' => 1,
            ),
            92 => 
            array (
                'id' => 93,
                'sortname' => 'GW',
                'name' => 'Guinea-Bissau',
                'subregion_id' => 1,
            ),
            93 => 
            array (
                'id' => 94,
                'sortname' => 'GY',
                'name' => 'Guyana',
                'subregion_id' => 3,
            ),
            94 => 
            array (
                'id' => 95,
                'sortname' => 'HT',
                'name' => 'Haiti',
                'subregion_id' => 4,
            ),
            95 => 
            array (
                'id' => 96,
                'sortname' => 'HM',
                'name' => 'Heard and McDonald Islands',
                'subregion_id' => 1,
            ),
            96 => 
            array (
                'id' => 97,
                'sortname' => 'HN',
                'name' => 'Honduras',
                'subregion_id' => 2,
            ),
            97 => 
            array (
                'id' => 98,
                'sortname' => 'HK',
                'name' => 'Hong Kong S.A.R.',
                'subregion_id' => 1,
            ),
            98 => 
            array (
                'id' => 99,
                'sortname' => 'HU',
                'name' => 'Hungary',
                'subregion_id' => 1,
            ),
            99 => 
            array (
                'id' => 100,
                'sortname' => 'IS',
                'name' => 'Iceland',
                'subregion_id' => 1,
            ),
            100 => 
            array (
                'id' => 101,
                'sortname' => 'IN',
                'name' => 'India',
                'subregion_id' => 1,
            ),
            101 => 
            array (
                'id' => 102,
                'sortname' => 'ID',
                'name' => 'Indonesia',
                'subregion_id' => 1,
            ),
            102 => 
            array (
                'id' => 103,
                'sortname' => 'IR',
                'name' => 'Iran',
                'subregion_id' => 1,
            ),
            103 => 
            array (
                'id' => 104,
                'sortname' => 'IQ',
                'name' => 'Iraq',
                'subregion_id' => 1,
            ),
            104 => 
            array (
                'id' => 105,
                'sortname' => 'IE',
                'name' => 'Ireland',
                'subregion_id' => 1,
            ),
            105 => 
            array (
                'id' => 106,
                'sortname' => 'IL',
                'name' => 'Israel',
                'subregion_id' => 1,
            ),
            106 => 
            array (
                'id' => 107,
                'sortname' => 'IT',
                'name' => 'Italy',
                'subregion_id' => 1,
            ),
            107 => 
            array (
                'id' => 108,
                'sortname' => 'JM',
                'name' => 'Jamaica',
                'subregion_id' => 4,
            ),
            108 => 
            array (
                'id' => 109,
                'sortname' => 'JP',
                'name' => 'Japan',
                'subregion_id' => 1,
            ),
            109 => 
            array (
                'id' => 110,
                'sortname' => 'XJ',
                'name' => 'Jersey',
                'subregion_id' => 1,
            ),
            110 => 
            array (
                'id' => 111,
                'sortname' => 'JO',
                'name' => 'Jordan',
                'subregion_id' => 1,
            ),
            111 => 
            array (
                'id' => 112,
                'sortname' => 'KZ',
                'name' => 'Kazakhstan',
                'subregion_id' => 1,
            ),
            112 => 
            array (
                'id' => 113,
                'sortname' => 'KE',
                'name' => 'Kenya',
                'subregion_id' => 1,
            ),
            113 => 
            array (
                'id' => 114,
                'sortname' => 'KI',
                'name' => 'Kiribati',
                'subregion_id' => 1,
            ),
            114 => 
            array (
                'id' => 115,
                'sortname' => 'KP',
                'name' => 'Korea North',
                'subregion_id' => 1,
            ),
            115 => 
            array (
                'id' => 116,
                'sortname' => 'KR',
                'name' => 'Korea South',
                'subregion_id' => 1,
            ),
            116 => 
            array (
                'id' => 117,
                'sortname' => 'KW',
                'name' => 'Kuwait',
                'subregion_id' => 1,
            ),
            117 => 
            array (
                'id' => 118,
                'sortname' => 'KG',
                'name' => 'Kyrgyzstan',
                'subregion_id' => 1,
            ),
            118 => 
            array (
                'id' => 119,
                'sortname' => 'LA',
                'name' => 'Laos',
                'subregion_id' => 1,
            ),
            119 => 
            array (
                'id' => 120,
                'sortname' => 'LV',
                'name' => 'Latvia',
                'subregion_id' => 1,
            ),
            120 => 
            array (
                'id' => 121,
                'sortname' => 'LB',
                'name' => 'Lebanon',
                'subregion_id' => 1,
            ),
            121 => 
            array (
                'id' => 122,
                'sortname' => 'LS',
                'name' => 'Lesotho',
                'subregion_id' => 5,
            ),
            122 => 
            array (
                'id' => 123,
                'sortname' => 'LR',
                'name' => 'Liberia',
                'subregion_id' => 1,
            ),
            123 => 
            array (
                'id' => 124,
                'sortname' => 'LY',
                'name' => 'Libya',
                'subregion_id' => 1,
            ),
            124 => 
            array (
                'id' => 125,
                'sortname' => 'LI',
                'name' => 'Liechtenstein',
                'subregion_id' => 1,
            ),
            125 => 
            array (
                'id' => 126,
                'sortname' => 'LT',
                'name' => 'Lithuania',
                'subregion_id' => 1,
            ),
            126 => 
            array (
                'id' => 127,
                'sortname' => 'LU',
                'name' => 'Luxembourg',
                'subregion_id' => 1,
            ),
            127 => 
            array (
                'id' => 128,
                'sortname' => 'MO',
                'name' => 'Macau S.A.R.',
                'subregion_id' => 1,
            ),
            128 => 
            array (
                'id' => 129,
                'sortname' => 'MK',
                'name' => 'Macedonia',
                'subregion_id' => 1,
            ),
            129 => 
            array (
                'id' => 130,
                'sortname' => 'MG',
                'name' => 'Madagascar',
                'subregion_id' => 1,
            ),
            130 => 
            array (
                'id' => 131,
                'sortname' => 'MW',
                'name' => 'Malawi',
                'subregion_id' => 1,
            ),
            131 => 
            array (
                'id' => 132,
                'sortname' => 'MY',
                'name' => 'Malaysia',
                'subregion_id' => 1,
            ),
            132 => 
            array (
                'id' => 133,
                'sortname' => 'MV',
                'name' => 'Maldives',
                'subregion_id' => 1,
            ),
            133 => 
            array (
                'id' => 134,
                'sortname' => 'ML',
                'name' => 'Mali',
                'subregion_id' => 1,
            ),
            134 => 
            array (
                'id' => 135,
                'sortname' => 'MT',
                'name' => 'Malta',
                'subregion_id' => 1,
            ),
            135 => 
            array (
                'id' => 136,
                'sortname' => 'XM',
            'name' => 'Man (Isle of)',
                'subregion_id' => 1,
            ),
            136 => 
            array (
                'id' => 137,
                'sortname' => 'MH',
                'name' => 'Marshall Islands',
                'subregion_id' => 1,
            ),
            137 => 
            array (
                'id' => 138,
                'sortname' => 'MQ',
                'name' => 'Martinique',
                'subregion_id' => 4,
            ),
            138 => 
            array (
                'id' => 139,
                'sortname' => 'MR',
                'name' => 'Mauritania',
                'subregion_id' => 1,
            ),
            139 => 
            array (
                'id' => 140,
                'sortname' => 'MU',
                'name' => 'Mauritius',
                'subregion_id' => 1,
            ),
            140 => 
            array (
                'id' => 141,
                'sortname' => 'YT',
                'name' => 'Mayotte',
                'subregion_id' => 1,
            ),
            141 => 
            array (
                'id' => 142,
                'sortname' => 'MX',
                'name' => 'Mexico',
                'subregion_id' => 2,
            ),
            142 => 
            array (
                'id' => 143,
                'sortname' => 'FM',
                'name' => 'Micronesia',
                'subregion_id' => 1,
            ),
            143 => 
            array (
                'id' => 144,
                'sortname' => 'MD',
                'name' => 'Moldova',
                'subregion_id' => 1,
            ),
            144 => 
            array (
                'id' => 145,
                'sortname' => 'MC',
                'name' => 'Monaco',
                'subregion_id' => 1,
            ),
            145 => 
            array (
                'id' => 146,
                'sortname' => 'MN',
                'name' => 'Mongolia',
                'subregion_id' => 1,
            ),
            146 => 
            array (
                'id' => 147,
                'sortname' => 'MS',
                'name' => 'Montserrat',
                'subregion_id' => 4,
            ),
            147 => 
            array (
                'id' => 148,
                'sortname' => 'MA',
                'name' => 'Morocco',
                'subregion_id' => 1,
            ),
            148 => 
            array (
                'id' => 149,
                'sortname' => 'MZ',
                'name' => 'Mozambique',
                'subregion_id' => 1,
            ),
            149 => 
            array (
                'id' => 150,
                'sortname' => 'MM',
                'name' => 'Myanmar',
                'subregion_id' => 1,
            ),
            150 => 
            array (
                'id' => 151,
                'sortname' => 'NA',
                'name' => 'Namibia',
                'subregion_id' => 5,
            ),
            151 => 
            array (
                'id' => 152,
                'sortname' => 'NR',
                'name' => 'Nauru',
                'subregion_id' => 1,
            ),
            152 => 
            array (
                'id' => 153,
                'sortname' => 'NP',
                'name' => 'Nepal',
                'subregion_id' => 1,
            ),
            153 => 
            array (
                'id' => 154,
                'sortname' => 'AN',
                'name' => 'Netherlands Antilles',
                'subregion_id' => 1,
            ),
            154 => 
            array (
                'id' => 155,
                'sortname' => 'NL',
                'name' => 'Netherlands The',
                'subregion_id' => 1,
            ),
            155 => 
            array (
                'id' => 156,
                'sortname' => 'NC',
                'name' => 'New Caledonia',
                'subregion_id' => 1,
            ),
            156 => 
            array (
                'id' => 157,
                'sortname' => 'NZ',
                'name' => 'New Zealand',
                'subregion_id' => 1,
            ),
            157 => 
            array (
                'id' => 158,
                'sortname' => 'NI',
                'name' => 'Nicaragua',
                'subregion_id' => 2,
            ),
            158 => 
            array (
                'id' => 159,
                'sortname' => 'NE',
                'name' => 'Niger',
                'subregion_id' => 1,
            ),
            159 => 
            array (
                'id' => 160,
                'sortname' => 'NG',
                'name' => 'Nigeria',
                'subregion_id' => 1,
            ),
            160 => 
            array (
                'id' => 161,
                'sortname' => 'NU',
                'name' => 'Niue',
                'subregion_id' => 1,
            ),
            161 => 
            array (
                'id' => 162,
                'sortname' => 'NF',
                'name' => 'Norfolk Island',
                'subregion_id' => 1,
            ),
            162 => 
            array (
                'id' => 163,
                'sortname' => 'MP',
                'name' => 'Northern Mariana Islands',
                'subregion_id' => 1,
            ),
            163 => 
            array (
                'id' => 164,
                'sortname' => 'NO',
                'name' => 'Norway',
                'subregion_id' => 1,
            ),
            164 => 
            array (
                'id' => 165,
                'sortname' => 'OM',
                'name' => 'Oman',
                'subregion_id' => 1,
            ),
            165 => 
            array (
                'id' => 166,
                'sortname' => 'PK',
                'name' => 'Pakistan',
                'subregion_id' => 1,
            ),
            166 => 
            array (
                'id' => 167,
                'sortname' => 'PW',
                'name' => 'Palau',
                'subregion_id' => 1,
            ),
            167 => 
            array (
                'id' => 168,
                'sortname' => 'PS',
                'name' => 'Palestinian Territory Occupied',
                'subregion_id' => 1,
            ),
            168 => 
            array (
                'id' => 169,
                'sortname' => 'PA',
                'name' => 'Panama',
                'subregion_id' => 2,
            ),
            169 => 
            array (
                'id' => 170,
                'sortname' => 'PG',
                'name' => 'Papua new Guinea',
                'subregion_id' => 1,
            ),
            170 => 
            array (
                'id' => 171,
                'sortname' => 'PY',
                'name' => 'Paraguay',
                'subregion_id' => 3,
            ),
            171 => 
            array (
                'id' => 172,
                'sortname' => 'PE',
                'name' => 'Peru',
                'subregion_id' => 3,
            ),
            172 => 
            array (
                'id' => 173,
                'sortname' => 'PH',
                'name' => 'Philippines',
                'subregion_id' => 1,
            ),
            173 => 
            array (
                'id' => 174,
                'sortname' => 'PN',
                'name' => 'Pitcairn Island',
                'subregion_id' => 1,
            ),
            174 => 
            array (
                'id' => 175,
                'sortname' => 'PL',
                'name' => 'Poland',
                'subregion_id' => 1,
            ),
            175 => 
            array (
                'id' => 176,
                'sortname' => 'PT',
                'name' => 'Portugal',
                'subregion_id' => 1,
            ),
            176 => 
            array (
                'id' => 177,
                'sortname' => 'PR',
                'name' => 'Puerto Rico',
                'subregion_id' => 4,
            ),
            177 => 
            array (
                'id' => 178,
                'sortname' => 'QA',
                'name' => 'Qatar',
                'subregion_id' => 1,
            ),
            178 => 
            array (
                'id' => 179,
                'sortname' => 'RE',
                'name' => 'Reunion',
                'subregion_id' => 1,
            ),
            179 => 
            array (
                'id' => 180,
                'sortname' => 'RO',
                'name' => 'Romania',
                'subregion_id' => 1,
            ),
            180 => 
            array (
                'id' => 181,
                'sortname' => 'RU',
                'name' => 'Russia',
                'subregion_id' => 1,
            ),
            181 => 
            array (
                'id' => 182,
                'sortname' => 'RW',
                'name' => 'Rwanda',
                'subregion_id' => 1,
            ),
            182 => 
            array (
                'id' => 183,
                'sortname' => 'SH',
                'name' => 'Saint Helena',
                'subregion_id' => 1,
            ),
            183 => 
            array (
                'id' => 184,
                'sortname' => 'KN',
                'name' => 'Saint Kitts And Nevis',
                'subregion_id' => 1,
            ),
            184 => 
            array (
                'id' => 185,
                'sortname' => 'LC',
                'name' => 'Saint Lucia',
                'subregion_id' => 1,
            ),
            185 => 
            array (
                'id' => 186,
                'sortname' => 'PM',
                'name' => 'Saint Pierre and Miquelon',
                'subregion_id' => 1,
            ),
            186 => 
            array (
                'id' => 187,
                'sortname' => 'VC',
                'name' => 'Saint Vincent And The Grenadines',
                'subregion_id' => 1,
            ),
            187 => 
            array (
                'id' => 188,
                'sortname' => 'WS',
                'name' => 'Samoa',
                'subregion_id' => 1,
            ),
            188 => 
            array (
                'id' => 189,
                'sortname' => 'SM',
                'name' => 'San Marino',
                'subregion_id' => 1,
            ),
            189 => 
            array (
                'id' => 190,
                'sortname' => 'ST',
                'name' => 'Sao Tome and Principe',
                'subregion_id' => 6,
            ),
            190 => 
            array (
                'id' => 191,
                'sortname' => 'SA',
                'name' => 'Saudi Arabia',
                'subregion_id' => 1,
            ),
            191 => 
            array (
                'id' => 192,
                'sortname' => 'SN',
                'name' => 'Senegal',
                'subregion_id' => 1,
            ),
            192 => 
            array (
                'id' => 193,
                'sortname' => 'RS',
                'name' => 'Serbia',
                'subregion_id' => 1,
            ),
            193 => 
            array (
                'id' => 194,
                'sortname' => 'SC',
                'name' => 'Seychelles',
                'subregion_id' => 1,
            ),
            194 => 
            array (
                'id' => 195,
                'sortname' => 'SL',
                'name' => 'Sierra Leone',
                'subregion_id' => 1,
            ),
            195 => 
            array (
                'id' => 196,
                'sortname' => 'SG',
                'name' => 'Singapore',
                'subregion_id' => 1,
            ),
            196 => 
            array (
                'id' => 197,
                'sortname' => 'SK',
                'name' => 'Slovakia',
                'subregion_id' => 1,
            ),
            197 => 
            array (
                'id' => 198,
                'sortname' => 'SI',
                'name' => 'Slovenia',
                'subregion_id' => 1,
            ),
            198 => 
            array (
                'id' => 199,
                'sortname' => 'XG',
                'name' => 'Smaller Territories of the UK',
                'subregion_id' => 1,
            ),
            199 => 
            array (
                'id' => 200,
                'sortname' => 'SB',
                'name' => 'Solomon Islands',
                'subregion_id' => 1,
            ),
            200 => 
            array (
                'id' => 201,
                'sortname' => 'SO',
                'name' => 'Somalia',
                'subregion_id' => 1,
            ),
            201 => 
            array (
                'id' => 202,
                'sortname' => 'ZA',
                'name' => 'South Africa',
                'subregion_id' => 5,
            ),
            202 => 
            array (
                'id' => 203,
                'sortname' => 'GS',
                'name' => 'South Georgia',
                'subregion_id' => 1,
            ),
            203 => 
            array (
                'id' => 204,
                'sortname' => 'SS',
                'name' => 'South Sudan',
                'subregion_id' => 1,
            ),
            204 => 
            array (
                'id' => 205,
                'sortname' => 'ES',
                'name' => 'Spain',
                'subregion_id' => 1,
            ),
            205 => 
            array (
                'id' => 206,
                'sortname' => 'LK',
                'name' => 'Sri Lanka',
                'subregion_id' => 1,
            ),
            206 => 
            array (
                'id' => 207,
                'sortname' => 'SD',
                'name' => 'Sudan',
                'subregion_id' => 1,
            ),
            207 => 
            array (
                'id' => 208,
                'sortname' => 'SR',
                'name' => 'Suriname',
                'subregion_id' => 3,
            ),
            208 => 
            array (
                'id' => 209,
                'sortname' => 'SJ',
                'name' => 'Svalbard And Jan Mayen Islands',
                'subregion_id' => 1,
            ),
            209 => 
            array (
                'id' => 210,
                'sortname' => 'SZ',
                'name' => 'Swaziland',
                'subregion_id' => 5,
            ),
            210 => 
            array (
                'id' => 211,
                'sortname' => 'SE',
                'name' => 'Sweden',
                'subregion_id' => 1,
            ),
            211 => 
            array (
                'id' => 212,
                'sortname' => 'CH',
                'name' => 'Switzerland',
                'subregion_id' => 1,
            ),
            212 => 
            array (
                'id' => 213,
                'sortname' => 'SY',
                'name' => 'Syria',
                'subregion_id' => 1,
            ),
            213 => 
            array (
                'id' => 214,
                'sortname' => 'TW',
                'name' => 'Taiwan',
                'subregion_id' => 1,
            ),
            214 => 
            array (
                'id' => 215,
                'sortname' => 'TJ',
                'name' => 'Tajikistan',
                'subregion_id' => 1,
            ),
            215 => 
            array (
                'id' => 216,
                'sortname' => 'TZ',
                'name' => 'Tanzania',
                'subregion_id' => 1,
            ),
            216 => 
            array (
                'id' => 217,
                'sortname' => 'TH',
                'name' => 'Thailand',
                'subregion_id' => 1,
            ),
            217 => 
            array (
                'id' => 218,
                'sortname' => 'TG',
                'name' => 'Togo',
                'subregion_id' => 1,
            ),
            218 => 
            array (
                'id' => 219,
                'sortname' => 'TK',
                'name' => 'Tokelau',
                'subregion_id' => 1,
            ),
            219 => 
            array (
                'id' => 220,
                'sortname' => 'TO',
                'name' => 'Tonga',
                'subregion_id' => 1,
            ),
            220 => 
            array (
                'id' => 221,
                'sortname' => 'TT',
                'name' => 'Trinidad And Tobago',
                'subregion_id' => 4,
            ),
            221 => 
            array (
                'id' => 222,
                'sortname' => 'TN',
                'name' => 'Tunisia',
                'subregion_id' => 1,
            ),
            222 => 
            array (
                'id' => 223,
                'sortname' => 'TR',
                'name' => 'Turkey',
                'subregion_id' => 1,
            ),
            223 => 
            array (
                'id' => 224,
                'sortname' => 'TM',
                'name' => 'Turkmenistan',
                'subregion_id' => 1,
            ),
            224 => 
            array (
                'id' => 225,
                'sortname' => 'TC',
                'name' => 'Turks And Caicos Islands',
                'subregion_id' => 1,
            ),
            225 => 
            array (
                'id' => 226,
                'sortname' => 'TV',
                'name' => 'Tuvalu',
                'subregion_id' => 1,
            ),
            226 => 
            array (
                'id' => 227,
                'sortname' => 'UG',
                'name' => 'Uganda',
                'subregion_id' => 1,
            ),
            227 => 
            array (
                'id' => 228,
                'sortname' => 'UA',
                'name' => 'Ukraine',
                'subregion_id' => 1,
            ),
            228 => 
            array (
                'id' => 229,
                'sortname' => 'AE',
                'name' => 'United Arab Emirates',
                'subregion_id' => 1,
            ),
            229 => 
            array (
                'id' => 230,
                'sortname' => 'GB',
                'name' => 'United Kingdom',
                'subregion_id' => 1,
            ),
            230 => 
            array (
                'id' => 231,
                'sortname' => 'US',
                'name' => 'United States',
                'subregion_id' => 1,
            ),
            231 => 
            array (
                'id' => 232,
                'sortname' => 'UM',
                'name' => 'United States Minor Outlying Islands',
                'subregion_id' => 1,
            ),
            232 => 
            array (
                'id' => 233,
                'sortname' => 'UY',
                'name' => 'Uruguay',
                'subregion_id' => 3,
            ),
            233 => 
            array (
                'id' => 234,
                'sortname' => 'UZ',
                'name' => 'Uzbekistan',
                'subregion_id' => 1,
            ),
            234 => 
            array (
                'id' => 235,
                'sortname' => 'VU',
                'name' => 'Vanuatu',
                'subregion_id' => 1,
            ),
            235 => 
            array (
                'id' => 236,
                'sortname' => 'VA',
            'name' => 'Vatican City State (Holy See)',
                'subregion_id' => 1,
            ),
            236 => 
            array (
                'id' => 237,
                'sortname' => 'VE',
                'name' => 'Venezuela',
                'subregion_id' => 3,
            ),
            237 => 
            array (
                'id' => 238,
                'sortname' => 'VN',
                'name' => 'Vietnam',
                'subregion_id' => 1,
            ),
            238 => 
            array (
                'id' => 239,
                'sortname' => 'VG',
            'name' => 'Virgin Islands (British)',
                'subregion_id' => 1,
            ),
            239 => 
            array (
                'id' => 240,
                'sortname' => 'VI',
            'name' => 'Virgin Islands (US)',
                'subregion_id' => 1,
            ),
            240 => 
            array (
                'id' => 241,
                'sortname' => 'WF',
                'name' => 'Wallis And Futuna Islands',
                'subregion_id' => 1,
            ),
            241 => 
            array (
                'id' => 242,
                'sortname' => 'EH',
                'name' => 'Western Sahara',
                'subregion_id' => 1,
            ),
            242 => 
            array (
                'id' => 243,
                'sortname' => 'YE',
                'name' => 'Yemen',
                'subregion_id' => 1,
            ),
            243 => 
            array (
                'id' => 244,
                'sortname' => 'YU',
                'name' => 'Yugoslavia',
                'subregion_id' => 1,
            ),
            244 => 
            array (
                'id' => 245,
                'sortname' => 'ZM',
                'name' => 'Zambia',
                'subregion_id' => 1,
            ),
            245 => 
            array (
                'id' => 246,
                'sortname' => 'ZW',
                'name' => 'Zimbabwe',
                'subregion_id' => 1,
            ),
        ));
        
        
    }
}
