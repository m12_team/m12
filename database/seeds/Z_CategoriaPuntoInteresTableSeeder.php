<?php

use Illuminate\Database\Seeder;
use App\CategoriaPuntoInteres;

class Z_CategoriaPuntoInteresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categoriaPI =[
			
			[
				'nombre'  => 'PokeParadas',
				'esProfesional'=>0
			],
			[
				'nombre'  => 'Gimnasios',
				'esProfesional'=>0
			],
			[
				'nombre'  => 'Pokemon',
				'esProfesional'=>0
			],
		];
		
		DB::table('categoria_puntointeres')->delete();
		foreach ($categoriaPI as $categoria){
			CategoriaPuntoInteres::create($categoria);
		}
    }
}