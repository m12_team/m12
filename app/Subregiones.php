<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Regiones;

class Subregiones extends Model
{
    protected $table="subregion";

    public function subregiones()
    {
        return $this->hasMany('App\Country');
    }

     public function region()
    {
    return $this->belongsTo('App\Regiones');

	   }
 	
  }
