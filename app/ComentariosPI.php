<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComentariosPI extends Model
{
    protected $table="comentarios";

    public function punto_interes()
    {
    return $this->belongsTo('App\PuntoInteres');
    }

    public function creador()
    {
    return $this->belongsTo('App\User');
    }
}
