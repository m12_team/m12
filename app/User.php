<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use App\City;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

     public function ciudad_natal()
    {
    return $this->belongsTo('App\City', 'ciudad_natal_id');
    }

     public function ciudad_actual()
    {
    return $this->belongsTo('App\City', 'ciudad_actual_id');
    }

    public function role()
    {
    return $this->hasOne('App\Role', 'id', 'role_id');
    }

     public function puntoInteres() {
        return $this->hasMany('App\PuntoInteres','creador_id');
    }

    public function preguntas() {
        return $this->hasMany('App\Pregunta','creador_id');
    }

    public function respuesta() {
        return $this->hasMany('App\Respuesta','creador_id');
    }

     public function rutas() {
        return $this->hasMany('App\Ruta','creador_id');
    }

     public function diarios() {
        return $this->hasMany('App\Diario','creador_id');
    }
	
	public function anuncios() {
        return $this->hasMany('App\Anuncio','creador_id');
    }


    public function emisor()
    {
        return $this->belongsToMany('App\EmisorReceptor','emisor_receptor_message');
    }

    public function receptor()
    {
        return $this->belongsToMany('App\EmisorReceptor','emisor_receptor_message', 'receptor_id' ,  'emisor_id' );
    }


    public function ofrece()
    {
        return $this->hasMany('App\PokemonIntercambio','emisor_id');
    }
    
  
    
    public function comentarios() {
        return $this->hasMany('App\ComentariosPI','creador_id');
    }

    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    
}
