<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Respuesta extends Model
{
    protected $table="respuesta";

     public function creador()
    {
    return $this->belongsTo('App\User', 'creador_id');
    }

}
