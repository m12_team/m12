<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\City;
use App\Country;
class State extends Model
{
    protected $table="states";

    public function cities()
    {
        return $this->hasMany('App\City');
    }

    public function country()
	{
    return $this->belongsTo('App\Country', 'country_id');
	}
}
