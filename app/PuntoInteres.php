<?php

namespace App;
use App\FotosPI;
use Illuminate\Database\Eloquent\Model;
use App\InformacionExtra;
class PuntoInteres extends Model
{
    protected $table="punto_interes";


     public function categoria()
    {
    return $this->belongsTo('App\CategoriaPuntoInteres', 'categoria_pi_id');
    }

    public function entorno()
    {
    return $this->belongsTo('App\Entorno', 'entorno_id');
    }
	
	public function anuncios() {
        return $this->hasMany('App\Anuncio','punto_interes_id');
    }

    public function transporte()
    {
    return $this->belongsTo('App\Transporte', 'transporte_id');
    }

    public function users()
    {
    return $this->belongsTo('App\User', 'creador_id');
    }

    public function comentarios() {
        return $this->hasMany('App\ComentariosPI','punto_interes_id');
    }
    public function fotos() {
        return $this->hasMany('App\FotosPI','punto_interes_id');
    }
	
	 public function puntosInteres() {
        return $this->hasMany('App\InformacionExtra','pi_id');
    }

   
}

  


