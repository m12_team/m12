<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Anuncio extends Model
{
    protected $table="anuncio";

	 public function creador()
    {
    return $this->belongsTo('App\User', 'creador_id');
    }
	
	 public function puntoInt()
    {
    return $this->belongsTo('App\PuntoInteres', 'punto_interes_id');
    }
}
