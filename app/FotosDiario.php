<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FotosDiario extends Model
{
    protected $table="fotos_diario";

    public function creador()
    {
        return $this->hasOne('App\User');
    }

    public function diario() {
    return $this->belongsTo('App\Diario');
    }
}
