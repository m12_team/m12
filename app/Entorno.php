<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entorno extends Model
{
    protected $table="entorno";

    public function pi()
    {
        return $this->hasMany('App\PuntoInteres');
    }
}
