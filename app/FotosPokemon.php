<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FotosPokemon extends Model
{
    protected $table="fotos_pokemon";

     public function pokemon()
	{
    return $this->belongsTo('App\Diario', 'diario_id');
	}

}
