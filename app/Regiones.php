<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Regiones extends Model
{
    protected $table="region";

    public function subregiones()
    {
        return $this->hasMany('App\Subregiones');
    }

}
