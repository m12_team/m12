<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Ruta extends Model
{
	protected $table="ruta";

	 public function creador()
    {
    return $this->belongsTo('App\User', 'creador_id');
    }

      public function puntointeres()
    {
		return $this->belongsToMany('App\PuntoInteres', 'pi_ruta' ,  'ruta_id' , 'punto_interes_id')->withPivot('id');
    }

       public function pokemon()
    {
		return $this->belongsToMany('App\Pokemon', 'pokemon_ruta' ,  'pokemon_id' , 'ruta_id')->withPivot('id');
    }



}
