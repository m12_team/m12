<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\PuntoInteres;

class CategoriaPuntoInteres extends Model
{
    protected $table="categoria_puntointeres";

    public function pi()
    {
        return $this->hasMany('App\PuntoInteres','id');
    }
}
