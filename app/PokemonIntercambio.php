<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PokemonIntercambio extends Model
{
    protected $table="pokemon_intercambio";

    public function usuario()
    {
        return $this->hasOne('App\User','id','emisor_id');
    }

    public function pokemon()
    {
        return $this->hasOne('App\Pokemon','id','pokemon_id');
    }

     public function cambiar()
    {
        return $this->hasOne('App\Pokemon','id','pokemon_que_cambia');
    }

}
