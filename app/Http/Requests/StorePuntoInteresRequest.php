<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Http\Requests\UserRequest;

class StorePuntoInteresRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       return [
			'nombre' => 'required | min:3 | max:20 | unique:punto_interes,nombre',
            'precio'=> 'required',
            'descripcion'=>'required',
            'latitude'=>'required',
            'longitude'=>'required',
            'categoria'=>'required',
            'entorno'=>'required',
            'transporte'=>'required',
			'photo' => 'required|array'

      ];
    }
}
