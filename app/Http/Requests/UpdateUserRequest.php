<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Http\Requests\UserRequest;

class UpdateUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       return [
			    'nombre' => 'required | min:3 | max:20 | regex:/^[a-zA-Z ]+$/',
      		'primer_apellido'=> 'required | min:3 | max:20 | regex:/^[a-zA-Z ]+$/',
      		'segundo_apellido'=> 'required | min:3 | max:20 | regex:/^[a-zA-Z ]+$/',
      		'email'=> 'required | min:3',
          'foto'=> 'mimes:jpg,png',
          'calendar' => 'required ',
      		'descripcion'=> 'required',
      		'username'=>'required | min:3 | max:20',
      		'cities'=>'required | numeric',
      		'cities_actual'=>'required | numeric',
      		'date_to'=> 'date_format:m/d/Y|after:tomorrow'
		];
    }
}
