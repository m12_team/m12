<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Http\Requests\UserRequest;

class SubregionesRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       return [
			'nombre' => 'required | min:3 | max:20 | unique:subregion,nombre',
            'regiones' => 'required'
      ];
    }
}
