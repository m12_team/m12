<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Http\Requests\UserRequest;

class UserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       return [
			    'nombre' => 'required | min:3 | max:20',
      		'primer_apellido'=> 'required | min:3 | max:20',
      		'segundo_apellido'=> 'required | min:3 | max:20',
      		'email'=> 'required | email | unique:users,email',
          'foto'=> 'required',
          'calendar' => 'required ',
          'descripcion'=> 'required',
          'cities'=>'required | numeric',
          'cities_actual'=>'required | numeric',
          'username'=>'required | min:3 | max:20 | unique:users,username',
      		'password'=> 'required |min:3 | max:20',
      		'confirm_password'=> 'required | max:20'
		];
    }
}
