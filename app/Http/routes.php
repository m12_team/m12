<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use App\State;
use App\Country;
use JasperPHP\JasperPHP as JasperPHP;

Route::group(['middleware' => ['web']], function () {
    
    Route::get('profile', [
        'middleware' => 'auth',
        'uses' => 'FrontEndController@profile'
    ]);

	/****
    
    Menú de navegación

    ***/
    Route::get('/buscar-pokeparadas', 'PuntoInteresController@getBuscarPuntoInteres');  
    Route::get('/buscar-gym', 'PuntoInteresController@getBuscarGYM');  
    Route::get('/buscar-pokemon', 'PokemonController@getBuscarPokemon');     
    Route::get('/buscar-ruta', 'RutaController@getBuscarRuta'); 
    Route::get('/buscar-usuarios', 'UsuarioController@getBuscarUsuarios');  
    Route::get('/preguntas-frequentes', 'FrontEndController@test');    

	//Route::get('/', 'FrontEndController@index');    
    Route::get('/', 'FrontEndController@test');    
    Route::get('/listados', 'FrontEndController@listado');  
    Route::get('/rutas', 'FrontEndController@listado2');    
    Route::get('/tour', 'FrontEndController@tour'); 
    Route::get('/vertour', 'FrontEndController@vertour');   
    Route::get('/quevisitar', 'FrontEndController@quevisitar'); 
    Route::get('/foro', 'FrontEndController@getForo'); 
    Route::get('pdf/{idPuntoInteres}', 'PdfController@invoice');
    Route::get('consultar-pais/{idPais}', 'FrontEndController@consultarPais');
    Route::get('consultar-state/{idState}', 'FrontEndController@consultarState');

    /****Foro ******/
    Route::get('piCategoria/{idCategoria}', 'ForoController@getPIporCategoria');
    Route::get('postsPorPuntoInteres/{idPuntoInteres}', 'ForoController@getPostPI');
    Route::get('respuesta/{idPregunta}', 'ForoController@getRespuesta');
    Route::post('crearPregunta', 'ForoController@crearPregunta');
    Route::post('crearRespuesta', 'ForoController@crearRespuesta');
    Route::get('valorarRespuesta/{idPregunta}', 'ForoController@valorarRespuesta');
	Route::get('reducirRespuesta/{idRespuesta}','ForoController@reducirValoracion');
    //Route::get('/profile', 'FrontEndController@profile');   
    Route::get('/detalle/{idPuntoInteres}', 'FrontEndController@detalle');   
    Route::get('/update', 'FrontEndController@postUpdate');   
    Route::post('/login', 'UsuarioController@postLogin'); 
    Route::post('/cambiarPassword', 'UsuarioController@postCambiarPassword'); 
    Route::get('/iniciar-sesion', 'UsuarioController@getIniciarSesion');   
    Route::get('/registro-de-usuario', 'UsuarioController@getRegistroDeUsuario');   
    Route::get('/cambiarPassword', 'UsuarioController@getCambiarPassword');   
    Route::post('/registro-de-usuario', 'UsuarioController@postRegistroDeUsuario');     
    Route::get('/logout', 'UsuarioController@getLogout');   
    Route::get('/recuperar-contraseña', 'UsuarioController@getRecuperarContrasena');   
    Route::post('/recuperar-contraseña', 'UsuarioController@postRecuperarContrasena'); 
    Route::post('modificar-datos','UsuarioController@postModificarDatos');
    Route::get('/modificar-ruta/{idRuta}', 'RutaController@editar');   
    Route::get('/enviarMensaje/{idReceptor}', 'UsuarioController@enviarMensaje');   
    Route::post('/enviarMensaje', 'UsuarioController@postEnviarMensaje');   
    Route::get('/darDebajaUsuario/{idUsuario}','UsuarioController@darDeBaja');
    Route::get('/comprobarEmail', 'UsuarioController@comprobarEmail');   
    Route::get('/comprobarUsername', 'UsuarioController@comprobarUsername');   

    /***Perfil de usuario****/
    Route::get('/eliminar-punto-interes/{idPuntoInteres}', 'PuntoInteresController@delete');  
    Route::get('/eliminar-ruta/{idRuta}', 'RutaController@delete');   
	Route::get('/eliminarFotosPuntoInteres/{idPuntoInteres}', 'PuntoInteresController@eliminarFotos');   
    Route::post('enviar-comentarios','PuntoInteresController@publicarComentarios');
    Route::post('enviar-comentarios-pokemon','PokemonController@publicarComentarios');
    Route::post('subir-fotos-pi','PuntoInteresController@subirFotos');
    Route::post('subir-fotos-diario','DiarioController@subirFotos');
    Route::post('subir-videos-diario','DiarioController@subirVideos');
    Route::post('crear-diario','DiarioController@crearDiario');
    Route::get('eliminar-diario/{idDiario}','DiarioController@eliminarDiario');
    Route::get('editar-diario/{idDiario}','AjaxController@obtenerDiario');
    Route::get('modificar-diario/{idDiario}','AjaxController@modificarDiario');
    Route::get('obtener-videos-diario/{idDiario}','AjaxController@obtenerVideoDiario');
    Route::get('obtener-puntosdeinteres-diario/{idDiario}','AjaxController@obtenerPuntoDeInteresDiario');
    Route::get('obtener-fotos-diario/{idDiario}','AjaxController@obtenerFotosDiario');
    Route::get('eliminar-video-diario/{idDiario}','DiarioController@eliminarVideoDiario');
    Route::get('eliminar-fotos-diario/{idDiario}','DiarioController@eliminarFotoDiario');
    Route::get('obtener-informacionextra-diario/{idDiario}','AjaxController@obtenerInformacionExtra');
	Route::get('registro-de-usuario-profesional','UsuarioController@getRegistroDeUsuarioProfesional');
	Route::post('registro-profesional','UsuarioController@postRegistroUsuarioProfesional');
	Route::post('addAnuncio','AnuncioController@postCreateAnuncio');

    /*** Peticiones Ajax****/
    Route::get('obtenerPIDeUnaRuta/{idRuta}', 'AjaxController@getPuntoDeInteresDeUnaRuta'); 
    Route::get('obtenerPaises', 'AjaxController@getPaises'); 
    Route::get('obtenerStates/{idPais}', 'AjaxController@getStates'); 
    Route::get('obtenerPIPorCategoria/{idCategoria}', 'AjaxController@getPuntosDeInteresPorCategoria');  
    Route::get('obtenerSubregion', 'AjaxController@getSubregion');     
    Route::get('obtenerRegiones', 'AjaxController@getRegiones');    
    Route::get('obtenerCities/{idState}', 'AjaxController@getCiudades');    
    Route::get('ordenarUsuariosPorNombre/{nombre}/{order}', 'AjaxController@getCiudades');    
    Route::get('obtenerPI/{idPuntoInteres}', 'AjaxController@getPuntoInteres'); 
    Route::get('obtenerTodasFotosPI/{idPuntoInteres}', 'AjaxController@obtenerTodasFotosPI'); 
    Route::get('obtenerRutas','AjaxController@getRutas');
    Route::post('/addPuntoInteres', 'PuntoInteresController@crearPuntoInteres');     
    Route::post('/addRuta', 'RutaController@crearRuta');     
    Route::get('obtenerPuntosInteres/{idPuntoInteres}','AjaxController@obtenerPuntosInteres');
    Route::get('obtenerTodosPuntosDeInteres','AjaxController@obtenerTodosPuntosInteres');
    Route::get('obtenerTodasRutas','AjaxController@obtenerTodasRutas');
    Route::get('modificarPI', 'AjaxController@modificarPuntoInteres'); 
    Route::get('obtenerRuta/{idRuta}','AjaxController@obtenerRuta');
    Route::get('eliminar-pi-ruta/{idRuta}','AjaxController@eliminarPIRuta');
    Route::get('add-pi-ruta/{idPuntoInteres}/{idRuta}','AjaxController@addPItoRuta');

    /***Consultar***/
    Route::get('/consultarPI/{idPuntoInteres}', 'PuntoInteresController@getConsultarPuntoInteres');  
    Route::get('/consultarRutas/{idRuta}', 'RutaController@getRuta');  
    Route::get('/consultarUsuario/{idUsuario}', 'UsuarioController@getConsultaDeUsuario');  
    Route::get('/consultarDiario/{idDiario}', 'DiarioController@getConsultarDiario');  
    Route::get('/consultarPokemon/{idPokemon}', 'PokemonController@getConsultarPokemon');  
    Route::get('/obtenerTipos','AjaxController@getObtenerTodosTipos');
    /***Buscar****/
    Route::get('/busqueda', 'UsuarioController@postBuscarUsuarios'); 
	Route::get('/consultarPokeParada/{lat}/{long}','PuntoInteresController@getPokeParadasCercanas');
	Route::get('/busquedaPuntoInteres','PuntoInteresController@buscarPuntoInteres');
    Route::get('/busquedaPokemon','PokemonController@buscarPokemon');
	Route::get('/buscar-diario', 'DiarioController@getDiario');  
    Route::get('/busquedaRuta', 'RutaController@buscarRutas'); 
    Route::get('obtenerCategorias', 'AjaxController@getCategorias'); 
    Route::get('obtenerEntornos', 'AjaxController@getEntornos'); 
    Route::get('obtenerTransportes', 'AjaxController@getTransportes');  
	Route::get('/busquedaDiario', 'DiarioController@getBuscarDiario'); 
    Route::get('getPIdeUnaRuta/{idRuta}','AjaxController@getPIdeUnaRuta');
    Route::get('obtenerPuntosDeInteresDeUnaRuta/{idRuta}','AjaxController@obtenerPuntosDeInteresDeUnaRuta');
    Route::get('eliminar-pi-ruta/{idPuntoInteres}/{idRuta}','AjaxController@eliminarPIdeRuta');

    /**Contar usuarios***/
    Route::get('contarUsuariosTotal','AjaxController@contarUsuariosTotal');
    
    /****Backend*****/
	Route::group(['middleware' => ['admin']], function () {
		Route::resource('mantenimiento_usuarios', 'MantenimientoUsuariosController');
        Route::get('mantenimiento_usuarios/{idUsuario}/eliminar','MantenimientoUsuariosController@destroy');
		Route::get('/main_admin_page', 'BackendController@getMainAdminBackend');    
		Route::resource('mantenimiento_categoria_pi', 'MantenimientoCategoriasController');
		Route::resource('mantenimiento_entorno', 'MantenimientoEntornoController');
		Route::resource('mantenimiento_paises', 'MantenimientoPaisesController');
		Route::resource('mantenimiento_estados', 'MantenimientoEstadosController');
		Route::resource('mantenimiento_ciudades', 'MantenimientoCiudadesController');
		Route::resource('mantenimiento_transporte', 'MantenimientoTransporteController');
		Route::resource('mantenimiento_roles', 'MantenimientoRoleController');
		Route::resource('mantenimiento_regiones', 'MantenimientoRegionesController');
		Route::resource('mantenimiento_subregiones', 'MantenimientoSubregionesController');
		Route::resource('mantenimiento_paises', 'MantenimientoPaisesController');
		Route::resource('mantenimiento_estados', 'MantenimientoEstadosController');
		Route::resource('mantenimiento_pi', 'MantenimientoPuntoInteresController');
		Route::resource('mantenimiento_rutas', 'MantenimientoRutasController');
		Route::resource('mantenimiento_diarios', 'MantenimientoDiariosController');
		Route::resource('mantenimiento_comentarios', 'MantenimientoComentariosController');
		Route::resource('mantenimiento_videos', 'MantenimientoVideosController');
		Route::get('/asignar-usuario/{idRol}', 'MantenimientoRoleController@getAsignarUsuarios');    
		Route::get('mantenimiento_roles/{idUser}/{idRol}/cambiarRole', 'MantenimientoRoleController@getConfirmarCambioRole');
		Route::get('/mantenimiento_roles_eliminar/{idRol}', 'MantenimientoRoleController@destroy');    
		Route::get('/mantenimiento_categoria_pi_eliminar/{idCategoria}', 'MantenimientoCategoriasController@destroy');  
		Route::get('/mantenimiento_entorno_eliminar/{idEntorno}', 'MantenimientoEntornoController@destroy');   
		Route::get('/mantenimiento_comentarios_eliminar/{idComentario}', 'MantenimientoComentariosController@destroy');    	
		Route::get('/mantenimiento_transporte_eliminar/{idTransporte}', 'MantenimientoTransporteController@destroy');    
		Route::get('mantenimientos_regiones_eliminar/{idRegiones}', 'MantenimientoRegionesController@destroy');    
		Route::get('mantenimiento_subregiones_eliminar/{idSubregion}', 'MantenimientoSubregionesController@destroy');    
		Route::get('mantenimiento_paises_eliminar/{idPais}', 'MantenimientoPaisesController@destroy');
		Route::get('mantenimiento_estados_eliminar/{idEstado}', 'MantenimientoEstadosController@destroy');   
		Route::get('mantenimiento_pi_eliminar/{idPI}', 'MantenimientoPuntoInteresController@destroy');        	
		Route::get('bloquearUsuario/{idUsuario}', 'UsuarioController@getBloquearUsuario');    
		Route::get('desbloquearUsuario/{idUsuario}', 'UsuarioController@getDesbloquearUsuario'); 
        Route::get('desbloquear_ruta/{idRuta}', 'RutaController@desbloquearRuta');   
        Route::get('bloquear_ruta/{idRuta}', 'RutaController@bloquearRuta');    
        Route::get('bloquear_pi/{idPuntoInteres}', 'PuntoInteresController@bloquearPI');    
        Route::get('desbloquear_pi/{idPuntoInteres}','PuntoInteresController@desbloquearPI');
        Route::get('bloquear_diario/{idDiario}', 'DiarioController@bloquearDiario');  
        Route::get('desbloquear_diario/{idDiario}','DiarioController@desbloquearDiario');
        Route::get('/usuariosPorPerfil', 'BackendController@usuariosPorPerfil'); 
        Route::get('mantenimiento_rutas_eliminar/{idRuta}', 'MantenimientoRutasController@destroy'); 
        Route::get('admin-eliminar-pi-ruta/{idPuntoInteres}','MantenimientoRutasController@eliminarPIRuta');
        Route::get('mantenimiento_diarios_eliminar/{idDiario}','MantenimientoDiariosController@destroy'); 
        Route::get('mantenimiento_pi_eliminarfoto/{idFoto}','MantenimientoPuntoInteresController@eliminarFoto');

	});

    Route::get('/administrador', 'BackendController@getAdminLogin'); 
    Route::post('/administrador', 'BackendController@postAdministrador'); 

   
});


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::get('/formularios', 'FrontEndController@formularios'); 


Route::group(['middleware' => ['web']], function () {
    //
});