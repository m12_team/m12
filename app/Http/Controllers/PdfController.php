<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\PuntoInteres;
use App\ListaFavoritos;

class PdfController extends Controller
{
    public function invoice($id) 
    {

      $puntointeres = PuntoInteres::find($id);
      $view =  \View::make('pdf.invoice')->with('puntointeres',$puntointeres)->render();
      $pdf = \App::make('dompdf.wrapper');
      $pdf->loadHTML($view);
      return $pdf->stream('pdf.invoice');
    }

}
