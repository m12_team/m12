<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Laravel\Socialite\Facades\Socialite;
use Providers\EventServiceProvider;
use App\User;
use App\Http\Requests\UserRequest;

class SocialController extends Controller
{
    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('google')->redirect();
    }

    

    public function redirectToProviderStripe(){
    	return Socialite::driver('stripe')->redirect();
    }

    public function redirectToProviderFacebook(){
    	return Socialite::driver('facebook')->redirect();
    }



     public function redirectToProviderTwitter() 
    {
		return Socialite::with('twitter')->redirect();
    }

    public function handleProviderCallback(){
        $user = Socialite::driver('google')->user();
        $usuario = new User();
        $usuario->nombre=$user->name;
        $usuario->email=$user->email;
        $usuario->password=\Hash::make("1234");
        $usuario->foto=$user->avatar;
        $usuario->role_id=1;
        $usuario->estaBloqueado=0;
        $usuario->save();       
        $buscarUsuario= User::where('email','=',$usuario->email)->get();
        $email= $buscarUsuario->email;
        $password=\Request::input('password');
        if (!$buscarUsuario->isEmpty()) { 
            if (Auth::attempt(['email' => $email, 'password' => $password,'estaBloqueado' => 0]))
            {
              return redirect()->intended('profile');
            }else{

                    return Redirect::back();      
            }
                
        }else{
            Session::flash('msg', 'El usuario y/o contraseña es incorrecto');
            return Redirect::back();        
        }
    }

    public function handleProviderCallbackFacebook(){
        $user = Socialite::driver('facebook')->user();
        $usuario = new User();
        $usuario->nombre=$user->name;
        $usuario->email=$user->email;
        $usuario->password=\Hash::make("1234");
        $usuario->save();     
        $usuario->role_id=1;  
        $buscarUsuario= User::where('email','=',$usuario->email)->get();
        return \Redirect::back();         
    }





}

