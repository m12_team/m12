<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CategoriaPuntoInteres;
use App\Http\Requests;
use App\Http\Requests\CategoriaPI_Request;
use App\Http\Requests\UpdateCategoriaPIRequest;

use Redirect;
use App\User;
use App\PuntoInteres;

class MantenimientoCategoriasController extends Controller
{
   	public function index()
    {
        $categoriasPI = CategoriaPuntoInteres::all();
        return view('admin.mant_categoria_pi.index')->with('categorias',$categoriasPI);
    }
    public function create()
    {
        return view('admin.mant_categoria_pi.create');
    }

    public function store(CategoriaPI_Request $request){
    	$categoriaPI = new CategoriaPuntoInteres();
    	$categoriaPI->nombre=$request->input('nombre');
        echo $categoriaPI;
        
    	$categoriaPI->save();
    	 if($categoriaPI->save()){
            \Session::flash('msg', 'La categoría se ha creado correctamente');
            return Redirect::to('/mantenimiento_categoria_pi');
        }else{
            return Redirect::back();
        }
    }

    public function edit($id){
        $categoria = CategoriaPuntoInteres::find($id);
        
        return view('admin.mant_categoria_pi.editar')->with('categoria',$categoria);
    }

    public function update($id){
		
        $categoria = CategoriaPuntoInteres::find($id);
		$nombre = \Request::input("nombre");
		
		$esProfesional=\Request::input("esProfesional");
		echo $esProfesional;
		
		if($categoria->nombre != $nombre){
			$categoria->nombre=$nombre;
		}
		
		
       
		//$categoria->save();
        
		//return Redirect::to('/mantenimiento_categoria_pi');
    }

    public function destroy($id){
        $countPI = PuntoInteres::where('categoria_pi_id','=',$id)->count();
        if($countPI>0){
             \Session::flash('msg_error', 'La categoría no se puede eliminar porqué tiene puntos de interés');
             return Redirect::back();   
        }else{
            $role = CategoriaPuntoInteres::find($id);
            $role->delete();
            \Session::flash('msg', 'La categoría se ha eliminado correctamente');
            return Redirect::back();   
        }
    }


}
