<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\StoreDiarioRequest;
use App\Diario;
use App\FotosDiario;
use App\PuntoInteres;
use Auth;
use App\User;
use App\VideosDiario;
use App\InformacionExtra;
class DiarioController extends Controller
{
    
    public function getDiario(){
		
		return view('diario.buscardiario');
    }

    public function eliminarFotoDiario($identificador){
      $eliminarFotoDiario = FotosDiario::find($identificador);
      $eliminarFotoDiario->delete();
    }

    public function eliminarVideoDiario($id){
      $videosDiario = VideosDiario::find($id);
      $videosDiario->delete();
    }
    

    public function getConsultarDiario($id){
      $diario = Diario::find($id);
      if($diario->estaBloqueado==0){
        $diario = Diario::find($id);
        $videosDiario = VideosDiario::where('diario_id','=',$diario->id)->get();
        return view('diario.detalle')->with('diario',$diario)->with('videosDiario',$videosDiario);
      }else{
        return view('errors.estaBloqueadoDiario');
      }
    }

    public function subirVideos(){
      $identificador = \Request::input('identificador');
      $video = \Request::input('video');
      $videosDiario = new VideosDiario();
      $videosDiario->url=$video;
      $videosDiario->creador_id=Auth::user()->id;
      $videosDiario->diario_id=$identificador;
      $videosDiario->save();
      if($videosDiario->save()){
            \Session::flash('msg', ' El vídeo se ha subido correctamente al diario');
            return \Redirect::back();
        }else{
            return \Redirect::back();
        }
  
    }

    public function subirfotos(){
   	  $identificador = \Request::input('identificador');
      $diarios= Diario::find($identificador);
      $image = \Input::file('photo');
       foreach($image as $file){
                $fotosDiario= new FotosDiario();
                $fotosDiario->ruta = 'images/diario/'.$diarios->id.'/'.$file->getClientOriginalName();
                $file->move('images/diario/'.$diarios->id.'/', $file->getClientOriginalName());
                $fotosDiario->creador_id=Auth::user()->id;
                $fotosDiario->diario_id=$diarios->id;
                $fotosDiario->save();
          }
        if($fotosDiario->save()){
            \Session::flash('msg', 'Las fotos se han subido correctamente al diario');
            return \Redirect::back();
        }else{
            return \Redirect::back();
        }

    }
	
	public function getBuscarDiario(){
		$nombre = \Request::input('nombre');
		$nombreUsuario = \Request::input('creador');
		$buscarPorNombre = Diario::where('nombre', 'LIKE', '%'.$nombre.'%')->where('estaBloqueado','=',0)->get();
    $buscarPorCreador = User::where('username', 'LIKE', '%'.$nombreUsuario.'%')->where('estaBloqueado','=',0)->with('diarios')->get();
		$diarios = Diario::where('estaBloqueado','=',0)->get();
		if($nombre!=NULL){
			return view('diario.postBuscarDiario')->with('diarios',$buscarPorNombre);
		}else if($nombreUsuario!=NULL){
			return view('diario.postBuscarDiarioPorCreador')->with('diarios',$buscarPorCreador);
		}else{
			return view('diario.postBuscarDiario')->with('diarios',$diarios);
		}
	}

  public function crearDiario(StoreDiarioRequest $request){

    $nombre = \Request::input("nombre");
    $descripcion = \Request::input("descripcion");
    $fechaSalida = \Request::input("fechaInicio");
    $fechaLlegada = \Request::input("fechaLlegada");
    $informacion = \Request::input("informacion");
	   $pi = \Request::input("puntoInt");
    $totalDatos = (count($informacion));

    /****
  
    Datos para crear el diario

    *****/

    $diario = new Diario();
    $diario->nombre=$nombre;
    $diario->descripcion=$descripcion;
    $diario->creador_id=Auth::user()->id;
    $diario->estaBloqueado=0; 
    $diario->save();

   /****

   Contamos el total de inputs en la variable $informacion;

   ****/
    
   

    for ($i=0; $i <$totalDatos ; $i++) { 
      $informacionExtra = new InformacionExtra();
      $informacionExtra->descripcion=$informacion[$i];
      $informacionExtra->diario_id=$diario->id;
      $informacionExtra->pi_id=$pi[$i];
      $informacionExtra->fecha_llegada=$fechaLlegada[$i];
      $informacionExtra->fecha_salida=$fechaSalida[$i];
      $informacionExtra->situacion=0;
      $informacionExtra->save();
    }
    
    //Subir vídeo
    $videos = new VideosDiario();
    $videos->url=\Request::input("video");
    $videos->creador_id=Auth::user()->id;
    $videos->diario_id = $diario->id;
    $videos->save();
          $image = \Input::file('photo');

    //Subir Fotos
      foreach($image as $file){
                  $fotosPI= new FotosDiario();
                  $fotosPI->ruta = 'images/punto_interes/'.$diario->id.'/'.$file->getClientOriginalName();
                  $file->move('images/punto_interes/'.$diario->id.'/', $file->getClientOriginalName());
                  $fotosPI->creador_id=Auth::user()->id;
                  $fotosPI->diario_id=$diario->id;
                  $fotosPI->save();
      } 
    
    /*if($diario->save()){
            \Session::flash('msg', 'El diario se creado correctamente');
            return \Redirect::to('/profile');
        }else{
			\Session::flash('msg_error', 'El diario no se ha creado correctamente');
            return \Redirect::back();
        }
  return \Redirect::to('/profile');*/
    

  }

  public function eliminarDiario($idDiario){
      $diario = Diario::find($idDiario);
      $diario->delete();            
      return \Redirect::to('/profile');
  }

   public function bloquearDiario($idDiario){  
      $diario = Diario::find($idDiario);
      $diario->estaBloqueado=1;
      $diario->save();
      if($diario->save()){
            \Session::flash('msg', 'El diario se bloqueado correctamente');
            return \Redirect::to('/mantenimiento_diarios');
        }else{
            return \Redirect::back();
        }
    }

    public function desbloquearDiario($idDiario){
      $diario = Diario::find($idDiario);
      $diario->estaBloqueado=0;
      $diario->save();
      if($diario->save()){
            \Session::flash('msg', 'El diario se ha desbloqueado correctamente');
            return \Redirect::to('/mantenimiento_diarios');
        }else{
            return \Redirect::back();
        }
    }
}
