<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Regiones;
use App\Http\Requests;
use App\Http\Requests\RegionesRequest;
use Redirect;
use App\Subregiones;
use App\PuntoInteres;
class MantenimientoRegionesController extends Controller
{
  	public function index()
    {
        $regiones = Regiones::all();
        return view('admin.mant_regiones.index')->with('regiones',$regiones);
    }

    public function create()
    {
        return view('admin.mant_regiones.create');
    }

    public function store(RegionesRequest $request){
    	$region = new Regiones();
        $region->nombre=$request->input('nombre');
        $region->save();
         if($region->save()){
            \Session::flash('msg', 'La región se ha creado correctamente');
            return Redirect::to('/mantenimiento_regiones');
        }else{
            return Redirect::back();
        }
    }

    public function edit($id){
        $region = Regiones::find($id);
        return view('admin.mant_regiones.editar')->with('region',$region);
    }

    public function update(RegionesRequest $request,$id){
        $nombreRegion =$request->input('nombre');
        $region = Regiones::find($id);
        $region->nombre = $nombreRegion;
        if($region->save()){
            \Session::flash('msg', 'La regíón se ha modificado correctamente');
            return Redirect::to('/mantenimiento_regiones');
        }else{
            return Redirect::back();
        }
    }

    public function destroy($id){
         $countRegiones = Subregiones::where('region_id','=',$id)->count();
        if($countRegiones>0){
             \Session::flash('msg_error', 'La región no se puede eliminar porqué contiene subregiones');
             return Redirect::back();   
        }else{
            $transporte = Regiones::find($id);
            $transporte->delete();
            \Session::flash('msg', 'La region se ha eliminado correctamente');
            return Redirect::back();   
        }
    }
}
