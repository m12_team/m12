<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\StoreRutaRequest;
use Redirect;
use App\Ruta;
use Auth;
use App\User;
use App\PuntoInteresRuta;

class RutaController extends Controller
{
    public function getBuscarRuta(){
      return view('ruta.buscarruta');
    }

    public function getRuta($id){
      $ruta = Ruta::find($id);
      if($ruta->estaBloqueado==0){
        $ruta = Ruta::find($id);
        return view('ruta.detalle')->with('ruta',$ruta);
      }else{
        return view('errors.estaBloqueadoRuta')->with('ruta',$ruta);
      }
    }

    /***

    Crear una ruta desde el perfil de usuario

    *****/

  	public function crearRuta(StoreRutaRequest $request)
    {
      $ruta = new Ruta();
      $ruta->nombre=$request->input('nombre');
      $ruta->descripcion=$request->input('descripcion');
      $ruta->creador_id=Auth::user()->id;
      $ruta->save();
      if($request->get('puntosInteres')!=NULL){
        $stringArray = $request->get('puntosInteres');
        /***Convertir array de string a integer***/
        $intArray = array_map(
        function($value) { return (int)$value; },
          $stringArray
            );
        /***Recorremos array integers ***/
        foreach ($stringArray as $string) {
          $pi = new PuntoInteresRuta();
            $pi->ruta_id=$ruta->id;
            $pi->punto_interes_id=$string;
            $pi->save();
        }
      }
      if($ruta->save()){
            \Session::flash('msg', 'La ruta se ha creado correctamente');
            return Redirect::to('/profile');
        }else{
            return Redirect::back();
        }
    }

    public function buscarRutas(){
      $nombre = \Request::input('nombre_ruta');
      $nombreUsuario = \Request::input('nombre_usuario');
      $username = \Request::input('username');
      $buscarPorNombre = Ruta::where('nombre', 'LIKE', '%'.$nombre.'%')->where('estaBloqueado','=',0)->get();
      $buscarPorCreador = User::where('nombre', 'LIKE', '%'.$nombreUsuario.'%')->where('estaBloqueado','=',0)->with('rutas')->get();
      $buscarPorUsername = User::where('username', 'LIKE', '%'.$username.'%')->where('estaBloqueado','=',0)->with('rutas')->get();
      $buscarRutas = Ruta::where('estaBloqueado','=',0)->get();
     if($nombre!=NULL){
        return view('ruta.postBuscarRutas')->with('rutas',$buscarPorNombre);
     }else if($nombreUsuario!=NULL){
        return view('ruta.postBuscarRutasPorCreador')->with('rutas',$buscarPorCreador);
     }else if($buscarPorUsername!=NULL){
        return view('ruta.postBuscarRutasPorCreador')->with('rutas',$buscarPorUsername);
     }else{
        echo "OK";
     }
    
    }

    public function delete($id){
      $ruta = Ruta::find($id);
      foreach($ruta->puntointeres as $pi){
		$puntointeresruta = PuntoInteresRuta::where('punto_interes_id','=',$pi->id)->where('ruta_id',$ruta->id)->get();
		$eliminarpuntointeresruta = PuntoInteresRuta::find($puntointeresruta[0]->id);
		$eliminarpuntointeresruta->delete();
      }
	  $ruta->delete();
	  if($ruta->delete()){
          \Session::flash('msg', 'La ruta se ha eliminado correctamente');
            return Redirect::to('/profile');
        }else{
             \Session::flash('msg', 'La ruta no se ha eliminaod correctamente');
            return Redirect::to('/profile');
        }
    
    }

    public function editar($id){
        $ruta = Ruta::find($id);
        $ruta->nombre=\Request::input('nombre');
        $ruta->descripcion=\Request::input('descripcion');
        if($ruta->save()){
          \Session::flash('msg', 'La ruta se ha modificado correctamente');
            return Redirect::to('/profile');
        }else{
             \Session::flash('msg', 'La ruta se ha modificado correctamente');
            return Redirect::to('/profile');
        }
    }

    public function bloquearRuta($idRuta){  
      $ruta = Ruta::find($idRuta);
      $ruta->estaBloqueado=1;
      $ruta->save();
      if($ruta->save()){
            \Session::flash('msg', 'La ruta se ha bloqueado correctamente');
            return \Redirect::to('/mantenimiento_rutas');
        }else{
            return \Redirect::back();
        }
    }

    public function desbloquearRuta($idRuta){
      $ruta = Ruta::find($idRuta);
      $ruta->estaBloqueado=0;
      $ruta->save();
      if($ruta->save()){
            \Session::flash('msg', 'La ruta se ha desbloqueado correctamente');
            return \Redirect::to('/mantenimiento_rutas');
        }else{
            return \Redirect::back();
        }
    }
}
