<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Http\Requests;
use App\User;
use Hash;
use Input;
use Redirect;
class MantenimientoUsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userAll = User::all();
        return view('admin.mant_usuarios.mantenimiento_de_usuarios')->with('users',$userAll);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = new User(); 
        return view('admin.mant_usuarios.create')->with('user',$user);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
            $foto = Input::file('foto');
            $user = new User();
           $user->nombre=$request->input('nombre');
            $user->primer_apellido=$request->input('primer_apellido');
            $user->segundo_apellido=$request->input('segundo_apellido');
            $user->email=$request->input('email');
            $user->password=Hash::make($request->password);
            $user->descripcion=$request->input('descripcion');
            $user->username=$request->input('username');
            $user->empresa_id = 1;
            $user->fecha_nacimiento=$request->input('calendar');
            $user->ciudad_natal_id=$request->input('cities');
            $user->ciudad_actual_id=$request->input('cities_actual');
            $user->role_id=1;
            $user->estaBloqueado=0;
            $user->fecha_nacimiento=$request->input('calendar');
            $user->save();

            $findUser = User::find($user->id);
            $findUser->foto = 'images/profile/'.$user->id.'/'.Input::file('foto')->getClientOriginalName();
            Input::file('foto')->move('images/profile/'.$user->id.'/', Input::file('foto')->getClientOriginalName());
            $findUser->save();
            if($user->save()){
                \Session::flash('msg', 'El usuario se ha creado correctamente');
                return Redirect::to('/mantenimiento_usuarios');
            }else{                
                return Redirect::back();
            }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {           
        $currentPath= \Route::getFacadeRoot()->current()->uri();
        $user = User::find($id);
        if (is_null ($user))
        {
        App::abort(404);
        }
        return view('admin.mant_usuarios.show')->with('user', $user)->with('currentPath',$currentPath);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $currentPath= \Route::getFacadeRoot()->current()->uri();
        $user = User::find($id);
        if (is_null ($user))
        {
        App::abort(404);
        }
        return view('admin.mant_usuarios.editar')->with('user', $user)->with('currentPath',$currentPath);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {       
            $foto = Input::file('foto');
            $password = Hash::make($request->input('password'));
            $user = User::find($id);
            $user->nombre=$request->input('nombre');
            $user->primer_apellido=$request->input('primer_apellido');
            $user->segundo_apellido=$request->input('segundo_apellido');
            $user->email=$request->input('email');
             if($request->input('password')!=NULL){
              $user->password=Hash::make($request->password);
            }
            $user->descripcion=$request->input('descripcion');
            $user->username=$request->input('username');
            $user->empresa_id = 1;
            $user->fecha_nacimiento=$request->input('calendar');
            $user->ciudad_natal_id=$request->input('cities');
            $user->ciudad_actual_id=$request->input('cities_actual');
            $user->role_id=1;
            $user->estaBloqueado=0;
            $user->fecha_nacimiento=$request->input('calendar');
            $user->save();
            if($foto!=NULL){
                $findUser = User::find($user->id);

                $findUser->foto = 'images/profile/'.$user->id.'/'.Input::file('foto')->getClientOriginalName();
                Input::file('foto')->move('images/profile/'.$user->id.'/', Input::file('foto')->getClientOriginalName());
                $findUser->save();
            }

            if($user->save()){
                \Session::flash('msg', 'El usuario se ha modificado correctamente');
                return Redirect::to('/mantenimiento_usuarios');
            }else{                
                return Redirect::back();
            }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $eliminarUsuario = User::find($id);
        $eliminarUsuario->delete();
        if($eliminarUsuario->delete()){
            \Session::flash('msg', 'El entorno se ha creado correctamente');
            return Redirect::to('/mantenimiento_transporte');
        }else{
            return Redirect::back();
        }
    }
}
