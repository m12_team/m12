<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Country;
use App\Http\Requests\StateRequest;
use Redirect;
use App\User;
use App\State;
use App\City;
class MantenimientoEstadosController extends Controller
{
  	public function index()
    {
        $states = State::all();
        return view('admin.mant_estados.index')->with('estados',$states);
    }

    public function create()
    {
        return view('admin.mant_estados.create');
    }

    public function store(StateRequest $request){
    	$state = new State();
        $state->name=$request->input('name');
        $state->country_id=$request->input('pais');
        $state->save();
         if($paises->save()){
            \Session::flash('msg', 'El estado se ha creado correctamente');
            return Redirect::to('/mantenimiento_estados');
        }else{
            return Redirect::back();
        }
    }

    public function edit($id){
        $currentPath= \Route::getFacadeRoot()->current()->uri();
        $state = State::find($id);
        return view('admin.mant_estados.editar')->with('state',$state)->with('currentPath',$currentPath);
    }

    /*public function update(CountryRequest $request,$id){
        $country = Country::find($id);
        $nombrePais =$request->input('name');
        $subregion =$request->input('pais');
        $country->name = $nombrePais;
        $country->subregion_id = $subregion;
        if($country->save()){
            \Session::flash('msg', 'El país se ha modificado correctamente');
            return Redirect::to('/mantenimiento_paises');
        }else{
            return Redirect::back();
        }
    }*/

    public function destroy($id){
         $countCities = City::where('state_id','=',$id)->count();
        if($countCities>0){
             \Session::flash('msg_error', 'El estado no se puede eliminar porqué contiene ciudades');
             return Redirect::back();   
        }else{
            $state = State::find($id);
            $state->delete();
            \Session::flash('msg', 'El estado se ha eliminado correctamente');
            return Redirect::back();   
        }
    }
}
