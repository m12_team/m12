<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Role;
use Redirect;
use App\User;
use Session;
use App\Http\Requests\StoreRoleRequest;
use App\Http\Requests\UpdateRoleRequest;

class MantenimientoRoleController extends Controller
{
    public function index()
    {
        $roles = Role::all();
        return view('admin.mant_roles.mantenimiento_de_roles')->with('roles',$roles);
    }

    public function create()
    {   
        $users = User::all();
        return view('admin.mant_roles.create')->with('users',$users);
    }

    public function store(StoreRoleRequest  $request){
        $nombreRol =$request->input('role');
        $role = new Role();        
        $role->nombre=$nombreRol;
        $role->save();
        if($role->save()){
            Session::flash('msg', 'El rol se ha creado correctamente');
            return Redirect::to('/mantenimiento_roles');
        }else{
            return Redirect::back();
        }
    }

    public function show($id){
        $role = Role::find($id);
        $rolesList = Role::all();
        $users = User::where('role_id','=',$id)->get();
        return view('admin.mant_roles.show')->with('role',$role)->with('users',$users)->with('rolesList',$rolesList);
    } 

    public function destroy($id){
        $countUsers = User::where('role_id','=',$id)->count();
        if($countUsers>0){
             Session::flash('msg_error', 'El role no se puede eliminar porqué contiene usuarios');
             return Redirect::back();   
        }else{
            $role = Role::find($id);
            $role->delete();
            Session::flash('msg', 'El rol se ha eliminado correctamente');
            return Redirect::back();   
        }

    }
    public function edit($id)
    {
        $role = Role::find($id);
        return view('admin.mant_roles.editar')->with('role',$role);
    } 
    public function update(UpdateRoleRequest $request , $id){
        $nombreRol =$request->input('role');
        $role = Role::find($id);
        $role->nombre = $nombreRol;
        $role->save();
        Session::flash('msg', 'El rol se ha modificado correctamente');
        return Redirect::to('/mantenimiento_roles');
    }

    public function getAsignarUsuarios($id){
        $role = Role::find($id);
        $users = User::where('role_id','!=',$id)->get();
        return view('admin.mant_roles.asignarusuarios')->with('users',$users)->with('role',$role);
    }

    public function getConfirmarCambioRole($idUser,$idRole){
        $user = User::find($idUser);
        $user->role_id=$idRole;
        $user->save();
        Session::flash('msg', 'El usuario se ha cambiado correctamente');
        return Redirect::back();   
    }

}
