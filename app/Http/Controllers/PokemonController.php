<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Pokemon;
use App\User;
use App\ComentariosPokemon;
use Auth;
use Redirect;

class PokemonController extends Controller
{
    public function getBuscarPokemon(){
    	return view('pokemon.buscarpokemon');
    }

    public function buscarPokemon(){
    	$nombre = \Request::input('nombre');
    	$pokemon = Pokemon::where('name', 'LIKE', '%'.$nombre.'%')->get();
    	return view('pokemon.busquedapokemon')->with('pokemon',$pokemon);
    }

    public function getConsultarPokemon($id){
    	$pokemon = Pokemon::find($id);

        /***Intercambio pokemon***/

    	$user = User::with('ofrece')->get();
    	$todo = [];
    	
    	foreach ($user as $key => $value) {
    		foreach($value->ofrece as $ofrecer){
    			if($ofrecer->pokemon_id==$pokemon->id){
                   $todo[] =  $ofrecer;
        		}
    		}
    	}

        /**Obtenemos comentarios**/
        $comentariosTake = ComentariosPokemon::where('pokemon_id','=',$id)->take(5)->get();
        $valoracion = \DB::table('comentarios_pokemon')->where('pokemon_id', $id)->avg('valoracion');
        $comentarios = ComentariosPokemon::where('pokemon_id','=',$id)->get();
        $countComentarios = count($comentarios);

    	return view('pokemon.consultar')->with('pokemon',$pokemon)->with('intercambiar',$todo)->with('comentariosTake',$comentariosTake)->with('valoracion',$valoracion)->with('comentariosPI',$comentarios)->with('countComentarios',$countComentarios);
    }

    /*****
    
    Publicar comentarios

    ****/

    public function publicarComentarios(Request $request){
      $identificador = \Request::input('identificador');
      $comentarios = new ComentariosPokemon();
      $comentarios->contenido=$request->input('contenido');
      $comentarios->valoracion=$request->input('valoracion');
      $comentarios->pokemon_id=$identificador;
      $comentarios->creador_id=Auth::user()->id;
      $comentarios->save();
      if($comentarios->save()){
          \Session::flash('msg', 'El comentario se ha creado correctamente');
          return Redirect::back();
      }else{
          \Session::flash('msg_error', 'El comentario no se ha creado correctamente');
          return Redirect::back();
      }
    }
}
