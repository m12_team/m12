<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\EntornosRequest;
use App\Http\Requests\UpdateEntornosRequest;
use App\Entorno;
use Redirect;
use App\PuntoInteres;
class MantenimientoEntornoController extends Controller
{
   	public function index()
    {
        $entornos = Entorno::all();
        return view('admin.mant_entorno.index')->with('entornos',$entornos);
    }

    public function create()
    {
        return view('admin.mant_entorno.create');
    }

    public function store(EntornosRequest $request){
    	$entorno = new Entorno();
        $entorno->nombre=$request->input('nombre');
        $entorno->save();
         if($entorno->save()){
            \Session::flash('msg', 'El entorno se ha creado correctamente');
            return Redirect::to('/mantenimiento_entorno');
        }else{
            return Redirect::back();
        }
    }

    public function edit($id){
        $entorno = Entorno::find($id);
        return view('admin.mant_entorno.editar')->with('entorno',$entorno);
    }

    public function update(EntornosRequest $request,$id){
        $nombreEntorno =$request->input('nombre');
        $entorno = Entorno::find($id);
        $entorno->nombre = $nombreEntorno;
        if($entorno->save()){
            \Session::flash('msg', 'El entorno se ha modificado correctamente');
            return Redirect::to('/mantenimiento_entorno');
        }else{
            return Redirect::back();
        }
        return Redirect::to('/mantenimento_entorno');
    }

    public function destroy($id){
         $countEntorno = PuntoInteres::where('entorno_id','=',$id)->count();
        if($countEntorno>0){
             \Session::flash('msg_error', 'El entorno no se puede eliminar porqué contiene puntos de interés');
             return Redirect::back();   
        }else{
            $entorno = Entorno::find($id);
            $entorno->delete();
            \Session::flash('msg', 'El entorno se ha eliminado correctamente');
            return Redirect::back();   
        }
    }

}
