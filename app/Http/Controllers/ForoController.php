<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\CategoriaPuntoInteres;
use App\Pregunta;
use App\Respuesta;
use Auth;
use App\PuntoInteres;
use Redirect;
class ForoController extends Controller
{
    
	public function getPIporCategoria($idCategoria){
		$puntosinteres = PuntoInteres::where('categoria_pi_id','=',$idCategoria)->get();
		return view('foro.picat')->with('puntosinteres',$puntosinteres);
	}

	public function getPostPI($idPuntoInteres){
		$puntoInteres = PuntoInteres::find($idPuntoInteres);
		$pregunta = Pregunta::where('punto_interes_id','=',$idPuntoInteres)->orderBy('created_at','DESC')->get();
		return view('foro.pregunta')->with('pregunta',$pregunta)->with('pi',$puntoInteres);
	}

	public function getRespuesta($idPregunta){
		$pregunta=Pregunta::find($idPregunta);
		$pi = PuntoInteres::find($pregunta->punto_interes_id);
		$respuesta = Respuesta::where('pregunta_id','=',$idPregunta)->get();
		return view('foro.respuesta')->with('respuesta',$respuesta)->with('pregunta',$pregunta)->with('pi',$pi);
	}

	public function crearPregunta(){
		$pregunta = new Pregunta();
		$pregunta->contenido=\Request::input("pregunta");
		$pregunta->punto_interes_id=\Request::input("identificador");
		$pregunta->creador_id=Auth::user()->id;
		$pregunta->save();
		return Redirect::back();      
	}

	public function crearRespuesta(){
		$respuesta = new Respuesta();
		$respuesta->contenido=\Request::input("respuesta");
		$respuesta->valoracion=0;
		$respuesta->punto_interes_id=\Request::input("punto_interes");
		$respuesta->creador_id=Auth::user()->id;
		$respuesta->pregunta_id=\Request::input("pregunta_id");
		$respuesta->save();
		return Redirect::back();      

	}

	public function valorarRespuesta($idRespuesta){
		$respuesta = Respuesta::find($idRespuesta);
		$respuesta->valoracion=$respuesta->valoracion+1;
		$respuesta->save();
		$respuesta->save();
		return Redirect::back();

	}
	
	public function reducirValoracion($idRespuesta){
		$respuesta = Respuesta::find($idRespuesta);
		$respuesta->valoracion=$respuesta->valoracion-1;
		$respuesta->save();
		$respuesta->save();
		return Redirect::back();
	}

}
