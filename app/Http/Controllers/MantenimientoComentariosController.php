<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\ComentariosPI;
use Redirect;

class MantenimientoComentariosController extends Controller
{
    public function index(){
		$comentariosPI = ComentariosPI::all();
        return view('admin.mant_comentarios.index')->with('comentarios',$comentariosPI);
	}
	public function destroy($id){
		$comentariosPI=ComentariosPI::find($id);
		$comentariosPI->delete();
		if($comentariosPI->delete()){
			\Session::flash('msg_error', 'El comentario se ha eliminado correctamente');
            return Redirect::to('/mantenimiento_comentarios');
		}else{
			\Session::flash('msg', 'El comentario  se ha eliminado correctamente');
            return Redirect::to('/mantenimiento_comentarios');
		}
	}
	
}
