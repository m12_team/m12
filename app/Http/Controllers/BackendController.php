<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use Auth;
use Redirect;
use DB;

class BackendController extends Controller
{
   public function getAdminLogin(){

   	   return view('admin.login');
   }

   public function getMainAdminBackend(){
      $user_info = \DB::table('users')
                    ->select('role_id', \DB::raw('count(*) as total'))
                    ->groupBy('role_id')
                    ->get();

    $data = [];

    if ($user_info) {
        foreach ($user_info as $month => $value) {
            $data[date("M", mktime(0, 0, 0, $month, 1, 0))] = [$value];
        }
    }

    return view('admin.main')->with('userInfo', $data);


   }

   public function postAdministrador(){
   	$email = \Request::input('email');
   	$password = \Request::input('password');
   	$buscarUsuario= User::where('email','=',$email)->get();
		$buscarUsuario= User::where('email','=',$email)->get();
    if (!$buscarUsuario->isEmpty()) { 
      if (Auth::attempt(['email' => $email, 'password' => $password,'role_id' => 3]))
          {
            return redirect()->intended('/main_admin_page');
          }else{

              return Redirect::back();      
          }
            
    }else{
          \Session::flash('msg', 'El usuario y/o contraseña es incorrecto');
          return Redirect::back();      
    }
   }

   public function usuariosPorPerfil(){
      
   }
}

