<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Anuncio;
use Auth;
use Session;
use Redirect;

class AnuncioController extends Controller
{
    public function postCreateAnuncio(){
		$identificador = \Request::input("identificador");
		$contenido= \Request::input("descripcion") ;
		$total=count($contenido);
		for($i=0;$i<$total;$i++){
			$anuncio = new Anuncio();
			$anuncio->descripcion=$contenido[$i];
			$anuncio->creador_id=Auth::user()->id;
			$anuncio->punto_interes_id=$identificador;
			$anuncio->save();
		}
		if($anuncio->save()){
          \Session::flash('msg', 'El anuncio se ha creado correctamente');
		  return Redirect::back();      
		}else{
			return Redirect::back();      
		}
	}
}
