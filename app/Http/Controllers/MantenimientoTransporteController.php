<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Transporte;
use App\Http\Requests\TransporteRequest;
use Redirect;
use App\PuntoInteres;
class MantenimientoTransporteController extends Controller
{
  	public function index()
    {
        $transportes = Transporte::all();
        return view('admin.mant_transporte.index')->with('transportes',$transportes);
    }

    public function create()
    {
        return view('admin.mant_transporte.create');
    }

    public function store(TransporteRequest $request){
    	$transporte = new Transporte();
        $transporte->nombre=$request->input('nombre');
        $transporte->save();
         if($transporte->save()){
            \Session::flash('msg', 'El entorno se ha creado correctamente');
            return Redirect::to('/mantenimiento_transporte');
        }else{
            return Redirect::back();
        }
    }

    public function edit($id){
        $transporte = Transporte::find($id);
        return view('admin.mant_transporte.editar')->with('transporte',$transporte);
    }

    public function update(TransporteRequest $request,$id){
        $nombreTransporte =$request->input('nombre');
        $transporte = Transporte::find($id);
        $transporte->nombre = $nombreTransporte;
        if($transporte->save()){
            \Session::flash('msg', 'El transporte se ha modificado correctamente');
            return Redirect::to('/mantenimiento_transporte');
        }else{
            return Redirect::back();
        }
    }

    public function destroy($id){
         $countTransporte = PuntoInteres::where('transporte_id','=',$id)->count();
        if($countTransporte>0){
             \Session::flash('msg_error', 'El transporte no se puede eliminar porqué contiene puntos de interés');
             return Redirect::back();   
        }else{
            $transporte = Transporte::find($id);
            $transporte->delete();
            \Session::flash('msg', 'El transporte se ha eliminado correctamente');
            return Redirect::back();   
        }
    }
}
