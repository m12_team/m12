<?php

namespace App\Http\Controllers;
use Input;

use Illuminate\Http\Request;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Requests\UserRequest;
use App\Http\Requests;
use App\Country;
use App\Country2;
use App\User;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Session;
use Hash;
use Illuminate\Support\Facades\Validator;
use Redirect;
use App\Ruta;
use App\Diario;
use App\PuntoInteres;
use App\EmisorReceptor;
use App\ContenidoMensaje;
use Response;
use App\Http\Requests\LoginRequest;

class UsuarioController extends Controller {

	public function getIniciarSesion(){
		return view('usuarios.login');
	}
	
	public function getRegistroDeUsuario(){
		return view('usuarios.registro');
	}
	
	public function getRegistroDeUsuarioProfesional(){
		return view('usuarios.registroprofesional');
	}

	public function getConsultaDeUsuario($id){
		$usuario = User::find($id);
		$rutas = Ruta::with('puntointeres')->where('creador_id','=',$usuario->id)->get();
		$puntoInteres = PuntoInteres::orderByRaw("RAND()")->with('fotos')->take(9)->get();
		$diarios = Diario::where('creador_id','=',$usuario->id)->get();
		return view('usuarios.detalle')->with('puntointeres',$puntoInteres)->with('rutas',$rutas)->with('diarios',$diarios)->with('usuario',$usuario);
	}

	
	public function getBloquearUsuario($idUsuario){
		$user = User::find($idUsuario);
		$user->estaBloqueado=1;
		$user->save();
		 if($user->save()){
                \Session::flash('msg', 'El usuario se ha bloqueado correctamente');
                return Redirect::to('/mantenimiento_usuarios');
            }else{                
                return Redirect::back();
            }
		return Redirect::back(); 
	}
	public function getDesbloquearUsuario($idUsuario){
		$user = User::find($idUsuario);
		$user->estaBloqueado=0;
		$user->save();
		 if($user->save()){
                \Session::flash('msg', 'El usuario se ha desbloqueado correctamente');
                return Redirect::to('/mantenimiento_usuarios');
            }else{                
                return Redirect::back();
            }
		return Redirect::back(); 
	}
	public function getRecuperarContrasena(){
		return view('usuarios.recuperar');
	}

	public function getCambiarPassword(){
		return view('usuarios.cambiar');

	}
	public function getBuscarUsuarios(){
		return view('usuarios.buscarusuarios');
	}
	
	public function postBuscarUsuarios(){
		$nombre = \Request::input('nombre');
		$primer_apellido = \Request::input('primer_apellido');
		$segundo_apellido = \Request::input('segundo_apellido');
		$email = \Request::input('email');
		$cumple = \Request::input('date_from1');
		$username = \Request::input('username');
		$desde = \Request::input('desde');
		$hasta = \Request::input('hasta');
		$allUsers = User::where('estaBloqueado','=',0)->get();
		$findUserByNombre =	User::where('nombre', 'LIKE', '%'.$nombre.'%')->where('estaBloqueado','=',0)->simplePaginate(6);
		$findUserByPrimerApellido = User::where('primer_apellido', 'LIKE', '%'.$primer_apellido.'%')->where('estaBloqueado','=',0)->paginate(6);
		$findUserBySegundoApellido = User::where('segundo_apellido', 'LIKE', '%'.$segundo_apellido.'%')->where('estaBloqueado','=',0)->paginate(6);
		$findUserByEmail = User::where('email', 'LIKE', '%'.$email.'%')->where('estaBloqueado','=',0)->paginate(6);
		$findUserByUsername = User::where('username', 'LIKE', '%'.$username.'%')->where('estaBloqueado','=',0)->paginate(6);
		if($nombre!=NULL){			
			return view('usuarios.postBuscarusuarios')->with('users',$findUserByNombre);				
		}else if($primer_apellido!=NULL){
			return view('usuarios.postBuscarusuarios')->with('users',$findUserByPrimerApellido);	
		}else if($segundo_apellido!=NULL) {
			return view('usuarios.postBuscarusuarios')->with('users',$findUserBySegundoApellido);	
		}else if($email!=NULL){
			return view('usuarios.postBuscarusuarios')->with('users',$findUserByEmail);
		}else if($username!=NULL){
			return view('usuarios.postBuscarusuarios')->with('users',$findUserByUsername);
		}else{
			Session::flash('msg', 'No hay ningún criterio de búsqueda');
			return view('usuarios.postBuscarusuarios')->with('users',$allUsers);
		}
		
	}

	public function postCambiarPassword(){
		$email= \Request::input('email');
		$password=\Hash::make('password');

		$buscarUsuario= User::where('email','=',$email)->get();
		
		if (!$buscarUsuario->isEmpty()) {
			$user = User::find($buscarUsuario[0]->id);
			$user->password=$password;
			$user->codigo=null;
			$user->save();
		}else{
			Session::flash('msg', 'El email no existe');
	      	return Redirect::back(); 
		}


	}
	
	/*****

	Modificar datos del perfil de usuario

	****/

	public function postModificarDatos(UpdateUserRequest $request){
			$file = Input::file('foto');
            $user = User::find(Auth::user()->id);
            $user->nombre=$request->input('nombre');
            $user->primer_apellido=$request->input('primer_apellido');
            $user->segundo_apellido=$request->input('segundo_apellido');
            $user->email=$request->input('email');
            if($request->input('password')!=NULL){
          	  $user->password=Hash::make($request->password);
        	}
            $user->descripcion=$request->input('descripcion');
            $user->username=$request->input('username');
            $user->empresa_id = 1;
			$user->fecha_nacimiento=$request->input('calendar');
            $user->ciudad_natal_id=$request->input('cities');
            $user->ciudad_actual_id=$request->input('cities_actual');
            $user->role_id=1;
            $user->estaBloqueado=0;
            $user->save();
            $findUser = User::find($user->id);
           	if($file!=NULL){
		 	 	$findUser->foto = 'images/profile/'.$user->id.'/'.$file->getClientOriginalName();
	            $file->move('images/profile/'.$user->id.'/', $file->getClientOriginalName());
	            $findUser->save(); 

		 	};         
            if($user->save() && $findUser){
                 \Session::flash('msg', 'El usuario se ha modificado correctamente');
                 \Log::info('El usuario se ha modificado correctamente');
                return Redirect::to('/profile');
            }else{
                return Redirect::back();
            }
	}

	/****

	Realiza el Login en la aplicación

	****/

 	public function postLogin(LoginRequest $request)
	{
		$email= \Request::input('email');
		$password=\Request::input('password');
		$buscarUsuario= User::where('email','=',$email)->get();
		if (!$buscarUsuario->isEmpty()) { 
			if (Auth::attempt(['email' => $email, 'password' => $password,'estaBloqueado' => 0]))
	        {
	          return redirect()->intended('profile');
	        }else{

		      		return Redirect::back();      
	        }
	        	
		}else{
	        Session::flash('msg', 'El usuario y/o contraseña es incorrecto');
	      	return Redirect::back();     	
		}
		
	}


 	public function postRecuperarContrasena(Request $request)
	{
	$email= \Request::input('email');
	$random = UsuarioController::randomString();
	$buscarUsuario= User::where('email','=',$email)->get();
		if (!$buscarUsuario->isEmpty()) { 
			 $data = array(
		        'name' => "Learning Laravel",
		        'randomNumber'=> $random,
		    );
			 
		    \Mail::send('emails.message', $data, function ($message) {

		        $message->from('yourEmail@domain.com', 'Learning Laravel');

		        $message->to(\Request::input('email'))->subject('Recupear cuenta en AlwaysTravelling');

		    });
		    Session::flash('msg', 'El mensaje se ha enviado correctamente');
		    return Redirect::back(); 
		}else{
			Session::flash('msg', 'El correo no existe');
		    return Redirect::back(); 
		}
	}

	/******

	Registro  de usuario en el sistema

	*******/

	public function postRegistroDeUsuario(UserRequest $request){
			/****Creamos el usuario****/
    		$user = new User();
    		$user->nombre=$request->input('nombre');
    		$user->primer_apellido=$request->input('primer_apellido');
    		$user->segundo_apellido=$request->input('segundo_apellido');
    		$user->email=$request->input('email');
    		$user->password=Hash::make($request->password);
    		$user->descripcion=$request->input('descripcion');
    		$user->username=$request->input('username');
			$user->empresa_id = 1;
			$user->ciudad_natal_id=$request->get('cities');
			$user->ciudad_actual_id=$request->get('cities_actual');
			$user->role_id=1;
			$user->estaBloqueado=0;
			$user->fecha_nacimiento=\Carbon\Carbon::parse($request->input('calendar'))->format('Y-m-d');
			$user->save();
			/**Guardamos la foto del usuario****/
			$findUser = User::find($user->id);
		 	$findUser->foto = 'images/profile/'.$user->id.'/'.Input::file('foto')->getClientOriginalName();
		 	Input::file('foto')->move('images/profile/'.$user->id.'/', Input::file('foto')->getClientOriginalName());
			$findUser->save();
			if($user->save()&&$findUser->save()){
				$data = array(
			        'username' => $request->input('username'),
			    );
				 
			    \Mail::send('emails.register', $data, function ($message) {

			        $message->from('admin@domain.com', 'AlwaysTravelling');

			        $message->to(\Request::input('email'))->subject('Bienvenido a AlwaysTravelling');

			    });
			}
			return Redirect::to('/');
	}
	
	function randomString(){
				// Available alpha caracters
		$characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

		// generate a pin based on 2 * 7 digits + a random character
		$pin = mt_rand(1000000, 9999999)
		    . mt_rand(1000000, 9999999)
		    . $characters[rand(0, strlen($characters) - 1)];

		// shuffle the result
		return str_shuffle($pin);

	}

	/****
	
	Cargar pagina para enviar mensajes

	****/

	public function enviarMensaje($idReceptor){
		
		$receptor = User::find($idReceptor);
		return view('usuarios.mensaje')->with('usuario',$receptor);
	}

	/*****

	Crear enviar mensaje

	*****/

	public function postEnviarMensaje (){
		$receptor = \Request::input("receptor");
		$emisor = Auth::user()->id;

		$emisor = EmisorReceptor::where('emisor_id','=',$emisor)->where('receptor_id','=',$receptor)->get();	
		if (!$emisor->isEmpty()) { 
			$emisor = EmisorReceptor::find($emisor[0]->id);
		}else{
			$emisor = new EmisorReceptor();
			$emisor->emisor_id=Auth::user()->id;
			$emisor->receptor_id=$receptor;
			$emisor->save();
		}
		$cntMensaje = new ContenidoMensaje();
		$cntMensaje->emisor_receptor= $emisor->id;
		$cntMensaje->contenido=\Request::input("contenido");
		$cntMensaje->save();
		if($cntMensaje->save()){
			Session::flash('msg', 'El mensaje se ha enviado correctamente');
                return Redirect::back();
		}else{
			Session::flash('msg', 'El mensaje no se ha podido enviar');
                return Redirect::back();
		}



	}

	public function getLogout(){
		Auth::logout();
		return Redirect::to('/');
	}

	public function darDeBaja($idUsuario){
		$buscarUsuario = User::find($idUsuario);
		
		$data = array(
			        'username' =>$buscarUsuario->username,
			    );
				 
			    \Mail::send('emails.baja', $data, function ($message) {

			        $message->from('admin@domain.com', 'AlwaysTravelling');

			        $message->to(Auth::user()->email)->subject('Baja de AlwaysTravelling');

			    });
		$buscarUsuario->delete();
		Auth::logout();			    
		return Redirect::to('/');

	}

	public function comprobarEmail()
	{
    $user = User::all()->where('email', Input::get('email'))->first();
    if ($user) {
            return Response::json(array('msg' => false));
    } else {
            return Response::json(array('msg' => true));
     }

	}


	public function comprobarUsername()
	{
		$user = User::all()->where('username', Input::get('username'))->first();
	    if ($user) {
            return Response::json(array('msg' => false));
	    } else {
            return Response::json(array('msg' =>true));
	    }
	}
	
	public function postRegistroUsuarioProfesional(UserRequest $request){
		/****Creamos el usuario****/
    		$user = new User();
    		$user->nombre=$request->input('nombre');
    		$user->primer_apellido=$request->input('primer_apellido');
    		$user->segundo_apellido=$request->input('segundo_apellido');
    		$user->email=$request->input('email');
    		$user->password=Hash::make($request->password);
    		$user->descripcion=$request->input('descripcion');
    		$user->username=$request->input('username');
			$user->empresa_id = 1;
			$user->ciudad_natal_id=$request->get('cities');
			$user->ciudad_actual_id=$request->get('cities_actual');
			$user->role_id=2;
			$user->estaBloqueado=0;
			$user->fecha_nacimiento=\Carbon\Carbon::parse($request->input('calendar'))->format('Y-m-d');
			$user->save();
			/**Guardamos la foto del usuario****/
			$findUser = User::find($user->id);
		 	$findUser->foto = 'images/profile/'.$user->id.'/'.Input::file('foto')->getClientOriginalName();
		 	Input::file('foto')->move('images/profile/'.$user->id.'/', Input::file('foto')->getClientOriginalName());
			$findUser->save();
			if($user->save()&&$findUser->save()){
				$data = array(
			        'username' => $request->input('username'),
			    );
				 
			    \Mail::send('emails.register', $data, function ($message) {

			        $message->from('admin@domain.com', 'AlwaysTravelling');

			        $message->to(\Request::input('email'))->subject('Bienvenido a AlwaysTravelling');

			    });
			}
			return Redirect::to('/');
	}

	
}


