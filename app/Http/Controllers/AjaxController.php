<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Country;
use App\User;
use App\City;
use App\State;
use App\Regiones;
use App\Transporte;
use App\Ruta;
use App\Subregiones;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use Input;
use Session;
use Hash;
use Illuminate\Support\Facades\Validator;
use Redirect;
use App\CategoriaPuntoInteres;
use App\Entorno;
use App\PuntoInteres;
use App\FotosPI;
use App\PuntoInteresRuta;
use App\Diario;
use App\InformacionExtra;
use App\FotosDiario;
use App\EmisorReceptor;
use App\ContenidoMensaje;
use App\Tipos;

class AjaxController extends Controller {

	

	public function obtenerPuntoDeInteresDiario($identificador){
		$diario = InformacionExtra::where('diario_id','=',$identificador)->get();
		return response()->json(['informacionextra' => $diario ]);
		
	}

	public function obtenerFotosDiario($identificador){
		$fotosDiario = FotosDiario::where('diario_id','=',$identificador)->get();
      	return response()->json(['fotosDiario' => $fotosDiario ]);

	}
	public function obtenerVideoDiario($idDiario){
		$diarios = Diario::with('videosdiario')->get();
		foreach ($diarios as $key => $diario) {
			foreach ($diario->videosdiario as $key => $video) {
		 		$videos[] = $video;
		 	}
		 }
      	return response()->json(['videos' => $videos ]);
	}

	public function obtenerDiario($idDiario){
      $diario = Diario::find($idDiario);
      return response()->json(['diario' => $diario ]);
    }

    public function modificarDiario($idDiario){
    	$diario = Diario::find($idDiario);
    	$diario->nombre=\Request::input("nombre");
    	$diario->descripcion=\Request::input("descripcion");
    	$diario->save();
    }

	public function getPuntoDeInteresDeUnaRuta($idRuta){
		$ruta = Ruta::with('puntointeres')->get();
		foreach ($ruta as $key => $r) {
			foreach ($r->puntointeres as $key => $puntointeres) {
				if($puntointeres->pivot->ruta_id == $idRuta){
						$pi[]=$puntointeres;
						$fotos[] = $puntointeres->fotos->first();
				}
			}
		}
		
		return response()->json(['puntosdeinteres' => $pi,'fotos' => $fotos]);
	}

	public function eliminarPIRuta($idRuta){
		$eliminar = PuntoInteresRuta::find($idRuta);
		$eliminar->delete();
	}


	public function obtenerPuntosInteres($id){
		$puntointeres = PuntoInteres::find($id);
		$fotosPI=FotosPI::where('punto_interes_id','=',$puntointeres->id)->get();
		$buscarCategoria = CategoriaPuntoInteres::where('id','=',$puntointeres->categoria_pi_id)->get();
		$buscarEntorno = Entorno::where('id','=',$puntointeres->entorno_id) ->get();
		$buscarTransporte = Transporte::where('id','=',$puntointeres->transporte_id) ->get();
		$categorias = CategoriaPuntoInteres::all();
		$entornos = Entorno::all();
		$transporte = Transporte::all();
		return response()->json(['puntointeres' => $puntointeres , 'categorias'=>$categorias , 'entornos'=>$entornos,'transporte' => $transporte,'buscarCategoria'=>$buscarCategoria,'buscarEntorno'=>$buscarEntorno,'buscarTransporte'=>$buscarTransporte,'fotosPI',$fotosPI]);
	}
	public function getPIdeUnaRuta($id){

		$ruta = Ruta::find($id);
		$puntosDeInteresRuta = Ruta::with('puntointeres')->get();
		foreach ($puntosDeInteresRuta as $pi) {
			if($ruta->id == $pi->id){
				echo $pi;
			}
			
		}

		return $pi;
	}
	
	public function eliminarPIdeRuta($punotInteres,$ruta){
		$eliminar = PuntoInteresRuta::where('punto_interes_id','=',$punotInteres)->where('ruta_id','=',$ruta)->get();
		echo $eliminar;
		}
	

	public function obtenerPuntosDeInteresDeUnaRuta($id){
		$ruta = Ruta::find($id);
		foreach($ruta->puntointeres as $pi){
			$puntosDeInteres[] = $pi;
		}
		return response()->json(['puntosDeInteres' => $puntosDeInteres ]);
	}

	public function obtenerTodosPuntosInteres(){
		$puntosDeInteres = PuntoInteres::with('fotos')->get();
		return response()->json(['puntosdeinteres' => $puntosDeInteres ]);
	}

	public function obtenerTodasRutas(){
		$rutas = Ruta::with('puntosinteres')->get();
		return response()->json(['rutas' => $rutas ]);
	}

	public function getPaises(){
		$user = Country::all();
		return $user;
	}


	/***Rutas del diario***/

	public function getRutas(){
		$rutas = Ruta::with('puntointeres')->get();
		foreach ($rutas as  $ruta ) {
			foreach ($ruta->puntointeres as $key => $puntointeres) {
				 $foto[] = $puntointeres->fotos->first()->ruta;
			}
		}
		return response()->json(['rutas'=>$rutas,'foto',$foto]);
	}

	/***

	Añadimos los Puntos de Interés a la ruta

	***/
	public function getPuntoInteres($id){
		$puntointeres = PuntoInteres::find($id);
		$foto=FotosPI::where('punto_interes_id','=',$id)->first();
		return response()->json(['puntointeres' => $puntointeres , 'foto'=>$foto]);
	}

	/**
	
	Obtenemos todas las fotos del punto de interés

	***/

	public function obtenerTodasFotosPI($id){
		$puntointeres = PuntoInteres::find($id);
		$foto=FotosPI::where('punto_interes_id','=',$id)->get();
		return response()->json(['puntointeres' => $puntointeres , 'foto'=>$foto]);
	}

	public function getTransportes(){
		$transporte = Transporte::all();
		return $transporte;
	}

	public function getSubregion(){
		$subregiones = Subregiones::all();
		return $subregiones;
	}
	public function getRegiones(){
		$regiones = Regiones::all();
		return $regiones;
	}

	public function getPuntosDeInteresPorCategoria($idCategoria){
				$puntointeres = PuntoInteres::where('categoria_pi_id',$idCategoria)->with('fotos')->get();
				foreach ($puntointeres as  $key => $pi) {
					foreach($pi->fotos as $p){
						$foto = $p->ruta;
					}
				}
				return response()->json(['puntoInteres' => $puntointeres,'foto' => $foto]);

	}
	public function getStates($idPais){
		$states = State::where('country_id','=',$idPais)->get();
		return $states;
	}
	
	public function getCiudades($idCiudades){
		$ciudades = City::where('state_id','=',$idCiudades)->get();
		return $ciudades;
	}

	public function getUsuariosPorNombre($nombre,$order){
		$findUserByNombre =	User::where('nombre', 'LIKE', '%'.$nombre.'%')->orderBy('nombre', $order)->get();
		return $findUserByNombre;
	}

	public function getCategorias(){
		$categorias = CategoriaPuntoInteres::all();
		return $categorias;
	}

	public function getEntornos(){
		$entornos = Entorno::all();
		return $entornos;
	}

	/****Modificar punto de interés***/
	public function modificarPuntoInteres(){
		$identificador = \Request::input('identificador');
		$puntointeres = PuntoInteres::find($identificador);
		$puntointeres->nombre=\Request::input('nombre');
		$puntointeres->descripcion=\Request::input('descripcion');
		$puntointeres->precio=\Request::input('precio');
		$puntointeres->latitud=\Request::input('latitud');
		$puntointeres->longitud=\Request::input('longitud');
		$puntointeres->categoria_pi_id=\Request::input('categoria');
		$puntointeres->entorno_id=\Request::input('entorno');
		$puntointeres->transporte_id=\Request::input('transporte');
		$puntointeres->creador_id=Auth::user()->id;
		$puntointeres->save();
		if($puntointeres->save()){
			return response()->json(['correcto' => 'El punto de interés se ha modificado correctamente']);
		}else{
			return response()->json(['error' => 'Ha habido error al modificar el punto de interés']);
		}
	}

	public function obtenerRuta($id){
		$buscarRuta = Ruta::find($id);
		
		return response()->json(['ruta' => $buscarRuta]);
	}

	public function addPItoRuta($idPuntoInteres,$idRuta){
		$piruta = new PuntoInteresRuta();
		$piruta->punto_interes_id=$idPuntoInteres;
		$piruta->ruta_id=$idRuta;
		$piruta->save();
	}

	/*****

	Obtenemos mensajes enviados

	*****/

	public function getMensajesEnviados(){
		$idEmisor = Auth::user()->id;
		$emisorReceptor = EmisorReceptor::where('emisor_id','=',$idEmisor)->get();
		foreach($emisorReceptor as $emisor){
			$contentMessage = ContenidoMensaje::where('emisor_receptor','=',$emisor->id)->get();
		}
			return $contentMessage;

		
	}

	/***
	
	Obtenemos tipos

	***/
	public function getObtenerTodosTipos(){
		$tipos = Tipos::all();
		return response()->json(['todos' => $tipos]);	
	}

	/**

	Contar usuarios registrados

	**/
	public function contarUsuariosTotal(){
		$usuarios = User::get()->count();
		return response()->json(['totalUsuarios' => $usuarios]);
	}
}

