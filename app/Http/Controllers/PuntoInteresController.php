<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Transporte;
use App\Http\Requests\SubirFotosRequest;
use App\Http\Requests\StoreComentarioRequest;
use App\Http\Requests\StorePuntoInteresRequest;
use Redirect;
use App\PuntoInteres;
use Auth;
use App\FotosPI;
use Input;
use App\User;
use App\ComentariosPI;
use App\ListaFavoritos;
class PuntoInteresController extends Controller
{
	/****
	
	Formulario para buscar las PokeParadas
	
	**/
    public function getBuscarPuntoInteres(){
      return view('punto_interes.buscarpuntosinteres');
    }
	/****
	
	Formulario para buscar un GYM
	
	***/
	public function getBuscarGYM(){
      return view('punto_interes.buscargym');
	}
	
	/**
	
	Obtenemos las PokeParadas más cercanas
	
	**/
	
	public function getPokeParadasCercanas($latitud,$longitud){
        $puntosDeInteres = PuntoInteres::where('categoria_pi_id','=',1)->get();
	    return view('punto_interes.cercanos')->with('punto_interes',$puntosDeInteres);
	}

   public function getConsultarPuntoInteres($id){ 
      $buscarPI = PuntoInteres::find($id);
      if($buscarPI->estaBloqueado==0){
           if(Auth::user()){
        $favoritos = new ListaFavoritos();
        $favoritos->punto_interes_id=$id;
        $favoritos->visitante_id=Auth::user()->id;
        $favoritos->save();
        }

        $puntointeres = PuntoInteres::find($id);
        $puntosInteresSimilares = PuntoInteres::where('categoria_pi_id','=',$puntointeres->categoria_pi_id)->get();
        $comentarios = ComentariosPI::where('punto_interes_id','=',$id)->get();
        $comentariosTake = ComentariosPI::where('punto_interes_id','=',$id)->take(5)->get();
        $valoracion = \DB::table('comentarios')->where('punto_interes_id', $id)->avg('valoracion');
        $countComentarios = count($comentarios);
        return view('detalle')->with('puntointeres',$puntointeres)->with('comentariosPI',$comentarios)->with('countComentarios',$countComentarios)->with('puntosInteresSimilares',$puntosInteresSimilares)->with('valoracion',$valoracion)->with('comentariosTake',$comentariosTake);
      }else{
        return view('errors.estaBloqueadoPI');
      }

     
       
    }

    public function subirfotos(){
      $image = \Input::file('photo');
      $errors = array_filter($image);
      if (!empty($errors)) {
        $identificador = \Request::input('identificador');
        $puntoInteres=PuntoInteres::find($identificador);
          foreach($image as $file){
                $fotosPI= new FotosPI();
                $fotosPI->ruta = 'images/punto_interes/'.$puntoInteres->id.'/'.$file->getClientOriginalName();
                $file->move('images/punto_interes/'.$puntoInteres->id.'/', $file->getClientOriginalName());
                $fotosPI->creador_id=Auth::user()->id;
                $fotosPI->punto_interes_id=$puntoInteres->id;
                $fotosPI->save();
          } 
           \Session::flash('msg', 'Las fotos se han subido correctamente');
          return Redirect::back();      
      }else{
          \Session::flash('msg_error', 'Las fotos no se han subido correctamente');
          return Redirect::back();
      }
    }

    public function publicarComentarios(StoreComentarioRequest $request){
      $identificador = \Request::input('identificador');
      $comentarios = new ComentariosPI();
      $comentarios->contenido=$request->input('contenido');
      $comentarios->valoracion=$request->input('valoracion');
      $comentarios->punto_interes_id=$identificador;
      $comentarios->creador_id=Auth::user()->id;
      $comentarios->save();
      if($comentarios->save()){
          \Session::flash('msg', 'El comentario se ha creado correctamente');
          return Redirect::back();
      }else{
          \Session::flash('msg_error', 'El comentario no se ha creado correctamente');
          return Redirect::back();
      }
    }

    /****

    Crear punto de interés en el perfil de usuario

    *****/

  	public function crearPuntoInteres(StorePuntoInteresRequest $request)
    {
       $puntoInteres = new PuntoInteres();
       $puntoInteres->nombre=$request->input('nombre');
       $puntoInteres->descripcion=$request->input('descripcion');
       $puntoInteres->precio=$request->input('precio');
       $puntoInteres->longitud=$request->input('longitude');
       $puntoInteres->latitud=$request->input('latitude');
       $puntoInteres->categoria_pi_id=$request->input('categoria');
       $puntoInteres->entorno_id=$request->input('entorno');
       $puntoInteres->transporte_id=$request->input('transporte');
       $puntoInteres->creador_id=Auth::user()->id;
       $puntoInteres->estaBloqueado=0;
       $puntoInteres->save();        
        $image = $request->file('photo');
        foreach($image as $file){
            $fotosPI= new FotosPI();
            $fotosPI->ruta = 'images/punto_interes/'.$puntoInteres->id.'/'.$file->getClientOriginalName();
            $file->move('images/punto_interes/'.$puntoInteres->id.'/', $file->getClientOriginalName());
            $fotosPI->creador_id=Auth::user()->id;
            $fotosPI->punto_interes_id=$puntoInteres->id;
            $fotosPI->save();
        }

     if($puntoInteres->save() && $fotosPI->save()){
            \Session::flash('msg', 'El punto de interés se ha creado correctamente');
            return Redirect::to('/profile');
        }else{
            return Redirect::back();
        }
    }

    /****
    
    Borramos el punto de interés desde el perfil

    ***/

    public function delete($id){
      $pi = PuntoInteres::find($id);
      $pi->delete();
       if($pi->delete()){
            \Session::flash('msg', 'El punto de interés se ha eliminado correctamente');
            return Redirect::to('/profile');
        }else{
            return Redirect::back();
        }
    }
	
	public function buscarPuntoInteres(){
	  $nombre = \Request::input('nombre');
      $precio = \Request::input('precio');
      $creador = \Request::input('creador');
	  $categoria = \Request::input('categoria');
    $desde = \Request::input('desde');
    $hasta = \Request::input('hasta');
	  $entorno = \Request::input('entorno');
	  $transporte = \Request::input('transporte');
      $buscarPorNombre = PuntoInteres::where('nombre', 'LIKE', '%'.$nombre.'%')->where('estaBloqueado','=',0)->get();
	  $buscarPorPrecio = PuntoInteres::whereBetween('precio', array($desde,$hasta))->where('estaBloqueado','=',0)->get();
	  $buscarPorCreador = User::where('nombre', 'LIKE', '%'.$creador.'%')->with('puntoInteres')->where('estaBloqueado','=',0)->get();
	  $buscarPorCategoria= PuntoInteres::where('categoria_pi_id', 'LIKE', '%'.$categoria.'%')->where('estaBloqueado','=',0)->get();
	  $buscarPorEntorno = PuntoInteres::where('categoria_pi_id', 'LIKE', '%'.$entorno.'%')->where('estaBloqueado','=',0)->get();
	  $buscarPorTransporte = PuntoInteres::where('transporte_id', 'LIKE', '%'.$transporte.'%')->where('estaBloqueado','=',0)->get();
    $puntoInteres = PuntoInteres::where('estaBloqueado','=',0)->get();
	  if($nombre!=NULL){
		 return view('punto_interes.postBuscarPI')->with('punto_interes',$buscarPorNombre);
		 echo $buscarPorNombre;
	  }else if($desde!=NULL){
		 return view('punto_interes.postBuscarPI')->with('punto_interes',$buscarPorPrecio);
	  }else if($creador!=NULL){
		 return view('punto_interes.postBuscarPIPorCreador')->with('punto_interes',$buscarPorCreador);
	  }else if($categoria!=NULL){
		 return view('punto_interes.postBuscarPI')->with('punto_interes',$buscarPorCategoria);
	  }else if($entorno!=NULL){
		 return view('punto_interes.postBuscarPI')->with('punto_interes',$buscarPorEntorno);
	  }else if($transporte!=NULL){
		 return view('punto_interes.postBuscarPI')->with('punto_interes',$buscarPorTransporte);
	  }else{
     return view('punto_interes.postBuscarPI')->with('punto_interes',$puntoInteres);
	  }
	  
	}
	
	public function eliminarFotos($idFotoPI){
	  $eliminarFotos = FotosPI::find($idFotoPI);
	  $eliminarFotos->delete();
    if($eliminarFotos->delete()){
        \Session::flash('msg', 'La foto se ha borrado correctamente');
        return Redirect::to('/profile');
    }else{
        return Redirect::back();
    }
	}

  public function bloquearPI($idPuntoInteres){
    $puntointeres = PuntoInteres::find($idPuntoInteres);
    $puntointeres->estaBloqueado=1;
    $puntointeres->save();
     if($puntointeres->save()){
            \Session::flash('msg', 'El punto de interés se ha bloqueado correctamente');
            return \Redirect::to('/mantenimiento_pi');
        }else{
            return \Redirect::back();
        }
  }

  public function desbloquearPI($idPuntoInteres){
    $puntointeres = PuntoInteres::find($idPuntoInteres);
    $puntointeres->estaBloqueado=0;
    $puntointeres->save();
     if($puntointeres->save()){
            \Session::flash('msg', 'El punto de interés se ha desbloqueado correctamente');
            return \Redirect::to('/mantenimiento_pi');
        }else{
            return \Redirect::back();
        }
  }
  
}
