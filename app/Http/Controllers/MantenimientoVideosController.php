<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\VideosDiario;

class MantenimientoVideosController extends Controller
{
    public function index(){
		$videos = VideosDiario::all();
		return view('admin.mant_videos.index')->with('videos',$videos);
	}
}
