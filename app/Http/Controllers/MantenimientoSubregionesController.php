<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subregiones;
use App\Http\Requests;
use App\Http\Requests\SubregionesRequest;
use Redirect;
use App\PuntoInteres;
use App\Country;
class MantenimientoSubregionesController extends Controller
{
  	public function index()
    {
        $subregiones = Subregiones::all();
        return view('admin.mant_subregiones.index')->with('subregiones',$subregiones);
    }

    public function create()
    {
        return view('admin.mant_subregiones.create');
    }

    public function store(SubregionesRequest $request){
    	$subregion = new Subregiones();
        $subregion->nombre=$request->input('nombre');
        $subregion->region_id=$request->input('regiones');
        $subregion->save();
         if($subregion->save()){
            \Session::flash('msg', 'La subregión se ha creado correctamente');
            return Redirect::to('/mantenimiento_subregiones');
        }else{
            return Redirect::back();
        }
    }

    public function edit($id){
        $currentPath= \Route::getFacadeRoot()->current()->uri();
        $subregiones = Subregiones::find($id);
        return view('admin.mant_subregiones.editar')->with('subregion',$subregiones)->with('currentPath',$currentPath);
    }

    public function update(SubregionesRequest $request,$id){
        $subregion = Subregiones::find($id);
        $subregion->nombre=$request->input('nombre');
        $subregion->region_id=$request->input('regiones');
        $subregion->save();
        if($subregion->save()){
            \Session::flash('msg', 'La subregión se ha editado correctamente');
            return Redirect::to('/mantenimiento_subregiones');
        }else{
            return Redirect::back();
        }
    }

    public function destroy($id){
         $countSubregiones = Country::where('subregion_id','=',$id)->count();
        if($countSubregiones>0){
             \Session::flash('msg_error', 'La subregion no se puede eliminar porqué contiene paises');
             return Redirect::back();   
        }else{
            $subregiones = Subregiones::find($id);
            $subregiones->delete();
            \Session::flash('msg', 'La subregion se ha eliminado correctamente');
            return Redirect::back();   
        }
    }
}
