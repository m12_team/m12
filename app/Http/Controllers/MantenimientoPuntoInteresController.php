<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\PuntoInteres;
use App\FotosPI;
use Auth;
use Redirect;
use App\Entorno;
use App\CategoriaPuntoInteres;
use App\Transporte;
use App\Http\Requests\StorePuntoInteresRequest;

class MantenimientoPuntoInteresController extends Controller
{
    public function index()
    {
        $puntoInteres = PuntoInteres::all();
        return view('admin.mant_pi.index')->with('punto_interes',$puntoInteres);
    }
	
	public function edit($id){
		$puntointeres = PuntoInteres::find($id);
	
		return view('admin.mant_pi.editar')->with('puntointeres',$puntointeres);
	}
	
	public function create()
	{
		 return view('admin.mant_pi.create');
	}
	
    public function destroy($id){
      $pi = PuntoInteres::find($id);
      $pi->delete();
       if($pi->delete()){
            \Session::flash('msg', 'El punto de interés se ha eliminado correctamente');
            return \Redirect::to('/profile');
        }else{
            return \Redirect::back();
        }
    }
	
	public function store(StorePuntoInteresRequest $request){
		   $puntoInteres = new PuntoInteres();
       $puntoInteres->nombre=$request->input('nombre');
       $puntoInteres->descripcion=$request->input('descripcion');
       $puntoInteres->precio=$request->input('precio');
       $puntoInteres->longitud=$request->input('longitude');
       $puntoInteres->latitud=$request->input('latitude');
       $puntoInteres->categoria_pi_id=$request->input('categoria');
       $puntoInteres->entorno_id=$request->input('entorno');
       $puntoInteres->transporte_id=$request->input('transporte');
       $puntoInteres->creador_id=Auth::user()->id;
       $puntoInteres->estaBloqueado=0;
       $puntoInteres->save();        
      
      $image = \Input::file('photo');
        foreach($image as $file){
            $fotosPI= new FotosPI();
            $fotosPI->ruta = 'images/punto_interes/'.$puntoInteres->id.'/'.$file->getClientOriginalName();
            $file->move('images/punto_interes/'.$puntoInteres->id.'/', $file->getClientOriginalName());
            $fotosPI->creador_id=Auth::user()->id;
            $fotosPI->punto_interes_id=$puntoInteres->id;
            $fotosPI->save();
        }

      if($puntoInteres->save() && $fotosPI->save()){
            \Session::flash('msg', 'El punto de interés se ha creado correctamente');
            return Redirect::to('/mantenimiento_pi');
        }else{
            return Redirect::back();
        }
	}

  public function eliminarFoto($idFotoPI){
    $eliminarFotos = FotosPI::find($idFotoPI);
    $eliminarFotos->delete();
    if($eliminarFotos->delete()){
       \Session::flash('msg', 'La foto se ha eliminado correctamente');
        return \Redirect::to('/mantenimiento_pi');
    }else{
       return \Redirect::back();
    }
  }
}
