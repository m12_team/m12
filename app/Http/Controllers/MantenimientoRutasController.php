<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Ruta;
use Auth;
use Redirect;
use App\Http\Requests\StoreRutaRequest;
use App\PuntoInteresRuta;

class MantenimientoRutasController extends Controller
{
    public function index(){
        $rutas = Ruta::all();
        return view('admin.mant_rutas.index')->with('rutas',$rutas);
    }

    public function create(){
    	return view('admin.mant_rutas.create');
    }

    public function edit($idRutas){
      $ruta = Ruta::find($idRutas);
      return view('admin.mant_rutas.editar')->with('ruta',$ruta);
    }

    public function store(StoreRutaRequest $request){
    	 $ruta = new Ruta();
      $ruta->nombre=$request->input('nombre');
      $ruta->descripcion=$request->input('descripcion');
      $ruta->creador_id=Auth::user()->id;
      $ruta->save();
      $stringArray = $request->get('puntosInteres');
      /***Convertir array de string a integer***/
      $intArray = array_map(
      function($value) { return (int)$value; },
        $stringArray
          );
      /***Recorremos array integers ***/
      foreach ($stringArray as $string) {
        $pi = new PuntoInteresRuta();
          $pi->ruta_id=$ruta->id;
          $pi->punto_interes_id=$string;
          $pi->save();
      }
      if($ruta->save() && $pi->save()){
            \Session::flash('msg', 'La ruta se ha creado correctamente');
            return Redirect::to('/profile');
        }else{
            return Redirect::back();
        }
    }

    public function destroy($id){
    	$ruta = Ruta::find($id);
    	$ruta->delete();
    	 if($ruta->delete()){
            \Session::flash('msg', 'La ruta se ha creado correctamente');
            return Redirect::to('/mantenimiento_rutas');
        }else{
            return Redirect::back();
        }
    }

    public function eliminarPIRuta(){
      
    }
}
