<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Country;
use App\Country2;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use App\PuntoInteres;
use App\Ruta;
use App\Diario;
use App\ListaFavoritos;
use App\CategoriaPuntoInteres;
use App\Regiones;
use App\Subregiones;
use App\User;
use App\Pokemon;
use App\State;
use App\City;

class FrontEndController extends Controller
{
    public function index()
	{
		return view('seleccionar');

	}
	
	public function test()
	{
		$rutas = Ruta::orderByRaw("RAND()")->with('puntointeres')->get();
		$pokeparadas = PuntoInteres::orderByRaw("RAND()")->with('fotos')->where('categoria_pi_id','=',1)->take(12)->get();
		$gimnasios = PuntoInteres::orderByRaw("RAND()")->with('fotos')->where('categoria_pi_id','=',2)->take(12)->get();
		$diarios = Diario::all();
		$regiones = Regiones::all();
		$pokemons = Pokemon::orderByRaw("RAND()")->with('fotos')->take(12)->get();
		$paises = Country::all();
		return view('welcome')->with('paises',$paises)->with('pokeparadas',$pokeparadas)->with('rutas',$rutas)->with('diarios',$diarios)->with('regiones',$regiones)->with('pokemons',$pokemons)->with('gimnasios',$gimnasios);

	}

	public function getForo(){
		$categorias = CategoriaPuntoInteres::all();
		return view('foro.index')->with('categorias',$categorias);
	}
	
	public function postUpdate(){
		echo "OK";
	}
	
	public function listado()
	{
		return view('listaview');
	}
	
	public function listados()
	{
		return view('listaview2');
	}
	public function detalle($id)
	{	
		$puntointeres = PuntoInteres::find($id);
		return view('detalle')->with('puntointeres',$puntointeres);
	}
	
	
	public function getBuscarUsuarios()
	{
		return view('usuarios.buscarusuarios');
	}
	
	public function tour()
	{
		return view('tour');
	}
	
	public function vertour(){
		return view('vertour');
	}
	
	public function quevisitar($id){
		$subregion = Subregiones::where('region_id','=',$id)->get();
		$users = User::with('ciudad_natal')->with('puntoInteres')->take(4)->get();
		$buscarRegion =Regiones::find($id);
		/****Recorremos los usuarios de la region****/
		foreach($users as $user){
			if($user->ciudad_actual->state->country->subregion->region_id == $id){
				$usuarios[] = $user;
			}	
		}

		foreach ($usuarios as $key => $usuario) {
			foreach ($usuario->puntointeres as $key => $puntointeres) {
				$puntoineres[]=$puntointeres;
			}
		}

		return view('quevisitar')->with('subregiones',$subregion)->with('usuarios',$usuarios)->with('region',$buscarRegion)->with('puntointeres',$puntointeres);
	}
	public function profile(){

		$id = Auth::user()->id;
		$listaFavoritos=ListaFavoritos::with('puntosinteres')->where('visitante_id','=',$id)->get();
		$pi = array();

		if(!empty($listaFavoritos)){
			foreach ($listaFavoritos as $key => $value) {
				$pi[] = PuntoInteres::find($value->punto_interes_id);
			}

		}

		$allPi = PuntoInteres::all();
		$puntoInteres = PuntoInteres::where('creador_id',$id)->with('fotos')->get();
		$ruta = Ruta::where('creador_id',$id)->get();
		$diarios = Diario::where('creador_id',$id)->get();
		
	
		
		$totalPI=count($puntoInteres);
		$totalRuta=count($ruta);
		$totalDiarios=count($diarios);

    	$data = array('totalPI' => $totalPI,
    				  'totalRuta' => $totalRuta,
    				  'totalDiarios' => $totalDiarios,
    				  'puntoInteres'=>$puntoInteres,
    				  'puntosinteres'=>$pi,
              		  'rutas' => $ruta,
              		  'puntointeres' => $allPi,
              		  'diarios'=>$diarios);

		return view('profile')->with($data);
	}

	public function formularios(){
		return view('formulario');
	}
	
	public function consultarPais($idPais){
		$pais = Country::find($idPais);
		$states = State::where('country_id','=',$idPais)->get();
		return view('provincias')->with('states',$states)->with('pais',$pais);
	}
	
	public function consultarState($idState){
		$state = State::find($idState);
		$cities = City::where('state_id','=',$idState)->get();
		return view('cities')->with('cities',$cities)->with('state',$state);
	}
	
	
}
