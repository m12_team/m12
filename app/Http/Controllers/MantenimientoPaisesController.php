<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Country;
use App\Http\Requests\CountryRequest;
use Redirect;
use App\User;
use App\State;
class MantenimientoPaisesController extends Controller
{
  	public function index()
    {
        $countries = Country::all();
        return view('admin.mant_countries.index')->with('paises',$countries);
    }

    public function create()
    {
        return view('admin.mant_countries.create');
    }

    public function store(CountryRequest $request){
    	$paises = new Country();
        $paises->name=$request->input('name');
        $paises->subregion_id=$request->input('pais');
        $paises->save();
         if($paises->save()){
            \Session::flash('msg', 'El país se ha creado correctamente');
            return Redirect::to('/mantenimiento_paises');
        }else{
            return Redirect::back();
        }
    }

    public function edit($id){
        $currentPath= \Route::getFacadeRoot()->current()->uri();
        $country = Country::find($id);
        return view('admin.mant_countries.editar')->with('countries',$country)->with('currentPath',$currentPath);
    }

    public function update(CountryRequest $request,$id){
        $country = Country::find($id);
        $nombrePais =$request->input('name');
        $subregion =$request->input('pais');
        $country->name = $nombrePais;
        $country->subregion_id = $subregion;
        if($country->save()){
            \Session::flash('msg', 'El país se ha modificado correctamente');
            return Redirect::to('/mantenimiento_paises');
        }else{
            return Redirect::back();
        }
    }

    public function destroy($id){
         $countSubregion = State::where('country_id','=',$id)->count();
        if($countSubregion>0){
             \Session::flash('msg_error', 'El país no se puede eliminar porqué contiene estados');
             return Redirect::back();   
        }else{
            $country = Country::find($id);
            $country->delete();
            \Session::flash('msg', 'El país se ha eliminado correctamente');
            return Redirect::back();   
        }
    }
}
