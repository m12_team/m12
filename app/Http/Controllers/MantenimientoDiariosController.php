<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Diario;
use Redirect;

class MantenimientoDiariosController extends Controller
{
 	public function index(){
 		$diario = Diario::all();
        return view('admin.mant_diarios.index')->with('diarios',$diario);
 	}

 	public function create(){
 		return view('admin.mant_diarios.create');
 	}

 	public function destroy($idDiario){
 		$diario = Diario::find($idDiario);
    	$diario->delete();
    	 if($diario->delete()){
            \Session::flash('msg', 'El diario se ha eliminado correctamente');
            return Redirect::to('/mantenimiento_diarios');
        }else{
            return Redirect::back();
        }
 	}
}
