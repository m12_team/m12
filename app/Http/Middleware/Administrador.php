<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Administrador
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if(Auth::user())
        {
            if(Auth::user()->role_id==3){
                return $next($request);
            }else{
                return redirect()->intended('/administrador');
            }
        }
        else
        {
            return redirect()->intended('/administrador');
        }
    }
}
