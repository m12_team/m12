<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FotosPI extends Model
{
    protected $table="fotos_punto_interes";

    public function punto_interes()
    {
    return $this->belongsTo('App\PuntoInteres');
    }

}
