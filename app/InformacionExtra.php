<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InformacionExtra extends Model
{
    protected $table="informacion_extra";

    public function diario()
	{
    return $this->belongsTo('App\Diario', 'diario_id');
	}
	
	 public function puntointeres()
    {
        return $this->hasOne('App\PuntoInteres','id','pi_id');
    }

}
