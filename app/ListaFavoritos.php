<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListaFavoritos extends Model
{
    protected $table="wishlist";


     public function puntosinteres()
    {
		return $this->belongsToMany('App\PuntoInteres','wishlist','punto_interes_id','visitante_id');
    }
}
