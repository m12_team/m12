<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transporte extends Model
{
    protected $table="transportes";

    public function pi()
    {
        return $this->hasMany('App\PuntoInteres');
    }
}
