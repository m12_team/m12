<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Diario extends Model
{
    protected $table="diarios";

    public function informacionExtra() {
        return $this->hasMany('App\InformacionExtra');
    }
	
	public function creador()
    {
    return $this->belongsTo('App\User','creador_id');
    }
    
     public function fotosDiario() {
        return $this->hasMany('App\FotosDiario','diario_id');
    }

    public function videosdiario() {
        return $this->hasMany('App\VideosDiario','diario_id');
    }

    public function puntointeres2()
    {
        return $this->belongsToMany('App\InformacionExtra', 'informacion_extra');
    }
}
