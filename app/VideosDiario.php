<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideosDiario extends Model
{
    protected $table="videos_diario";

   	public function diario()
    {
    return $this->belongsTo('App\Diario','id');
    }
}
