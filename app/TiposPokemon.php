<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TiposPokemon extends Model
{
    protected $table="tipos_pokemon";

    public function tipo()
    {
        return $this->hasOne('App\Tipos');
    }
}
