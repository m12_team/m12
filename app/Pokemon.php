<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pokemon extends Model
{
	protected $table="pokemon";
	
    public function fotos() {
        return $this->hasMany('App\FotosPokemon','pokemon_id');
    }
	
	public function tipos()
    {
		return $this->belongsToMany('App\Tipos', 'tipos_pokemon' ,  'pokemon_id' , 'tipos_id')->withPivot('id');
    }

  
	
}
