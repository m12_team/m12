<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\State;
use App\User;
class City extends Model
{
    protected $table="ciudad";

    public function state()
	{
    return $this->belongsTo('App\State', 'state_id');
	}

	public function users()
    {
        return $this->hasMany('App\User');
    }
}
