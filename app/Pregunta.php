<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pregunta extends Model
{
    protected $table="pregunta";

    
     public function creador()
    {
    return $this->belongsTo('App\User', 'creador_id');
    }

}
