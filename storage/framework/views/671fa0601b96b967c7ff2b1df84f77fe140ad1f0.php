<?php $__env->startSection('content'); ?>
            <div id="slideshow" class="slideshow-bg full-screen">
                <div class="flexslider">
                    <ul class="slides">
                        <li>
                            <div class="slidebg" style="background-image: url(<?php echo e(URL::asset('images/slider/img_1.jpg')); ?>);"></div>
                        </li>
                        <li>
                            <div class="slidebg" style="background-image: url(<?php echo e(URL::asset('images/slider/img_2.jpg')); ?>);"></div>
                        </li>
                        <li>
                            <div class="slidebg" style="background-image: url(<?php echo e(URL::asset('images/slider/img_3.jpg')); ?>);"></div>
                        </li>
                        <li>
                            <div class="slidebg" style="background-image: url(<?php echo e(URL::asset('images/slider/img_4.jpg')); ?>);"></div>
                        </li>
                        <li>
                            <div class="slidebg" style="background-image: url(<?php echo e(URL::asset('images/slider/img_5.jpg')); ?>);"></div>
                        </li>
                    </ul>
                </div>                
            </div>
            <div class="section white-bg">
                <div class="container">
                    <div class="text-center description block">
                        <h1>Puntos de interés</h1>
                    </div>
                    <div class="tour-packages row add-clearfix image-box">
                        <?php if(count($puntointeres)>0): ?>

                        <?php foreach($puntointeres as $pI): ?>
                        <a href="<?php echo e(URL::asset('consultarPI/'.$pI->id)); ?>"><div class="col-xs-12 col-sm-6 col-md-4">
                            <article class="box animated" data-animation-type="fadeInLeft">
                                <figure>
                                    <img style="height:200px;"src="<?php echo e($pI->fotos->first()->ruta); ?>" alt="">
                                    <figcaption>
                                        <span class="price"><?php echo e($pI->precio); ?></span>
                                        <h2 class="caption-title"><?php echo e($pI->nombre); ?></h2>
                                    </figcaption>
                                </figure>
                            </article>
                        </div></a>
                        <?php endforeach; ?>
                        <?php else: ?>
                            <div class="col-sm-6 col-md-12">
                                <div class="alert alert-info">
                                    No hay ningún punto de interés
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="section">
                <div class="container">
                    <h2>Rutas</h2>
                    <div class="tour-guide image-carousel style2 flexslider animated" data-animation="slide" data-item-width="270" data-item-margin="30" data-animation-type="fadeInUp">
                        <ul class="slides image-box">
                            <?php if(count($rutas)>0): ?>
                            <?php foreach($rutas as $ruta): ?>
                            <a href="<?php echo e(URL::asset('consultarRutas/'.$ruta->id)); ?>">
                            <li>
                                <article class="box">
                                    <figure>
                                        <?php foreach($ruta->puntointeres as $key => $value): ?>  
                                        <?php if($key == 0): ?> 
                                            <img style="height:160px;" alt="" src="<?php echo e($value->fotos->first()->ruta); ?>">
                                        <?php endif; ?>
                                        <?php endforeach; ?>
                                    </figure>
                                    <div class="details">
                                        <h4><?php echo e($ruta->nombre); ?></h4>
                                        <p><?php echo e($ruta->descripcion); ?></p>
                                    </div>
                                </article></a>
                            </li>
                            <?php endforeach; ?>
                            <?php else: ?>
                            <div class="col-xs-12 col-sm-6 col-md-12">
                                <div class="alert alert-info">
                                    No hay ninguna ruta
                                </div>
                            </div>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
            </div>
             <div class="section">
                <div class="container">
                    <h2>Diarios</h2>
                    <div class="tour-guide image-carousel style2 flexslider animated" data-animation="slide" data-item-width="270" data-item-margin="30" data-animation-type="fadeInUp">
                        <ul class="slides image-box">
                            <?php if(count($diarios)>0): ?>
                            <?php foreach($diarios as $diario): ?>
                            <a href="<?php echo e(URL::asset('consultarRutas/'.$ruta->id)); ?>">
                            <li>
                                <article class="box">
                                     <figure>
                                        <?php foreach($diario->informacionExtra as $key => $extra): ?>
                                            <?php if($key==0): ?>                     
                                            <img style="height:160px;" src="<?php echo e(URL::asset($extra->puntointeres->fotos->first()->ruta)); ?>" alt="">
                                              <?php endif; ?>
                                        <?php endforeach; ?>
                                    </figure>
                                    <div class="details">
                                        <h4><?php echo e($diario->nombre); ?></h4>
                                        <p><?php echo e($diario->descripcion); ?></p>
                                    </div>
                                </article></a>
                            </li>
                            <?php endforeach; ?>
                            <?php else: ?>
                            <div class="col-xs-12 col-sm-6 col-md-12">
                                <div class="alert alert-info">
                                    No hay ningún diario
                                </div>
                            </div>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
            </div>
            
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>