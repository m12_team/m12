<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'mandrill' => [
        'secret' => env('MANDRILL_SECRET'),
    ],

    'ses' => [
        'key'    => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'stripe' => [
        'key'    => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'google' => [
        'client_id' => '42327681681-os389ugtlog158vvq64cb033rien2he3.apps.googleusercontent.com',
        'client_secret' => 'D9YyiFQQajMF6zvqylOiux3l',
        'redirect' => 'http://localhost/m12local/public/auth/google/callback',
    ],

    'twitter' => [
        'client_id' => env('INSTAGRAM_KEY'),
        'client_secret' => env('INSTAGRAM_SECRET'),
        'redirect' => env('INSTAGRAM_REDIRECT_URI'),  
    ], 

    //Socialite
    'facebook' => [
        'client_id'     => '493220794204855',
        'client_secret' => 'fdfb21c6e0457b483d87fdaf002efd11',
        'redirect'      => 'http://localhost/m12local/public/auth/facebook/callback',
    ],

];
