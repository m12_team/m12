/*****

Editamos el punto de perfil de usuario

1 - La función editarPI() carga el formulario con la informacion del punto de interés

****/
        function editarPI(idPuntoInteres){
        	    $('.editarPunInt').fadeIn();
                $(".listarPuntosInteres").hide();
              $.ajax({
                url:   'obtenerPuntosInteres/'+idPuntoInteres,
                type:  'get',
                success:  function (data) {
                   
                    $('.editarPunInt').append('') 
                    .append("<form class='form-horizontal' role='form' action='modificarPuntoIn' method='POST' enctype='multipart/form-data' files='true'>")
					.append("<input type='hidden' class='input-text full-width identificadorPI' name='' value='"+data.puntointeres.id+"'>")
                    .append("<div class='form-group'><input type='text' class='input-text full-width nombrePI' value='"+data.puntointeres.nombre+"'></div>")
                    .append("<div class='form-group'><input type='text' class='input-text full-width precioPI' value='"+data.puntointeres.precio+"'></div>")
                    .append("<div class='form-group'><textarea rows='5' class='input-text full-width descripcionPI' >"+data.puntointeres.descripcion+"</textarea></div>")
                    .append("<div class='form-group'><input type='text' class='input-text full-width longitudPI' value='"+data.puntointeres.longitud+"'></div>")
                    .append("<div class='form-group'><input type='text' class='input-text full-width latitudPI' value='"+data.puntointeres.latitud+"'></div>")
                    .append("<div class='form-group'><select class='full-width entornoPI' name='entornoPI' id='entornoPI'><option value='"+data.buscarCategoria[0].id+"'>"+data.buscarCategoria[0].nombre+"</option></select></div>")      
                    .append("<div class='form-group'><select class='full-width categoriaPI' name='categoriaPI' id='categoriaPI'><option value='"+data.buscarEntorno[0].id+"'>"+data.buscarEntorno[0].nombre+"</option></select></div>")   
                    .append("<div class='form-group'><select class='full-width transportePI' name='transportePI' id='transportePI'><option value='"+data.buscarTransporte[0].id+"'>"+data.buscarTransporte[0].nombre+"</option></select></div>")                                                                                                          
                    .append("<div class='from-group col-md-12'><button onclick='modificarPI()' type='submit' class='btn-medium col-sms-6 col-sm-4'>MODIFICAR PUNTO DE INTERÉS</button></div>")
                    .append("</form>");   
                    $.each(data.categorias, function(key, categorias) {
                        $('#categoriaPI').append("<option value='" + categorias.id + "'>" + categorias.nombre + "</option>");
                    });
                    
					$.each(data.entornos, function(key, entornos) {
                        $('#entornoPI').append("<option value='" + entornos.id + "'>" + entornos.nombre + "</option>");
                    });					$.each(data.entornos, function(key, entornos) {
                        $('#entornoPI').append("<option value='" + entornos.id + "'>" + entornos.nombre + "</option>");
                    });
                    $.each(data.transporte, function(key, transporte) {
                        $('#transportePI').append("<option value='" + transporte.id + "'>" + transporte.nombre + "</option>");
                    });
                    obtenerPuntoInteres(data.puntointeres.id);
                }
            });
        }

        /***Envío de los datos a través de POST***/

        function modificarPI(){
			    var identificador = $(".identificadorPI").val();
                var nombre = $(".nombrePI").val();
                var precio = $(".precioPI").val();
                var descripcion = $(".descripcionPI").val();
                var longitud = $(".longitudPI").val();
                var latitud = $(".latitudPI").val();
                var categoria = $(".categoriaPI").val();
                var entorno = $(".entornoPI").val();
                var transporte = $(".transportePI").val();
            var parametros = {
				"identificador" : identificador,
                "nombre" : nombre,
                "precio" : precio,
                "descripcion" : descripcion,
                "longitud" : longitud,
                "latitud" : latitud,
                "categoria" : categoria,
                "entorno" : entorno,
                "transporte" : transporte

            };
            $.ajax({
                data:  parametros,
                url:   'modificarPI',
                type:  'get',
                success:  function (data) {
                  location.reload();
                }
            });
        }

        /***Obtener categorías***/
             $.ajax({
                
                url:   'obtenerCategorias',
                type:  'get',
                success:  function (data) {

                    $.each(data, function(key, element) {
                        console.log(element);    
                        $('#categoria').append("<option value='" + element.id + "'>" + element.nombre + "</option>");
                        $('#categoria_ruta').append("<option value='" + element.id + "'>" + element.nombre + "</option>");
                    });
                }
            });
        /****Obtener entornos***/
              $.ajax({
                
                url:   'obtenerEntornos',
                type:  'get',
                success:  function (data) {
                    
                    $.each(data, function(key, element) {
                        $('#entorno').append("<option value='" + element.id + "'>" + element.nombre + "</option>");
                        $('.entornoPI').append("<option value='" + element.id + "'>aaaaa</option>");

                    });
                }
            });
        /***Obtener transporte**/
          $.ajax({
                
                url:   'obtenerTransportes',
                type:  'get',
                success:  function (data) {
                    
                    $.each(data, function(key, element) {
                        $('#transporte').append("<option value='" + element.id + "'>" + element.nombre + "</option>");
                    });
                }
            });
function obtenerPuntoInteres(identificador){
 $.ajax({
                
                url:   'obtenerTodasFotosPI/'+identificador,
                type:  'get',
                success:  function (data) {                      
                    $.each(data.foto, function(key, foto) {                        
                        $('.imagenPI').append("<div class='form-group'><div class='col-md-4'><img style='width:100%;height:130px;margin:5px;' src='" + foto.ruta + "'></img><a class='btn btn-info col-md-12' onclick='eliminarFoto("+foto.id+")'>Eliminar foto</a></div></div>");
                    });
                }
            });
 
}

function eliminarFoto(identificador){
	$('.imagenPI').empty();
	$.ajax({
                
                url:   'eliminarFotosPuntoInteres/'+identificador,
                type:  'get',
                success:  function (data) {                      
                  location.reload();
                }
            });
}
