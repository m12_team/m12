function editarDiario(identificador){
	tjq(".editarDiario").fadeIn();

	/****
	
	Editamos la información básica del diario

	*****/
	   $.ajax({
                
                url:   'editar-diario/'+identificador,
                type:  'get',
                success:  function (data) {
                   $('.frmEditarDiario').append('') 
                    .append("<div class='form-group'><input type='text' class='input-text full-width nombreRuta' value="+data.diario.nombre+"></div>")                                                     
                    .append("<div class='form-group'><textarea rows='5' class='input-text full-width descripcionRuta' name='descripcion'>"+data.diario.descripcion+"</textarea></div>")                                                                                     
                    .append("<div class='from-group' style='margin-bottom:5px;''><button onclick='modificarDiario("+data.diario.id+")' type='submit' class='btn-medium col-sms-6 col-sm-6 col-md-12'>MODIFICAR DIARIO</button></div>")
                }
            });

	 /*******

	Obtenemos los videos del diario

	 ******/

	$.ajax({
                
                url:   'obtener-videos-diario/'+identificador,
                type:  'get',
                success:  function (data) {
                    $.each(data.videos, function(key, element) {
                        $('.frmEditarVideos').append('') 
                        .append('<div class="col-md-4" style="margin-bottom:5px;"><iframe height="315" src="'+element.url+'" frameborder="0" allowfullscreen></iframe><a onclick="eliminarVideoDiario('+element.id+')" class="col-md-12 btn btn-info">Eliminar vídeos diario</a></div>');
                    });
                }
    });

    /*******

    Obtenemos las fotos del diario

    ******/

    $.ajax({
                
                url:   'obtener-fotos-diario/'+identificador,
                type:  'get',
                success:  function (data) {
                    $.each(data.fotosDiario, function(key, fotos) {                       
                        $('.fotosDiario').append('') 
                        .append('<div class="col-md-4"><img style="width:100%;height:130px;" src="'+fotos.ruta+'"></img><a  onclick="eliminarFotosDiario('+fotos.id+')" class="col-md-12 btn btn-info">Eliminar fotos diario</a></div>');

                    });
                }
    });
	 

}

function modificarDiario(identificador){
	var nombre = $(".nombreRuta").val();
    var descripcion = $(".descripcionRuta").val();
    
    var parametros = {
                "nombre" : nombre,
                "descripcion" : descripcion
    };
    $.ajax({
                data:  parametros,
                url:   'modificar-diario/'+identificador,
                type:  'get',
                success:  function (data) {
                  location.reload();
                }
        });
}

function eliminarVideoDiario(identificador){
       $.ajax({
                url:   'eliminar-video-diario/'+identificador,
                type:  'get',
                success:  function (data) {
                  location.reload();
                }
        });
}

function eliminarFotosDiario(identificador){
    $.ajax({
                url:   'eliminar-fotos-diario/'+identificador,
                type:  'get',
                success:  function (data) {
                  location.reload();
                }
        });
}