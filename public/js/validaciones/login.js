
  $().ready(function() {
    // validate the comment form when it is submitted
    $("#commentForm").validate();

    // validate signup form on keyup and submit
    $("#loginForm").validate({
      rules: {
        email: {
          required: true,
          email:true
        },
        password: {
          required: true
        },
       
      },
      messages: {
        email: {
          required: "Por favor , introduce un correo",
          email:"El formato del correo es incorrecto"
        },
        password: "Por favor , introduce tu contraseña",

      }

    });



  });
