 $.validator.setDefaults({
    submitHandler: function(form) {
      form.submit();
    }
  });

  $().ready(function() {
      $.validator.addMethod("regex",  function(value, element, regexp) {
            if (regexp.constructor != RegExp)
                regexp = new RegExp(regexp);
            else if (regexp.global)
                regexp.lastIndex = 0;
            return this.optional(element) || regexp.test(value);
        });

    // validate signup form on keyup and submit
    $("#creardiarioForm").validate({
      rules: {
        nombre: {
          required: true,
          minlength: 3,
          maxlength : 20
        },
        video: {
          regex: /(?:https?:\/\/)?(?:www\.)?youtu\.?be(?:\.com)?\/?.*(?:watch|embed)?(?:.*v=|v\/|\/)([\w\-_]+)\&?/

        },
         'photo[]': {
          extension: "jpg|png",
        }


      },
      messages: {
        nombre: {
          required: "Por favor , introduce un nombre",
          minlength: "El nombre tiene que tener tres carácteres mínimos",
          maxlength: "El nombre tiene que tener un máximo de 20 carácteres"
        },
        video: {
          regex:"El formato de la URL es incorrecto"
        },
        

      }

    });



  });
