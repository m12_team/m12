 $.validator.setDefaults({
    submitHandler: function(form) {
      form.submit();
    }
  });

  $().ready(function() {

    $.validator.addMethod("regex",  function(value, element, regexp) {
            if (regexp.constructor != RegExp)
                regexp = new RegExp(regexp);
            else if (regexp.global)
                regexp.lastIndex = 0;
            return this.optional(element) || regexp.test(value);
        });



    // validate signup form on keyup and submit
    $("#puntoInteresForm").validate({
      rules: {
        nombre: {
          required: true,
          minlength: 3,
          maxlength : 20
        },
        precio: {
          required: true,
          regex :/^[0-9]+(\.[0-9]{1,2})?$/

        },
        descripcion: {
          required: true
        },
        longitude: {
          required: true,
          regex : /^(\+|-)?(?:180(?:(?:\.0{1,6})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,6})?))$/
        },
        latitude: {
          required: true,
          regex:/^(\+|-)?(?:90(?:(?:\.0{1,6})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{1,6})?))$/
        },
        entorno: {
          required: true
        },
        transporte: {
          required: true
        },
        categoria: {
          required: true
        },
        'photo[]': {
          required: true,
          extension: "jpg|png",
        }
        
      },
      messages: {
        nombre: {
          required: "Por favor , introduce un nombre",
          minlength: "El nombre tiene que tener tres carácteres mínimos",
          maxlength: "El nombre tiene que tener un máximo de 20 carácteres"
        },
        precio: {
          required: "Por favor , introduce un precio",
          regex :"Por favor , introduce sólo números "
        },
        descripcion: {
          required: "Por favor , introduce una descripcion"
        },
        longitude: {
          required: "Por favor , introduce una longitud",
          regex:"El formato de la longitud es incorrecta"
        },
        latitude: {
          required: "Por favor , introduce una latitud",
          regex:"El formato de la latitud es incorrecta"
        },
        entorno: {
          required: "Por favor , selecciona un tipo de entorno"
        },
        transporte: {
          required: "Por favor , selecciona un tipo de transporte"
        },
        categoria: {
          required: "Por favor , seleccionar una categoria"
        },
      }

    });



  });
