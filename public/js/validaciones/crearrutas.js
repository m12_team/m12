 $.validator.setDefaults({
    submitHandler: function(form) {
      form.submit();
    }
  });

  $().ready(function() {

    // validate signup form on keyup and submit
    $("#crearrutasForm").validate({
      rules: {
        nombre: {
          required: true,
          minlength: 3,
          maxlength : 20
        },
        descripcion: {
          required: true
        },
      },
      messages: {
        nombre: {
          required: "Por favor , introduce un nombre",
          minlength: "El nombre tiene que tener tres carácteres mínimos",
          maxlength: "El nombre tiene que tener un máximo de 20 carácteres"
        },
        descripcion: {
          required: "Por favor , introduce una descripcion"
        },
      }

    });



  });
