
  $().ready(function() {

    $.validator.addMethod("regex",  function(value, element, regexp) {
            if (regexp.constructor != RegExp)
                regexp = new RegExp(regexp);
            else if (regexp.global)
                regexp.lastIndex = 0;
            return this.optional(element) || regexp.test(value);
    });

    // validate signup form on keyup and submit
    $("#opinionForm").validate({
      rules: {
        valoracion: {
          required: true,
          digits: true,
          regex:/[0-5]/
        },
        contenido: {
          required: true
        },
       
      },
      messages: {
        valoracion: {
          required: "Por favor , introduce la valoracion",
          digits: "La valoración sólo puede ser numérica" ,
          regex:"Sólo números entre 0 y 5"
        },
        contenido: "Por favor , introduce el contenido",

      }

    });



  });
