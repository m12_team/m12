 $.validator.setDefaults({
    submitHandler: function(form) {
      form.submit();
    }
  });


  $().ready(function() {

    $.validator.addMethod("regex",  function(value, element, regexp) {
            if (regexp.constructor != RegExp)
                regexp = new RegExp(regexp);
            else if (regexp.global)
                regexp.lastIndex = 0;
            return this.optional(element) || regexp.test(value);
        }, 'Introduce sólo letras');
    // validate signup form on keyup and submit

    $("#signupForm").validate({
      rules: {
        nombre: {
          required: true,
          minlength: 3,
          maxlength : 20,
          regex : /^[a-zA-Z'.\s]{1,40}$/
      },
        primer_apellido: {
          required: true,
          minlength: 3,
          maxlength : 20,
          regex : /^[a-zA-Z'.\s]{1,40}$/ 
        },
        segundo_apellido: {
          required: true,
          minlength: 3,
          maxlength : 20,
          regex : /^[a-zA-Z'.\s]{1,40}$/
        }, 

        email: {
          required: true,
          email: true

        },

        foto: {
          required: true,
          extension: "jpg|png"
        },

        calendar: {
          required: true
        },

        descripcion: {
          required: true
        },

        cities: {
          required: true
        },

        cities_actual: {
          required: true
        },

        username: {
          required: true,
          minlength:3,
          maxlength:20 
      
        },

        password: {
          required: true,
          minlength: 5
        },
        confirm_password: {
          required: true,
          minlength: 5,
          equalTo: "#password"
        },
        
       
      },
      
          messages: {
        nombre: {
          required: "Por favor , introduce un nombre",
          minlength: "El nombre tiene que tener tres carácteres mínimos",
          maxlength: "El nombre tiene que tener un máximo de 20 carácteres",
          accept: "[a-zA-Z]+"
        },

        primer_apellido: {
          required: "Por favor , introduce el primer apellido",
          minlength: "El primer apellido tiene que tener tres carácteres mínimos",
          maxlength: "El primer apellido tiene que tener un máximo de 20 carácteres"
        },

        segundo_apellido: {
          required: "Por favor , introduce el segundo apellido",
          minlength: "El segundo apellido tiene que tener tres carácteres mínimos",
          maxlength: "El segundo apellido tiene que tener un máximo de 20 carácteres"
        },
        email: {
          required: "Por favor , introduce el email",
          email: "El formato del correo es incorrecto" ,
          remote : "El correo está siendo utilizado"
        },
        foto: {
          required: "Por favor , introduce  una foto de perfil",
          extension:"No es una foto con formato JPG o PNG",

        },
        calendar: {
          required: "Por favor , introduce tu fecha de nacimiento",
        },
        descripcion: {
          required: "Por favor , introduce tu descripción",
        },
        username: {
          required: "Por favor , introduce tu nombre de usuario",
          minlength: "El nombre de usuario tiene que tener tres carácteres mínimos",
          maxlength: "El nombre de usuario tiene que tener un máximo de 20 carácteres"
        },

        password: {
          required: "Por favor , introduce la contraseña",
          minlength: "La contraseña tiene que tener cinco carácteres mínimos"
        },

        cities_actual: {
          required: "Por favor , introduce la ciudad actual"
        },


        cities: {
          required: "Por favor , introduce la ciudad de nacimiento"
        },

        confirm_password: {
          required: "Por favor , introduce la confirmación",
          minlength: "La contraseña tiene que tener tres carácteres mínimos",
          equalTo: "La confirmaciçón de la contraseña tiene que ser igual a la contraseña"
        },
      
      
      

      }

    });



  });
