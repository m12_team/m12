 /****

 Seleccionadmos la ruta para añadir al diario

 *****/

   tjq(".seleccionaruta").click(function(e) {
                tjq(".mostrarRutas").fadeIn();
                     $.ajax({
                    
                    url:   'obtenerRutas',
                    type:  'get',
                    success:  function (data) {
                        $.each(data.rutas, function(key, element) {
                            $.each(element.puntointeres, function(key, pi) {
                               if(key==0){
                                    $.each(pi.fotos, function(key, foto) {
                                        if(key==0){
                                            $('.verRutas').append('')
                                            .append('<div class="col-md-4"><article class="box"><figure><img width="300" height="160" alt="" src="'+foto.ruta+'"></figure><div class="details"><h4 class="box-title">'+element.nombre+'</h4></div></article><a onclick="addRutaDiario('+element.id+')" class=" col-md-12 btn btn-info">Añadir al diario</a></div>');
                                        }
                                    });

                               }
                            });
                            
                        }); 
                    }
                });
            });

    /*****

    Se añade la información extra para la ruta seleccionada 
    (Es decir añade los puntos de interés de la ruta al diario)

    ******/

        function addRutaDiario(id){
            $.ajax({
                
                url:   'obtenerPuntosDeInteresDeUnaRuta/'+id,
                type:  'get',
                success:  function (data) {
                    var numero=0;
                    $.each(data.puntosDeInteres, function(key, element) {
                        $('.addToDiario').append('')                        
                        .append('<div class="alert alert-success nombres" style="margin-top:5px;">'+element.nombre+'</div>')
                        .append('<input name="elemento[]" type="checkbox" value="1">OK')
                        .append('<input type="hidden" name="puntoInt[]" value="'+element.id+'">')
                        .append('<div class="form-group"><label>FECHA LLEGADA</label><div class="datepicker-wrap" style="color:black;"><p><input type="text" id="datepicker" class="input-text full-width lock" name="fechaInicio[]"></p></div></div>')
                        .append('<label>FECHA SALIDA</label><div class="datepicker-wrap" style="color:black;"><p><input type="text" id="datepicker" class="input-text full-width lock" name="fechaLlegada[]"></p></div>  ')
                        .append('<textarea rows=5 class="form-control lock " name="informacion[]"></textarea>');

                   });
                }
            });

        }

        /******

       Listado de los puntos de interes para añadir a los diarios

        ******/

        function selectPI(){
            $.ajax({
                
                url:   'obtenerTodosPuntosDeInteres',
                type:  'get',
                success:  function (data) {
                    $.each(data.puntosdeinteres, function(key, element) {
                            $.each(element.fotos, function(key, element2) {
                                if(key==0){
                                    $('.seleccionarPuntoInteres').append('')
                                     .append('<div class="col-md-4"><article class="box"><figure><a class="hover-effect" title=""><img width="300" height="160" alt="" src="'+element2.ruta+'"></a></figure><div class="details"><h4 class="box-title">'+element.nombre+'</h4></div></article><a name="addPI" onclick="addPuntoInteresToDiario('+element.id+')" class="col-md-12 btn btn-info">Añadir al diario</a></div>');
                                }
                            });
                       
                    });
                }
            });
        }

        /*****

        Añadir punto de interés al Diario

        ****/

        
        function addPuntoInteresToDiario(identificador){
            var numero=0;
             $.ajax({
                url:   'obtenerPI/'+identificador,
                type:  'get',
                success:  function (data) {

                        $('.addToDiario').append('')                    
                        .append('<div class="alert alert-success nombres" style="margin-top:5px;">'+data.puntointeres.nombre+'</div>')
                        .append('<input name="elemento[]" type="checkbox" value="1">OK')
                        .append('<input type="hidden" name="puntoInt[]" value="'+data.puntointeres.id+'">')
                        .append('<div class="form-group"><label>FECHA LLEGADA</label><div class="datepicker-wrap" style="color:black;"><p><input type="text" id="datepicker" class="input-text full-width lock" name="fechaInicio[]"></p></div></div>')
                        .append('<label>FECHA SALIDA</label><div class="datepicker-wrap" style="color:black;"><p><input type="text" id="datepicker" class="input-text full-width lock" name="fechaLlegada[]"></p></div>  ')
                        .append('<textarea rows=5 class="form-control lock " name="informacion[]"></textarea>'); 
                }


            });
        

        }
      