 /***Obtener categorías***/
             $.ajax({
                
                url:   'obtenerCategorias',
                type:  'get',
                success:  function (data) {
                    
                    $.each(data, function(key, element) {
                        console.log(element);    
                        $('#categoria').append("<option value='" + element.id + "'>" + element.nombre + "</option>");
                    });
                }
            });
        /****Obtener entornos***/
              $.ajax({
                
                url:   'obtenerEntornos',
                type:  'get',
                success:  function (data) {
                    
                    $.each(data, function(key, element) {
                        $('#entorno').append("<option value='" + element.id + "'>" + element.nombre + "</option>");
                    });
                }
            });
        /***Obtener transporte**/
          $.ajax({
                
                url:   'obtenerTransportes',
                type:  'get',
                success:  function (data) {
                    
                    $.each(data, function(key, element) {
                        $('#transporte').append("<option value='" + element.id + "'>" + element.nombre + "</option>");
                    });
                }
            });