/******

Descripción : Toda la funcionalidad para editar el perfil de usuario está en este archivo.

Rutas utilizadas : 



*******/
function editarRuta(idRuta){
    $('.editar-ruta').fadeIn();
    $('.listarRutas').hide();

    var ruta  = idRuta;

    /*******/
    $.ajax({
                url:   'obtenerRuta/'+idRuta,
                type:  'get',
                success:  function (data) {
                   $('.editarRuta').append('') 
                    .append("<div class='form-group'><input type='text' class='input-text full-width nombreRuta' value="+data.ruta.nombre+"></div>")                                                     
                    .append("<div class='form-group'><textarea rows='5' class='input-text full-width descripcionRuta' name='descripcion'>"+data.ruta.descripcion+"</textarea></div>")                                                                                     
                    .append("<div class='from-group'><button onclick='modificarRuta("+data.ruta.id+")' type='submit' class='btn-medium col-sms-6 col-sm-4 col-md-6'>MODIFICAR RUTA</button></div>")
                }
            });
    
        $.ajax({
                url:   'obtenerPIDeUnaRuta/'+idRuta,
                type:  'get',
                success:  function (data) {
                    
                    $.each(data.puntosdeinteres, function(key, element) {
                        $('.obtenerPIruta').append('') 
                        .append('<div class="col-md-4"><article class="box"><figure><img width="300" height="160" alt="" src="'+data.fotos[key].ruta+'"></figure><div class="details"><h4 class="box-title">'+element.nombre+'</h4></div><a onclick="eliminarPuntoDeInteresRuta('+element.pivot.id+')" class="col-md-12 btn btn-info">Eliminar punto de interes de la ruta</a></article></div>');
                    });
                }
            })
}

function modificarRuta(identificador){
    var nombre = $(".nombreRuta").val();
    var descripcion = $(".descripcionRuta").val();
    
    var parametros = {
                "nombre" : nombre,
                "descripcion" : descripcion
    };
    $.ajax({
                data:  parametros,
                url:   'modificar-ruta/'+identificador,
                type:  'get',
                success:  function (data) {
                  location.reload();
                }
            });
}

function eliminarPuntoDeInteresRuta(identificador){
    $.ajax({
                url:   'eliminar-pi-ruta/'+identificador,
                type:  'get',
                success:  function (data) {
                  location.reload();
                }
            });
}

function obtenerCategorias(){
    $.ajax({
                url:   'obtenerCategorias',
                type:  'get',
                success:  function (data) {
                   $.each(data, function(key, element) {
                        $('#categoria_ruta_add').append("<option value='" + element.id + "'>" + element.nombre + "</option>");
                    });
                }
            });

   
}

function addPIaRuta(ruta){
    obtenerCategorias();
    var idRuta = ruta;
     $('.selectCategoria').append()
     .append('<div class="form-group">')
     .append('<label>Categoria</label>')
     .append('<div class="selector">')
     .append('<select class="full-width categoria_ruta_add" name="categoria_ruta_add" id="categoria_ruta_add">')
     .append('</select>')
     .append('</div>')
     .append('  </div> ')     
      $('select#categoria_ruta_add').on('change',function(){
            var idcategoria = $(this).val();
            /**Tendria que pasar el identificador de la ruta**/
            puntosInteres(idcategoria,idRuta);
    });                                       
}

function puntosInteres(idcategoria,idRuta){
    /***Obtener puntos de interés de la categoría que filtramos en el desplegable seleccionar categoría. ****/
    var ruta = idRuta;
    $.ajax({
                
                url:   'obtenerPIPorCategoria/'+idcategoria,
                type:  'get',
                success:  function (data) {
                    
                    $.each(data.puntoInteres, function(key, element) {
                            $.each(element.fotos, function(key, elemento2) {
                               if(key==0){

                                    $('.puntosInteresFiltradosAdd').append('<div class="col-md-4"><article class="box"><figure><a ><img width="300" height="160" src="'+elemento2.ruta+'"></a></figure><div class="details"><h4 class="box-title">'+element.nombre+'</h4></div></article><a class="col-md-12 btn btn-info" onclick="addInterestPointToRuta('+element.id+','+idRuta+')">Añadir a la ruta</a></div>');  
                                }
                    });
                    });
                }
            });   
}

function addInterestPointToRuta(identificadorPI,idRuta){
    $.ajax({
                url:   'add-pi-ruta/'+identificadorPI+'/'+idRuta,
                type:  'get',
                success:  function (data) {
                  location.reload();
                }
            });
}
    
   /***Obtener puntos de interés filtrando la categoría ****/
            $('select#categoria_ruta').on('change',function(){
            var idCategoria = $(this).val();

             $.ajax({
                
                url:   'obtenerPIPorCategoria/'+idCategoria,
                type:  'get',
                success:  function (data) {
                    
                    $.each(data.puntoInteres, function(key, element) {
                            
                            $.each(element.fotos, function(key, elemento2) {
                                if(key==0){
                                    $('.puntosInteresFiltrados').append('')
                                    .append('<div class="col-md-4"><article class="box"><figure><a class="hover-effect" title=""><img width="300" height="160" alt="" src="'+elemento2.ruta+'"></a></figure><div class="details"><h4 class="box-title">'+element.nombre+'</h4></div></article><a onclick="myFunction('+element.id+')" class="col-md-12 btn btn-info">Añadir a la ruta</a></div>');
                                }
                    
                        
                    });
                    });
                }
            });
        });


       function myFunction(id) {
            /****Obtener un punto de interés y añadirlo a la ruta****/
            $.ajax({
                
                url:   'obtenerPI/'+id,
                type:  'get',
                success:  function (data) {
                    /***Añadimos al array***/
                    $('.addRutaPI').append('')
                    .append('<div class="col-md-4"><article class="box"><figure><a class="hover-effect" title=""><img width="300" height="160" alt="" src="'+data.foto.ruta+'"></a></figure><div class="details"><h4 class="box-title">'+data.puntointeres.nombre+'</h4></div></article><input type="hidden" name="puntosInteres[]" value='+data.puntointeres.id+'></div>');
                }
            });
        }   