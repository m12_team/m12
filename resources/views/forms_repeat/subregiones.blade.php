   <input type="hidden" name="_token" value="{{ csrf_token() }}">
<label>Nombre subregion</label>
<div class="form-group">
@if(isset($subregion))
<input type="text" class="input-text full-width lock" name="nombre" value="{{$subregion->nombre}}">
@else
<input type="text" class="input-text full-width" name="nombre">
@endif                        
</div>
<label>Seleccionar region</label>
                             <div class="form-group">
                                <div class="col-md-4">
                                     @if(isset($subregion))
                                        @if(isset($currentPath))
                                            <select id="regiones" class="lock" name="regiones" style="width:100%;">
                                                <option value="{{$subregion->region->id}}">{{$subregion->region->nombre}}</option>
                                            </select>                                           
                                           
                                        @else
                                            <select id="regiones" class="lock" name="regiones" style="width:100%;">
                                                <option>Selecciona un país</option>
                                            </select>                                                                       
                                        @endif
                                        @else
                                        <select id="regiones" class="lock" name="regiones" style="width:100%;">
                                                <option>Selecciona un país</option>
                                            </select>     
                                     @endif
                                </div>
                               