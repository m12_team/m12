   <article class="image-box style2 box innerstyle personal-details">
                                        <figure>
                                            <a title="" href="#"><img width="270" height="263" alt="" src="{{Auth::user()->foto}}"></a>
                                        </figure>
                                        <div class="details">
                                            <a href="#" class="button btn-mini pull-right edit-profile-btn">Editar perfil</a>
                                            <h2 class="box-title fullname">{{Auth::user()->nombre}} {{Auth::user()->primer_apellido}} {{Auth::user()->segundo_apellido}}</h2>
                                            <dl class="term-description">
                                                <dt>Nombre de usuario:</dt><dd>{{Auth::user()->email}}</dd>
                                                <dt>Nombre:</dt><dd>{{Auth::user()->nombre}}</dd>
                                                <dt>Apellidos:</dt><dd>{{Auth::user()->primer_apellido}} {{Auth::user()->segundo_apellido}}</dd>
                                                <dt>Fecha de nacimiento:</dt><dd>{{Auth::user()->fecha_nacimiento}}</dd>
                                                <dt>Ciudad natal | Pais</dt><dd>{{Auth::user()->ciudad_natal->name}} | {{Auth::user()->ciudad_natal->state->country->name}}</dd>
                                                <dt>Ciudad actual | Pais</dt><dd>{{Auth::user()->ciudad_actual->name}} | {{Auth::user()->ciudad_actual->state->country->name}}</dd>

                                            </dl>
                                        </div>
                                    </article>
                                    <hr>
                                    <h2>About You</h2>
                                        <div class="intro">
                                        <p>{{Auth::user()->descripcion}}</p>
                                    </div>