   <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <label>Nombre</label>
                            <div class="form-group">
                                @if(isset($user))
                                    <input type="text" class="input-text full-width lock" name="nombre" value="{{$user->nombre}}">
                                @else
                                    <input type="text" class="input-text full-width" name="nombre">
                                @endif
                            </div>
                            <label>Primer apellido</label>
                            <div class="form-group">
                                @if(isset($user))
                                <input type="text" class="input-text full-width lock" name="primer_apellido" value="{{$user->primer_apellido}}">
                                @else
                                <input type="text" class="input-text full-width" name="primer_apellido">
                                @endif
                            </div>
                            <label>Segundo apellido</label>
                            <div class="form-group">
                                 @if(isset($user))
                                <input type="text" class="input-text full-width lock" name="segundo_apellido" value="{{$user->segundo_apellido}}">
                                @else
                                <input type="text" class="input-text full-width" name="segundo_apellido">
                                @endif
                            </div>    
                            <label>Email</label>                   
                            <div class="form-group">
                                 @if(isset($user))
                                <input type="text" class="input-text full-width lock" name="email" value="{{$user->email}}">
                                @else
                                <input type="text" class="input-text full-width" name="email">
                                @endif
                            </div>
                            
                            <label>Foto de perfil</label>
                            <div class="form-group">
                                    @if(isset($user))
                                        <img src="{{ URL::asset(''.$user->foto.'') }}" style="width:180px;height:180px;">
                                        <input type="file" class="form-control lock ocultar" name="foto" value="{{$user->foto}}" >                         
                                    @else
                                        <input type="file" class="form-control lock ocultar" name="foto" >
                                    @endif
                        
                            </div>       
                            <label>Descripcion</label>             
                             <div class="form-group">

                                <textarea class="input-text full-width" rows="5" class="descripcion" name="descripcion">
                                </textarea>
                            </div>
                            <label>Fecha de nacimiento</label>
                            <div class="form-group">
                                @if(isset($user))
                                   <div class="datepicker-wrap" style="color:black;">
                                        <p><input type="text" id="datepicker" class="input-text full-width lock" name="calendar" value="{{$user->fecha_nacimiento}}"></p>
                                    </div>                          
                                @else
                                   <div class="datepicker-wrap" style="color:black;">
                                        <p><input type="text" id="datepicker" class="input-text full-width" name="calendar"></p>
                                    </div>
                                @endif
                                 
                                </div>
                             <label>Ciudad natal</label>
                             <div class="form-group">
                                <div class="col-md-4">
                                    
                                     @if(isset($user))
                                    <select id="paises" class="lock" name="paises" style="width:100%;">
                                        <option>Hola</option>
                                    </select>                               
                                    @else
                                     <select id="paises" name="paises" style="width:100%;">
                                            <option>Selecciona un país</option>
                                    </select>
                                    @endif
                                </div>
                            </div>
                            <label>Username</label>
                             <div class="form-group">
                                 @if(isset($user))
                                <input type="text" class="input-text full-width lock"  name="username" value="{{$user->username}}">                           
                                @else
                                <input type="text" class="input-text full-width"  name="username">
                                @endif
                            </div>
                            <label>Contraseña</label>
                            <div class="form-group">
                                <input type="password" class="input-text full-width lock" name="password">
                            </div>
                            <label>Confirmar contraseña</label>
                            <div class="form-group">
                                <input type="password" class="input-text full-width lock" name="confirm_password">
                            </div>   