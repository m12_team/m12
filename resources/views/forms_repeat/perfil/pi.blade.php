 <h2>Puntos de interés</h2>
                                <div class="row">
                                    <div class="col-sm-6 col-md-4">
                                        <a href="#" class="button btn-mini pull-left create-profile-btn">CREAR POKEPARADA O GYM</a>
                                    </div>
                                </div>
                                <div class="crearpuntointeres">
								<h2>Crear punto de interés</h2>                        
									<form class="form-horizontal" id="puntoInteresForm" role="form" action="{{ URL::asset('/addPuntoInteres') }}" method="POST" enctype="multipart/form-data" files="true">
										@include('forms_repeat.perfil.crearpuntointeres')
									</form>									
                                </div>
                                <div class="row image-box listing-style2 add-clearfix ocultarPI listarPuntosInteres">
                                    @if(count($puntoInteres)>0)
                                    @foreach($puntoInteres as $pI)
                                    <div class="col-sm-6 col-md-4">
                                        <article class="box">
                                            <figure>
                                               <img width="300" height="160" alt="" src="{{$pI->fotos->first()->ruta}}">
                                            </figure>
                                            <div class="details col-md-12">
                                                <h4 class="box-title">{{$pI->nombre}}</h4>
                                                <label class="price-wrapper">
                                                    <span class="price-per-unit">{{$pI->precio}}</span>
                                                </label>
                                                <p>
                                                <a href="{{ URL::asset('consultarPI/'.$pI->id) }}" class="button uppercase col-md-4" title="Consultar PI">VER</a>
                                                <a onclick="editarPI('{{$pI->id}}')" class="button uppercase col-md-4" title="View all">EDITAR</a>
                                                <a href="{{ URL::asset('eliminar-punto-interes/'.$pI->id) }}" class="button uppercase col-md-4" title="View all">ELIMINAR</a>
                                                
                                            </div>
                                        </article>
                                    </div> 
                                    @endforeach
                                    @else
                                        <div class="alert alert-info">No hay ningún punto de interés</div>
                                    @endif                                                                                                                       
                                </div>
                                <div class="editarPI">
                                    <div class="editarPunInt"></div>
                                </div>
                                <div class="imagenPI">
                                </div>