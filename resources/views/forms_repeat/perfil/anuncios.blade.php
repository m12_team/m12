 <h2>Anuncios</h2>
 <div class="row">
	<div class="col-sm-6 col-md-4">
        <a class="button btn-mini pull-left crearAnuncio">Crear anuncio</a>
    </div>
</div>
 <div class="crearanuncio">
	   <div class="row image-box listing-style2 add-clearfix ocultarPI listarPuntosInteres">
                                    @if(count($puntoInteres)>0)
                                    @foreach($puntoInteres as $pI)
                                    <div class="col-sm-6 col-md-4">
                                        <article class="box">
                                            <figure>
                                               <img width="300" height="160" alt="" src="{{$pI->fotos->first()->ruta}}">
                                            </figure>
                                            <div class="details col-md-12">
                                                <h4 class="box-title">{{$pI->nombre}}</h4>
                                                <label class="price-wrapper">
                                                    <span class="price-per-unit">{{$pI->precio}}</span>
                                                </label>
                                                <p>
                                                <a href="{{ URL::asset('consultarPI/'.$pI->id) }}" class="button uppercase col-md-4" title="Consultar PI">VER</a>
                                                <a onclick="editarPI('{{$pI->id}}')" class="button uppercase col-md-4" title="View all">EDITAR</a>
                                                <a href="{{ URL::asset('eliminar-punto-interes/'.$pI->id) }}" class="button uppercase col-md-4" title="View all">ELIMINAR</a>
												<a onclick="crearAnuncio({{$pI->id}})" class="button uppercase col-md-4" title="View all">Crear anuncio</a>
                                            </div>
                                        </article>
                                    </div> 
                                    @endforeach
										<div class="addAnuncio">
										<div class="col-md-12">
											<form action="addAnuncio" method="POST">
											    <input type="hidden" name="_token" value="{{ csrf_token() }}">
												<div class="crear-anuncio"></div>
												<input class="btn btn-info" type="submit" value="Crear anuncio">
											</form>
										</div>
									</div>
                                    @else
                                        <div class="alert alert-info">No hay ningún punto de interés</div>
                                    @endif   
									
                                </div>
								
</div>
<script type="text/javascript">
	function crearAnuncio(ident){

        $('.crear-anuncio').append('')      
		.append('<input type="hidden" name="identificador" value="'+ident+'">')
        .append('<textarea rows="10" class="form-control descripcion" name="descripcion[]" ></textarea>');

            
	}
</script>