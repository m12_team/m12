   <article class="image-box style2 box innerstyle personal-details">
                                        <figure>
                                            @if(Auth::user()->foto!=NULL) <img width="270" height="263" src="{{Auth::user()->foto}}"> @else <img width="270" height="263" src="{{ URL::asset('images/profile/default/default.png') }}"> @endif
                                        </figure>
                                        <div class="details">
                                            <h2 class="box-title fullname">@include('forms_repeat.perfil.infouser.name_user_auth')</h2>
                                              <dl class="term-description">
                                                <dt>Nombre:</dt><dd>@include('forms_repeat.perfil.infouser.name_user_auth')</dd>
                                                <dt>Nombre de usuario:</dt><dd>@include('forms_repeat.perfil.infouser.username')</dd>
                                                <dt>Fecha de nacimiento:</dt><dd>@include('forms_repeat.perfil.infouser.fechanacimiento')</dd>
                                                <dt>Ciudad natal </dt><dd>@include('forms_repeat.perfil.infouser.ciudad_natal')</dd>
                                                <dt>Ciudad actual </dt><dd>@include('forms_repeat.perfil.infouser.ciudad_actual')</dd>
                                            </dl>
                                        </div>
                                    </article>
                                    <h2>About You</h2>
                                        <div class="intro">
                                        <p>@if(Auth::user()->descripcion!=NULL) {{Auth::user()->descripcion}} @else No hay ninguna descripción añadida @endif</p>
                                    </div>
    <a href="{{ URL::asset('/darDebajaUsuario/'.Auth::user()->id) }}" class="btn btn-danger">Dar de baja</a>