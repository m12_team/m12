 <h2>Rutas</h2>
                                <div class="row">
                                    <div class="col-sm-6 col-md-4">
                                        <a href="#" class="button btn-mini pull-left crear-rutas">Crear rutas</a>
                                    </div>
                                </div>
                                 <div class="crearrutas">
                                    <h2>Crear rutas</h2>
                                    <form id="crearrutasForm" action="{{ URL::asset('addRuta') }}" lass="edit-profile-form" method="POST">                                       
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div class="col-sm-9 no-padding no-float">
                                            <div class="row form-group">
                                                <div class="col-sms-6 col-md-12">
                                                    <label>Nombre</label>
                                                    <input type="text" class="input-text full-width" name="nombre">
                                                </div>                                                
                                            </div>
                                             <div class="form-group">
                                                <label>Descripcion</label>
                                                <textarea rows="5" class="input-text full-width" name="descripcion"></textarea>
                                            </div>
                                              <div class="form-group">
                                                    <label>Categoria</label>
                                                    <div class="selector"> 
                                                        <select class="full-width categoria_ruta" name="categoria_ruta" id="categoria_ruta">
                                                            <option value="">Selecciona unA categoria </option>
                                                        </select>
                                                    </div>
                                                </div>   
                                               
                                         </div>
                                         <div class="puntosInteresFiltrados"></div>
                                        <div class="form-group col-md-12">
                                            <h3>Puntos de interés para añadir al diario</h3>
                                        <div class="addRutaPI"></div>
                                        </div>
                                          <div class="from-group">
                                                <button type="submit" class="btn-medium col-sms-6 col-sm-4">CREAR RUTA</button>
                                            </div>
                                    </form>
                                </div>
                                 <div class="row image-box listing-style2 add-clearfix listarRutas">
                                    @if(count($rutas)>0)
                                    @foreach($rutas as $ruta)
                                    <div class="col-sm-6 col-md-4">
                                        <article class="box">
                                            <figure>
                                                @foreach($ruta->puntointeres as $key => $value)  
                                                @if($key == 0) 
                                                    <img width="270" height="160" alt="" src="{{$value->fotos->first()->ruta}}">
                                                @endif
                                                @endforeach
                                            </figure>
                                            <div class="details col-md-12">
                                                <h4 class="box-title">{{$ruta->nombre}}</h4>
                                                <p>
                                                <a href="{{ URL::asset('consultarRutas/'.$ruta->id) }}" class="button uppercase col-md-4" title="Consultar PI">VER</a>
                                                <a onclick="editarRuta({{$ruta->id}})" class="button uppercase col-md-4" title="View all">EDITAR</a>
                                                <a href="{{ URL::asset('eliminar-ruta/'.$ruta->id) }}" class="button uppercase col-md-4" title="View all">ELIMINAR</a>                                                
                                            </div>
                                        </article>
                                    </div> 
                                    @endforeach 
                                    @else
                                        <div class="alert alert-info">No hay ninguna ruta</div>
                                    @endif                                                                                                                      
                                </div>
                                      <div class="editar-ruta">
                                    <h2>Editar rutas</h2>
                                    <div class="editarRuta"></div> 
                                    @if(count($rutas)>0)
                                    <div class='from-group'><button onclick="addPIaRuta({{$ruta->id}})" type='submit' class='btn-medium col-sms-6 col-sm-6 col-md-6 btn-warning'>AÑADIR PUNTOS DE INTERÉS A LA RUTA</button></div>
                                    @endif
                                    <div class="col-md-12">                                                                >
                                        <div class="obtenerPIruta"></div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="selectCategoria"></div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="puntosInteresFiltradosAdd"></div>
                                    </div>
                                </div>
