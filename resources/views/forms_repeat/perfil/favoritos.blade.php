
 <div class="row image-box listing-style2 add-clearfix ocultarPI listarPuntosInteres">
@if(count($puntosinteres)>0)
@foreach($puntosinteres as $pI)
<div class="col-sm-6 col-md-4">
    <article class="box">
        <figure>
             <img width="300" height="160" alt="" src="{{$pI->fotos->first()->ruta}}">
        </figure>
        <div class="details col-md-12">
            <h4 class="box-title">{{$pI->nombre}}</h4>
            <label class="price-wrapper">
                <span class="price-per-unit">{{$pI->precio}}</span>
            </label>
            <p>
            <a href="{{ URL::asset('consultarPI/'.$pI->id) }}" class="button uppercase col-md-4" title="Consultar PI">VER</a>                                                                                              
        </div>
    </article>
</div> 
@endforeach
@else
<div class="alert alert-info">No has visitado ningún punto de interés</div>
@endif
</div>
