 <h2>Diarios</h2>
                                <div class="row">
                                    <div class="col-sm-6 col-md-4">
                                        <a href="#" class="button btn-mini pull-left crear-diario">Crear diario</a>
                                    </div>
                                </div>

                                <div class="creardiario">
                                    <div class="row">
                                    <div class="col-sm-6 col-md-12">
                                        <a onclick="seleccionarRutas()" class="button btn-mini pull-right seleccionaruta">Seleccionar ruta</a>
                                        <a onclick="selectPI()" class="button btn-mini pull-left  ">Seleccionar punto de interés</a>
                                    </div>
                                    <div class="col-sm-6 col-md-12 mostrarRutas">
                                            <h2>Lista de rutas</h2>
                                            <div class="verRutas"></div>
                                    </div>
                                    <div class="col-sm-6 col-md-12 mostrarPuntoInteres">
                                            <h2>Lista de puntes de interes</h2>
                                            <div class="seleccionarPuntoInteres"></div>
                                    </div>
                                    <div class="col-md-12">
                                            <h2>PI en el diario</h2>
                                            <div class="verPIDiario"></div>
                                    </div>
                                </div>
                                </div>
                                <div class="row image-box listing-style2 add-clearfix">
                                @if(count($diarios)>0)
                                    @foreach($diarios as $diario)
                                    <div class="col-sm-6 col-md-4">
                                        <article class="box">
                                            <figure >
                                                @foreach($diario->informacionExtra as $key => $extra)
                                                    @if($key==0)                     
                                                    <img width="300px" height="160px" src="{{ URL::asset($extra->puntointeres->fotos->first()->ruta)}}" alt="">
                                                    @endif
                                                @endforeach
                                            </figure>                                    
                                            <div class="details">
                                                <h4 class="box-title">{{$diario->nombre}}</h4>
                                                <p>
                                                <a href="{{ URL::asset('consultarDiario/'.$diario->id)}}" class="pull-right button uppercase" href="" title="View all">VER</a>
                                                <a onclick="editarDiario({{$diario->id}})" class="pull-right button uppercase" title="View all">EDITAR</a>
                                                <a href="{{ URL::asset('eliminar-diario/'.$diario->id)}}" class="pull-right button uppercase" href="" title="View all">ELIMINAR</a>
                                            </div>
                                        </article>
                                    </div> 
                                    @endforeach  
                                @else
                                    <div class="alert alert-info">No hay ningún diario</div>
                                @endif                                                                                                                     
                                </div>
                                <div class="crearDiario">
                                <form id="creardiarioForm" action="{{ URL::asset('crear-diario') }}" method="POST" files="true" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="row form-group">
                                                <div class="col-sms-6 col-md-12">
                                                    <label name ="contar">Nombre</label>
                                                    <input type="text" class="input-text full-width" name="nombre">
                                                </div>                                                
                                            </div>
                                             <div class="form-group">
                                                <label>Descripcion</label>
                                        <textarea rows="5" class="input-text full-width" name="descripcion"></textarea>                                
                                    </div>
                                    <div class="addToDiario"></div>
                                      <div class="row form-group">
                                                <div class="col-sms-6 col-md-12">
                                                    <label name ="contar">URL Vídeo</label>
                                                    <input type="text" class="input-text full-width" name="video">
                                                </div>                                                
                                            </div>
                                              <div class="row form-group">
                                                <div class="col-sms-12 col-sm-12 no-float">
                                                    <div class="fileinput full-width">
                                                        <input type="file" class="input-text" name="photo[]" multiple>
                                                    </div>
                                                </div>
                                            </div>  
                                     
                                    <button type="submit" class="btn-medium col-sms-6 col-sm">CREAR DIARIO</button>
                                </form>
                            </div>
                            <div class="editarDiario">
                                <div class="frmEditarDiario">
                                </div>
                                <div class="col-md-12">
                                <div class="frmEditarVideos">
                                </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="fotosDiario"></div>
                                </div>

                            </div>