<h1 class="no-margin skin-color">Hola @include('forms_repeat.perfil.infouser.name_user_auth')</h1>
                                <br />
                                <div class="row block">
                                    <div class="col-sm-6 col-md-4">
                                            <div class="fact blue">
                                                <div class="numbers counters-box">
                                                    <dl>
                                                        <dt class="display-counter" data-value="{{$totalPI}}">0</dt>
                                                        <dd>Puntos de interés</dd>
                                                    </dl>
                                                    <i class="icon soap-icon-hotel"></i>
                                                </div>
                                                <div class="description">
                                                    <i class="icon soap-icon-longarrow-right"></i>
                                                    <span>Rutas</span>
                                                </div>
                                            </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4">
                                            <div class="fact yellow">
                                                <div class="numbers counters-box">
                                                    <dl>
                                                        <dt class="display-counter" data-value="{{$totalRuta}}">0</dt>
                                                        <dd>Rutas</dd>
                                                    </dl>
                                                    <i class="icon soap-icon-plane"></i>
                                                </div>
                                                <div class="description">
                                                    <i class="icon soap-icon-longarrow-right"></i>
                                                    <span>Diarios</span>
                                                </div>
                                            </div>
                                    </div>
                                    <div class="col-sm-6 col-md-4">
                                            <div class="fact red">
                                                <div class="numbers counters-box">
                                                    <dl>
                                                        <dt class="display-counter" data-value="{{$totalDiarios}}">0</dt>
                                                        <dd>Diarios</dd>
                                                    </dl>
                                                    <i class="icon soap-icon-car"></i>
                                                </div>
                                                <div class="description">
                                                    <i class="icon soap-icon-longarrow-right"></i>
                                                    <span>View Cars</span>
                                                </div>
                                            </div>
                                    </div>                                 
                                </div>
                                <div class="notification-area">
                                    <div class="info-box block">
                                        <span class="close"></span>
                                        <p>Bienvenido a tu Perfil personal , desde aquí vas a poder gestionar absolutamente todo lo que necesites para llevar a cabo una estancia agradable allí dónde vayas.</p>                                       
                                        @include('forms_repeat.success')                                                
                                        @include('forms_repeat.errors') 
                                    </div>
                                </div>                                                        
                                <hr>                  