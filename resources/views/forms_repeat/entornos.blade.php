   <input type="hidden" name="_token" value="{{ csrf_token() }}">
<label>Nombre entorno</label>
<div class="form-group">
@if(isset($entorno))
<input type="text" class="input-text full-width lock" name="nombre" value="{{$entorno->nombre}}">
@else
<input type="text" class="input-text full-width" name="nombre">
@endif                        
</div>