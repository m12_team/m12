   <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <label>Nombre categoría</label>
                            <div class="form-group">
                                @if(isset($categoria))
                                    <input type="text" class="input-text full-width lock" name="nombre" value="{{$categoria->nombre}}">
                                @else
                                    <input type="text" class="input-text full-width" name="nombre">
                                @endif

                                Si la categoría va a ser sólo disponible para usuarios profesionales activa el checkbox.
                                @if(isset($categoria))
									@if($categoria->esProfesional==1)
										<input name="esProfesional" type="checkbox" value="1" checked>
									@else
										<input name="esProfesional" type="checkbox" value="0">
									@endif
								@else
								<input name="esProfesional" type="checkbox" value="yes">
								@endif

    </div>