@if(isset($user))
    {!! Form::model($user, ['route' => ['mantenimiento_usuarios.update', $user->id], 'method' => 'patch']) !!}
@else 
    {!! Form::open(['route' => 'mantenimiento_usuarios.store']) !!}
@endif
<form class="form-horizontal" role="form" method="POST" action="{{ URL::asset('/registro-de-usuario') }}"  enctype="multipart/form-data" files="true">

   <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <label>Nombre</label>
                            <div class="form-group">
                                @if(isset($user))
                                    <input type="text" class="input-text full-width lock" name="nombre" value="{{$user->nombre}}">
                                @else
                                    <input type="text" class="input-text full-width" name="nombre">
                                @endif
                            </div>
                            <label>Primer apellido</label>
                            <div class="form-group">
                                @if(isset($user))
                                <input type="text" class="input-text full-width lock" name="primer_apellido" value="{{$user->primer_apellido}}">
                                @else
                                <input type="text" class="input-text full-width" name="primer_apellido">
                                @endif
                            </div>
                            <label>Segundo apellido</label>
                            <div class="form-group">
                                 @if(isset($user))
                                <input type="text" class="input-text full-width lock" name="segundo_apellido" value="{{$user->segundo_apellido}}">
                                @else
                                <input type="text" class="input-text full-width" name="segundo_apellido">
                                @endif
                            </div>    
                            <label>Email</label>                   
                            <div class="form-group">
                                 @if(isset($user))
                                <input type="text" class="input-text full-width lock" name="email" value="{{$user->email}}">
                                @else
                                <input type="text" class="input-text full-width" name="email">
                                @endif
                            </div>
                            <label>Ciudad natal</label>
							<div class="form-group">
                                <div class="col-md-4">
                                 @if(isset($user))
                                <select id="paises" class="lock" name="paises" style="width:100%;">
                                        <option>{{$user->ciudad_natal->state->country->name}}</option>
                                </select>                               
                                @else
                                 <select id="paises" name="paises" style="width:100%;">
                                        <option>Selecciona un país</option>
                                </select>
                                @endif
								
                                </div>
                                <div class="col-md-4">
                                    @if(isset($user))
    								<select id="states" class="lock" name="states" style="width:100%;">
    								        <option>{{$user->ciudad_natal->state->name}}</option>
    								</select>
                                    @else
                                    <select id="states" name="states" style="width:100%;">
                                            <option>OK</option>
                                    </select>
                                    @endif
                                </div>
                                <div class="col-md-4">
                                    @if(isset($user))
    								<select id="cities"  class="lock" name="cities" style="width:100%;">
    								        <option>{{$user->ciudad_natal->name}}</option>
    								</select>
                                    @else
                                    <select id="cities" name="cities" style="width:100%;">
                                            <option>Tiene que tener una estado seleccionado</option>
                                    </select>
                                    @endif
                                </div>
							</div>
                            <label>Ciudad actual</label>
                            <div class="form-group">
                                <div class="col-md-4">
                                     @if(isset($user))
                                     <select id="paises_actual" class="lock" name="paises_actual" style="width:100%;">
                                            <option>{{$user->ciudad_actual->state->country->name}}</option>
                                    </select>                           
                                    @else
                                       <select id="paises_actual" name="paises_actual" style="width:100%;">
                                            <option>Selecciona un país</option>
                                    </select>
                                    @endif

                                </div>
                                <div class="col-md-4">
                                     @if(isset($user))
                                     <select id="states_actual"  class="lock" name="states_actual" style="width:100%;">
                                            <option>{{$user->ciudad_actual->state->name}}</option>
                                    </select>                           
                                    @else
                                       <select id="states_actual" name="states_actual" style="width:100%;">
                                            <option>Selecciona un país</option>
                                    </select>
                                    @endif
                                </div>
                                <div class="col-md-4">
                                   @if(isset($user))
                                     <select id="cities_actual" class="lock" name="cities_actual" style="width:100%;">
                                            <option>{{$user->ciudad_actual->name}}</option>
                                    </select>                           
                                    @else
                                       <select id="cities_actual" name="cities_actual" style="width:100%;">
                                            <option>Selecciona un país</option>
                                    </select>
                                    @endif
                                </div>
                            </div>
                            <label>Foto de perfil</label>
                            <div class="form-group">
                                <img src="{{ URL::asset(''.$user->foto.'') }}" style="width:180px;height:180px;">
                                <input type="file" class="form-control lock ocultar" name="foto" >
                            </div>       
                            <label>Descripcion</label>             
                             <div class="form-group">

                                <textarea class="input-text full-width" rows="5" class="descripcion" name="descripcion">
                                </textarea>
                            </div>
                            <label>Fecha de nacimiento</label>
                            <div class="form-group">
                                @if(isset($user))
                                   <div class="datepicker-wrap" style="color:black;">
                                        <p><input type="text" id="datepicker" class="input-text full-width lock" name="calendar" value="{{$user->fecha_nacimiento}}"></p>
                                    </div>                          
                                @else
                                   <div class="datepicker-wrap" style="color:black;">
                                        <p><input type="text" id="datepicker" class="input-text full-width" name="calendar"></p>
                                    </div>
                                @endif
                                 
                                </div>
                            <label>Username</label>
                             <div class="form-group">
                                 @if(isset($user))
                                <input type="text" class="input-text full-width lock"  name="username" value="{{$user->username}}">                           
                                @else
                                <input type="text" class="input-text full-width"  name="username">
                                @endif
                            </div>
                            <label>Contraseña</label>
                            <div class="form-group">
                                <input type="password" class="input-text full-width lock" name="password">
                            </div>
                            <label>Confirmar contraseña</label>
                            <div class="form-group">
                                <input type="password" class="input-text full-width lock" name="confirm_password">
                            </div>   