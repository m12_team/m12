<input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <label>Nombre</label>
                            <div class="form-group">
                                @if(isset($user))
                                    <input type="text" class="input-text full-width lock nombre letras" name="nombre" value="{{$user->nombre}}">
                                @else
                                    <input class="input-text full-width lock nombre" name="nombre" type="text">
                                @endif
                            </div>
                            <label>Primer apellido</label>
                            <div class="form-group">
                                @if(isset($user))
                                <input type="text" class="input-text full-width lock letras" name="primer_apellido" value="{{$user->primer_apellido}}">
                                @else
                                <input type="text" class="input-text full-width" name="primer_apellido" type="text">
                                @endif
                            </div>
                            <label>Segundo apellido</label>
                            <div class="form-group">
                                 @if(isset($user))
                                <input type="text" class="input-text full-width lock letras" name="segundo_apellido" value="{{$user->segundo_apellido}}">
                                @else
                                <input type="text" class="input-text full-width" name="segundo_apellido" type="text">
                                @endif
                            </div>    
                            <label>Email</label>                   
                            <div class="form-group">
                                 @if(isset($user))
                                <input type="text" class="input-text full-width lock correo" name="email" value="{{$user->email}}">
                                @else
                                <input type="text" class="input-text full-width correo" name="email" >
                                @endif
                            </div>
                            
                            <label>Foto de perfil</label>
                            <div class="form-group">
                                    @if(isset($user))
                                        <img src="{{ URL::asset(''.$user->foto.'') }}" class="lock ocultar"style="width:180px;height:180px;">
                                        <input type="file" class="form-control lock " name="foto" value="{{$user->foto}}" >                         
                                    @else
                                        <input type="file" class="form-control lock " name="foto" >
                                    @endif
                        
                            </div>                               
                            <label>Fecha de nacimiento</label>
                            <div class="form-group">
                                @if(isset($user))
                                   <div class="datepicker-wrap" style="color:black;">
                                        <p><input type="text" id="datepicker" class="input-text full-width lock" name="calendar" value="{{$user->fecha_nacimiento}}"></p>
                                    </div>                          
                                @else
                                   <div class="datepicker-wrap" style="color:black;">
                                        <p><input type="text" id="datepicker" class="input-text full-width lock" name="calendar" ></p>
                                    </div>    
                                @endif
                                 
                                </div>
                               <label>Descripcion</label>
                                <div class="form-group">
                                    @if(isset($user))
                                        @if(isset($currentPath))                                        
                                <textarea class="form-control total lock descripcion" name="descripcion">{{$user->descripcion}}                                   
                                </textarea>   
                                        @else                                         
                                <textarea class="form-control lock descripcion" name="descripcion" >                                   
                                </textarea>
                                        @endif                   
                                    @else
                                <textarea class="form-control descripcion" name="descripcion" >                                   
                                </textarea>
                                    @endif                    
                                </div>

                             <label>Ciudad natal</label>
                             <div class="form-group">
                                <div class="col-md-4">
                                     @if(isset($user))
                                        @if(isset($currentPath))
                                            <select id="paises" class="lock" name="paises" style="width:100%;">
                                                <option>{{$user->ciudad_natal->state->country->name}}</option>
                                            </select>                                           
                                           
                                        @else
                                            <select id="paises" class="lock" name="paises" style="width:100%;">
                                                <option>Selecciona un país</option>
                                            </select>                                                                       
                                        @endif
                                        @else
                                        <select id="paises" class="lock" name="paises" style="width:100%;">
                                                <option>Selecciona un país</option>
                                            </select>     
                                     @endif
                                </div>
                                <div class="col-md-4">
                                     @if(isset($user))
                                        @if(isset($currentPath))
                                            <select id="states" class="lock" name="states" style="width:100%;">
                                                <option>{{$user->ciudad_natal->state->name}}</option>
                                            </select>                                           
                                           
                                        @else
                                            <select id="states" class="lock" name="states" style="width:100%;">
                                                <option>Selecciona un país</option>
                                            </select>
                                        @endif                                                                       
                                        @else
                                            <select id="states" class="lock" name="states" style="width:100%;">
                                                <option>Selecciona un país</option>
                                            </select>
                                     @endif
                                </div>
                                <div class="col-md-4">
                                     @if(isset($user))
                                        @if(isset($currentPath))
                                            <select id="cities" class="lock ciudad_natal" name="cities" style="width:100%;">
                                                <option value="{{$user->ciudad_natal->id}}">{{$user->ciudad_natal->name}}</option>
                                            </select>                                           
                                           
                                        @else
                                            <select id="cities" class="lock ciudad_natal" name="cities" style="width:100%;">
                                                <option>Selecciona un país</option>
                                            </select>                                                                       
                                        @endif
                                        @else
                                            <select id="cities" class="lock ciudad_natal" name="cities" style="width:100%;">
                                                <option>Selecciona un país</option>
                                            </select> 
                                     @endif
                                </div>
                            </div>
                            <label>Ciudad actual</label>
                            <div class="form-group">
                                <div class="col-md-4">
                                     @if(isset($user))
                                        @if(isset($currentPath))
                                            <select id="paises_actual" class="lock" name="paises_actual" style="width:100%;">
                                                <option>{{$user->ciudad_actual->state->country->name}}</option>
                                            </select>                                           
                                           
                                        @else
                                            <select id="paises_actual" class="lock" name="paises_actual" style="width:100%;">
                                                <option>Selecciona un país</option>
                                            </select>                                                                       
                                        @endif
                                        @else
                                            <select id="paises_actual" class="lock" name="paises_actual" style="width:100%;">
                                                <option>Selecciona un país</option>
                                            </select>    
                                     @endif
                                </div>
                                <div class="col-md-4">
                                     @if(isset($user))
                                        @if(isset($currentPath))
                                            <select id="states_actual" class="lock" name="states_actual" style="width:100%;">
                                                <option>{{$user->ciudad_actual->state->name}}</option>
                                            </select>                                           
                                           
                                        @else
                                            <select id="states_actual" class="lock" name="states_actual" style="width:100%;">
                                                <option>Selecciona un país</option>
                                            </select>                                                                       
                                        @endif                                          
                                        @else

                                          <select id="states_actual" class="lock" name="states_actual" style="width:100%;">
                                                <option>Selecciona un país</option>
                                            </select>

                                     @endif
                                </div>
                                <div class="col-md-4">
                                     @if(isset($user))
                                        @if(isset($currentPath))
                                            <select id="cities_actual" class="lock ciudad_actual" name="cities_actual" style="width:100%;">

                                                <option value="{{$user->ciudad_actual->id}}">{{$user->ciudad_actual->name}}</option>
                                            </select>                                           
                                           
                                        @else
                                            <select id="cities_actual" class="lock ciudad_actual" name="cities_actual" style="width:100%;">
                                                <option>Selecciona un país</option>
                                            </select>                                                                       
                                        @endif
                                        @else
                                            <select id="cities_actual" class="lock ciudad_actual" name="cities_actual" style="width:100%;">
                                                <option>Selecciona un país</option>
                                            </select>  
                                     @endif
                                </div>
                            </div>
                            <label>Username</label>
                             <div class="form-group">
                                 @if(isset($user->ciudad_natal))
                                <input type="text" class="input-text full-width lock username"  name="username" value="{{$user->username}}">                           
                                @else
                                <input type="text" class="input-text full-width username"  name="username" >
                                @endif
                            </div>
                            <label>Contraseña</label>
                            <div class="form-group">
                                 @if(isset($user->ciudad_natal))
                                <input type="password" id="password" class="input-text full-width lock" name="password">
                                @else
                                <input type="password" id="password" class="input-text full-width lock" name="password" >
                                @endif
                            </div>
                            <label>Confirmar contraseña</label>
                            <div class="form-group">
                                <input type="password" class="input-text full-width lock" id="confirm_password" name="confirm_password" >
                            </div>  