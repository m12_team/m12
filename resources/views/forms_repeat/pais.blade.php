   <input type="hidden" name="_token" value="{{ csrf_token() }}">
<label>Nombre país</label>
<div class="form-group">
@if(isset($countries))
<input type="text" class="input-text full-width lock" name="name" value="{{$countries->name}}">
@else
<input type="text" class="input-text full-width" name="name">
@endif                        
</div>
<label>Seleccionar subregion</label>
                             <div class="form-group">
                                <div class="col-md-4">
                                     @if(isset($countries))
                                        @if(isset($currentPath))
                                            <select id="pais" class="lock" name="pais" style="width:100%;">
                                                <option value="{{$countries->subregion->id}}">{{$countries->subregion->nombre}}</option>
                                            </select>                                           
                                           
                                        @else
                                            <select id="pais" class="lock" name="pais" style="width:100%;">
                                                <option>Selecciona una subregion</option>
                                            </select>                                                                       
                                        @endif
                                        @else
                                        <select id="pais" class=" input-text full-width lock" name="pais" style="width:100%;">
                                                <option>Selecciona una subregion</option>
                                            </select>     
                                     @endif
                                </div>
                               