@extends('layouts.master')
@section('content')
    <div id="page-wrapper">
        <div class="page-title-container">
            <div class="container">
                <div class="page-title pull-left">
                    <h2 class="entry-title">Búsqueda de diarios</h2>
                </div>
                <ul class="breadcrumbs pull-right">
                    <li><a href="{{ URL::asset('/') }}">INICIO</a></li>
                    <li class="active">Búsqueda de diarios</li>
                </ul>
            </div>
        </div>
        <section id="content white">
            <div class="container">
                <div id="main">
                    <div class="row">                        
                        <div class="col-md-offset-1 col-md-10">
                            <div class="hotel-list listing-style3 hotel">
                                @if(count($diarios)>0)
                                @foreach($diarios as $diario)
                                    @foreach($diario->diarios as $diario)                                    
                                            <article class="box">
                                    <figure class="col-sm-5 col-md-4">
                                        @foreach($diario->informacionExtra as $key => $extra)
                                            @if($key==0)                     
                                            <img width="114" height="85" src="{{ URL::asset($extra->puntointeres->fotos->first()->ruta)}}" alt="">
                                              @endif
                                        @endforeach
                                    </figure>
                                    <div class="details col-sm-7 col-md-8">
                                        <a href="{{ URL::asset('consultarDiario/'.$diario->id) }}">
                                        <div>                                                
                                            <h4 class="box-title">{{$diario->nombre}}</h4> 
                                            @if($diario->creador!=NULL)                                                                           
                                            <a href="{{ URL::asset('consultarUsuario/'.$diario->creador->id) }}">Creado por {{$diario->creador->username}}</a>
                                            @else
                                            <a>El usuario se ha dado de baja del sistema</a>
                                            @endif
                                        </div>
                                        <div>
                                            <p>{{$diario->descripcion}}</p>
                                            <div>
                                                <a href="{{ URL::asset('consultarDiario/'.$diario->id) }}" class="button btn-small full-width text-center">CONSULTAR</a>
                                            </div>
                                        </div>
                                                                              


                                    </div>
                                </article>
                                    @endforeach
                                @endforeach
                                @else
                                  <div class="col-sm-6 col-md-12">
                                    <div class="alert alert-info">
                                        No hay ningún criterio relacionado con la búsqueda
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection