@extends('layouts.master')
@section('content')
    <div id="page-wrapper" >
        <div class="page-title-container">
            <div class="container">
                <div class="page-title pull-left">
                    <h2 class="entry-title">{{$diario->nombre}}</h2>
                </div>
                <ul class="breadcrumbs pull-right">
                    <li><a href="{{ URL::asset('/')}}">HOME</a></li>
                    <li class="active">{{$diario->nombre}}</li>
                </ul>
            </div>
        </div>
        <section id="content">
            <div class="container">
                <div class="row">
                    <div id="main" class="col-md-9">

                          <div class="tab-content">
                                <div id="photos-tab" class="tab-pane fade in active">
                                    <div class="photo-gallery style1" data-animation="slide" data-sync="#photos-tab .image-carousel">
                                        <ul class="slides">
                                            @foreach($diario->fotosDiario as $foto)
                                            <li><img src="{{ URL::asset($foto->ruta)}}" alt="" /></li>
                                            @endforeach
                                
                                        </ul>
                                    </div>
                                    <div class="image-carousel style1" data-animation="slide" data-item-width="70" data-item-margin="10" data-sync="#photos-tab .photo-gallery">
                                        <ul class="slides">
                                            @foreach($diario->fotosDiario as $foto)
                                            <li><img src="{{ URL::asset($foto->ruta)}}" alt="" /></li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>                                                      
                            </div>
                        
                        <div id="hotel-features" class="tab-container">
                            <ul class="tabs">
                                <li class="active"><a href="#hotel-description" data-toggle="tab">Descripcion</a></li>
                                @if(Auth::user())
                                <li><a href="#fotos-upload" data-toggle="tab">Subir fotos</a></li>
                                <li><a href="#videos-diario" data-toggle="tab">Videos Diario</a></li>
                                @endif
                            </ul>
                             <div class="forms-group">
                                @include('forms_repeat.success')                                                
                                @include('forms_repeat.error') 
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="hotel-description">
                                    <div class="intro table-wrapper full-width hidden-table-sms">
                                        <div class="col-sm-5 col-lg-4 features table-cell">
                                            <ul>                                                                                         
                                                <li><label>CREADOR:</label>@if($diario->creador!=NULL)<a href="{{ URL::asset('consultarUsuario/'.$diario->creador->id) }}">{{$diario->creador->username}}</a>@else<a>El usuario se ha dado de baja en el sistema</a>@endif</li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-7 col-lg-8 table-cell testimonials">

                                            <div class="testimonial style1">
                                                <ul class="slides ">
                                                    {{$diario->descripcion}}
                                                </ul>
                                            
                                            </div>
                                        </div>
                                    </div>
                                    <div class="long-description">
                                        <h2>Sobre {{$diario->nombre}}</h2>
                                        <p>
                                        @foreach($diario->informacionExtra as $extra)
                                        <div class="alert alert-info"><a href="{{ URL::asset('consultarPI/'.$extra->puntointeres->id)}}">{{$extra->puntointeres->nombre}}</a></div>
                                            <div class="form-group">
                                                <div class="col-md-4"><b>Fecha llegada</b> : {{$extra->fecha_llegada}}</div>
                                                <div class="col-md-4"><b>Fecha salida</b>:{{$extra->fecha_salida}}</div>
                                                @if($extra->situacion==0)
                                                <div class="col-md-4 ">No estoy</div>
                                                @endif
                                                @if($extra->situacion==1)
                                                <div class="col-md-4 ">Estoy</div>
                                                @endif
                                            </div>
                                            {{$extra->descripcion}}
                                        @endforeach
                                        </p>
                                        @foreach($videosDiario as $video)
                                               <iframe width="45%" height="315" src="{{$video->url}}" frameborder="0" allowfullscreen></iframe>                                          
                                            @endforeach
                                    </div>
                                </div>                                                                                                                   
                                <div class="tab-pane fade" id="fotos-upload">
                                    <form class="form-horizontal" role="form" action="{{ URL::asset('subir-fotos-diario') }}" method="POST" enctype="multipart/form-data" files="true">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
                                        <input type="hidden" name="identificador" value="{{$diario->id}}"> 
                                        <div class="form-group col-md-5 no-float no-padding">
                                            <h4 class="title">Tienes fotos para compartir? <small>(Opcional)</small> </h4>
                                            <div class="fileinput full-width">
                                                <input type="file" class="input-text" name="photo[]" multiple>
                                            </div>
                                        </div>                                      
                                        <div class="form-group col-md-5 no-float no-padding no-margin">
                                            <button type="submit" class="btn-large full-width">SUBIR FOTO</button>
                                        </div>
                                    </form>
                                </div>
                                <div class="tab-pane fade" id="videos-diario">
                                    <h4 class="title">Tienes vídeos para compartir? <small>(Opcional)</small> </h4>
                                    <form class="form-horizontal" role="form" action="{{ URL::asset('subir-videos-diario') }}" method="POST" enctype="multipart/form-data" files="true">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
                                        <input type="hidden" name="identificador" id="identificador" value="{{$diario->id}}">                                                                                                                    
                                        <div class="form-group col-md-5 no-float no-padding">
                                            <input type="text" class="input-text col-md-8" name="video">
                                        </div>
                                         <div class="form-group col-md-5 no-float no-padding no-margin">
                                            <button type="submit" class="btn-large full-width">SUBIR VÍDEO</button>
                                        </div>
                                    </form>
                                </div>
                                
                            </div>
                        
                        </div>
                    </div>
                    <div class="sidebar col-md-3">
                        <article class="detailed-logo">
                            <figure>   
                            @foreach($diario->informacionExtra as $key => $extra)
                                @if($key==0)                     
                                <img width="114" height="85" src="{{ URL::asset($extra->puntointeres->fotos->first()->ruta)}}" alt="">
                                  @endif
                            @endforeach
                            </figure>
                            <div class="details">
                                <h2 class="box-title">{{$diario->nombre}}</h2>   
                                        <ul class="features check">
                                            @foreach($diario->informacionExtra as $extra)
                                            <li><a href="{{ URL::asset('consultarPI/'.$extra->puntointeres->id) }}">{{$extra->puntointeres->nombre}}</a></li>
                                            @endforeach
                                        </ul>
                            </div>

                        </article>                                                              
                    </div>
                </div>
            </div>
        </section>
    </div>

    <!-- Javascript -->
    <script type="text/javascript" src="{{ URL::asset('js/jquery-1.11.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery.noconflict.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/modernizr.2.7.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery-migrate-1.2.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery.placeholder.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery-ui.1.10.4.min.js') }}"></script>
    
    <!-- Twitter Bootstrap -->
    <script type="text/javascript" src="{{ URL::asset('js/bootstrap.js') }}"></script>
    
    <!-- load revolution slider scripts -->
    <script type="text/javascript" src="{{ URL::asset('components/revolution_slider/js/jquery.themepunch.plugins.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('components/revolution_slider/js/jquery.themepunch.revolution.min.js') }}"></script>
    
    <!-- load BXSlider scripts -->
    <script type="text/javascript" src="{{ URL::asset('components/jquery.bxslider/jquery.bxslider.min.js') }}"></script>
    
    <!-- load FlexSlider scripts -->
    <script type="text/javascript" src="{{ URL::asset('components/flexslider/jquery.flexslider-min.js') }}"></script>
    
    <!-- Google Map Api -->
    <script src="http://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>
    
    <script type="text/javascript" src="{{ URL::asset('js/calendar.js') }}"></script>
    
    <!-- parallax -->
    <script type="text/javascript" src="{{ URL::asset('js/jquery.stellar.min.js') }}"></script>
    
    <!-- waypoint -->
    <script type="text/javascript" src="{{ URL::asset('js/waypoints.min.js') }}"></script>

    <!-- load page Javascript -->
    <script type="text/javascript" src="{{ URL::asset('js/theme-scripts.js') }}"></script>
    
    <script type="text/javascript">
        var latitud = $('.latitud').val();
        var longitud = $('.longitud').val();
        var valoracion = $('.valoracion').val();


        tjq('a[href="#map-tab"]').on('shown.bs.tab', function (e) {
            google.maps.event.trigger(map, "resize");
            map.setCenter(new google.maps.LatLng(latitud,longitud));
        });
        tjq('a[href="#steet-view-tab"]').on('shown.bs.tab', function (e) {
            fenway = panorama.getPosition();
            panoramaOptions.position = fenway;
            panorama = new google.maps.StreetViewPanorama(document.getElementById('steet-view-tab'), panoramaOptions);
            map.setStreetView(new google.maps.LatLng(latitud,longitud));
        });
        var map = null;
        var panorama = null;
        var fenway = new google.maps.LatLng(latitud, longitud);
        var mapOptions = {
            center: fenway,
            zoom: 12
        };
        var panoramaOptions = {
            position: fenway,
            pov: {
                heading: 34,
                pitch: 10
            }
        };
        function initialize() {
            tjq("#map-tab").height(tjq("#hotel-main-content").width() * 0.6);
            map = new google.maps.Map(document.getElementById('map-tab'), mapOptions);
            panorama = new google.maps.StreetViewPanorama(document.getElementById('steet-view-tab'), panoramaOptions);
            map.setStreetView(panorama);

        }
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
@endsection

