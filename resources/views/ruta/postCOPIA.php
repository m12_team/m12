@extends('layouts.master')
@section('content')
    <!-- Javascript -->
 
    <div id="page-wrapper">
        <div class="page-title-container">
            <div class="container">
                <div class="page-title pull-left">
                    <h2 class="entry-title">Búsqueda de rutas</h2>
                </div>
                <ul class="breadcrumbs pull-right">
                    <li><a href="#">INICIO</a></li>
                    <li class="active">Búsqueda de rutas</li>
                </ul>
            </div>
        </div>
        <section id="content">
            <div class="container">
                <div id="main">
                    <div class="row">                        
                        <div class="col-md-offset-1 col-md-10">
                            <div class="hotel-list listing-style3 hotel">
                                @foreach($rutas as $rutad)                               
										 <article class="box">
										 	@foreach($rutad->puntointeres as $ruta)                               
											<figure class="col-sm-5 col-md-4">
											<a href="{{ URL::asset('detalle') }}" class="hover-effect"><img width="270" height="160" alt="" src="{{$ruta->fotos->first()->ruta}}"></a>
											</figure>
											@endforeach
											<div class="details col-sm-7 col-md-8">
												<a href="{{ URL::asset('detalle') }}">
												<div>                                                
													<h4 class="box-title">{{$rutad->}}</h4> 
													<br>
													Creado por : AAA                                                                                                         
												</div>
												<div>
													<p>AAA</p>
													<div>
														<a class="button btn-small full-width text-center" title="" href="detalle">CONSULTAR</a>
													</div>
												</div>
																					  

											</div>
										</article>
                                @endforeach                              
                                </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection