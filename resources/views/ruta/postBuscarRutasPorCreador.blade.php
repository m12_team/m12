@extends('layouts.master')
@section('content')
    <!-- Javascript -->
 
    <div id="page-wrapper">
        <div class="page-title-container">
            <div class="container">
                <div class="page-title pull-left">
                    <h2 class="entry-title">Búsqueda de rutas</h2>
                </div>
                <ul class="breadcrumbs pull-right">
                    <li><a href="{{ URL::asset('/') }}">INICIO</a></li>
                    <li class="active">Búsqueda de rutas</li>
                </ul>
            </div>
        </div>
        <section id="content white">
            <div class="container">
                <div id="main">
                    <div class="row">                        
                        <div class="col-md-offset-1 col-md-10">
                            <div class="hotel-list listing-style3 hotel">
                                @if(count($rutas)>0)
								@foreach($rutas as $ruta)   
									@foreach($ruta->rutas as $infoRuta)								
									<article class="box">
										<figure class="col-sm-5 col-md-4">
											@foreach($infoRuta->puntointeres as $key => $imagenes)  
												@if($key == 0)
												<img width="270" height="160" alt="" src="{{$imagenes->fotos->first()->ruta}}">
												@endif
											@endforeach
										</figure>
										<div class="details col-sm-7 col-md-8">
											<a href="{{ URL::asset('consultarRutas/'.$infoRuta->id) }}">										
                                             <div>                                                
                                            <h4 class="box-title">{{$infoRuta->nombre}}</h4>                                                                                 
                                            <a href="{{ URL::asset('consultarUsuario/'.$infoRuta->creador->id)}}">Creado por {{$infoRuta->creador->username}}</a>
                                            </div>
											<div>
												<p>{{$infoRuta->descripcion}}</p>
												<div>
                                                <a href="{{ URL::asset('consultarRutas/'.$infoRuta->id) }}" class="button btn-small full-width text-center" >CONSULTAR</a>
												</div>
											</div>
										</div>
									</article>
									@endforeach
                                @endforeach 
                                @else
                                    <div class="col-sm-6 col-md-12">
                                        <div class="alert alert-info">
                                            No hay ningún criterio relacionado con la búsqueda
                                        </div>
                                    </div> 
                                @endif                          
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection