@extends('layouts.master')
@section('content')
        <div class="page-title-container">
            <div class="container">
                <div class="page-title pull-left">
                    <h2 class="entry-title">{{$ruta->nombre}} </h2>
                </div>
                <ul class="breadcrumbs pull-right">
                    <li><a href="{{ URL::asset('/')}}">HOME</a></li>
                    <li class="active">{{$ruta->nombre}}</li>
                </ul>
            </div>
        </div>
        <section id="content">     
            <div class="section">
                <div class="container">
                    <div class="tour-guide image-carousel style2 flexslider animated" data-animation="slide" data-item-width="270" data-item-margin="30" >
                        <ul class="slides image-box">
                        @if($ruta->creador!=NULL)                                                                                                                                                            
                        <div class="alert alert-info">La ruta ha sido creada por <a href="{{ URL::asset('consultarUsuario/'.$ruta->creador->id)}}">{{$ruta->creador->nombre}} {{$ruta->creador->primer_apellido}} {{$ruta->creador->segundo_apellido}}</a></div>
                        @else
                        <div class="alert alert-info">El usuario se ha dado de baja del sistema</div>
                        @endif
                        @foreach($ruta->puntointeres as $puntointeres)
                        <li>
                                <article class="box">
                                    <figure>
                                            <img width="270" height="160" alt="" src="{{ URL::asset($puntointeres->fotos->first()->ruta)}}">
                                    </figure>
                                    <div class="details">
                                        <h4>{{$puntointeres->nombre}}</h4>
                                        <a href="{{ URL::asset('consultarPI/'.$puntointeres->id)}}" class="btn btn-success">CONSULTAR PUNTO DE INTERÉS</a>
                                    </div>
                                </article>
                            </li>

                            @endforeach
                        </ul>

                    </div>
                </div>
            </div>
        </section>
@endsection

