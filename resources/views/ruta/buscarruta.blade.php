@extends('layouts.master')
@section('content')
    <div id="page-wrapper">
        <div class="page-title-container">
            <div class="container">
                <div class="page-title pull-left">
                    <h2 class="entry-title">Buscar ruta</h2>
                </div>
                <ul class="breadcrumbs pull-right">
                    <li><a href="{{ URL::asset('/') }}">INICIO</a></li>
                    <li class="active"><a href="{{ URL::asset('/buscar-ruta') }}">Buscar ruta</a></li>
                </ul>
            </div>
        </div>

        <section id="content " >
            <div class="container" >
                <div id="main">
                    <div class="row">
                        <div class="col-md-offset-2 col-md-8" >
                            <div class="">
                    <div class="alert alert-info">El fórmulario sólo puede buscar por un criterio seleccionado</div>
              <form action="{{ URL::asset('busquedaRuta') }}" class="form-horizontal" role="form" method="GET" enctype="multipart/form-data" >
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group">
                                <input type="text" class="input-text full-width" placeholder="Nombre" name="nombre_ruta" id="nombre_ruta">
                            </div>
                            <div class="form-group">
                                <input type="text" class="input-text full-width" placeholder="Nombre del usuario" name="nombre_usuario" id="nombre_usuario">
                            </div>
                               <div class="form-group">
                                <input type="text" class="input-text full-width" placeholder="Username" name="username" id="username">
                            </div>
                        
                                               
                            <button type="submit" class="full-width btn-medium">Buscar rutas</button>
                        </form>
						</div>                      
                    </div>
                </div>
            </div>
		</div>
</section>
<script src="{{ URL::asset('js/buscar-ruta.js') }}"></script>
@endsection