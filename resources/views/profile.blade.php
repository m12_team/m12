@extends('layouts.master')
@section('content')
    <div id="page-wrapper">
        
        <div class="page-title-container">
            <div class="container">
                <div class="page-title pull-left">
                    <h2 class="entry-title">Mi cuenta</h2>
                </div>
                <ul class="breadcrumbs pull-right">
                    <li><a href="{{ URL::asset('/') }}">INICIO</a></li>
                    <li class="active"><a href="{{ URL::asset('/profile') }}">Mi perfil</a></li>
                </ul>
            </div>
        </div>
        <section id="content" class="gray-area">
            <div class="container">
                <div id="main">
                    <div class="tab-container full-width-style arrow-left dashboard">
                        <ul class="tabs">
                            <li class="active"><a data-toggle="tab" href="#dashboard"><i class="soap-icon-anchor circle"></i>Principal</a></li>
                            <li class=""><a data-toggle="tab" href="#profile"><i class="soap-icon-user circle"></i>Perfil</a></li>
                            <li class=""><a data-toggle="tab" href="#pi"><i class="soap-icon-wishlist circle"></i>Puntos de interés</a></li>
                            <li class=""><a data-toggle="tab" href="#rutas"><i class="soap-icon-conference circle"></i>Rutas</a></li>
                           <!-- <li class=""><a data-toggle="tab" href="#diarios"><i class="soap-icon-conference circle"></i>Diarios</a></li>-->
                            <li class=""><a data-toggle="tab" href="#favoritos"><i class="soap-icon-conference wishlist"></i>Hist. PokeParadas</a></li>
                            <li class=""><a data-toggle="tab" href="#favoritos"><i class="soap-icon-conference wishlist"></i>Histórico GYM</a></li>
                            <li class=""><a data-toggle="tab" href="#favoritos"><i class="soap-icon-conference wishlist"></i>Histórico Pokemons</a></li>

							@if(Auth::user()->role_id==2)
							<li class=""><a data-toggle="tab" href="#anuncios"><i class="soap-icon-conference wishlist"></i>Anuncios</a></li>
							@endif                        
                        </ul>
                        <div class="tab-content">
						    
                            <div id="dashboard" class="tab-pane fade in active">
									<div class="alert alert-info">
    									@if(Auth::user()->role_id==1)
    										Usuario registrado
    									@elseif(Auth::user()->role_id==2)
    										Usuario registrado profesional
    									@else
    										Usuario administrador
    									@endif
									</div>
                                    @include('forms_repeat.perfil.main')       
                            </div>
                            <div id="profile" class="tab-pane fade">
                                <div class="view-profile">
                                    @include('forms_repeat.perfil.datosusuariosperfil') 
                                </div>
                                <div class="edit-profile">
                                    @include('forms_repeat.perfil.edit') 
                                </div>
                            </div>                          
                            <div id="pi" class="tab-pane fade">
                                @include('forms_repeat.perfil.pi') 
                            </div>
                             <div id="rutas" class="tab-pane fade">
                                @include('forms_repeat.perfil.rutas') 
                            </div>                            
                            <!--<div id="diarios" class="tab-pane fade">
                                @include('forms_repeat.perfil.diarios') 
                            </div>  -->
                            <div id="favoritos" class="tab-pane fade">
                                @include('forms_repeat.perfil.favoritos')                                
                            </div> 
                            <div id="anuncios" class="tab-pane fade">
								 @include('forms_repeat.perfil.anuncios') 
							</div>
                            <div id="mensajes" class="tab-pane fade">
                                 @include('forms_repeat.perfil.mensajes') 
                            </div>                      
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <!-- Javascript -->
    <script type="text/javascript" src="{{ URL::asset('js/jquery-1.11.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery.noconflict.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/modernizr.2.7.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery-migrate-1.2.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery.placeholder.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery-ui.1.10.4.min.js') }}"></script>
    
    <!-- Twitter Bootstrap -->
    <script type="text/javascript" src="{{ URL::asset('js/bootstrap.js') }}"></script>

    <!-- Flex Slider -->
    <script type="text/javascript" src="{{ URL::asset('components/flexslider/jquery.flexslider-min.js') }}"></script>
    
    <!-- parallax -->
    <script type="text/javascript" src="{{ URL::asset('js/jquery.stellar.min.js') }}"></script>
    
    <!-- waypoint -->
    <script type="text/javascript" src="{{ URL::asset('js/waypoints.min.js') }}"></script>

    <!-- load page Javascript -->
    <script type="text/javascript" src="{{ URL::asset('js/theme-scripts.js') }}"></script>
	
    <script type="text/javascript" src="{{ URL::asset('js/editar-rutas.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/editar-puntointeres-profile.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/countries.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/diarios.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/usuarios.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/editar-diario.js') }}"></script>

    <script type="text/javascript">

        tjq(document).ready(function() {
            tjq(".crearpuntointeres").hide();
            tjq(".crearrutas").hide();
            tjq(".mapa").hide();
            tjq(".mostrarPI").hide();
            tjq(".creardiario").hide();
            tjq(".mostrarRutas").hide();
            tjq(".mostrarRutas").hide();
            tjq(".crearDiario").hide();
			tjq(".crearanuncio").hide();
            tjq('.editar-ruta').hide();
            tjq('.editarDiario').hide();
            tjq("#profile .edit-profile-btn").click(function(e) {
                e.preventDefault();
                tjq(".view-profile").fadeOut();
                tjq(".edit-profile").fadeIn();
            });

       

             tjq(".create-profile-btn").click(function(e) {
                e.preventDefault();
                tjq(".crearpuntointeres").fadeIn();
                tjq(".listarPuntosInteres").hide();
            });

             tjq(".crear-diario").click(function(e) {

                e.preventDefault();
                tjq(".creardiario").fadeIn();
                tjq(".crearDiario").fadeIn();
            });



             tjq(".crear-rutas").click(function(e) {

                e.preventDefault();
                tjq(".crearrutas").fadeIn();
                tjq(".view-profile").fadeOut();
                tjq(".listarRutas").fadeOut();
            });
	
			tjq(".crearAnuncio").click(function(e) {

                e.preventDefault();
                tjq(".crearanuncio").fadeIn();
            });
            

        });
        tjq('a[href="#profile"]').on('shown.bs.tab', function (e) {
            tjq(".view-profile").show();
            tjq(".edit-profile").hide();
            tjq(".crearpuntointeres").hide();
            tjq(".crearrutas").hide();
        });

        tjq( ".longitud" ).click(function() {
           $( ".longitud" ).keyup(function() {
                var longitud = $('.longitud').val();
                tjq(".mapa").fadeIn();
            });
        });

        tjq( ".latitud" ).click(function() {
          $( ".latitud" ).keyup(function() {
                var latitud = $('.latitud').val();
                tjq(".mapa").fadeIn();
            });
        });

          /***Obtener puntos de interés filtrando la categoría ****/
            $('select#categoria_ruta').on('change',function(){
            var idCategoria = $(this).val();

             $.ajax({
                
                url:   'obtenerPIPorCategoria/'+idCategoria,
                type:  'get',
                success:  function (data) {
                    
                    $.each(data.puntoInteres, function(key, element) {
							
                            $.each(element.fotos, function(key, elemento2) {
                                if(key==0){
                                    $('.puntosInteresFiltrados').append('')
                                    .append('<div class="col-md-4"><article class="box"><figure><a class="hover-effect" title=""><img width="300" height="160" alt="" src="'+elemento2.ruta+'"></a></figure><div class="details"><h4 class="box-title">'+element.nombre+'</h4></div></article><a onclick="myFunction('+element.id+')" class="col-md-12 btn btn-info">Añadir a la ruta</a></div>');
                                }
                    
                        
                    });
                    });
                }
            });
        });




        $(function() {
            $( "#datepicker" ).datepicker({
                dateFormat : 'yy/mm/dd',
                changeMonth : true,
                changeYear : true,
                yearRange: '-100y:c+nn',
                maxDate: '0'
            });
        });


    </script>
 
@endsection