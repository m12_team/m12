<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="theme-color" content="#3e454c">
    
    <title>Panel de administración</title>

      <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
      <script src="//code.jquery.com/jquery-1.10.2.js"></script>
      <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <!-- Font awesome -->
    <link rel="stylesheet" href="{{ URL::asset('/admin_resources/css/font-awesome.min.css') }}">
    <!-- Sandstone Bootstrap CSS -->
    <link rel="stylesheet" href="{{ URL::asset('/admin_resources/css/bootstrap.min.css') }}">
    <!-- Bootstrap Datatables -->
    <link rel="stylesheet" href="{{ URL::asset('/admin_resources/css/dataTables.bootstrap.min.css') }}">
    <!-- Bootstrap social button library -->
    <link rel="stylesheet" href="{{ URL::asset('/admin_resources/css/bootstrap-social.css') }}">
    <!-- Bootstrap select -->
    <link rel="stylesheet" href="{{ URL::asset('/admin_resources/css/bootstrap-select.css') }}">
    <!-- Bootstrap file input -->
    <link rel="stylesheet" href="{{ URL::asset('/admin_resources/css/fileinput.min.css') }}">
    <!-- Awesome Bootstrap checkbox -->
    <link rel="stylesheet" href="{{ URL::asset('/admin_resources/css/awesome-bootstrap-checkbox.css') }}">
    <!-- Admin Stye -->
    <link rel="stylesheet" href="{{ URL::asset('/admin_resources/css/style.css') }}">
        <!-- Admin Stye -->
    <link rel="stylesheet" href="{{ URL::asset('/css/style.css') }}">
º
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
    <div class="brand clearfix">
        <a href="{{ URL::asset('/') }}" class="logo"><img src="admin_resources/img/logo.jpg" class="img-responsive" alt=""></a>
        <span class="menu-btn"><i class="fa fa-bars"></i></span>
        <ul class="ts-profile-nav">
            <li><a href="#">Help</a></li>
            <li><a href="#">Settings</a></li>
            <li class="ts-account">
                <a href="#"><img src="{{ URL::asset('../../admin_resources/img/ts-avatar.jpg') }}" class="ts-avatar hidden-side" alt=""> Account <i class="fa fa-angle-down hidden-side"></i></a>
                <ul>
                    <li><a href="#">My Account</a></li>
                    <li><a href="#">Edit Account</a></li>
                    <li><a href="#">Logout</a></li>
                </ul>
            </li>
        </ul>
    </div>

    <div class="ts-main-content">
        <nav class="ts-sidebar">
            <ul class="ts-sidebar-menu">
                <li class="ts-label">Search</li>
                <li>
                    <input type="text" class="ts-sidebar-search" placeholder="Search here...">
                </li>
                <li class="ts-label">Main</li>
                <li class="open"><a href="{{ URL::asset('/mantenimiento_usuarios') }}"><i class="fa fa-dashboard"></i>Mantenimieto de Usuarios</a></li>            
                <li class="open"><a href="{{ URL::asset('/mantenimiento_roles') }}"><i class="fa fa-dashboard"></i>Mantenimiento Roles</a></li>
                <li class="open"><a href="{{ URL::asset('/mantenimiento_entorno') }}"><i class="fa fa-dashboard"></i>Mantenimiento de Entorno</a></li>
                <li class="open"><a href="{{ URL::asset('/mantenimiento_transporte') }}"><i class="fa fa-dashboard"></i>Mantenimiento de Transporte</a></li>
                <li class="open"><a href="{{ URL::asset('/mantenimiento_categoria_pi') }}"><i class="fa fa-dashboard"></i>Mantenimiento Categorías Punto Interes</a></li>
                <li><a href="#"><i class="fa fa-desktop"></i> UI Elements</a>
                    <ul>
                        <li><a href="panels.html">Panels and Wells</a></li>
                        <li><a href="buttons.html">Buttons</a></li>
                        <li><a href="notifications.html">Notifications</a></li>
                        <li><a href="typography.html">Typography</a></li>
                        <li><a href="icon.html">Icon</a></li>
                        <li><a href="grid.html">Grid</a></li>
                    </ul>
                </li>
                <li><a href="tables.html"><i class="fa fa-table"></i> Tables</a></li>
                <li><a href="forms.html"><i class="fa fa-edit"></i> Forms</a></li>
                <li><a href="charts.html"><i class="fa fa-pie-chart"></i> Charts</a></li>
                <li><a href="#"><i class="fa fa-sitemap"></i> Multi-Level Dropdown</a>
                    <ul>
                        <li><a href="#">2nd level</a></li>
                        <li><a href="#">2nd level</a></li>
                        <li><a href="#">3rd level</a>
                            <ul>
                                <li><a href="#">3rd level</a></li>
                                <li><a href="#">3rd level</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li><a href="#"><i class="fa fa-files-o"></i> Sample Pages</a>
                    <ul>
                        <li><a href="blank.html">Blank page</a></li>
                        <li><a href="login.html">Login page</a></li>
                    </ul>
                </li>

                <!-- Account from above -->
                <ul class="ts-profile-nav">
                    <li><a href="#">Help</a></li>
                    <li><a href="#">Settings</a></li>
                    <li class="ts-account">
                        <a href="#"><img src="img/ts-avatar.jpg" class="ts-avatar hidden-side" alt=""> Account <i class="fa fa-angle-down hidden-side"></i></a>
                        <ul>
                            <li><a href="#">My Account</a></li>
                            <li><a href="#">Edit Account</a></li>
                            <li><a href="#">Logout</a></li>
                        </ul>
                    </li>
                </ul>

            </ul>
        </nav>
        <div class="content-wrapper">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        @yield('content')
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- Loading Scripts -->
    <script src="{{ URL::asset('/admin_resources/js/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('/admin_resources/js/bootstrap-select.min.js') }}"></script>
    <script src="{{ URL::asset('/admin_resources/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('/admin_resources/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('/admin_resources/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('/admin_resources/js/Chart.min.js') }}"></script>
    <script src="{{ URL::asset('/admin_resources/js/fileinput.js') }}"></script>
    <script src="{{ URL::asset('/admin_resources/js/chartData.js') }}"></script>
    <script src="{{ URL::asset('/admin_resources/js/main.js') }}"></script>
   
    <script type="text/javascript">
        $.ajax({
                url:   '../obtenerPaises',
                type:  'get',
                success:  function (data) {
                    $.each(data, function(key, element) {
                        $('#paises').append("<option value='" + element.id + "'>" + element.name + "</option>");        
                        $('#paises_actual').append("<option value='" + element.id + "'>" + element.name + "</option>");                     
                    });
                    
                }
        });

        $('select#paises').on('change',function(){
            $('#states').empty();
            var idPais = $(this).val();
             $.ajax({
                
                url:   '../obtenerStates/'+idPais,
                type:  'get',
                success:  function (data) {
                    
                    $.each(data, function(key, element) {
                        $('#states').append("<option value='" + element.id + "'>" + element.name + "</option>");
                    });
                }
            });
        });

        $('select#paises_actual').on('change',function(){
            $('#states_actual').empty();
            var idPais = $(this).val();
             $.ajax({
                
                url:   '../obtenerStates/'+idPais,
                type:  'get',
                success:  function (data) {
                    
                    $.each(data, function(key, element) {
                        $('#states_actual').append("<option value='" + element.id + "'>" + element.name + "</option>");
                    });
                }
            });
        });

      
        
        $('select#states').on('change',function(){
            $('#cities').empty();
            var idStates = $(this).val();
            $.ajax({
                url:   '../obtenerCities/'+idStates,
                type:  'get',
                success:  function (data) {
                    $.each(data, function(key, element) {   
                        $('#cities').append("<option value='" + element.id + "'>" + element.name + "</option>");
                    });
                }
            });
        });
        
        $('select#states_actual').on('change',function(){
            $('#cities_actual').empty();
            var idStates = $(this).val();
            $.ajax({
                url:   '../obtenerCities/'+idStates,
                type:  'get',
                success:  function (data) {
                    $.each(data, function(key, element) {   
                        $('#cities_actual').append("<option value='" + element.id + "'>" + element.name + "</option>");
                    });
                }
            });
        });
        
        
    });


  </script>
    <script type="text/javascript">
        tjq("#slideshow .flexslider").flexslider({
            animation: "fade",
            controlNav: false,
            animationLoop: true,
            directionNav: false,
            slideshow: true,
            slideshowSpeed: 5000
        });
    </script>

</body>

</html>