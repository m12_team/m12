<!doctype html>
<html lang="en" class="no-js">
<head>
    <!-- Page Title -->
    <title>AlwaysTravelling</title>

    <script src="{{ URL::asset('/admin_resources/js/jquery.min.js') }}"></script>
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

    <!-- Theme Styles -->
    <link rel="stylesheet" href="{{ URL::asset('/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('/css/font-awesome.min.css') }}">
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ URL::asset('/css/animate.min.css') }}">
    
    <!-- Main Style -->
    <link id="main-style" rel="stylesheet" href="{{ URL::asset('/css/style.css') }}">
    
    <!-- Updated Styles -->
    <link rel="stylesheet" href="{{ URL::asset('/css/updates.css') }}">

    <!-- Custom Styles -->
    <link rel="stylesheet" href="{{ URL::asset('/css/custom.css') }}">
    
    <!-- Responsive Styles -->
    <link rel="stylesheet" href="{{ URL::asset('/css/responsive.css') }}">
     <!-- Awesome Bootstrap checkbox -->
    <link rel="stylesheet" href="{{ URL::asset('/admin_resources/css/awesome-bootstrap-checkbox.css') }}">
    <!-- Admin Stye -->
    <link rel="stylesheet" href="{{ URL::asset('/admin_resources/css/style.css') }}">
    <!-- CSS for IE -->
    <!--[if lte IE 9]>
        <link rel="stylesheet" type="text/css" href="css/ie.css" />
    <![endif]-->
    
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script type='text/javascript' src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
      <script type='text/javascript' src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
    <![endif]-->
    <style type="text/css">
        .ui-datepicker-next ui-corner-all{
            color:black;
        }
        .ui-datepicker-prev ui-corner-all{
            color:black;
        }
        .ui-datepicker-month{
            color:black;
        }
        .ui-datepicker-year{
            color:black;
        }
    </style>
</head>

<body>
    <div class="brand clearfix">
        <a href="{{ URL::asset('/main_admin_page') }}" class="logo"><img src="{{ URL::asset('/admin_resources/img/logo.jpg') }}" class="img-responsive" alt=""></a>
        <span class="menu-btn"><i class="fa fa-bars"></i></span>
        <ul class="ts-profile-nav">
            <li><a href="#">Ayuda</a></li>
            <li><a href="#">Configuración</a></li>
            <li class="ts-account">
                <a href="#"><img src="{{ URL::asset('admin_resources/img/ts-avatar.jpg') }}" class="ts-avatar hidden-side" alt=""> Account <i class="fa fa-angle-down hidden-side"></i></a>
                <ul>
                    <li><a href="#">Mi cuenta</a></li>
                    <li><a href="#">Edit cuenta</a></li>
                    <li><a href="{{ URL::asset('logout') }}">Cerrar sesión</a></li>
                </ul>
            </li>
        </ul>
    </div>

    <div class="ts-main-content">
        <nav class="ts-sidebar">
            <ul class="ts-sidebar-menu">
                <li class="ts-label">Main</li>
                <li class="open"><a href="{{ URL::asset('/main_admin_page') }}"><i class="fa fa-dashboard"></i>Página principal</a></li>            
                <li class="open"><a href="{{ URL::asset('/mantenimiento_usuarios') }}"><i class="fa fa-dashboard"></i>Mantenimiento de Usuarios</a></li>            
                <li class="open"><a href="{{ URL::asset('/mantenimiento_roles') }}"><i class="fa fa-dashboard"></i>Mantenimiento Roles</a></li>
                <li class="open"><a href="{{ URL::asset('/mantenimiento_entorno') }}"><i class="fa fa-dashboard"></i>Mantenimiento de Entorno</a></li>
                <li class="open"><a href="{{ URL::asset('/mantenimiento_transporte') }}"><i class="fa fa-dashboard"></i>Mantenimiento de Transporte</a></li>
                <li class="open"><a href="{{ URL::asset('/mantenimiento_categoria_pi') }}"><i class="fa fa-dashboard"></i>Mantenimiento Categorías Punto Interes</a></li>           
                <li class="open"><a href="{{ URL::asset('/mantenimiento_regiones') }}"><i class="fa fa-dashboard"></i>Mantenimiento de Regiones</a></li>           
                <li class="open"><a href="{{ URL::asset('/mantenimiento_subregiones') }}"><i class="fa fa-dashboard"></i>Mantenimiento de SubRegiones</a></li>           
                <li class="open"><a href="{{ URL::asset('/mantenimiento_paises') }}"><i class="fa fa-dashboard"></i>Mantenimiento de Países</a></li>           
                <li class="open"><a href="{{ URL::asset('/mantenimiento_estados') }}"><i class="fa fa-dashboard"></i>Mantenimiento de Estados</a></li> 
                <li class="open"><a href="{{ URL::asset('/mantenimiento_pi') }}"><i class="fa fa-dashboard"></i>Mantenimiento de Puntos de interés</a></li>
                <li class="open"><a href="{{ URL::asset('/mantenimiento_rutas') }}"><i class="fa fa-dashboard"></i>Mantenimiento de Rutas</a></li>
                <li class="open"><a href="{{ URL::asset('/mantenimiento_diarios') }}"><i class="fa fa-dashboard"></i>Mantenimiento de Diarios</a></li>
                <li class="open"><a href="{{ URL::asset('/mantenimiento_videos') }}"><i class="fa fa-dashboard"></i>Mantenimiento de Vídeos</a></li>                                    
                <li class="open"><a href="{{ URL::asset('/mantenimiento_comentarios') }}"><i class="fa fa-dashboard"></i>Mantenimiento de Comentarios PI</a></li>                  

                <!--<li class="open"><a href="{{ URL::asset('/mantenimiento_ciudades') }}"><i class="fa fa-dashboard"></i>Mantenimiento de Ciudades</a></li>           -->

                <ul class="ts-profile-nav">
                    <li><a href="#">Help</a></li>
                    <li><a href="#">Settings</a></li>
                    <li class="ts-account">
                        <a href="#"><img src="img/ts-avatar.jpg" class="ts-avatar hidden-side" alt=""> Account <i class="fa fa-angle-down hidden-side"></i></a>
                        <ul>
                            <li><a href="#">My Account</a></li>
                            <li><a href="#">Edit Account</a></li>
                            <li><a href="#">Logout</a></li>
                        </ul>
                    </li>
                </ul>

            </ul>
        </nav>
        <div class="content-wrapper">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        @yield('content')
                    </div>
                </div>

            </div>
        </div>
    </div>

    <script src="{{ URL::asset('/admin_resources/js/jquery.dataTables.min.js') }}"></script>

     <!-- Loading Scripts -->
    <script src="{{ URL::asset('/admin_resources/js/bootstrap-select.min.js') }}"></script>
    <script src="{{ URL::asset('/admin_resources/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('/admin_resources/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('/admin_resources/js/Chart.min.js') }}"></script>
    <script src="{{ URL::asset('/admin_resources/js/fileinput.js') }}"></script>
    <script src="{{ URL::asset('/admin_resources/js/chartData.js') }}"></script>
    <script src="{{ URL::asset('/admin_resources/js/main.js') }}"></script>

    <script type="text/javascript">
        $.ajax({
                url:   '../obtenerPaises',
                type:  'get',
                success:  function (data) {
                    $.each(data, function(key, element) {
                        $('#paises').append("<option value='" + element.id + "'>" + element.name + "</option>");        
                        $('#paises_actual').append("<option value='" + element.id + "'>" + element.name + "</option>");                     
                    });
                    
                }
        });

        $('select#paises').on('change',function(){
            $('#states').empty();
            var idPais = $(this).val();
             $.ajax({
                
                url:   'obtenerStates/'+idPais,
                type:  'get',
                success:  function (data) {
                    
                    $.each(data, function(key, element) {
                        $('#states').append("<option value='" + element.id + "'>" + element.name + "</option>");
                    });
                }
            });
        });

        $('select#paises_actual').on('change',function(){
            $('#states_actual').empty();
            var idPais = $(this).val();
             $.ajax({
                
                url:   'obtenerStates/'+idPais,
                type:  'get',
                success:  function (data) {
                    
                    $.each(data, function(key, element) {
                        $('#states_actual').append("<option value='" + element.id + "'>" + element.name + "</option>");
                    });
                }
            });
        });

      
        
        $('select#states').on('change',function(){
            $('#cities').empty();
            var idStates = $(this).val();
            $.ajax({
                url:   'obtenerCities/'+idStates,
                type:  'get',
                success:  function (data) {
                    $.each(data, function(key, element) {   
                        $('#cities').append("<option value='" + element.id + "'>" + element.name + "</option>");
                    });
                }
            });
        });
        
        $('select#states_actual').on('change',function(){
            $('#cities_actual').empty();
            var idStates = $(this).val();
            $.ajax({
                url:   'obtenerCities/'+idStates,
                type:  'get',
                success:  function (data) {
                    $.each(data, function(key, element) {   
                        $('#cities_actual').append("<option value='" + element.id + "'>" + element.name + "</option>");
                    });
                }
            });
        });
        
        $(function() {
        $( "#datepicker" ).datepicker({
            dateFormat : 'dd/mm/yy',
            changeMonth : true,
            changeYear : true,
            yearRange: '-100y:c+nn',
            maxDate: '0'
        });
    });


  </script>
    <script type="text/javascript">
        tjq("#slideshow .flexslider").flexslider({
            animation: "fade",
            controlNav: false,
            animationLoop: true,
            directionNav: false,
            slideshow: true,
            slideshowSpeed: 5000
        });
    </script>

</body>

</html>