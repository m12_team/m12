<!DOCTYPE html>
<!--[if IE 8]>          <html class="ie ie8"> <![endif]-->
<!--[if IE 9]>          <html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->  <html> <!--<![endif]-->
<head>
    <!-- Page Title -->
    <title>PokemonGO - Comunidad</title>
    
    <!-- Meta Tags -->
    <meta charset="utf-8">
    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Aplicación web de ayuda para los usuarios que jueguen a PokemonGO">
    <meta name="author" content="Juan Fco Fernández Herreros">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Theme Styles -->
    <link rel="stylesheet" href="{{ URL::asset('css/validarformularios.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/estilosExtras.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/font-awesome.min.css') }}">
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{ URL::asset('css/animate.min.css') }}">
    <!-- Main Style -->
    <link id="main-style" rel="stylesheet" href="{{ URL::asset('css/style.css') }}"> 
    <!-- Updated Styles -->
    <link rel="stylesheet" href="{{ URL::asset('css/updates.css') }}">

    <!-- Responsive Styles -->
    <link rel="stylesheet" href="{{ URL::asset('css/responsive.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('js/jquery-ui.css') }}">
    <script src="{{ URL::asset('js/jquery-1.10.2.js') }}"></script>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
     <script src="{{ URL::asset('js/jquery.validate.js') }}"></script>
    <script src="{{ URL::asset('js/validaciones/additional-methods.min.js') }}"></script>
	<script src="{{ URL::asset('js/validaciones/registrousuario.js') }}"></script>
     <script src="{{ URL::asset('js/validaciones/login.js') }}"></script>
     <script src="{{ URL::asset('js/validaciones/opinionform.js') }}"></script>
     <script src="{{ URL::asset('js/validaciones/pregunta.js') }}"></script>
     <script src="{{ URL::asset('js/validaciones/respuesta.js') }}"></script>

</head>

<body>
    <header id="header" class="navbar-FIXED-top">
            <div class="topnav hidden-xs">
                <div class="container">
                    <ul class="quick-menu pull-right">
                         @if (Auth::guest())
                     <li><a href="{{ URL::asset('/iniciar-sesion') }}" >Iniciar Sesión</a></li>
                        <li><a href="{{ URL::asset('/registro-de-usuario') }}">Registro de usuario</a></li>
                            @else
                                <li><a href="#">{{ Auth::user()->username }}</a></li>
                                    <li><a href="{{ URL::asset('/profile') }}">MY ACCOUNT</a></li>
                                <li><a href="{{ URL::asset('/logout') }}">Logout</a></li>
                        @endif
                    </ul>
                </div>
            </div>
           
            <div class="main-header">
                
                <a href="#mobile-menu-01" data-toggle="collapse" class="mobile-menu-toggle">
                    Mobile Menu Toggle
                </a>

                <div class="container">
                    <h1 class=" navbar-brand">
                        <a href="{{ URL::asset('/') }}" title="PokemonGO - Comunidad">
                            <img src="{{ URL::asset('/images/logo.png') }}" alt="PokemonGO - Comunidad"/>
                        </a>
                    </h1>
                    
                    <nav id="main-menu" role="navigation">
                        <ul class="menu">
                            <li class="menu-item-has-children">
                                <a href="{{ URL::asset('/') }}">Inicio</a>
                                
                            </li>
                            <li class="menu-item-has-children">
                                <a href="{{ URL::asset('/buscar-pokeparadas') }}">Pokeparadas</a>

                            </li>
                            <li class="menu-item-has-children">
                                <a href="{{ URL::asset('/buscar-gym') }}">GYM</a>

                            </li>
                            <li class="menu-item-has-children">
                                <a href="{{ URL::asset('/buscar-pokemon') }}">Pokemons</a>

                            </li>
                            <li class="menu-item-has-children">
                                <a href="{{ URL::asset('/buscar-ruta') }}">Rutas</a>
                               
                            </li>
                             <li class="menu-item-has-children">
                                <a href="{{ URL::asset('/buscar-usuarios') }}">Usuarios</a>
                               
                            </li>
                            <li class="menu-item-has-children">
                                <a href="{{ URL::asset('/preguntas-frequentes') }}">FAQ</a>
                               
                            </li>                               
                        </ul>
                    </nav>
                </div>
                
                <nav id="mobile-menu-01" class="mobile-menu collapse">Pokemons
                    <ul id="mobile-primary-menu" class="menu">
                        <li>
                            <a href="{{ URL::asset('/') }}">Inicio</a>                            
                        </li>
                        <li>
                            <a href="{{ URL::asset('/buscar-pokeparadas') }}">Pokeparadas</a>                            
                        </li>
                        <li>
                            <a href="{{ URL::asset('/buscar-gimnasios') }}">GYM</a>                            
                        </li>
                        <li>
                            <a href="{{ URL::asset('/buscar-pokemon') }}">Pokemons</a>                            
                        </li>
                        <li>
                            <a href="{{ URL::asset('/buscar-ruta') }}">Rutas</a>                            
                        </li>

                        <li>
                            <a href="{{ URL::asset('/buscar-usuarios') }}">Usuarios</a>                            
                        </li>
                        <li class="menu-item-has-children">
                                <a href="{{ URL::asset('/preguntas-frequentes') }}">FAQ</a>                            
                        </li>   
                        <li>
                            <a href="{{ URL::asset('/registro-de-usuario') }}">Registro de usuario</a>
                            
                        </li>
                        <li >
                            <a href="{{ URL::asset('/registro-de-usuario-profesional') }}">Registro de usuario profesional</a>                    
                        </li>
                        <li >
                            <a href="{{ URL::asset('/iniciar-sesion') }}">Iniciar sesión</a>                        
                        </li>
                    </ul>
                    
                </nav>
            </div>

        </header>
    <div id="page-wrapper">
        
        @yield("content")

    </div>

    <!-- Javascript -->
    <script type="text/javascript" src="{{ URL::asset('js/jquery-1.11.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery.noconflict.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/modernizr.2.7.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery-migrate-1.2.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery.placeholder.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery-ui.1.10.4.min.js') }}"></script>
    
    <!-- Twitter Bootstrap -->
    <script type="text/javascript" src="{{ URL::asset('js/bootstrap.js') }}"></script>

    <!-- Flex Slider -->
    <script type="text/javascript" src="{{ URL::asset('components/flexslider/jquery.flexslider-min.js') }}"></script>
    
    <!-- parallax -->
    <script type="text/javascript" src="{{ URL::asset('js/jquery.stellar.min.js') }}"></script>
    
    <!-- waypoint -->
    <script type="text/javascript" src="{{ URL::asset('js/waypoints.min.js') }}"></script>

    <!-- load page Javascript -->
    <script type="text/javascript" src="{{ URL::asset('js/theme-scripts.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/scripts.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/slide.js') }}"></script>
    <script type="text/javascript">

        $('.descripcion').val('');
        $('.ciudad_actual').val('');
        $('.ciudad_natal').val('');

        $.ajax({
                url:   'obtenerPaises',
                type:  'get',
                success:  function (data) {
                    $.each(data, function(key, element) {
                        $('#paises').append("<option value='" + element.id + "'>" + element.name + "</option>");        
                        $('#paises_actual').append("<option value='" + element.id + "'>" + element.name + "</option>");                     
                    });
                    
                }
        });

        $('select#paises').on('change',function(){
            $('#states').empty();
            var idPais = $(this).val();
             $.ajax({
                
                url:   'obtenerStates/'+idPais,
                type:  'get',
                success:  function (data) {
                    
                    $.each(data, function(key, element) {
                        $('#states').append("<option value='" + element.id + "'>" + element.name + "</option>");
                    });
                }
            });
        });

        $('select#paises_actual').on('change',function(){
            $('#states_actual').empty();
            var idPais = $(this).val();
             $.ajax({
                
                url:   'obtenerStates/'+idPais,
                type:  'get',
                success:  function (data) {
                    
                    $.each(data, function(key, element) {
                        $('#states_actual').append("<option value='" + element.id + "'>" + element.name + "</option>");
                    });
                }
            });
        });

      
        
        $('select#states').on('change',function(){
            $('#cities').empty();
            var idStates = $(this).val();
            $.ajax({
                url:   'obtenerCities/'+idStates,
                type:  'get',
                success:  function (data) {
                    $.each(data, function(key, element) {   
                        $('#cities').append("<option value='" + element.id + "'>" + element.name + "</option>");
                    });
                }
            });
        });
        
        $('select#states_actual').on('change',function(){
            $('#cities_actual').empty();
            var idStates = $(this).val();
            $.ajax({
                url:   'obtenerCities/'+idStates,
                type:  'get',
                success:  function (data) {
                    $.each(data, function(key, element) {   
                        $('#cities_actual').append("<option value='" + element.id + "'>" + element.name + "</option>");
                    });
                }
            });
        });
    </script>
    <!--<script src="{{ URL::asset('js/jquery.validate.js') }}"></script>
    <script src="{{ URL::asset('js/validaciones/additional-methods.min.js') }}"></script>
    <script src="{{ URL::asset('js/usuarios/registro.js') }}"></script>
    <script src="{{ URL::asset('js/validaciones/registrousuario.js') }}"></script>
    <script src="{{ URL::asset('js/validaciones/puntointeres.js') }}"></script>
    <script src="{{ URL::asset('js/validaciones/crearrutas.js') }}"></script>
    <script src="{{ URL::asset('js/validaciones/creardiarios.js') }}"></script>
    <script src="{{ URL::asset('js/validaciones/editardatosusuarios.js') }}"></script>
    <script src="{{ URL::asset('js/validaciones/login.js') }}"></script>
    <script src="{{ URL::asset('js/validaciones/pregunta.js') }}"></script>
    <script src="{{ URL::asset('js/validaciones/respuesta.js') }}"></script>
    <script src="{{ URL::asset('js/validaciones/opinionform.js') }}"></script>
    <script src="{{ URL::asset('js/validaciones/subirFotosPI.js') }}"></script>-->
</body>
</html>
