@extends('layouts.master')
@section('content')
            <div id="slideshow" class="slideshow-bg full-screen">
                <div class="flexslider">
                    <ul class="slides">
                        <li>
                            <div class="slidebg" style="background-image: url({{ URL::asset('images/slider/img_1.jpg') }});"></div>
                        </li>
                        <li>
                            <div class="slidebg" style="background-image: url({{ URL::asset('images/slider/img_2.jpg') }});"></div>
                        </li>
                        <li>
                            <div class="slidebg" style="background-image: url({{ URL::asset('images/slider/img_3.jpg') }});"></div>
                        </li>
                        <li>
                            <div class="slidebg" style="background-image: url({{ URL::asset('images/slider/img_4.jpg') }});"></div>
                        </li>
                        <li>
                            <div class="slidebg" style="background-image: url({{ URL::asset('images/slider/img_5.jpg') }});"></div>
                        </li>
                    </ul>
                </div>                
            </div>
            <div class="section white-bg">
			<div class="container">
				<button class="col-xs-12 col-md-2" onclick="getLocation()">Mostrar PokeParadas cercanas</button>
				<p id="demo"></p>
			</div>			
                <div class="container">
                    <div class="text-center description block">
                        <h1>PokeParadas</h1>
                    </div>
                    <div class="tour-packages row add-clearfix image-box">
                        @if(count($pokeparadas)>0)

                        @foreach($pokeparadas as $pI)
                        <a href="{{ URL::asset('consultarPI/'.$pI->id)}}"><div class="col-xs-12 col-sm-6 col-md-4">
                            <article class="box animated" data-animation-type="fadeInLeft">
                                <figure>
                                    <img style="height:200px;"src="{{$pI->fotos->first()->ruta}}" alt="">
                                    <figcaption>
                                        <span class="price">{{$pI->precio}}</span>
                                        <h2 class="caption-title">{{$pI->nombre}}</h2>
                                    </figcaption>
                                </figure>
                            </article>
                        </div></a>
                        @endforeach
                        @else
                            <div class="col-sm-6 col-md-12">
                                <div class="alert alert-info">
                                    No hay ningún punto de interés
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
				  <div class="container">
                    <div class="text-center description block">
                        <h1>Gimnasios</h1>
                    </div>
                    <div class="tour-packages row add-clearfix image-box">
                        @if(count($gimnasios)>0)

                        @foreach($gimnasios as $pi)
                        <a href="{{ URL::asset('consultarPokemon/'.$pi->id)}}"><div class="col-xs-12 col-sm-6 col-md-4">
                            <article class="box animated" data-animation-type="fadeInLeft">
                                <figure>
                                    <img style="height:200px;"src="{{$pi->fotos->first()->ruta}}" alt="">
                                    <figcaption>
                                        <h2 class="caption-title">{{$pi->nombre}}</h2>
                                    </figcaption>
                                </figure>
                            </article>
                        </div></a>
                        @endforeach
                        @else
                            <div class="col-sm-6 col-md-12">
                                <div class="alert alert-info">
                                    No hay ningún punto de interés
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
                 <div class="container">
                    <div class="text-center description block">
                        <h1>Pokemon</h1>
                    </div>
                    <div class="tour-packages row add-clearfix image-box">
                        @if(count($pokemons)>0)

                        @foreach($pokemons as $pokemon)
                        <a href="{{ URL::asset('consultarPokemon/'.$pokemon->id)}}"><div class="col-xs-12 col-sm-6 col-md-3">
                            <article class="box animated" data-animation-type="fadeInLeft">
                                <figure>
                                    <img style="height:200px;"src="{{$pokemon->fotos->first()->ruta}}" alt="">
                                    <figcaption>
                                        <h2 class="caption-title">{{$pokemon->name}}</h2>
                                    </figcaption>
                                </figure>
                            </article>
                        </div></a>
                        @endforeach
                        @else
                            <div class="col-sm-6 col-md-12">
                                <div class="alert alert-info">
                                    No hay ningún punto de interés
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="section">
                <div class="container">
                    <h2>Rutas</h2>
                    <div class="tour-guide image-carousel style2 flexslider animated" data-animation="slide" data-item-width="270" data-item-margin="30" data-animation-type="fadeInUp">
                        <ul class="slides image-box">
                            @if(count($rutas)>0)
                            @foreach($rutas as $ruta)
                            <a href="{{ URL::asset('consultarRutas/'.$ruta->id)}}">
                            <li>
                                <article class="box">
                                    <figure>
                                        @foreach($ruta->puntointeres as $key => $value)  
                                        @if($key == 0) 
                                            <img style="height:160px;" alt="" src="{{$value->fotos->first()->ruta}}">
                                        @endif
                                        @endforeach
                                    </figure>
                                    <div class="details">
                                        <h4>{{$ruta->nombre}}</h4>
                                        <p>{{$ruta->descripcion}}</p>
                                    </div>
                                </article></a>
                            </li>
                            @endforeach
                            @else
                            <div class="col-xs-12 col-sm-6 col-md-12">
                                <div class="alert alert-info">
                                    No hay ninguna ruta
                                </div>
                            </div>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
            
			<script>
var x = document.getElementById("demo");

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
}

function showPosition(position) {
	window.location.replace("http://localhost/m12local/public/consultarPokeParada/"+ position.coords.latitude +"/"+ position.coords.longitude);
}
</script>
@endsection
