<form action="{{ URL::asset('/login') }}" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="form-group">
                        <input type="text" class="input-text full-width" placeholder="email address" name="email">
                    </div>
                    <div class="form-group">
                        <input type="password" class="input-text full-width" placeholder="password" name="password">
                    </div>
                    <div class="form-group">
                        <a href="#" class="forgot-password pull-right">Forgot password?</a>
                        <div class="checkbox checkbox-inline">
                            <label>
                                <input type="checkbox"> Remember me
                            </label>
                        </div>
                    </div>
					<button type="submit" class="full-width btn-medium">INICIAR SESIÓN</button>
                </form>