
@extends('layouts.master')
@section('content')

    <div id="page-wrapper" >
        <div class="page-title-container">
            <div class="container">
                <div class="page-title pull-left">
                    <h2 class="entry-title">Foro</h2>
                </div>
                <ul class="breadcrumbs pull-right">
                    <li><a href="{{ URL::asset('/')}}">INICIO</a></li>
                    <li class="active">Foro</li>
                </ul>
            </div>
        </div>
        <section id="content">
            <div class="container">
                <div class="row">
                    <div id="main" class="col-md-9">                        
                        <div id="hotel-features" class="tab-container">
                            <ul class="tabs">
                                <li class="active"><a  data-toggle="tab">Categorías puntos de interés</a></li>
                            </ul>
                            <div class="forms-group">
                                @include('forms_repeat.success')                                                
                                @include('forms_repeat.error') 
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="hotel-description">
                                    <div class="intro table-wrapper full-width hidden-table-sms">
                                        <div class="col-sm-5 col-lg-4 features table-cell">
                                            <ul>
                                                @foreach($categorias as $categoria)
                                                	<p>
													<div class="alert alert-info"><a href="{{ URL::asset('piCategoria/'.$categoria->id)}}">{{$categoria->nombre}}</a></div>
												@endforeach                                                                                                                                                                       
                                            </ul>
                                        </div>
                                    </div>
                                </div>                                                          
                                
                            </div>
                        
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <!-- Javascript -->
    <script type="text/javascript" src="{{ URL::asset('js/jquery-1.11.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery.noconflict.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/modernizr.2.7.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery-migrate-1.2.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery.placeholder.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery-ui.1.10.4.min.js') }}"></script>
    
    <!-- Twitter Bootstrap -->
    <script type="text/javascript" src="{{ URL::asset('js/bootstrap.js') }}"></script>

    
@endsection

