@extends('layouts.master')
@section('content')
  <div id="page-wrapper" >
        <div class="page-title-container">
            <div class="container">
                <div class="page-title pull-left">
                    <h2 class="entry-title">Respuesta</h2>
                </div>
                <ul class="breadcrumbs pull-right">
                    <li><a href="{{ URL::asset('/')}}">INICIO</a></li>
                    <li class="active">Respuesta</li>
                </ul>
            </div>
        </div>
    </div>
     <section id="content">
            <div class="container">
                <div class="row">
                    <div id="main" class="col-md-9">                        
                        <div id="hotel-features" class="tab-container">
                            <ul class="tabs">
                                <li class="active"><a href="#hotel-description" data-toggle="tab">Preguntas realizadas</a></li>
                                 @if(Auth::check() && Auth::user()->role_id==2)
                                <li><a href="#respuesta" data-toggle="tab">Escribir una respuesta</a></li>
                                @endif
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="hotel-description">
                                    <div class="intro table-wrapper full-width hidden-table-sms">
                                        <div class="col-sm-7 col-lg-8 table-cell testimonials">
                                           
                                            <div class="testimonial style1">
                                                <ul class="slides ">
                                                	La pregunta ha sido creada por <a href="{{ URL::asset('consultarUsuario/'.$pregunta->id)}}">{{$pregunta->creador->username}}</a> a las {{$pregunta->created_at}}
                                                	<p>
                                                    {{$pregunta->contenido}}
                                                </ul>
                                            
                                            </div>
                                        </div>
                                    </div>
                                    <div class="intro table-wrapper full-width hidden-table-sms">
                                        <div class="col-sm-7 col-lg-8 table-cell testimonials">
                                           
                                            <div class="testimonial style1">
                                            	@foreach($respuesta as $res)
                                                <ul class="slides ">
                                                	La respuesta ha sido creada por <a href="{{ URL::asset('consultarUsuario/'.$pregunta->id)}}">{{$res->creador->username}}</a> a las 25/05/2015
                                                	<p>
                                               		{{$res->contenido}}
                                               		<p>
                                               		{{$res->valoracion}}
                                               		<p>
                                               		<a class="btn btn-info" href="{{ URL::asset('valorarRespuesta/'.$res->id)}}">+</a>
													<p>
													<a class="btn btn-info" href="{{ URL::asset('reducirRespuesta/'.$res->id)}}">-</a>
                                                </ul>
                                                @endforeach                                            
                                            </div>
                                        </div>
                                    </div>
                                </div>  
                                <div class="tab-pane fade" id="respuesta">
                                    <form id="respuestaForm" class="form-horizontal" role="form" action="{{ URL::asset('crearRespuesta')}}" method="POST" enctype="multipart/form-data">
                                        <input type="hidden" name="punto_interes" value="{{$pi->id}}">
                                        <input type="hidden" name="pregunta_id" value="{{$pregunta->id}}">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                        <div class="form-group">
                                            <h4 class="title">Contenido</h4>
                                            <textarea class="input-text full-width" name="respuesta" rows="5"></textarea>
                                        </div>                                                                                                                                                   
                                        <div class="form-group col-md-5 no-float no-padding no-margin">
                                            <button type="submit" class="btn-large full-width">ENVIAR RESPUESTA</button>
                                        </div>
                                    </form>
                                    
                                </div>
                        
                            </div>
                        
                        </div>
                    </div>
                    <div class="sidebar col-md-3">
                        <article class="detailed-logo">
                             <figure>
                                                 @foreach($pi->fotos as $key => $foto)  
                                                    @if($key==0)                               
                                                        <img src="{{ URL::asset($foto->ruta)}}" alt="">
                                                    @endif
                                                @endforeach
                                            </figure>
                            <div class="details">
                                <h2 class="box-title">{{$pi->nombre}}<small><i class="soap-icon-departure yellow-color"></i></small></h2>
                                <span class="price clearfix">
                                    <small class="pull-left">PRECIO</small>
                                    <span class="pull-right">{{$pi->precio}}</span>
                                </span>
                                <div class="feedback clearfix">
                                    <small class="pull-left">TOTAL</small>
                                    <span class="review pull-right">0 comentarios</span>
                                </div>
                                <p class="description">Descripcion</p>
                                <li><label>ENTORNO : </label>{{$pi->entorno->nombre}}</li>
                                <li><label>TRANSPORTE : </label>{{$pi->transporte->nombre}}</li>   
                            </div>
                        </article>                                                                                    
                    </div>
                </div>
            </div>
        </section>
@endsection


