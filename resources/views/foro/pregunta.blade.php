@extends('layouts.master')
@section('content')
  <div id="page-wrapper" >
        <div class="page-title-container">
            <div class="container">
                <div class="page-title pull-left">
                    <h2 class="entry-title">Pregunta</h2>
                </div>
                <ul class="breadcrumbs pull-right">
                    <li><a href="{{ URL::asset('/')}}">INICIO</a></li>
                    <li class="active">Pregunta</li>
                </ul>
            </div>
        </div>
    </div>
    <section id="content">
            <div class="container">
                <div class="row">
                    <div id="main" class="col-md-9">                        
                        <div id="hotel-features" class="tab-container">
                            <ul class="tabs">
                                <li class="active"><a href="#hotel-description" data-toggle="tab">Preguntas realizadas</a></li>
                                 @if(Auth::check())
                                <li><a href="#pregunta" data-toggle="tab">Escribir una pregunta</a></li>
                                @endif
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="hotel-description">
                                    <div class="intro table-wrapper full-width hidden-table-sms">
                                        <div class="col-sm-7 col-lg-8 table-cell testimonials">
                                           
                                            <div class="testimonial style1">
                                                <ul class="slides ">
                                                    @foreach($pregunta as $preg)
                                                        <p>
                                                        <label>Creado por </label> {{$preg->creador->username}} <label>el día </label> {{ $preg->created_at->format('H-m-Y') }}
                                                        <p>
                                                        <div class="alert alert-info"><a href="{{ URL::asset('respuesta/'.$preg->id)}}">{{$preg->contenido}}</a></div>
                                                    @endforeach
                                                </ul>
                                            
                                            </div>
                                        </div>
                                    </div>
                                </div>                                                         
                                <div class="tab-pane fade" id="pregunta">
                                    <form id="preguntaForm" class="form-horizontal" role="form" action="{{ URL::asset('crearPregunta')}}" method="POST" enctype="multipart/form-data">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="identificador" value="{{$pi->id}}">
                                        <div class="form-group">
                                            <h4 class="title">Contenido</h4>
                                            <textarea class="input-text full-width" name="pregunta" rows="5"></textarea>
                                        </div>                                                                                                                                                   
                                        <div class="form-group col-md-5 no-float no-padding no-margin">
                                            <button type="submit" class="btn-large full-width">ENVIAR PREGUNTA</button>
                                        </div>
                                    </form>
                                    
                                </div>                            
                            </div>
                        
                        </div>
                    </div>
                    <div class="sidebar col-md-3">
                        <article class="detailed-logo">
                             <figure>
                                                 @foreach($pi->fotos as $key => $foto)  
                                                    @if($key==0)                               
                                                        <img src="{{ URL::asset($foto->ruta)}}" alt="">
                                                    @endif
                                                @endforeach
                                            </figure>
                            <div class="details">
                                <h2 class="box-title">{{$pi->nombre}}<small><i class="soap-icon-departure yellow-color"></i></small></h2>
                                <span class="price clearfix">
                                    <small class="pull-left">PRECIO</small>
                                    <span class="pull-right">{{$pi->precio}}</span>
                                </span>
                                <div class="feedback clearfix">
                                    <small class="pull-left">TOTAL</small>
                                    <span class="review pull-right">0 comentarios</span>
                                </div>
                                <p class="description">Descripcion</p>
                                <li><label>ENTORNO : </label>{{$pi->entorno->nombre}}</li>
                                <li><label>TRANSPORTE : </label>{{$pi->transporte->nombre}}</li>   
                            </div>
                        </article>                                                                                    
                    </div>
                </div>
            </div>
        </section>
@endsection
