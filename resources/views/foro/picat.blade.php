@extends('layouts.master')
@section('content')

    <div id="page-wrapper" >
        <div class="page-title-container">
            <div class="container">
                <div class="page-title pull-left">
                    <h2 class="entry-title">Foro</h2>
                </div>
                <ul class="breadcrumbs pull-right">
                    <li><a href="{{ URL::asset('/')}}">INICIO</a></li>
                    <li class="active">Foro</li>
                </ul>
            </div>
        </div>
        <section id="content">
            <div class="container">
                <div class="row">
                    <div id="main" class="col-md-9">                        
                        <div id="hotel-features" class="tab-container">
                            <ul class="tabs">
                                <li class="active"><a  data-toggle="tab">Puntos de interés de la categoría</a></li>
                            </ul>
                            <div class="forms-group">
                                @include('forms_repeat.success')                                                
                                @include('forms_repeat.error') 
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="hotel-description">
                                    <div class="intro table-wrapper full-width hidden-table-sms">
                                        <div class="col-sm-5 col-lg-4 features table-cell">
                                            <ul>
												@foreach($puntosinteres as $pi)
         	                                       	<p>
													<div class="alert alert-info"><a href="{{ URL::asset('postsPorPuntoInteres/'.$pi->id)}}">{{$pi->nombre}}</a></div>
												@endforeach                                                                                                                                                                       
                                            </ul>
                                        </div>
                                    </div>
                                </div>                                                          
                                
                            </div>
                        
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
   
@endsection

