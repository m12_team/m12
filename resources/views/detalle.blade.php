@extends('layouts.master')
@section('content')
<input type="hidden" class="latitud" value="{{$puntointeres->latitud}}">
<input type="hidden" class="longitud" value="{{$puntointeres->longitud}}">
<input type="hidden" class="valoracion" value="{{$valoracion}}">

    <div id="page-wrapper" >
        <div class="page-title-container">
            <div class="container">
                <div class="page-title pull-left">
                    <h2 class="entry-title">{{$puntointeres->nombre}}</h2>
                </div>
                <ul class="breadcrumbs pull-right">
                    <li><a href="{{ URL::asset('/')}}">HOME</a></li>
                    <li class="active">{{$puntointeres->nombre}}</li>
                </ul>
            </div>
        </div>
        <section id="content">
            <div class="container">
                <div class="row">
                    <div id="main" class="col-md-9">
                        <div class="tab-container style1" id="hotel-main-content">
                            <ul class="tabs">
                                <li class="active"><a data-toggle="tab" href="#photos-tab">Fotos</a></li>
                                <li><a data-toggle="tab" href="#map-tab">map</a></li>
                                <li><a data-toggle="tab" href="#steet-view-tab">street view</a></li>
                                <li class="pull-right"><a href="{{ URL::asset('postsPorPuntoInteres/'.$puntointeres->id)}}" class="button btn-small yellow-bg white-color">FORO PUNTO DE INTERÉS</a></li>
                            </ul>
                            <div class="tab-content">
                                <div id="photos-tab" class="tab-pane fade in active">
                                    <div class="photo-gallery style1" data-animation="slide" data-sync="#photos-tab .image-carousel">
                                        <ul class="slides">
                                            @foreach($puntointeres->fotos as $foto)
                                            <li><img src="{{ URL::asset($foto->ruta)}}" alt="" /></li>
                                            @endforeach
                                
                                        </ul>
                                    </div>
                                    <div class="image-carousel style1" data-animation="slide" data-item-width="70" data-item-margin="10" data-sync="#photos-tab .photo-gallery">
                                        <ul class="slides">
                                            @foreach($puntointeres->fotos as $foto)
                                            <li><img src="{{ URL::asset($foto->ruta)}}" alt="" /></li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                                <div id="map-tab" class="tab-pane fade">
                                    
                                </div>
                                <div id="steet-view-tab" class="tab-pane fade" style="height: 500px;">
                                    
                                </div>                               
                            </div>
                        </div>
                        
                        <div id="hotel-features" class="tab-container">
                            <ul class="tabs">
                                <li class="active"><a href="#hotel-description" data-toggle="tab">Descripcion</a></li>
                                <li><a href="#hotel-reviews" data-toggle="tab">Opiniones</a></li>
                                @if(Auth::user())
                                <li><a href="#hotel-write-review" data-toggle="tab">Escribir una opinión</a></li>
                                <li><a href="#fotos-upload" data-toggle="tab">Subir fotos</a></li>
                                @endif
								<li><a href="#anuncios" data-toggle="tab">Anuncios</a></li>
                            </ul>
                            <div class="forms-group">
                                @include('forms_repeat.success')                                                
                                @include('forms_repeat.error') 
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="hotel-description">
                                    <div class="intro table-wrapper full-width hidden-table-sms">
                                        <div class="col-sm-5 col-lg-4 features table-cell">
                                            <ul>
                                                <li><label>CATEGORIA:</label>{{$puntointeres->categoria->nombre}}</li>
                                                <li><label>ENTORNO:</label>{{$puntointeres->entorno->nombre}}</li>
                                                <li><label>TRANSPORTE:</label>{{$puntointeres->transporte->nombre}}</li>   
                                                <li><label>CREADOR:</label><a href="{{ URL::asset('consultarUsuario/'.$puntointeres->users->id) }}">{{$puntointeres->users->username}}</a></li>
                                                <li><label>PDF:</label><a href="{{ URL::asset('pdf/'.$puntointeres->id) }}">PDF</a></li>                                                                                                                                                                                 
                                            </ul>
                                        </div>
                                        <div class="col-sm-7 col-lg-8 table-cell testimonials">

                                            <div class="testimonial style1">
                                                <ul class="slides ">
                                                    @foreach($comentariosTake as $comentarios)
                                                    <li>
                                                        <p class="description">"{{$comentarios->contenido}}"</p>
                                                        <div class="author clearfix">
                                                            <a href="#"><img src="{{ URL::asset($comentarios->creador->foto)}}" alt="" width="74" height="74" /></a>
                                                            <a href="{{ URL::asset('consultarUsuario/'.$puntointeres->users->id) }}"><h5 class="name">{{$comentarios->creador->username}}</h5></a>
                                                        </div>
                                                    </li>
                                                    @endforeach
                                                </ul>
                                            
                                            </div>
                                        </div>
                                    </div>
                                    <div class="long-description">
                                        <h2>Sobre {{$puntointeres->nombre}}</h2>
                                        <p>
                                        {{$puntointeres->descripcion}}
                                        </p>
                                    </div>
                                </div>
                                                         
                                <div class="tab-pane fade" id="hotel-reviews">
                                    <div class="intro table-wrapper full-width hidden-table-sms">
                                        <div class="rating table-cell col-sm-4">
                                            @if(count($valoracion)>0)
                                            <span class="score">{{$valoracion}}/5.0</span>
                                            @else
                                                <div class="alert alert-info">No hay ninguna opinión</div>
                                            @endif
                                        </div>
                                        
                                    </div>
                                    <div class="guest-reviews">
                                        @foreach($comentariosPI as $comentarios)
                                        <div class="guest-review table-wrapper">
                                            <div class="col-xs-3 col-md-2 author table-cell">
                                                <a href="#"><img src="{{ URL::asset($comentarios->creador->foto)}}" alt="" width="270" height="263" /></a>
                                                <p class="name"><a href="{{ URL::asset('consultarUsuario/'.$puntointeres->users->id) }}">{{$comentarios->creador->username}}</a></p>
                                                <p class="date">{{$comentarios->created_at}}</p>
                                            </div>
                                            <div class="col-xs-9 col-md-10 table-cell comment-container">
                                                <div class="comment-header clearfix">
                                                    <div class="review-score">
                                                        <span class="score">{{$comentarios->valoracion}}/5.0</span>
                                                    </div>
                                                </div>
                                                <div class="comment-content">
                                                    <p>{{$comentarios->contenido}}</p>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                        
                                        
                                    </div>
                                </div>                                                            
                                <div class="tab-pane fade" id="hotel-write-review">
                                    <form id="opinionForm" class="form-horizontal" role="form" action="{{ URL::asset('enviar-comentarios') }}" method="POST" enctype="multipart/form-data" files="true">
                                        <input type="hidden" name="identificador" value="{{$puntointeres->id}}">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div class="form-group  no-float no-padding">
                                            <h4 class="title">Valoracion</h4>
                                            <input type="number" name="valoracion" class="input-text full-width" />
                                        </div>
                                        <div class="form-group">
                                            <h4 class="title">Contenido</h4>
                                            <textarea class="input-text full-width" name="contenido" rows="5"></textarea>
                                        </div>                                                                                                                                                   
                                        <div class="form-group col-md-5 no-float no-padding no-margin">
                                            <button type="submit" class="btn-large full-width">ENVIAR COMENTARIOS</button>
                                        </div>
                                    </form>
                                    
                                </div>
                                  <div class="tab-pane fade" id="fotos-upload">
                                    <form id="subirFotosForm" class="form-horizontal" role="form" action="{{ URL::asset('subir-fotos-pi') }}" method="POST" enctype="multipart/form-data" files="true">
                                        <input type="hidden" name="identificador" value="{{$puntointeres->id}}">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">                                                           
                                        <div class="form-group col-md-5 no-float no-padding">
                                            <h4 class="title">Tienes fotos para compartir? <small>(Opcional)</small> </h4>
                                            <div class="fileinput full-width">
                                                <input type="file" class="input-text" name="photo[]" multiple>
                                            </div>
                                        </div>                                      
                                        <div class="form-group col-md-5 no-float no-padding no-margin">
                                            <button type="submit" class="btn-large full-width">SUBIR FOTO</button>
                                        </div>
                                    </form>
                                    
                                </div>
								 <div class="tab-pane fade" id="anuncios">
                                    @foreach($puntointeres->anuncios as $anuncios)
										<div class="col-md-12">
											{{$anuncios->descripcion}}
										</div>
                                    @endforeach
                                </div>
                                
                            </div>
                        
                        </div>
                    </div>
                    <div class="sidebar col-md-3">
                        <article class="detailed-logo">
                            <figure>
                                 @foreach($puntointeres->fotos as $key => $foto)  
                                    @if($key==0)                               
                                        <img width="114" height="85" src="{{ URL::asset($foto->ruta)}}" alt="">
                                    @endif
                                @endforeach
                            </figure>
                            <div class="details">
                                <h2 class="box-title">{{$puntointeres->nombre}}<small><i class="soap-icon-departure yellow-color"></i></small></h2>
                                <span class="price clearfix">
                                    <small class="pull-left">PRECIO</small>
                                    <span class="pull-right">{{$puntointeres->precio}}€</span>
                                </span>
                                <div class="feedback clearfix">
                                    <small class="pull-left">TOTAL</small>
                                    <span class="review pull-right">{{$countComentarios}} comentarios</span>
                                </div>
                                <p class="description">{{$puntointeres->descripcion}}</p>
								<p>
								@if($puntointeres->users->role_id==2)
									<div class="btn btn-info"><a href="{{ URL::asset('enviarMensaje/'.$puntointeres->users->id)}}" >Enviar mensaje a usuario</a></div>
								@endif
                            </div>
                        </article>                       
                        <div class="travelo-box">
                            <h4>Puntos de interés similares</h4>
                            <div class="image-box style14">                                
                                @foreach($puntosInteresSimilares as $key => $puntointeres)
                                <article class="box">
                                    <figure>                                        
                                        @foreach($puntointeres->fotos as $key => $foto)  
                                            @if($key==0)                               
                                                <img width="114" height="85" src="{{ URL::asset($foto->ruta)}}" alt="">
                                            @endif
                                        @endforeach
                                    </figure>
                                    <div class="details">
                                        <h5 class="box-title"><a href="{{ URL::asset('consultarPI/'.$puntointeres->id)}}">{{$puntointeres->nombre}}</a></h5>
                                        <label class="price-wrapper">
                                            <span class="price-per-unit">{{$puntointeres->precio}} €</span>precio
                                        </label>
                                    </div>
                                </article>
                                @endforeach                                                        
                            </div>
                        </div>                                         
                    </div>
                </div>
            </div>
        </section>
    </div>

    <!-- Javascript -->
    <script type="text/javascript" src="{{ URL::asset('js/jquery-1.11.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery.noconflict.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/modernizr.2.7.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery-migrate-1.2.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery.placeholder.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/jquery-ui.1.10.4.min.js') }}"></script>
    
    <!-- Twitter Bootstrap -->
    <script type="text/javascript" src="{{ URL::asset('js/bootstrap.js') }}"></script>
    
    <!-- load revolution slider scripts -->
    <script type="text/javascript" src="{{ URL::asset('components/revolution_slider/js/jquery.themepunch.plugins.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('components/revolution_slider/js/jquery.themepunch.revolution.min.js') }}"></script>
    
    <!-- load BXSlider scripts -->
    <script type="text/javascript" src="{{ URL::asset('components/jquery.bxslider/jquery.bxslider.min.js') }}"></script>
    
    <!-- load FlexSlider scripts -->
    <script type="text/javascript" src="{{ URL::asset('components/flexslider/jquery.flexslider-min.js') }}"></script>
    
    <!-- Google Map Api -->
    <script src="http://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>
    
    <script type="text/javascript" src="{{ URL::asset('js/calendar.js') }}"></script>
    
    <!-- parallax -->
    <script type="text/javascript" src="{{ URL::asset('js/jquery.stellar.min.js') }}"></script>
    
    <!-- waypoint -->
    <script type="text/javascript" src="{{ URL::asset('js/waypoints.min.js') }}"></script>

    <!-- load page Javascript -->
    <script type="text/javascript" src="{{ URL::asset('js/theme-scripts.js') }}"></script>
    
    <script type="text/javascript">
        var latitud = $('.latitud').val();
        var longitud = $('.longitud').val();
        var valoracion = $('.valoracion').val();


        tjq('a[href="#map-tab"]').on('shown.bs.tab', function (e) {
            google.maps.event.trigger(map, "resize");
            map.setCenter(new google.maps.LatLng(latitud,longitud));
        });
        tjq('a[href="#steet-view-tab"]').on('shown.bs.tab', function (e) {
            fenway = panorama.getPosition();
            panoramaOptions.position = fenway;
            panorama = new google.maps.StreetViewPanorama(document.getElementById('steet-view-tab'), panoramaOptions);
            map.setStreetView(new google.maps.LatLng(latitud,longitud));
        });
        var map = null;
        var panorama = null;
        var fenway = new google.maps.LatLng(latitud, longitud);
        var mapOptions = {
            center: fenway,
            zoom: 12
        };
        var panoramaOptions = {
            position: fenway,
            pov: {
                heading: 34,
                pitch: 10
            }
        };
        function initialize() {
            tjq("#map-tab").height(tjq("#hotel-main-content").width() * 0.6);
            map = new google.maps.Map(document.getElementById('map-tab'), mapOptions);
            panorama = new google.maps.StreetViewPanorama(document.getElementById('steet-view-tab'), panoramaOptions);
            map.setStreetView(panorama);

        }
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
@endsection

