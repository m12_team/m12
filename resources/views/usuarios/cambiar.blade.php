@extends('layouts.master')
@section("content")
        <section id="content" class="tour" style="background-color:white;width:50%;margin:0 auto;">
                @if(Auth::user())
                    Ya has iniciado la sesión correctamente - Para acceder a tu perfil  <b> <a href="{{ URL::asset('/profile') }}" >HAZ CLICK</a></b>
                @else
                <form action="{{ URL::asset('/cambiarPassword') }}" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="form-group">
                        <input type="text" class="input-text full-width" placeholder="Email" name="email" style="border-color:yellow;">
                    </div>
                    <div class="form-group">
                        <input type="text" class="input-text full-width" placeholder="Introducir código" name="condigo" style="border-color:yellow;">
                    </div>
                    <div class="form-group">
                        <input type="password" class="input-text full-width" placeholder="Intoducir nueva contraseña" name="password">
                    </div>
                    <div class="form-group">
                        <input type="password" class="input-text full-width" placeholder="Confirmar nueva contraseña" name="password">
                    </div>              
                    <button type="submit" class="full-width btn-medium">Cambiar contraseña</button>
                </form>
                @endif
                <div class="seperator"></div>
                <p>No tienes una cuenta ? <a href="{{ URL::asset('/registro-de-usuario') }}" >Registro de usuario</a></p>
        </section>
@endsection
