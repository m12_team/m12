@extends('layouts.master')
@section('content')
 <script>
  $(function() {
     $( "#datepicker" ).datepicker({
     	dateFormat: "dd-mm-yy",
	    changeMonth: true,
	    changeYear: true,
	    yearRange:'-90:+0'
	});
  });
  </script>

        <section id="content" class="tour" style="background-color:white;width:50%;margin:0 auto;">
					<h2>Registro de usuario profesional</h2>
                    <form id="signupForm" class="form-horizontal cmxform" role="form" action="{{ URL::asset('/registro-profesional') }}" method="POST" enctype="multipart/form-data" files="true">
                             @include('forms_repeat.users')
                            <button type="submit" class="full-width btn-medium">REGISTRAR USUARIO PROFESIONAL</button>
                    </form>
                  @include('forms_repeat.errors')
        </section>

@endsection

