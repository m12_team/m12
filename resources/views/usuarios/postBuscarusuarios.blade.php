@extends('layouts.master')
@section('content')
    <!-- Javascript -->
    <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.noconflict.js"></script>
    <script type="text/javascript" src="js/modernizr.2.7.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.placeholder.js"></script>
    <script type="text/javascript" src="js/jquery-ui.1.10.4.min.js"></script>
    <div id="page-wrapper">
        <div class="page-title-container">
            <div class="container">
                <div class="page-title pull-left">
                    <h2 class="entry-title">Búsqueda de usuarios</h2>
                </div>
                <ul class="breadcrumbs pull-right">
                    <li><a href="{{ URL::asset('/') }}">INICIO</a></li>
                    <li class="active">Búsqueda de usuarios</li>
                </ul>
            </div>
        </div>
        <section id="content white">
            <div class="container">
                <div id="main">
                    <div class="row">                        
                        <div class="col-md-offset-1 col-md-10">
                            <div class="hotel-list listing-style3 hotel">
                            @if(count($users)>0)
                                @foreach($users as $usuario)
                               <article class="box">
                                    <figure class="col-sm-5 col-md-4">
                                        <img width="270" height="160" src="{{$usuario->foto}}"></a>
                                    </figure>
                                    <div class="details col-sm-7 col-md-8">
                                        <a href="{{ URL::asset('consultarUsuario/'.$usuario->id)}}">
                                        <div>
                                            <div>
                                                <h4 class="box-title">{{$usuario->nombre}} {{$usuario->primer_apellido}} {{$usuario->segundo_apellido}}
                                                <p>
                                                <i class="soap-icon-departure yellow-color"></i> {{$usuario->fecha_nacimiento}}</small>
                                                </h4>
                                               
                                            </div>
                                          
                                        </div>
                                        <div>
                                            <p>{{$usuario->descripcion}}</p>
                                            <div>
                                                <a href="{{ URL::asset('consultarUsuario/'.$usuario->id)}}" class="button btn-small full-width text-center" title="" href="detalle">CONSULTAR USUARIO</a>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                                @endforeach                              
                                </div>
                                @else
                                     <div class="col-sm-6 col-md-12">
                                    <div class="alert alert-info">
                                        No hay ningún criterio relacionado con la búsqueda
                                    </div>
                                </div>
                                @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection