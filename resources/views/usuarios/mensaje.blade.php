@extends('layouts.master')
@section('content')
        <div class="page-title-container">
            <div class="container">
                <div class="page-title pull-left">
                    <h2 class="entry-title">{{$usuario->nombre}} {{$usuario->primer_apellido}}</h2>
                </div>
                <ul class="breadcrumbs pull-right">
                    <li><a href="{{ URL::asset('/')}}">HOME</a></li>
                    <li class="active">{{$usuario->nombre}} {{$usuario->primer_apellido}}</li>
                </ul>
            </div>
            <div class="section white-bg">

                <div class="container">
                    <div class="forms-group">
                                @include('forms_repeat.success')                                                
                                @include('forms_repeat.error') 
                    </div>
                    <div class="description block col-md-8 pull-left">
                    @if(Auth::user())
                    <form action="{{ URL::asset('enviarMensaje/')}}" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="receptor" value="{{$usuario->id}}">
                        <textarea rows="5" class="input-text full-width" name="contenido"></textarea>                                
                        <input type="submit" href="{{ URL::asset('enviarMensaje/'.$usuario->id)}}" class="btn btn-info col-md-12">
                    </form>
                    @else 
                    <div class="alert alert-info">Para enviar un mensaje tienes que ser un usuario registrado</div>
                    @endif
                    </div> 
                    <div class="description block col-md-4 pull-right">
                        <img width="270" height="160" src="{{ URL::asset($usuario->foto)}}">
                    </div>                  
                </div>
            </div>
        </div>
             
@endsection

