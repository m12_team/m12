@extends('layouts.master')
@section('content')
    <div id="page-wrapper">
        <div class="page-title-container">
            <div class="container">
                <div class="page-title pull-left">
                    <h2 class="entry-title">Buscar usuarios</h2>
                </div>
                <ul class="breadcrumbs pull-right">
                    <li><a href="{{ URL::asset('/') }}">INICIO</a></li>
                    <li class="active"><a href="{{ URL::asset('/buscar-usuarios') }}">Buscar usuarios</a></li>
                </ul>
            </div>
        </div>

        <section id="content " >
            <div class="container" >
                <div id="main">
                    <div class="row">
                        <div class="col-md-offset-2 col-md-8" >          
                <div class="alert alert-info">El fórmulario sólo puede buscar por un criterio seleccionado</div>                  
              <form action="{{ URL::asset('busqueda') }}" class="form-horizontal" role="form" method="GET" enctype="multipart/form-data" >
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group">
                                <input type="text" class="input-text full-width" placeholder="Nombre" name="nombre" id="nombre">
                            </div>
                            <div class="form-group">
                                <input type="text" class="input-text full-width" placeholder="Primer apellido" name="primer_apellido" id="primer_apellido">
                            </div>
                            <div class="form-group">
                                <input type="text" class="input-text full-width" placeholder="Segundo apellido" name="segundo_apellido" id="segundo_apellido">
                            </div>                       
                            <div class="form-group">
                                <input type="text" class="input-text full-width" placeholder="Email" name="email" id="email">
                            </div>										
                             <div class="form-group">
                                <input type="text" class="input-text full-width" placeholder="Username" name="username" id="username">
                            </div>
                                               
                            <button type="submit" class="full-width btn-medium">Buscar usuarios</button>
                        </form>
						</div>                      
                    </div>
                </div>
            </div>
        </section>
        
        
    </div>

    <!-- Javascript -->
    <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.noconflict.js"></script>
    <script type="text/javascript" src="js/modernizr.2.7.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.placeholder.js"></script>
    <script type="text/javascript" src="js/jquery-ui.1.10.4.min.js"></script>
    
    <!-- Twitter Bootstrap -->
    <script type="text/javascript" src="js/bootstrap.js"></script>
    
    <!-- load revolution slider scripts -->
    <script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.plugins.min.js"></script>
    <script type="text/javascript" src="components/revolution_slider/js/jquery.themepunch.revolution.min.js"></script>
    
    <!-- load BXSlider scripts -->
    <script type="text/javascript" src="components/jquery.bxslider/jquery.bxslider.min.js"></script>
    
    <!-- load FlexSlider scripts -->
    <script type="text/javascript" src="components/flexslider/jquery.flexslider-min.js"></script>
    
    <!-- Google Map Api -->
    <script type='text/javascript' src="http://maps.google.com/maps/api/js?sensor=false&amp;language=en"></script>
    <script type="text/javascript" src="js/gmap3.min.js"></script>
    
    <!-- parallax -->
    <script type="text/javascript" src="js/jquery.stellar.min.js"></script>
    
    <!-- waypoint -->
    <script type="text/javascript" src="js/waypoints.min.js"></script>

    <!-- load page Javascript -->
    <script type="text/javascript" src="js/theme-scripts.js"></script>
    <script type="text/javascript" src="js/scripts.js"></script>
    
	<script type="text/javascript">
		$('#nombre').on('keypress',function(){
			$("#primer_apellido").val("");
            $("#segundo_apellido").val("");
            $("#email").val("");
            //$("#calendario").val("");
            $("#username").val("");
		});
		$('#primer_apellido').on('keypress',function(){
            $("#nombre").val("");
            $("#segundo_apellido").val("");
            $("#email").val("");
            //$("#calendario").val("");
            $("#username").val("");
		});
		$('#segundo_apellido').on('keypress',function(){
            $("#primer_apellido").val("");
            $("#nombre").val("");
            $("#email").val("");
            //$("#calendario").val("");
            $("#username").val("");
		});
		$('#email').on('keypress',function(){
            $("#primer_apellido").val("");
            $("#nombre").val("");
            $("#segundo_apellido").val("");
            //$("#calendario").val("");
            $("#username").val("");
		});
        //No detecta on keypress
		/*$('#calendario').on('keypress',function(){
            $("#primer_apellido").val("");
            $("#nombre").val("");
            $("#email").val("");
            $("#segundo_apellido").val("");
            $("#username").val("");
		});*/
		$('#username').on('keypress',function(){
            $("#primer_apellido").val("");
            $("#nombre").val("");
            $("#email").val("");
            $("#segundo_apellido").val("");
            $("#calendario").val("");
		});
  </script>
@endsection