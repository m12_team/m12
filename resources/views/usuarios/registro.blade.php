@extends('layouts.master')
@section('content')
 <script>
  $(function() {
     $( "#datepicker" ).datepicker({
     	dateFormat: "dd-mm-yy",
	    changeMonth: true,
	    changeYear: true,
	    yearRange:'-90:+0'
	});
  });
  </script>

        <section id="content" class="tour" style="background-color:white;width:50%;margin:0 auto;">
					<h2>Registro de usuario</h2>
                    <form id="signupForm" class="form-horizontal cmxform" role="form" action="{{ URL::asset('/registro-de-usuario') }}" method="POST" enctype="multipart/form-data" files="true">
                             @include('forms_repeat.users')
                            <a href="{{ URL::asset('auth/google')}}"><img src="{{ URL::asset('images/icon/sign-google.png')}}" style="height:50px;"></img></a>
                            <a href="{{ URL::asset('auth/facebook')}}"><img src="{{ URL::asset('images/icon/sign-facebook.png')}}" style="height:50px;"></img></a>
                            <button type="submit" class="full-width btn-medium">REGISTRAR USUARIO</button>
                    </form>
                  @include('forms_repeat.errors')
        </section>

@endsection

