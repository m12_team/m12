@extends('layouts.master')
@section('content')
        <div class="page-title-container">
            <div class="container">
                <div class="page-title pull-left">
                    <h2 class="entry-title">{{$usuario->nombre}} {{$usuario->primer_apellido}}</h2>
                </div>
                <ul class="breadcrumbs pull-right">
                    <li><a href="{{ URL::asset('/')}}">HOME</a></li>
                    <li class="active">{{$usuario->nombre}} {{$usuario->primer_apellido}}</li>
                </ul>
            </div>
             <div class="section white-bg">
                <div class="container">
                    <div class="description block col-md-4 pull-left">
                        <h1>Datos del usuario</h1>
                        <p>
                       <b>Información</b> : {{$usuario->nombre}} {{$usuario->primer_apellido}} {{$usuario->segundo_apellido}}
                       <p>
                       <b>Email</b>: {{$usuario->email}}
                       <p>
                       <b>Username</b>: {{$usuario->username}}
                       <p>
                        <b>Fecha de nacimiento</b> : {{$usuario->fecha_nacimiento}}
                        <p>
                        <b>Descripcion</b> : {{$usuario->descripcion}}
                         <p>
                        <b>Ciudad natal</b> : {{$usuario->ciudad_natal->name}}
                         <p>
                        <b>Ciudad actual</b> : {{$usuario->ciudad_actual->name}}
                        <p>
                        <a href="{{ URL::asset('enviarMensaje/'.$usuario->id)}}" class="btn btn-info col-md-12">Enviar mensaje</a>
                    </div> 
                    <div class="description block col-md-4 pull-right">
                        <img width="270" height="160" src="{{ URL::asset($usuario->foto)}}">
                    </div>                  
                </div>
            </div>
        </div>
             
@endsection

