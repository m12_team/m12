@extends('layouts.master')
@section('content')
        <section id="content" class="tour" style="background-color:white;width:50%;margin:0 auto;">
                @include('forms_repeat.errors')

                @if(Auth::user())
                    Ya has iniciado la sesión correctamente - Para acceder a tu perfil  <b> <a href="{{ URL::asset('/profile') }}" >HAZ CLICK</a></b>
                @else
                <form id="loginForm" class="form-horizontal" role="form" action="{{ URL::asset('login') }}" method="POST" enctype="multipart/form-data" files="true">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="form-group">
                        <input type="text" class="input-text full-width" placeholder="email address" name="email" style="border-color:yellow;">
                    </div>
                    <div class="form-group">
                        <input type="password" class="input-text full-width" placeholder="password" name="password">
                    </div>
                    <div class="form-group">
                    <a href="{{ URL::asset('/recuperar-contraseña') }}" class="pull-right">Recuperar contraseña</a>          
                    </div>
                    <button type="submit" class="full-width btn-medium">INICIAR SESIÓN</button>
                </form>
                <div class="seperator"></div>
                <p>No tienes una cuenta ? <a href="{{ URL::asset('/registro-de-usuario') }}" >Registro de usuario</a></p>
                @endif
        </section>

@endsection

