@extends('layouts.master')
@section('content')
    <!-- Javascript -->
 
    <div id="page-wrapper">
        <div class="page-title-container">
            <div class="container">
                <div class="page-title pull-left">
                    <h2 class="entry-title">Búsqueda de Pokemon</h2>
                </div>
                <ul class="breadcrumbs pull-right">
                    <li><a href="#">INICIO</a></li>
                    <li class="active"><a href="{{ URL::asset('/buscar-punto-de-interes') }}">Búsqueda de Pokemon</a></li>
                </ul>
            </div>
        </div>
        <section id="content white">
            <div class="container">
                <div id="main">
                    <div class="row">                        
                        <div class="col-md-offset-1 col-md-10">
                            <div class="hotel-list listing-style3 hotel">     
                                @if(count($pokemon)>0)
                                     @foreach($pokemon as $pokem)                                                    
                                      <article class="box">
                                        <div class="col-md-4">              
                                            <h4 class="box-title"><b>{{$pokem->id}}</b> - {{$pokem->name}}</h4>     
                                            <div class="col-xs-12">
                                                @foreach($pokem->tipos as $tipo)
                                                <p>
                                                <a>{{$tipo->nombre}} </a>
                                                @endforeach                                                    
                                            </div>
                                            <img width="270" height="160" alt="" src="{{$pokem->fotos->first()->ruta}}">                                        
                                               <div class="col-xs-12 col-md-12">
                                                    <a href="{{ URL::asset('consultarPokemon/'.$pokem->id) }}" class="button btn-small full-width text-center">CONSULTAR</a>
                                                </div>   
                                        </div>
                                        <div class="col-md-8" >
                                            HOLA QUE ASE
                                        </div>

                                    </article> 
                                    @endforeach
                                @else
                                    <div class="col-sm-6 col-md-12">
                                        <div class="alert alert-info">
                                            No hay ningún criterio relacionado con la búsqueda
                                        </div>
                                    </div> 
                                @endif                                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection