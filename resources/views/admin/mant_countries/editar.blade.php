@extends('layouts.layout_admin')
@section('content')
				<div class="row">
					<div class="col-md-12">
						<!-- Zero Configuration Table -->
						<div class="panel panel-default">
						
						<div class="alert alert-info">Editando el pais {{$countries->name}}</div>
							@include('forms_repeat.errors')		                            		    
							<div class="panel-body">	
							    	{!! Form::model($countries, ['route' => ['mantenimiento_paises.update', $countries->id], 'method' => 'patch']) !!}							
		                             @include('forms_repeat.pais')		                            		    
		                            <button type="submit" class="full-width btn-medium">EDITAR PAÍS</button>
		                           	{!!Form::close()!!}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
  <script type="text/javascript">
   $.ajax({
                url:   '../../obtenerSubregion',
                type:  'get',
                success:  function (data) {
					$.each(data, function(key, element) {
						$('#pais').append("<option value='" + element.id + "'>" + element.nombre + "</option>");		
					});
					
                }
        });
</script>

@endsection
