@extends('layouts.layout_admin')
@section('content')

				<div class="row">
					<div class="col-md-12">
						<!-- Zero Configuration Table -->
						<div class="panel panel-default">
						
						<div class="alert alert-info">Mantenimiento de usuarios</div>
							<div class="panel-body">
								<a class="btn btn-success">Crear</a>
								<table id="zctb" class="display table table-striped table-bordered table-hover" cellspacing="0" width="100%">
									<thead>
										<tr>
											<th>Datos del usuario</th>
											<th>Email</th>
											<th>Username</th>
											<th>Age</th>
											<th>Acciones</th>
										</tr>
									</thead>								
									<tbody>
										@foreach($users as $user)
										<tr>
											<td>{{$user->nombre}} {{$user->primer_apellido}} {{$user->segundo_apellido}}</td>
											<td>{{$user->email}}</td>
											<td>{{$user->username}}</td>
											<td>@if($user->estaBloqueado==0)
													No está bloqueado
													<p>
													<a href="{{ URL::asset('/bloquearUsuario/'.$user->id.'') }}">Bloquear</a>
												@else
													Está bloqueado
													<p>
													<a href="{{ URL::asset('/desbloquearUsuario/'.$user->id.'') }}">Desbloquear</a>
												@endif
											</td>
											<td><a class="btn btn-warning">Modificar</a><a class="btn btn-danger">Eliminar</a><a class="btn btn-info">Ver</a></td>
										</tr>
										@endforeach							
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection
