@extends('layouts.layout_admin')
@section('content')
				<div class="row">
					<div class="col-md-12">
						<!-- Zero Configuration Table -->
						<div class="panel panel-default">
						
						<div class="alert alert-info">Creación de subregiones</div>
							@include('forms_repeat.errors')
							@include('forms_repeat.error')		                            		    	                            		    
							<div class="panel-body">
								<form class="form-horizontal" role="form" action="{{ URL::asset('/mantenimiento_subregiones') }}" method="POST" enctype="multipart/form-data" files="true">

		                             @include('forms_repeat.subregiones')		                            		    
		                            <button type="submit" class="full-width btn-medium">CREAR SUBREGION</button>
		                        </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
<script type="text/javascript">
   $.ajax({
                url:   '../obtenerRegiones',
                type:  'get',
                success:  function (data) {
					$.each(data, function(key, element) {
						$('#regiones').append("<option value='" + element.id + "'>" + element.nombre + "</option>");		
					});
					
                }
        });
</script>
@endsection
