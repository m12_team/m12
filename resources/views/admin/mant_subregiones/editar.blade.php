@extends('layouts.layout_admin')
@section('content')
				<div class="row">
					<div class="col-md-12">
						<!-- Zero Configuration Table -->
						<div class="panel panel-default">
						
						<div class="alert alert-info">Estás editando la subregión {{$subregion->nombre}}</div>
							@include('forms_repeat.errors')		                            		    
							<div class="panel-body">	
									{!! Form::model($subregion, ['route' => ['mantenimiento_subregiones.update', $subregion->id], 'method' => 'patch']) !!}														
		                             @include('forms_repeat.subregiones')		                            		    
		                            <button type="submit" class="full-width btn-medium">EDITAR SUBREGION</button>
		                           {!!Form::close()!!}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
<script type="text/javascript">
   $.ajax({
                url:   '../../obtenerRegiones',
                type:  'get',
                success:  function (data) {
					$.each(data, function(key, element) {
						$('#regiones').append("<option value='" + element.id + "'>" + element.nombre + "</option>");		
					});
					
                }
        });
</script>
@endsection
