@extends('layouts.layout_admin')
@section('content')

				<div class="row">
					<div class="col-md-12">
						<!-- Zero Configuration Table -->
						<div class="panel panel-default">
						
						<div class="alert alert-info">Mantenimiento de Diarios</div>
						@include('forms_repeat.success')
						@include('forms_repeat.error')	
							<div class="panel-body">

								<a href="{{ URL::asset('/mantenimiento_diarios/create') }}" class="btn btn-success">Crear</a>
								<table id="zctb" class="display table table-striped table-bordered table-hover" cellspacing="0" width="100%">
									<thead>
										<tr>
											<th>ID</th>
											<th>Nombre</th>
											<th>Descripcion</th>
											<th>Estado diario</th>

											<th>Acciones</th>
										</tr>
									</thead>								
									<tbody>
										@foreach($diarios as $diario)
										<tr>
											<td>{{$diario->id}}</td>
											<td>{{$diario->nombre}}</td>
											<td>{{$diario->descripcion}}</td>												
											<td>@if($diario->estaBloqueado==0)
													<a href="{{ URL::asset('/bloquear_diario/'.$diario->id) }}">Bloquear diario</a>
												@else
													<a href="{{ URL::asset('/desbloquear_diario/'.$diario->id) }}">Desbloquear diario</a>
												@endif
											</td>
											<td><a class="btn btn-danger" href="{{ URL::asset('/mantenimiento_diarios_eliminar/'.$diario->id) }}">Eliminar diario</a></td>																			
										</tr>
										@endforeach							
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection
