@extends('layouts.layout_admin')
@section('content')

				<div class="row">
					<div class="col-md-12">
						<!-- Zero Configuration Table -->
						<div class="panel panel-default">
						
						<div class="alert alert-info">Creación de Diarios</div>
							 @include('forms_repeat.errors')		                            		    
							<div class="panel-body">
							<form class="form-horizontal" role="form" action="{{ URL::asset('/mantenimiento_pi') }}" method="POST" enctype="multipart/form-data" files="true">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div class="col-sm-9 no-padding no-float">
                                            <div class="row form-group">
                                                <div class="col-sms-6 col-md-6">
                                                    <label>Nombre</label>
                                                    <input type="text" class="input-text full-width" name="nombre">
                                                </div>
                                                <div class="col-sms-6 col-sm-6">
                                                    <label>Precio</label>
                                                    <input type="text" class="input-text full-width" name="precio">
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label>Descripcion</label>
                                                <textarea rows="5" class="input-textarea full-width" name="descripcion"></textarea>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col-sms-6 col-sm-6">
                                                    <label>Longitud</label>
                                                    <input type="text" class="input-text full-width longitud" name="longitude" long >
                                                </div>
                                                <div class="col-sms-6 col-sm-6">
                                                    <label>Latitud</label>
                                                    <input type="text" class="input-text full-width latitud" name="latitude">
                                                </div>
                                            </div>    
											<div class="row form-group">
                                               
                                                <div class="col-sms-6 col-sm-4">
                                                    <label>Entorno</label>
                                                    <div class="selector">
                                                        <select class="full-width" name="entorno" id="entorno">
                                                            <option value="">Selecciona un entorno</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                 <div class="col-sms-6 col-sm-4">
                                                    <label>Transporte</label>
                                                    <div class="selector">
                                                        <select class="full-width" name="transporte" id="transporte">
                                                            <option value="">Selecciona un transporte </option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sms-6 col-sm-4">
                                                    <label>Categoria</label>
                                                    <div class="selector">
                                                        <select class="full-width" name="categoria" id="categoria">
                                                            <option value="">Selecciona un transporte </option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>    
											 <div class="row form-group">
                                                <div class="col-sms-12 col-sm-12 no-float">
                                                    <div class="fileinput full-width">
                                                        <input type="file" class="input-text" name="photo[]" multiple>
                                                    </div>
                                                </div>
                                            </div>   
                                        </div>
										  <div class="from-group">
                                                <button type="submit" class="btn-medium col-sms-6 col-sm-4">CREAR PUNTO DE INTERÉS</button>
                                            </div>
								</form>
							</div>
						</div>
					</div>
				</div>
<script type="text/javascript" src="{{ URL::asset('js/crearPI_back.js') }}"></script>
@endsection
