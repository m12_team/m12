@extends('layouts.layout_admin')
@section('content')

				<div class="row">
					<div class="col-md-12">
						<!-- Zero Configuration Table -->
						<div class="panel panel-default">
						
						<div class="alert alert-info">Mantenimiento de Vídeos</div>
						@include('forms_repeat.success')
						@include('forms_repeat.error')	
							<div class="panel-body">

								<table id="zctb" class="display table table-striped table-bordered table-hover" cellspacing="0" width="100%">
									<thead>
										<tr>
											<th>ID</th>
											<th>Contenido</th>
											<th>Creador</th>
											<th>Opciones</th>
										</tr>
									</thead>								
									<tbody>
										@foreach($videos as $video)
										<tr>
											<td>{{$video->id}}</td>		
											<td>{{$video->url}}</td>		
											<td>Creador</td>																																
											<td><a class="btn btn-info">Ver el diario</a></td>																					
										</tr>
										@endforeach							
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection
