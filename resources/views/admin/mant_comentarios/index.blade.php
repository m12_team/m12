@extends('layouts.layout_admin')
@section('content')

				<div class="row">
					<div class="col-md-12">
						<!-- Zero Configuration Table -->
						<div class="panel panel-default">
						
						<div class="alert alert-info">Mantenimiento de Comentarios</div>
						@include('forms_repeat.success')
						@include('forms_repeat.error')	
							<div class="panel-body">

								<table id="zctb" class="display table table-striped table-bordered table-hover" cellspacing="0" width="100%">
									<thead>
										<tr>
											<th>ID</th>
											<th>Contenido</th>
											<th>Valoracion</th>
											<th>Creador</th>
											<th>Opciones</th>
										</tr>
									</thead>								
									<tbody>
										@foreach($comentarios as $comentario)
										<tr>
											<td>{{$comentario->id}}</td>
											<td>{{$comentario->contenido}}</td>
											<td>{{$comentario->valoracion}}</td>
											<td>{{$comentario->creador->nombre}}</td>																					
											<td><a href="{{ URL::asset('/mantenimiento_comentarios_eliminar/'.$comentario->id.'') }}" class="btn btn-danger">Eliminar</a><a class="btn btn-info" href="{{ URL::asset('consultarPI/'.$comentario->punto_interes->id) }}">Ver punto de interés del comentario</a></td>
										</tr>
										@endforeach							
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection
