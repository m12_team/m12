@extends('layouts.layout_admin')
@section('content')

				<div class="row">
					<div class="col-md-12">
						<!-- Zero Configuration Table -->
						<div class="panel panel-default">
						
						<div class="alert alert-info">Mantenimiento de categorías</div>
							@include('forms_repeat.success')
							@include('forms_repeat.error')	
							<div class="panel-body">
								<a href="{{ URL::asset('/mantenimiento_categoria_pi/create') }}" class="btn btn-success">Crear</a>
								<table id="zctb" class="display table table-striped table-bordered table-hover" cellspacing="0" width="100%">
									<thead>
										<tr>
											<th>ID</th>
											<th>Nombre</th>
											<th>Tipo de catgoría</th>
											<th>Acciones</th>
										</tr>
									</thead>								
									<tbody>
										@foreach($categorias as $categoria)
										<tr>
											<td>{{$categoria->id}}</td>
											<td>{{$categoria->nombre}}</td>
											<td>@if($categoria->esProfesional==0)
													La categoría está disponible para usuarios registrados
												@else
													La categoría está disponible para usuarios registrados profesionales
												@endif
											</td>
											<td><a href="{{ URL::asset('/mantenimiento_categoria_pi/'.$categoria->id.'/edit') }}" class="btn btn-warning">Modificar</a><a href="{{ URL::asset('/mantenimiento_categoria_pi_eliminar/'.$categoria->id) }}"class="btn btn-danger">Eliminar</a></td>
										</tr>
										@endforeach							
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection
