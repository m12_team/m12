@extends('layouts.layout_admin')
@section('content')

				<div class="row">
					<div class="col-md-12">
						<!-- Zero Configuration Table -->
						<div class="panel panel-default">
						
						<div class="alert alert-info">Estás editando la categoría {{$categoria->nombre}}</div>
							@include('forms_repeat.errors')		                            		    
							<div class="panel-body">	
								{!! Form::model($categoria, ['route' => ['mantenimiento_categoria_pi.update', $categoria->id], 'method' => 'patch']) !!}					
		                             @include('forms_repeat.categorias')		                            		    
		                            <button type="submit" class="full-width btn-medium">EDITAR USUARIO</button>
		                    	{!!Form::close()!!}
							</div>
						</div>
					</div>
				</div>
@endsection
