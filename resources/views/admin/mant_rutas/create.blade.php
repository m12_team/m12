@extends('layouts.layout_admin')
@section('content')
    <div class="row">
	<div class="col-md-12">					<!-- Zero Configuration Table -->
		  <div class="panel panel-default">
						
				<div class="alert alert-info">Creación de Rutas</div>
					@include('forms_repeat.errors')		                            		    
					<div class="panel-body">
							     <form class="form-horizontal" role="form" action="{{ URL::asset('/mantenimiento_rutas') }}" method="POST" enctype="multipart/form-data" files="true">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <div class="form-group">
                                                    <label>Nombre</label>
                                                    <input type="text" class="input-text full-width" name="nombre">                                                
                                            </div>
                                             <div class="form-group">
                                                <label>Descripcion</label>
                                                <textarea rows="5" class="input-textarea full-width" name="descripcion"></textarea>
                                            </div>                                           										  
                                            <div class="form-group">
                                                    <select class="full-width" name="rutas" id="rutas">
                                                            <option>Seleccionar una categoría</option>
                                                    </select>
                                            </div>
                                            <div class="form-group">
                                                <div class="puntosInteresFiltrados"></div>
                                            </div>
                                            <div class="form-group">
                                                <div class="addRutaPI"></div>
                                            </div>
                                            <div class="form-group">
                                                <button type="submit" class="btn-medium col-sms-6 col-sm-4">CREAR RUTA</button>
                                            </div>
								</form>
				</div>
		  </div>
		</div>
    </div>
<script type="text/javascript">
    $.ajax({
                url:   '../obtenerCategorias',
                type:  'get',
                success:  function (data) {
                   $.each(data, function(key, element) {
                        $('#rutas').append("<option value='" + element.id + "'>" + element.nombre + "</option>");
                    });
                }
            });

    /***Obtener puntos de interés filtrando la categoría ****/
            $('select#rutas').on('change',function(){
            var idCategoria = $(this).val();

             $.ajax({
                
                url:   '../obtenerPIPorCategoria/'+idCategoria,
                type:  'get',
                success:  function (data) {
                    
                    $.each(data.puntoInteres, function(key, element) {
                            
                            $.each(element.fotos, function(key, elemento2) {
                                if(key==0){
                                    $('.puntosInteresFiltrados').append('')
                                    .append('<div class="col-md-4"><article class="box"><figure><a class="hover-effect" title=""><img width="300" height="160" alt="" src="'+elemento2.ruta+'"></a></figure><div class="details"><h4 class="box-title">'+element.nombre+'</h4></div></article><a onclick="myfunction('+element.id+')" class="col-md-12 btn btn-info">Añadir a la ruta</a></div>');
                                }
                    
                        
                        });
                    });
                }
            });
        });
    function myfunction(id) {
            /****Obtener un punto de interÃ©s y aÃ±adirlo a la ruta****/
            $.ajax({
                
                url:   '../obtenerPI/'+id,
                type:  'get',
                success:  function (data) {
                    console.log(data);
                    /***AÃ±adimos al array***/
                    $('.addRutaPI').append('')
                    .append('<div class="col-md-4"><article class="box"><figure><a class="hover-effect" title=""><img width="300" height="160" alt="" src="'+data.foto.ruta+'"></a></figure><div class="details"><h4 class="box-title">'+data.puntointeres.nombre+'</h4></div></article><input type="hidden" name="puntosInteres[]" value='+data.puntointeres.id+'></div>');
                }
            });
        }   

</script>
@endsection
