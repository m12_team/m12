@extends('layouts.layout_admin')
@section('content')

				<div class="row">
					<div class="col-md-12">
						<!-- Zero Configuration Table -->
						<div class="panel panel-default">
						
						<div class="alert alert-info">Mantenimiento de Rutas</div>
						@include('forms_repeat.success')
						@include('forms_repeat.error')	
							<div class="panel-body">

								<a href="{{ URL::asset('/mantenimiento_rutas/create') }}" class="btn btn-success">Crear</a>
								<table id="zctb" class="display table table-striped table-bordered table-hover" cellspacing="0" width="100%">
									<thead>
										<tr>
											<th>ID</th>
											<th>Nombre</th>
											<th>Descripcion</th>
											<th>¿Está bloqueada?</th>
											<th>Acciones</th>
										</tr>
									</thead>								
									<tbody>
										@foreach($rutas as $ruta)
										<tr>
											<td>{{$ruta->id}}</td>
											<td>{{$ruta->nombre}}</td>
											<td>{{$ruta->descripcion}}</td>												
											<td>@if($ruta->estaBloqueado==0)
													<a href="{{ URL::asset('/bloquear_ruta/'.$ruta->id) }}">No está bloqueada</a>
												@else
													<a href="{{ URL::asset('/desbloquear_ruta/'.$ruta->id) }}">Está bloqueada</a>
												@endif
											</td>
											<td><a class="btn btn-danger" href="{{ URL::asset('mantenimiento_rutas_eliminar/'.$ruta->id) }}">Eliminar ruta</a><a class="btn btn-warning" href="{{ URL::asset('mantenimiento_rutas/'.$ruta->id.'/edit') }}">Editar ruta</a><a class="btn btn-info" href="{{ URL::asset('consultarRutas/'.$ruta->id) }}">Ver ruta</a></td>																		
										</tr>
										@endforeach							
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

@endsection
