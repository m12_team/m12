@extends('layouts.layout_admin')
@section('content')
				<div class="row">
					<div class="col-md-12">
						<!-- Zero Configuration Table -->
						<div class="panel panel-default">						
						<div class="alert alert-info">Estás editando la ruta {{$ruta->nombre}} </div>
							 @include('forms_repeat.errors')		                            		    
							<div class="panel-body">
							     <form class="form-horizontal" role="form" action="{{ URL::asset('/mantenimiento_pi') }}" method="POST" enctype="multipart/form-data" files="true">
								    <input type="hidden" name="_token" value="{{ csrf_token() }}">
									<input type="hidden" name="identificador" value="{{$ruta->id}}">
                                            <div class="form-group">
                                                <label>Nombre</label>
                                                <input type="text" class="input-text full-width" name="nombre" value="{{$ruta->nombre}}">
                                            </div>
                                            <div class="form-group">
                                                <label>Descripcion</label>
                                                <textarea rows="5" class="input-textarea full-width" name="descripcion">{{$ruta->descripcion}}</textarea>
                                            </div>

                                            @foreach($ruta->puntointeres as $pI)
                                                <div class="col-sm-6 col-md-4">
                                                    <article class="box">
                                                        <figure>
                                                           <img width="300" height="160" alt="" src="{{ URL::asset($pI->fotos->first()->ruta) }}">
                                                        </figure>
                                                        <div class="details col-md-12">
                                                            <h4 class="box-title">{{$pI->nombre}}</h4>
                                                            <label class="price-wrapper">
                                                                <span class="price-per-unit">{{$pI->precio}}</span>
                                                            </label>
                                                            <p>
                                                            <a href="{{ URL::asset('consultarPI/'.$pI->id) }}" class="button uppercase col-md-4" title="Consultar PI">VER</a>
                                                            <a onclick="editarPI('{{$pI->id}}')" class="button uppercase col-md-4" title="View all">EDITAR</a>
                                                            <a href="{{ URL::asset('admin-eliminar-pi-ruta/'.$pI->id) }}" class="button uppercase col-md-4" title="View all">ELIMINAR</a>
                                                            
                                                        </div>
                                                    </article>
                                                </div> 
                                            @endforeach
                                    </form>									
                                </div>
										  
							</div>
						</div>
					</div>
@endsection
