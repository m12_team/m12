@extends('layouts.layout_admin')
@section('content')
<head>
  <meta charset="utf-8">
  <title>Registro de usuario</title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
</head>

				<div class="row">
					<div class="col-md-12">
						<!-- Zero Configuration Table -->
						<div class="panel panel-default">
						
						<div class="alert alert-info">Creación de usuarios</div>
							@include('forms_repeat.errors')		                            		    
							<div class="panel-body">
								<form class="form-horizontal" role="form" action="{{ URL::asset('/mantenimiento_transporte') }}" method="POST" enctype="multipart/form-data" files="true">

		                             @include('forms_repeat.transporte')		                            		    
		                            <button type="submit" class="full-width btn-medium">CREAR USUARIO</button>
		                        </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
  <script>

@endsection
