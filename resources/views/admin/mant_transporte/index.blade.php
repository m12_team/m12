@extends('layouts.layout_admin')
@section('content')

				<div class="row">
					<div class="col-md-12">
						<!-- Zero Configuration Table -->
						<div class="panel panel-default">
						
						<div class="alert alert-info">Mantenimiento de transporte</div>
							@include('forms_repeat.success')		                            		    
							@include('forms_repeat.error')		                            		    
							<div class="panel-body">
								<a href="{{ URL::asset('/mantenimiento_transporte/create') }}" class="btn btn-success">Crear</a>
								<table id="zctb" class="display table table-striped table-bordered table-hover" cellspacing="0" width="100%">
									<thead>
										<tr>
											<th>ID</th>
											<th>Nombre</th>
											<th>Acciones</th>
										</tr>
									</thead>								
									<tbody>
										@foreach($transportes as $transporte)
										<tr>
											<td>{{$transporte->id}}</td>
											<td>{{$transporte->nombre}}</td>
											<td><a href="{{ URL::asset('/mantenimiento_transporte/'.$transporte->id.'/edit') }}" class="btn btn-warning">Modificar</a><a href="{{ URL::asset('/mantenimiento_transporte_eliminar/'.$transporte->id.'') }}" class="btn btn-danger">Eliminar</a></td>
										</tr>
										@endforeach							
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection
