@extends('layouts.layout_admin')
@section('content')
<head>
  <meta charset="utf-8">
  <title>Registro de usuario</title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
</head>

				<div class="row">
					<div class="col-md-12">
						<!-- Zero Configuration Table -->
						<div class="panel panel-default">
						
						<div class="alert alert-info">Listado de usuarios que se pueden cambiar al rol {{$role->nombre}}</div>
									@if(Session::has('msg'))
							<div class="alert alert-success">{{Session::get('msg')}}</div>

							@endif
								<table id="zctb" class="display table table-striped table-bordered table-hover" cellspacing="0" width="100%">
									<thead>
										<tr>
											<th>ID</th>
											<th>Datos del usuario</th>
											<th>Email</th>
											<th>Username</th>
											<th>Age</th>
										</tr>
									</thead>								
									<tbody>
										@foreach($users as $user)
										<tr>
											<td>{{$user->id}}</td>
											<td>{{$user->nombre}} {{$user->primer_apellido}} {{$user->segundo_apellido}}</td>
											<td>{{$user->email}}</td>
											<td>{{$user->username}}</td>
											<td>@if($user->estaBloqueado==0)
													No está bloqueado
													<p>
													<a href="{{ URL::asset('/bloquearUsuario/'.$user->id.'') }}">Bloquear</a>
												@else
													Está bloqueado
													<p>
													<a href="{{ URL::asset('/desbloquearUsuario/'.$user->id.'') }}">Desbloquear</a>
												@endif
											</td>
										</tr>
										@endforeach							
									</tbody>
								</table>
						</div>
					</div>
			</div>
			
				
				

@endsection
