@extends('layouts.layout_admin')
@section('content')

				<div class="row">
					<div class="col-md-12">
						<!-- Zero Configuration Table -->
						<div class="panel panel-default">
						
						<div class="alert alert-info">Mantenimiento de roles</div>
						@include('forms_repeat.success')
						@include('forms_repeat.error')		                            		    		                            		    
						<div class="panel-body">
								<a href="{{ URL::asset('/mantenimiento_roles/create') }}" class="btn btn-success">Crear</a>
								<table id="zctb" class="display table table-striped table-bordered table-hover" cellspacing="0" width="100%">
									<thead>
										<tr>
											<th>ID</th>
											<th>Nombre</th>
											<th>Acciones</th>																																
										</tr>
									</thead>								
									<tbody>
										@foreach($roles as $role)
										<tr>
											<td>{{$role->id}}</td>
											<td>{{$role->nombre}}</td>
											<td><a href="{{ URL::asset('/mantenimiento_roles/'.$role->id.'/edit') }}" class="btn btn-warning">Modificar</a><a href="{{ URL::asset('/mantenimiento_roles_eliminar/'.$role->id.'') }}" class="btn btn-danger">Eliminar</a><a href="{{ URL::asset('mantenimiento_roles/'.$role->id.'/') }}" class="btn btn-info">Ver</a><a href="{{ URL::asset('/asignar-usuario/'.$role->id.'/') }}" class="btn btn-info">Asignar usuario</a></td>				
										</tr>
										@endforeach							
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

@endsection
