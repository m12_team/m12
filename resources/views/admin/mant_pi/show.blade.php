@extends('layouts.layout_admin')
@section('content')
<head>
  <meta charset="utf-8">
  <title>Registro de usuario</title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
</head>

				<div class="row">
					<div class="col-md-12">
						<!-- Zero Configuration Table -->
						<div class="panel panel-default">
						
						<div class="alert alert-info">Estás visualizando al usuario {{$user->nombre}}  {{$user->primer_apellido}}  {{$user->segundo_apellido}} | Tiene el rol de {{$user->role->nombre}} </div>

							<div class="panel-body">
		                             @include('forms_repeat.users')		                            		    
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
  <script type="text/javascript">
      $(".lock").attr("disabled", "disabled"); 
      $("textarea").attr("disabled", "disabled").addClass("disabled");
      $('.ocultar').hide();
  </script>
@endsection
