@extends('layouts.layout_admin')
@section('content')
				<div class="row">
					<div class="col-md-12">
						<!-- Zero Configuration Table -->
						<div class="panel panel-default">
						
						<div class="alert alert-info">Estás editando el punto de interés {{$puntointeres->nombre}} </div>
							 @include('forms_repeat.errors')		                            		    
							<div class="panel-body">
							<form class="form-horizontal" role="form" action="{{ URL::asset('/mantenimiento_pi') }}" method="POST" enctype="multipart/form-data" files="true">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
									<input type="hidden" name="identificador" value="{{$puntointeres->id}}">
                                        <div class="col-sm-9 no-padding no-float">
                                            <div class="row form-group">
                                                <div class="col-sms-6 col-md-6">
                                                    <label>Nombre</label>
                                                    <input type="text" class="input-text full-width" name="nombre" value="{{$puntointeres->nombre}}">
                                                </div>
                                                <div class="col-sms-12 col-sm-12 col-md-12">
                                                    <label>Precio</label>
                                                    <input type="text" class="input-text full-width" name="precio" value="{{$puntointeres->precio}}">
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label>Descripcion</label>
                                                <textarea rows="5" class="input-textarea full-width" name="descripcion" style="padding:5px;">{{$puntointeres->descripcion}}</textarea>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col-sms-12 col-sm-12 col-md-12">
                                                    <label>Longitud</label>
                                                    <input type="text" class="input-text full-width longitud" name="longitude" value="{{$puntointeres->longitud}}" >
                                                </div>
                                                <div class="col-sms-12 col-sm-12 col-md-12">
                                                    <label>Latitud</label>
                                                    <input type="text" class="input-text full-width latitud" name="latitude" value="{{$puntointeres->latitud}}">
                                                </div>
                                            </div>    										
                                            </div>		
											 @foreach($puntointeres->fotos as $foto)
                                                <div class="col-md-4">                                 
    												<img style="width:300px;height:160px;" src="{{ URL::asset($foto->ruta) }}"></img>
                                                    <a href="{{ URL::asset('/mantenimiento_pi_eliminarfoto/'.$foto->id) }}">Eliminar foto del punto de interés</a>
                                                </div>
											@endforeach
                                        </div>
										  <div class="from-group">
                                                <button type="submit" class="btn-medium col-sms-6 col-sm-4">MODIFICAR PUNTO DE INTERÉS</button>
                                            </div>
								</form>
							</div>
						</div>
					</div>
				</div>
@endsection
