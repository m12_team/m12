@extends('layouts.layout_admin')
@section('content')

				<div class="row">
					<div class="col-md-12">
						<!-- Zero Configuration Table -->
						<div class="panel panel-default">
						
						<div class="alert alert-info">Mantenimiento de Puntos de interés</div>
						@include('forms_repeat.success')
						@include('forms_repeat.error')	
							<div class="panel-body">

								<a href="{{ URL::asset('/mantenimiento_pi/create') }}" class="btn btn-success">Crear</a>
								<table id="zctb" class="display table table-striped table-bordered table-hover" cellspacing="0" width="100%">
									<thead>
										<tr>
											<th>ID</th>
											<th>Nombre</th>
											<th>Categoria</th>
											<th>Entorno</th>
											<th>Transporte</th>
											<th>Username</th>
											<th>¿Está bloqueada?</th>
											<th>Opciones</th>
										</tr>
									</thead>								
									<tbody>
										@foreach($punto_interes as $pi)
										<tr>
											<td>{{$pi->id}}</td>
											<td>{{$pi->nombre}}</td>
											<td>{{$pi->categoria->nombre}}</td>
											<td>{{$pi->entorno->nombre}}</td>
											<td>{{$pi->transporte->nombre}}</td>
											<td>{{$pi->users->username}}</td>
											<td>@if($pi->estaBloqueado==0)<a href="{{ URL::asset('bloquear_pi/'.$pi->id) }}">Bloquear punto de interés</a> @else <a href="{{ URL::asset('desbloquear_pi/'.$pi->id) }}">Desbloquear</a> @endif</td>
											<td><a href="{{ URL::asset('/mantenimiento_pi/'.$pi->id.'/edit') }}" class="btn btn-warning">Modificar</a><a href="{{ URL::asset('/mantenimiento_pi_eliminar/'.$pi->id.'') }}" class="btn btn-danger">Eliminar</a><a href="{{ URL::asset('/consultarPI/'.$pi->id.'') }}"class="btn btn-info">Ver</a></td>
										</tr>
										@endforeach							
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection
