@extends('layouts.layout_admin')
@section('content')

				<div class="row">
					<div class="col-md-12">
						<!-- Zero Configuration Table -->
						<div class="panel panel-default">
						
						<div class="alert alert-info">Creación de Puntos de interés</div>
							 @include('forms_repeat.errors')		                            		    
							<form action="{{ URL::asset('mantenimiento_pi') }}" method="POST">
								@include('forms_repeat.perfil.crearpuntointeres')		                            		    
							</form>
						</div>
					</div>
				</div>
<script type="text/javascript" src="{{ URL::asset('js/crearPI_back.js') }}"></script>
@endsection
