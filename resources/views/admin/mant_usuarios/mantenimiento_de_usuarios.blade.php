@extends('layouts.layout_admin')
@section('content')

				<div class="row">
					<div class="col-md-12">
						<!-- Zero Configuration Table -->
						<div class="panel panel-default">
						
						<div class="alert alert-info">Mantenimiento de usuarios</div>
	
							<div class="panel-body">
								<a href="{{ URL::asset('/mantenimiento_usuarios/create') }}" class="btn btn-success">Crear</a>
								<table id="zctb" class="display table table-striped table-bordered table-hover" cellspacing="0" width="100%">
									<thead>
										<tr>
											<th>ID</th>
											<th>Datos del usuario</th>
											<th>Email</th>
											<th>Username</th>
											<th>Age</th>
											<th>Acciones</th>
										</tr>
									</thead>								
									<tbody>
										@foreach($users as $user)
										<tr>
											<td>{{$user->id}}</td>
											<td>{{$user->nombre}} {{$user->primer_apellido}} {{$user->segundo_apellido}}</td>
											<td>{{$user->email}}</td>
											<td>{{$user->username}}</td>
											<td>@if($user->estaBloqueado==0)
													No está bloqueado
													<p>
													<a href="{{ URL::asset('/bloquearUsuario/'.$user->id.'') }}">Bloquear</a>
												@else
													Está bloqueado
													<p>
													<a href="{{ URL::asset('/desbloquearUsuario/'.$user->id.'') }}">Desbloquear</a>
												@endif
											</td>
											<td><a href="{{ URL::asset('/mantenimiento_usuarios/'.$user->id.'/edit') }}" class="btn btn-warning">Modificar</a><a href="{{ URL::asset('/mantenimiento_usuarios/'.$user->id.'/eliminar') }}" class="btn btn-danger">Eliminar</a><a href="{{ URL::asset('/mantenimiento_usuarios/'.$user->id.'') }}"class="btn btn-info">Ver</a></td>
										</tr>
										@endforeach							
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection
