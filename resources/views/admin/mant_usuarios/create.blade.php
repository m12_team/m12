@extends('layouts.layout_admin')
@section('content')

				<div class="row">
					<div class="col-md-12">
						<!-- Zero Configuration Table -->
						<div class="panel panel-default">
						
						<div class="alert alert-info">Creación de usuarios</div>
							 @include('forms_repeat.errors')		                            		    
							<div class="panel-body">
								<form class="form-horizontal" role="form" action="{{ URL::asset('/mantenimiento_usuarios') }}" method="POST" enctype="multipart/form-data" files="true">

		                             @include('forms_repeat.users')		                            		    
		                            <button type="submit" class="full-width btn-medium">CREAR USUARIO</button>
		                        </form>
							</div>
						</div>
					</div>
				</div>
     <script>
  $(function() {
     $( "#datepicker" ).datepicker({
        dateFormat: "yy-m-d",
        changeMonth: true,
        changeYear: true,
        yearRange:'-90:+0'
    });
  });
  </script>
<script type="text/javascript">
      $('.ocultar').hide();

        $.ajax({
                url:   '../obtenerPaises',
                type:  'get',
                success:  function (data) {
					$.each(data, function(key, element) {
						$('#paises').append("<option value='" + element.id + "'>" + element.name + "</option>");		
                        $('#paises_actual').append("<option value='" + element.id + "'>" + element.name + "</option>");                		
					});
					
                }
        });

		$('select#paises').on('change',function(){
            $('#states').empty();
			var idPais = $(this).val();
			 $.ajax({
				
                url:   '../obtenerStates/'+idPais,
                type:  'get',
                success:  function (data) {
					
					$.each(data, function(key, element) {
						$('#states').append("<option value='" + element.id + "'>" + element.name + "</option>");
					});
                }
			});
		});

        $('select#paises_actual').on('change',function(){
            $('#states_actual').empty();
            var idPais = $(this).val();
             $.ajax({
                
                url:   '../obtenerStates/'+idPais,
                type:  'get',
                success:  function (data) {
                    
                    $.each(data, function(key, element) {
                        $('#states_actual').append("<option value='" + element.id + "'>" + element.name + "</option>");
                    });
                }
            });
        });

      
		
		$('select#states').on('change',function(){
            $('#cities').empty();
			var idStates = $(this).val();
			$.ajax({
                url:   '../obtenerCities/'+idStates,
                type:  'get',
                success:  function (data) {
					$.each(data, function(key, element) {	
						$('#cities').append("<option value='" + element.id + "'>" + element.name + "</option>");
					});
                }
			});
		});
		
        $('select#states_actual').on('change',function(){
            $('#cities_actual').empty();
            var idStates = $(this).val();
            $.ajax({
                url:   '../obtenerCities/'+idStates,
                type:  'get',
                success:  function (data) {
                    $.each(data, function(key, element) {   
                        $('#cities_actual').append("<option value='" + element.id + "'>" + element.name + "</option>");
                    });
                }
            });
        });

    
  </script>
@endsection
