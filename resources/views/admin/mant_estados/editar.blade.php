@extends('layouts.layout_admin')
@section('content')
				<div class="row">
					<div class="col-md-12">
						<!-- Zero Configuration Table -->
						<div class="panel panel-default">
						
						<div class="alert alert-info">Editando el estado {{$state->name}}</div>
							@include('forms_repeat.errors')		                            		    
							<div class="panel-body">	
							    	{!! Form::model($state, ['route' => ['mantenimiento_paises.update', $state->id], 'method' => 'patch']) !!}							
		                             @include('forms_repeat.pais')		                            		    
		                            <button type="submit" class="full-width btn-medium">EDITAR ESTADO</button>
		                           	{!!Form::close()!!}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
  <script type="text/javascript">
   $.ajax({
                url:   '../../obtenerSubregion',
                type:  'get',
                success:  function (data) {
					$.each(data, function(key, element) {
						$('#pais').append("<option value='" + element.id + "'>" + element.nombre + "</option>");		
					});
					
                }
        });
</script>

@endsection
