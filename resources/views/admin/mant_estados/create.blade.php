@extends('layouts.layout_admin')
@section('content')
				<div class="row">
					<div class="col-md-12">
						<!-- Zero Configuration Table -->
						<div class="panel panel-default">
						
						<div class="alert alert-info">Crear estado</div>
							@include('forms_repeat.errors')		                            		    
							<div class="panel-body">
								<form class="form-horizontal" role="form" action="{{ URL::asset('/mantenimiento_estados') }}" method="POST" enctype="multipart/form-data" files="true">

		                             @include('forms_repeat.estado')		                            		    
		                            <button type="submit" class="full-width btn-medium">CREAR ESTADO</button>
		                        </form>
							</div>
						</div>
					</div>
				</div>
<script type="text/javascript">
   $.ajax({
                url:   '../obtenerPaises',
                type:  'get',
                success:  function (data) {
					$.each(data, function(key, element) {
						$('#pais').append("<option value='" + element.id + "'>" + element.name + "</option>");		
					});
					
                }
        });
</script>
@endsection
