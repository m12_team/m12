@extends('layouts.layout_admin')
@section('content')
			<div class="row">
					<div class="col-md-12">
						<!-- Zero Configuration Table -->
						<div class="panel panel-default">
						
						<div class="alert alert-info">Mantenimiento de regiones</div>
							@include('forms_repeat.success')		                            		    
							@include('forms_repeat.error')		                            		    
							<div class="panel-body">
								<a href="{{ URL::asset('/mantenimiento_regiones/create') }}" class="btn btn-success">Crear</a>
								<table id="zctb" class="display table table-striped table-bordered table-hover" cellspacing="0" width="100%">
									<thead>
										<tr>
											<th>ID</th>
											<th>Nombre</th>
											<th>Acciones</th>
										</tr>
									</thead>								
									<tbody>
										@foreach($regiones as $region)
										<tr>
											<td>{{$region->id}}</td>
											<td>{{$region->nombre}}</td>
											<td><a href="{{ URL::asset('/mantenimiento_regiones/'.$region->id.'/edit') }}" class="btn btn-warning">Modificar</a><a href="{{ URL::asset('/mantenimientos_regiones_eliminar/'.$region->id.'') }}" class="btn btn-danger">Eliminar</a></td>
										</tr>
										@endforeach							
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection
