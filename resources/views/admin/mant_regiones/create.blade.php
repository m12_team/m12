@extends('layouts.layout_admin')
@section('content')
				<div class="row">
					<div class="col-md-12">
						<!-- Zero Configuration Table -->
						<div class="panel panel-default">
						
						<div class="alert alert-info">Creación de regiones</div>
							@include('forms_repeat.errors')		                            		    
							<div class="panel-body">
								<form class="form-horizontal" role="form" action="{{ URL::asset('/mantenimiento_regiones') }}" method="POST" enctype="multipart/form-data" files="true">

		                             @include('forms_repeat.regiones')		                            		    
		                            <button type="submit" class="full-width btn-medium">CREAR REGION</button>
		                        </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

@endsection
