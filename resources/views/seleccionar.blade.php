<html>
	<head>
		<script src="{{ URL::asset('js/jquery-1.10.2.js') }}"></script>
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
		<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	</head>
<body>
	<div class="col-md-6">
	<p>Click the button to get your coordinates.</p>

<button onclick="getLocation()">Try It</button>

<a>LOCATION</a>

<p id="demo"></p>

</div>
<div class="col-md-6">
	    <form id="signupForm" class="form-horizontal cmxform" role="form" action="{{ URL::asset('/test') }}" method="GET" enctype="multipart/form-data" files="true">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            

                             <label>Ciudad natal</label>
                             <div class="form-group">
                                <div class="col-md-4">
                                     @if(isset($user))
                                        @if(isset($currentPath))
                                            <select id="paises" class="lock" name="paises" style="width:100%;">
                                                <option>{{$user->ciudad_natal->state->country->name}}</option>
                                            </select>                                           
                                           
                                        @else
                                            <select id="paises" class="lock" name="paises" style="width:100%;">
                                                <option>Selecciona un país</option>
                                            </select>                                                                       
                                        @endif
                                        @else
                                        <select id="paises" class="lock" name="paises" style="width:100%;">
                                                <option>Selecciona un país</option>
                                            </select>     
                                     @endif
                                </div>
                                <div class="col-md-4">
                                     @if(isset($user))
                                        @if(isset($currentPath))
                                            <select id="states" class="lock" name="states" style="width:100%;">
                                                <option>{{$user->ciudad_natal->state->name}}</option>
                                            </select>                                           
                                           
                                        @else
                                            <select id="states" class="lock" name="states" style="width:100%;">
                                                <option>Selecciona un país</option>
                                            </select>
                                        @endif                                                                       
                                        @else
                                            <select id="states" class="lock" name="states" style="width:100%;">
                                                <option>Selecciona un país</option>
                                            </select>
                                     @endif
                                </div>
                                <div class="col-md-4">
                                     @if(isset($user))
                                        @if(isset($currentPath))
                                            <select id="cities" class="lock ciudad_natal" name="cities" style="width:100%;">
                                                <option value="{{$user->ciudad_natal->id}}">{{$user->ciudad_natal->name}}</option>
                                            </select>                                           
                                           
                                        @else
                                            <select id="cities" class="lock ciudad_natal" name="cities" style="width:100%;">
                                                <option>Selecciona un país</option>
                                            </select>                                                                       
                                        @endif
                                        @else
                                            <select id="cities" class="lock ciudad_natal" name="cities" style="width:100%;">
                                                <option>Selecciona un país</option>
                                            </select> 
                                     @endif
                                </div>
                            </div>					
                            <button type="submit" class="full-width btn-medium">LOCATION</button>
                    </form>
  </div>
	
</body>
<script type="text/javascript" src="{{ URL::asset('js/bootstrap.js') }}"></script>
<script>
var x = document.getElementById("demo");

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
}

function showPosition(position) {
    x.innerHTML = "Latitude: " + position.coords.latitude +
    "<br>Longitude: " + position.coords.longitude;
}
</script>
    <script type="text/javascript">

        $('.descripcion').val('');
        $('.ciudad_actual').val('');
        $('.ciudad_natal').val('');

        $.ajax({
                url:   'obtenerPaises',
                type:  'get',
                success:  function (data) {
                    $.each(data, function(key, element) {
                        $('#paises').append("<option value='" + element.id + "'>" + element.name + "</option>");        
                        $('#paises_actual').append("<option value='" + element.id + "'>" + element.name + "</option>");                     
                    });
                    
                }
        });

        $('select#paises').on('change',function(){
            $('#states').empty();
            var idPais = $(this).val();
             $.ajax({
                
                url:   'obtenerStates/'+idPais,
                type:  'get',
                success:  function (data) {
                    
                    $.each(data, function(key, element) {
                        $('#states').append("<option value='" + element.id + "'>" + element.name + "</option>");
                    });
                }
            });
        });

        $('select#paises_actual').on('change',function(){
            $('#states_actual').empty();
            var idPais = $(this).val();
             $.ajax({
                
                url:   'obtenerStates/'+idPais,
                type:  'get',
                success:  function (data) {
                    
                    $.each(data, function(key, element) {
                        $('#states_actual').append("<option value='" + element.id + "'>" + element.name + "</option>");
                    });
                }
            });
        });

      
        
        $('select#states').on('change',function(){
            $('#cities').empty();
            var idStates = $(this).val();
            $.ajax({
                url:   'obtenerCities/'+idStates,
                type:  'get',
                success:  function (data) {
                    $.each(data, function(key, element) {   
                        $('#cities').append("<option value='" + element.id + "'>" + element.name + "</option>");
                    });
                }
            });
        });
        
        $('select#states_actual').on('change',function(){
            $('#cities_actual').empty();
            var idStates = $(this).val();
            $.ajax({
                url:   'obtenerCities/'+idStates,
                type:  'get',
                success:  function (data) {
                    $.each(data, function(key, element) {   
                        $('#cities_actual').append("<option value='" + element.id + "'>" + element.name + "</option>");
                    });
                }
            });
        });
    </script>
</html>
