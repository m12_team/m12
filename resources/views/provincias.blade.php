@extends('layouts.master')
@section('content')
        <div class="page-title-container">
            <div class="container">
                <div class="page-title pull-left">
                    <h2 class="entry-title">{{$pais->name}}</h2>
                </div>
                <ul class="breadcrumbs pull-right">
                    <li><a href="#">CONSULTAR PAIS</a></li>
                    <li class="active"><a href="#">{{$pais->name}}</a></li>
                </ul>
            </div>
        </div>

        <section id="content">
            <div class="container">
                <div id="main">
                    <div class="row add-clearfix image-box style1 tour-locations">
						@foreach($states as $state)
                        <div class="col-sm-6 col-md-4">
                            <article class="box">
                                <figure>
                                    <a href="#" class="hover-effect">
                                        <img src="http://placehold.it/370x200" alt="">
                                    </a>
                                </figure>
                                <div class="details">
                                    <h4 class="box-title">{{$state->name}}</h4>
                                    <a href="{{ URL::asset('consultar-state/'.$state->id)}}" class="button btn-small full-width">CONSULTAR</a>
                                </div>
                            </article>
                        </div>
						@endforeach
                       </div>
                    </div>
                </div>
        </section>
        
        
    </div>
    

    <!-- Javascript -->
    <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.noconflict.js"></script>
    <script type="text/javascript" src="js/modernizr.2.7.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.placeholder.js"></script>
    <script type="text/javascript" src="js/jquery-ui.1.10.4.min.js"></script>
    
    <!-- Twitter Bootstrap -->
    <script type="text/javascript" src="js/bootstrap.js"></script>
    
    <!-- parallax -->
    <script type="text/javascript" src="js/jquery.stellar.min.js"></script>
    
    <!-- waypoint -->
    <script type="text/javascript" src="js/waypoints.min.js"></script>

    <!-- load page Javascript -->
    <script type="text/javascript" src="js/theme-scripts.js"></script>
    <script type="text/javascript" src="js/scripts.js"></script>
@endsection