@extends('layouts.master')
@section('content')
    <div id="page-wrapper">
        <div class="page-title-container">
            <div class="container">
                <div class="page-title pull-left">
                    <h2 class="entry-title">Buscar GYM</h2>
                </div>
                <ul class="breadcrumbs pull-right">
                    <li><a href="{{ URL::asset('/') }}">INICIO</a></li>
                    <li class="active"><a href="{{ URL::asset('/buscar-gym') }}">Buscar GYM</a></li>
                </ul>
            </div>
        </div>

        <section id="content white">
            <div class="container" >
                <div id="main">
                    <div class="row">
                        <div class="col-md-offset-2 col-md-8" >
                            <div class="alert alert-info">El fórmulario sólo puede buscar por un criterio seleccionado</div>
                    <form action="{{ URL::asset('busquedaPuntoInteres') }}" class="form-horizontal" role="form" method="GET" enctype="multipart/form-data" >
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group">
                                <input type="text" class="input-text full-width" placeholder="Nombre" name="nombre" id="nombre">
                            </div>                           
                            <div class="form-group">
                                <input type="text" class="input-text full-width" placeholder="Creador" name="creador" id="creador">
                            </div>                              
							<div class="form-group">							
                            <select class="full-width" name="entorno" id="entorno">
                                                            <option value="">Selecciona un entorno</option>
                                                        </select>
							</div>
							
                            <button type="submit" class="full-width btn-medium">Buscar puntos de interés</button>
                        </form>
						</div>                      
                    </div>
                </div>
            </div>
        </section>
    </div>
    <script src="{{ URL::asset('js/cargarDropDowns.js') }}"></script>
    <script src="{{ URL::asset('js/buscar-punto-interes.js') }}"></script>
@endsection