@extends('layouts.master')
@section('content')
    <!-- Javascript -->
 
    <div id="page-wrapper">
        <div class="page-title-container">
            <div class="container">
                <div class="page-title pull-left">
                    <h2 class="entry-title">Búsqueda de puntos de interés</h2>
                </div>
                <ul class="breadcrumbs pull-right">
                    <li><a href="{{ URL::asset('/home') }}">INICIO</a></li>
                    <li class="active"><a href="{{ URL::asset('/buscar-punto-de-interes') }}">Búsqueda de Puntos de Interes</a></li>
                </ul>
            </div>
        </div>
        <section id="content white">
            <div class="container">
                <div id="main">
                    <div class="row">                        
                        <div class="col-md-offset-1 col-md-10">
                            <div class="hotel-list listing-style3 hotel">
                            @if(count($punto_interes)>0)
								@foreach($punto_interes as $pi)   
										@foreach($pi->puntoInteres as $key => $pi) 
									<article class="box">
										<figure class="col-sm-5 col-md-4">
										<a href="{{ URL::asset('detalle') }}" class="hover-effect"><img width="270" height="160" src="{{$pi->fotos->first()->ruta}}"></a>
										</figure>
										<div class="details col-sm-7 col-md-8">
                                        <a href="{{ URL::asset('consultarPI/'.$pi->id) }}">
                                        <div>                                                
                                            <h4 class="box-title">{{$pi->nombre}}</h4>                                                                                 
                                             @if($pi->users!=NULL)                                                                            
                                                <a href="{{ URL::asset('consultarUsuario/'.$pi->users->id)}}">Creado por {{$pi->users->username}}</a>
                                            @else
                                                <a>El usuario se ha dado de baja en el sistema </a>
                                            @endif
                                        </div>
                                        <div>
                                            <p>{{$pi->descripcion}}</p>
                                            <div>
                                                <a href="{{ URL::asset('consultarPI/'.$pi->id) }}" class="button btn-small full-width text-center">CONSULTAR</a>
                                            </div>
                                        </div>
                                                                              

                                    </div>
									</article>
									@endforeach
                                @endforeach    
                                @else
                                    <div class="col-sm-6 col-md-12">
                                    <div class="alert alert-info">
                                        No hay ningún criterio relacionado con la búsqueda
                                    </div>
                                </div>
                                @endif                       
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection