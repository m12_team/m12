@extends('layouts.master')
@section('content')
    <div id="page-wrapper">
        
        <div class="page-title-container">
            <div class="container">
                <div class="page-title pull-left">
                    <h2 class="entry-title">Mi cuenta</h2>
                </div>
                <ul class="breadcrumbs pull-right">
                    <li><a href="{{ URL::asset('profile') }}">HOME</a></li>
                    <a href="{{ URL::asset('profile') }}"><li class="active">Mi perfil</li></a>
                </ul>
            </div>
        </div>
        <section id="content" class="gray-area">
            <div class="container">
                <div id="main">
                    <div class="tab-container full-width-style arrow-left dashboard">
                        <ul class="tabs">
                            <li class="active"><a data-toggle="tab" href="#dashboard"><i class="soap-icon-anchor circle"></i>Principal</a></li>
                            <li class=""><a data-toggle="tab" href="#profile"><i class="soap-icon-user circle"></i>Perfil</a></li>
                            <li class=""><a data-toggle="tab" href="#wishlist"><i class="soap-icon-wishlist circle"></i>Puntos de interés</a></li>
                            <li class=""><a data-toggle="tab" href="#rutas"><i class="soap-icon-conference circle"></i>Rutas</a></li>
                            <li class=""><a data-toggle="tab" href="#diarios"><i class="soap-icon-conference circle"></i>Diarios</a></li>
                            <li class=""><a data-toggle="tab" href="#travel-stories"><i class="soap-icon-conference circle"></i>Diarios</a></li>
                            <li class=""><a data-toggle="tab" href="#settings"><i class="soap-icon-settings circle"></i>Settings</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="dashboard" class="tab-pane fade in active">
                                <h1 class="no-margin skin-color">Hola  {{ Auth::user()->nombre}} {{ Auth::user()->primer_apellido}} {{Auth::user()->segundo_apellido}}!</h1>
                                <br />
                                <div class="row block">
                                    <div class="col-sm-6 col-md-4">
                                        <a href="hotel-list-view.html">
                                            <div class="fact blue">
                                                <div class="numbers counters-box">
                                                    <dl>
                                                        <dt class="display-counter" data-value="{{$totalPI}}">0</dt>
                                                        <dd>Puntos de interés</dd>
                                                    </dl>
                                                    <i class="icon soap-icon-hotel"></i>
                                                </div>
                                                <div class="description">
                                                    <i class="icon soap-icon-longarrow-right"></i>
                                                    <span>Rutas</span>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-sm-6 col-md-4">
                                        <a href="flight-list-view.html">
                                            <div class="fact yellow">
                                                <div class="numbers counters-box">
                                                    <dl>
                                                        <dt class="display-counter" data-value="{{$totalRuta}}">0</dt>
                                                        <dd>Rutas</dd>
                                                    </dl>
                                                    <i class="icon soap-icon-plane"></i>
                                                </div>
                                                <div class="description">
                                                    <i class="icon soap-icon-longarrow-right"></i>
                                                    <span>Diarios</span>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-sm-6 col-md-4">
                                        <a href="car-list-view.html">
                                            <div class="fact red">
                                                <div class="numbers counters-box">
                                                    <dl>
                                                        <dt class="display-counter" data-value="{{$totalDiarios}}">0</dt>
                                                        <dd>Diarios</dd>
                                                    </dl>
                                                    <i class="icon soap-icon-car"></i>
                                                </div>
                                                <div class="description">
                                                    <i class="icon soap-icon-longarrow-right"></i>
                                                    <span>View Cars</span>
                                                </div>
                                            </div>
                                        </a>
                                    </div>                                 
                                </div>
                                <div class="notification-area">
                                    <div class="info-box block">
                                        <span class="close"></span>
                                        <p>Bienvenido a tu Perfil personal , desde aquí vas a poder gestionar absolutamente todo lo que necesites para llevar a cabo una estancia agradable allí dónde vayas.</p>                                       
                                    </div>
                                </div>
                                
                                <div class="row block">
                                    <div class="col-md-4 notifications">
                                        <h2>Notificaciones</h2>
                                        <a href="#">
                                            <div class="icon-box style1 fourty-space">
                                                <i class="soap-icon-plane-right takeoff-effect yellow-bg"></i>
                                                <span class="time pull-right">JUST NOW</span>
                                                <p class="box-title">London to Paris flight in <span class="price">$120</span></p>
                                            </div>
                                        </a>
                                        <a href="#">
                                            <div class="icon-box style1 fourty-space">
                                                <i class="soap-icon-hotel blue-bg"></i>
                                                <span class="time pull-right">10 Mins ago</span>
                                                <p class="box-title">Hilton hotel &amp; resorts in <span class="price">$247</span></p>
                                            </div>
                                        </a>
                                        <a href="#">
                                            <div class="icon-box style1 fourty-space">
                                                <i class="soap-icon-car red-bg"></i>
                                                <span class="time pull-right">39 Mins ago</span>
                                                <p class="box-title">Economy car for 2 days in <span class="price">$39</span></p>
                                            </div>
                                        </a>
                                        <a href="#">
                                            <div class="icon-box style1 fourty-space">
                                                <i class="soap-icon-cruise green-bg"></i>
                                                <span class="time pull-right">1 hour ago</span>
                                                <p class="box-title">Baja Mexico 4 nights in <span class="price">$537</span></p>
                                            </div>
                                        </a>
                                        <a href="#">
                                            <div class="icon-box style1 fourty-space">
                                                <i class="soap-icon-hotel blue-bg"></i>
                                                <span class="time pull-right">2 hours ago</span>
                                                <p class="box-title">Roosevelt hotel in <span class="price">$170</span></p>
                                            </div>
                                        </a>
                                        <a href="#">
                                            <div class="icon-box style1 fourty-space">
                                                <i class="soap-icon-hotel blue-bg"></i>
                                                <span class="time pull-right">3 hours ago</span>
                                                <p class="box-title">Cleopatra Resort in <span class="price">$247</span></p>
                                            </div>
                                        </a>
                                        <a href="#">
                                            <div class="icon-box style1 fourty-space">
                                                <i class="soap-icon-car red-bg"></i>
                                                <span class="time pull-right">3 hours ago</span>
                                                <p class="box-title">Elite Car per day in <span class="price">$48.99</span></p>
                                            </div>
                                        </a>
                                        <a href="#">
                                            <div class="icon-box style1 fourty-space">
                                                <i class="soap-icon-cruise green-bg"></i>
                                                <span class="time pull-right">last night</span>
                                                <p class="box-title">Rome to Africa 1 week in <span class="price">$875</span></p>
                                            </div>
                                        </a>
                                        <a href="#">
                                            <div class="load-more">. . . . . . . . . . . . . </div>
                                        </a>
                                    </div>
                                    <div class="col-md-6">
                                        <h2>Actividad reciente</h2>
                                        <div class="recent-activity">
                                            <ul>
                                                <li>
                                                    <a href="#">
                                                        <i class="icon soap-icon-plane-right circle takeoff-effect yellow-color"></i>
                                                        <span class="price"><small>avg/person</small>$120</span>
                                                        <h4 class="box-title">
                                                            Indianapolis to Paris<small>Oneway flight</small>
                                                        </h4>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="icon soap-icon-car circle red-color"></i>
                                                        <span class="price"><small>per day</small>$45.39</span>
                                                        <h4 class="box-title">
                                                            Economy Car<small>bmw mini</small>
                                                        </h4>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="icon soap-icon-cruise circle green-color"></i>
                                                        <span class="price"><small>from</small>$578</span>
                                                        <h4 class="box-title">
                                                            Jacksonville to Asia<small>4 nights</small>
                                                        </h4>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="icon soap-icon-hotel circle blue-color"></i>
                                                        <span class="price"><small>Avg/night</small>$620</span>
                                                        <h4 class="box-title">
                                                            Hilton Hotel &amp; Resorts<small>Paris france</small>
                                                        </h4>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="icon soap-icon-hotel circle blue-color"></i>
                                                        <span class="price"><small>avg/night</small>$170</span>
                                                        <h4 class="box-title">
                                                            Roosevelt Hotel<small>new york</small>
                                                        </h4>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="icon soap-icon-plane-right circle takeoff-effect yellow-color"></i>
                                                        <span class="price"><small>avg/person</small>$348</span>
                                                        <h4 class="box-title">
                                                            Mexico to England<small>return flight</small>
                                                        </h4>
                                                    </a>
                                                </li>
                                            </ul>
                                            <a href="#" class="button green btn-small full-width">VIEW ALL ACTIVITIES</a>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                           
                            </div>
                            <div id="profile" class="tab-pane fade">
                                <div class="view-profile">
                                    <article class="image-box style2 box innerstyle personal-details">
                                        <figure>
                                            <a title="" href="#"><img width="270" height="263" alt="" src="{{Auth::user()->foto}}"></a>
                                        </figure>
                                        <div class="details">
                                            <a href="#" class="button btn-mini pull-right edit-profile-btn">Editar perfil</a>
                                            <h2 class="box-title fullname">{{Auth::user()->nombre}} {{Auth::user()->primer_apellido}} {{Auth::user()->segundo_apellido}}</h2>
                                            <dl class="term-description">
                                                <dt>Nombre de usuario:</dt><dd>{{Auth::user()->email}}</dd>
                                                <dt>Nombre:</dt><dd>{{Auth::user()->nombre}}</dd>
                                                <dt>Apellidos:</dt><dd>{{Auth::user()->primer_apellido}} {{Auth::user()->segundo_apellido}}</dd>
                                                <dt>Fecha de nacimiento:</dt><dd>{{Auth::user()->fecha_nacimiento}}</dd>
                                                <dt>Ciudad natal | Pais</dt><dd>{{Auth::user()->ciudad_natal->name}} | {{Auth::user()->ciudad_natal->state->country->name}}</dd>
                                                <dt>Ciudad actual | Pais</dt><dd>{{Auth::user()->ciudad_actual->name}} | {{Auth::user()->ciudad_actual->state->country->name}}</dd>

                                            </dl>
                                        </div>
                                    </article>
                                    <hr>
                                    <h2>About You</h2>
                                        <div class="intro">
                                        <p>{{Auth::user()->descripcion}}</p>
                                    </div>
                                 
                                </div>
                                <div class="edit-profile">
                                    <form action="{{ URL::asset('cambiarPassword') }}" lass="edit-profile-form" method="POST">                                       
                                        <h2>Editar usuario</h2>
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div class="col-sm-9 no-padding no-float">
                                            <div class="row form-group">
                                                <div class="col-sms-6 col-sm-6">
                                                    <label>Nombre</label>
                                                    <input type="text" class="input-text full-width" placeholder="" value="{{Auth::user()->nombre}}">
                                                </div>
                                                <div class="col-sms-6 col-sm-6">
                                                    <label>Primer apellido</label>
                                                    <input type="text" class="input-text full-width" placeholder="" value="{{Auth::user()->primer_apellido}}">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col-sms-6 col-sm-6">
                                                    <label>Segundo apellido</label>
                                                    <input type="text" class="input-text full-width" placeholder="" value="{{Auth::user()->segundo_apellido}}">
                                                </div>
                                                <div class="col-sms-6 col-sm-6">
                                                    <label>Email</label>
                                                    <input type="text" class="input-text full-width" placeholder="" value="{{Auth::user()->email}}">
                                                </div>

                                            </div> 
                                            <div class="row">
                                            <div class="col-sms-6 col-sm-6">
                                                    <label>Username</label>
                                                    <input type="text" class="input-text full-width" placeholder="" value="{{Auth::user()->username}}">
                                                </div>                                         
                                            <label>Date of Birth</label>
                                            <div class="row form-group">
                                                <div class="col-sms-4 col-sm-2">
                                                    <div class="selector">
                                                        <select class="full-width">
                                                            <option value="">date</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sms-4 col-sm-2">
                                                    <div class="selector">
                                                        <select class="full-width">
                                                            <option value="">month</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sms-4 col-sm-2">
                                                    <div class="selector">
                                                        <select class="full-width">
                                                            <option value="">year</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                            <hr>
                                            <h2>Detalle de contacto</h2>                                            
                                            <div class="row form-group">
                                                <label>Ciudad natal</label>
                                                <div class="col-sms-6 col-sm-4">
                                                    <label>País</label>
                                                    <div class="selector">
                                                        <select class="full-width">
                                                            <option value="">{{Auth::user()->ciudad_natal->state->country->name}}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sms-6 col-sm-4">
                                                    <label>Estado</label>
                                                    <div class="selector">
                                                        <select class="full-width">
                                                            <option value="">{{Auth::user()->ciudad_natal->state->name}}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                 <div class="col-sms-6 col-sm-4">
                                                    <label>Ciudad</label>
                                                    <div class="selector">
                                                        <select class="full-width">
                                                            <option value="">{{Auth::user()->ciudad_natal->name}}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                             <div class="row form-group">
                                                <label>Ciudad actual</label>
                                                <div class="col-sms-6 col-sm-4">
                                                    <label>País</label>
                                                    <div class="selector">
                                                        <select class="full-width">
                                                            <option value="">{{Auth::user()->ciudad_actual->state->country->name}}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sms-6 col-sm-4">
                                                    <label>Estado</label>
                                                    <div class="selector">
                                                        <select class="full-width">
                                                            <option value="">{{Auth::user()->ciudad_actual->state->name}}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                 <div class="col-sms-6 col-sm-4">
                                                    <label>Ciudad</label>
                                                    <div class="selector">
                                                        <select class="full-width">
                                                            <option value="">{{Auth::user()->ciudad_actual->name}}</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <hr>
                                            <h2>Cambiar foto perfil</h2>
                                            <div class="row form-group">
                                                <div class="col-sms-12 col-sm-6 no-float">
                                                    <div class="fileinput full-width">
                                                        <input type="file" class="input-text" data-placeholder="{{Auth::user()->foto}}">
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <h2>Describe Yourself</h2>
                                            <div class="form-group">
                                                <textarea rows="5" class="input-text full-width" placeholder="{{Auth::user()->descripcion}}"></textarea>
                                            </div>
                                            <div class="from-group">
                                                <button type="submit" class="btn-medium col-sms-6 col-sm-4">ACTUALIZAR CAMBIOS</button>
                                            </div>

                                        </div>
                                    </form>
                                </div>
                            </div>                          
                            <div id="wishlist" class="tab-pane fade">
                                <h2>Puntos de interés</h2>
                                <div class="row">
                                    
                                    <a href="#" class="button btn-mini pull-left create-profile-btn">Crear punto de interes</a>
                                </div>
                                <div class="row image-box listing-style2 add-clearfix">
                                    <div class="col-sm-6 col-md-4">
                                        <article class="box">
                                            <figure>
                                                <a class="hover-effect" title="" href="#"><img width="300" height="160" alt="" src="http://placehold.it/300x160"></a>
                                            </figure>
                                            <div class="details">
                                                <a class="pull-right button uppercase" href="" title="View all">select</a>
                                                <h4 class="box-title">Savona to Italy</h4>
                                                <label class="price-wrapper">
                                                    <span class="price-per-unit">$170</span>avg/night
                                                </label>
                                            </div>
                                        </article>
                                    </div>                                                                                                                        
                                </div>
                            </div>
                             <div id="rutas" class="tab-pane fade">
                                <h2>Rutas</h2>
                                <div class="row image-box listing-style2 add-clearfix">
                                    <div class="col-sm-6 col-md-4">
                                        <article class="box">
                                            <figure>
                                                <a class="hover-effect" title="" href="#"><img width="300" height="160" alt="" src="http://placehold.it/300x160"></a>
                                            </figure>
                                            <div class="details">
                                                <a class="pull-right button uppercase" href="" title="View all">select</a>
                                                <h4 class="box-title">Savona to Italy</h4>
                                                <label class="price-wrapper">
                                                    <span class="price-per-unit">$170</span>avg/night
                                                </label>
                                            </div>
                                        </article>
                                    </div>                                                                                                                        
                                </div>
                            </div>
                            <div id="diarios" class="tab-pane fade">
                                <h2>Diarios</h2>
                                <div class="row image-box listing-style2 add-clearfix">
                                    <div class="col-sm-6 col-md-4">
                                        <article class="box">
                                            <figure>
                                                <a class="hover-effect" title="" href="#"><img width="300" height="160" alt="" src="http://placehold.it/300x160"></a>
                                            </figure>
                                            <div class="details">
                                                <a class="pull-right button uppercase" href="" title="View all">select</a>
                                                <h4 class="box-title">Savona to Italy</h4>
                                                <label class="price-wrapper">
                                                    <span class="price-per-unit">$170</span>avg/night
                                                </label>
                                            </div>
                                        </article>
                                    </div>                                                                                                                        
                                </div>
                            </div>
                            <div id="travel-stories" class="tab-pane fade">
                                <h2>Share Your Story</h2>
                                <div class="col-sm-9 no-float no-padding">
                                    <form>
                                        <div class="row form-group">
                                            <div class="col-sms-6 col-sm-6">
                                                <label>Story Title</label>
                                                <input type="text" class="input-text full-width">
                                            </div>
                                            <div class="col-sms-6 col-sm-6">
                                                <label>Name</label>
                                                <input type="text" class="input-text full-width">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-sms-6 col-sm-6">
                                                <label>Select Miles</label>
                                                <div class="selector full-width">
                                                    <select>
                                                        <option>4,528 Miles</option>
                                                        <option>5,218 Miles</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sms-6 col-sm-6">
                                                <label>Email Address</label>
                                                <input type="text" class="input-text full-width">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-sms-6 col-sm-6">
                                                <label>Select Category</label>
                                                <div class="selector full-width">
                                                    <select>
                                                        <option value="">Adventure, Romance, Beach</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Type Your Story</label>
                                            <textarea rows="6" class="input-text full-width" placeholder="please tell us about you"></textarea>
                                        </div>
                                        <hr>
                                        <div class="form-group">
                                            <h4>Do you have photos to share? <small>(Optional)</small> </h4>
                                            <div class="fileinput col-sm-6 no-float no-padding">
                                                <input type="file" class="input-text" data-placeholder="select image/s" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <h4>Share with friends <small>(Optional)</small></h4>
                                            <p>Share your review with your friends on different social media networks.</p>
                                            <ul class="social-icons icon-circle clearfix">
                                                <li class="twitter"><a title="Twitter" href="#" data-toggle="tooltip"><i class="soap-icon-twitter"></i></a></li>
                                                <li class="facebook"><a title="Facebook" href="#" data-toggle="tooltip"><i class="soap-icon-facebook"></i></a></li>
                                                <li class="googleplus"><a title="GooglePlus" href="#" data-toggle="tooltip"><i class="soap-icon-googleplus"></i></a></li>
                                                <li class="pinterest"><a title="Pinterest" href="#" data-toggle="tooltip"><i class="soap-icon-pinterest"></i></a></li>
                                            </ul>
                                        </div>
                                        <br>
                                        <div class="form-group col-sm-5 col-md-4 no-float no-padding no-margin">
                                            <button type="submit" class="btn-medium full-width">SUBMIT REVIEW</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div id="settings" class="tab-pane fade">
                                <h2>Account Settings</h2>
                                <h5 class="skin-color">Change Your Password</h5>
                                <form>
                                    <div class="row form-group">
                                        <div class="col-xs-12 col-sm-6 col-md-4">
                                            <label>Old Password</label>
                                            <input type="text" class="input-text full-width">
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-xs-12 col-sm-6 col-md-4">
                                            <label>Enter New Password</label>
                                            <input type="text" class="input-text full-width">
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-xs-12 col-sm-6 col-md-4">
                                            <label>Confirm New password</label>
                                            <input type="text" class="input-text full-width">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn-medium">UPDATE PASSWORD</button>
                                    </div>
                                </form>
                                <hr>
                                <h5 class="skin-color">Change Your Email</h5>
                                <form>
                                    <div class="row form-group">
                                        <div class="col-xs-12 col-sm-6 col-md-4">
                                            <label>Old email</label>
                                            <input type="text" class="input-text full-width">
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-xs-12 col-sm-6 col-md-4">
                                            <label>Enter New Email</label>
                                            <input type="text" class="input-text full-width">
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col-xs-12 col-sm-6 col-md-4">
                                            <label>Confirm New Email</label>
                                            <input type="text" class="input-text full-width">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn-medium">UPDATE EMAIL ADDRESS</button>
                                    </div>
                                </form>
                                <hr>
                                <h5 class="skin-color">Send Me Emails When</h5>
                                <form>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> Travelo has periodic offers and deals on really cool destinations.
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> Travelo has fun company news, as well as periodic emails.
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> I have an upcoming reservation.
                                        </label>
                                    </div>
                                    <button class="btn-medium uppercase">Update All Settings</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        
        <footer id="footer">
            <div class="footer-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-md-3">
                            <h2>Discover</h2>
                            <ul class="discover triangle hover row">
                                <li class="col-xs-6"><a href="#">Safety</a></li>
                                <li class="col-xs-6"><a href="#">About</a></li>
                                <li class="col-xs-6"><a href="#">Travelo Picks</a></li>
                                <li class="col-xs-6"><a href="#">Latest Jobs</a></li>
                                <li class="active col-xs-6"><a href="#">Mobile</a></li>
                                <li class="col-xs-6"><a href="#">Press Releases</a></li>
                                <li class="col-xs-6"><a href="#">Why Host</a></li>
                                <li class="col-xs-6"><a href="#">Blog Posts</a></li>
                                <li class="col-xs-6"><a href="#">Social Connect</a></li>
                                <li class="col-xs-6"><a href="#">Help Topics</a></li>
                                <li class="col-xs-6"><a href="#">Site Map</a></li>
                                <li class="col-xs-6"><a href="#">Policies</a></li>
                            </ul>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <h2>Travel News</h2>
                            <ul class="travel-news">
                                <li>
                                    <div class="thumb">
                                        <a href="#">
                                            <img src="http://placehold.it/63x63" alt="" width="63" height="63" />
                                        </a>
                                    </div>
                                    <div class="description">
                                        <h5 class="s-title"><a href="#">Amazing Places</a></h5>
                                        <p>Purus ac congue arcu cursus ut vitae pulvinar massaidp.</p>
                                        <span class="date">25 Sep, 2013</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="thumb">
                                        <a href="#">
                                            <img src="http://placehold.it/63x63" alt="" width="63" height="63" />
                                        </a>
                                    </div>
                                    <div class="description">
                                        <h5 class="s-title"><a href="#">Travel Insurance</a></h5>
                                        <p>Purus ac congue arcu cursus ut vitae pulvinar massaidp.</p>
                                        <span class="date">24 Sep, 2013</span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <h2>Mailing List</h2>
                            <p>Sign up for our mailing list to get latest updates and offers.</p>
                            <br />
                            <div class="icon-check">
                                <input type="text" class="input-text full-width" placeholder="your email" />
                            </div>
                            <br />
                            <span>We respect your privacy</span>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <h2>About Travelo</h2>
                            <p>Nunc cursus libero purus ac congue arcu cursus ut sed vitae pulvinar massaidp nequetiam lore elerisque.</p>
                            <br />
                            <address class="contact-details">
                                <span class="contact-phone"><i class="soap-icon-phone"></i> 1-800-123-HELLO</span>
                                <br />
                                <a href="#" class="contact-email">help@travelo.com</a>
                            </address>
                            <ul class="social-icons clearfix">
                                <li class="twitter"><a title="twitter" href="#" data-toggle="tooltip"><i class="soap-icon-twitter"></i></a></li>
                                <li class="googleplus"><a title="googleplus" href="#" data-toggle="tooltip"><i class="soap-icon-googleplus"></i></a></li>
                                <li class="facebook"><a title="facebook" href="#" data-toggle="tooltip"><i class="soap-icon-facebook"></i></a></li>
                                <li class="linkedin"><a title="linkedin" href="#" data-toggle="tooltip"><i class="soap-icon-linkedin"></i></a></li>
                                <li class="vimeo"><a title="vimeo" href="#" data-toggle="tooltip"><i class="soap-icon-vimeo"></i></a></li>
                                <li class="dribble"><a title="dribble" href="#" data-toggle="tooltip"><i class="soap-icon-dribble"></i></a></li>
                                <li class="flickr"><a title="flickr" href="#" data-toggle="tooltip"><i class="soap-icon-flickr"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottom gray-area">
                <div class="container">
                    <div class="logo pull-left">
                        <a href="" title="Travelo - home">
                            <img src="images/logo.png" alt="Travelo HTML5 Template" />
                        </a>
                    </div>
                    <div class="pull-right">
                        <a id="back-to-top" href="#" class="animated" data-animation-type="bounce"><i class="soap-icon-longarrow-up circle"></i></a>
                    </div>
                    <div class="copyright pull-right">
                        <p>&copy; 2014 Travelo</p>
                    </div>
                </div>
            </div>
        </footer>
    </div>

    <!-- Javascript -->
    <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.noconflict.js"></script>
    <script type="text/javascript" src="js/modernizr.2.7.1.min.js"></script>
    <script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.placeholder.js"></script>
    <script type="text/javascript" src="js/jquery-ui.1.10.4.min.js"></script>
    
    <!-- Twitter Bootstrap -->
    <script type="text/javascript" src="js/bootstrap.js"></script>

    <!-- Flex Slider -->
    <script type="text/javascript" src="components/flexslider/jquery.flexslider-min.js"></script>
    
    <!-- parallax -->
    <script type="text/javascript" src="js/jquery.stellar.min.js"></script>
    
    <!-- waypoint -->
    <script type="text/javascript" src="js/waypoints.min.js"></script>

    <!-- load page Javascript -->
    <script type="text/javascript" src="js/theme-scripts.js"></script>
    
    <script type="text/javascript">
        tjq(document).ready(function() {
            tjq("#profile .edit-profile-btn").click(function(e) {
                e.preventDefault();
                tjq(".view-profile").fadeOut();
                tjq(".edit-profile").fadeIn();
            });

        });
        tjq('a[href="#profile"]').on('shown.bs.tab', function (e) {
            tjq(".view-profile").show();
            tjq(".edit-profile").hide();
        });
    </script>
@endsection